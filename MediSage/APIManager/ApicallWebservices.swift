//
//  ApicallWebservices.swift
//  MediSage
//
//  Created by Harekrishna on 11/01/20.
//  Copyright © 2020 PFACode. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class ApicallWebservices :  NSObject {
    
    //MARK: - Use in any custom function in all class (file name. shared instance . (andar na badha function use kari kari sakay))
    class var SharedInstance: ApicallWebservices {
        struct Singleton {
            static let Instance = ApicallWebservices()
        }
        return Singleton.Instance
    }
    
    //MARK: - Internet Connection check karva
    func InternetConnection() -> Bool {
        let internetConnection = Reachability()
        if (internetConnection?.isReachable)! {
            return true
        }else {
            return false
        }
    }
    
    func VIMEOVIDEOApiCallwithHeader(methodname: String, type: HTTPMethod, header: HTTPHeaders, parameter: [String : String], strLoaderString: String, completion: @escaping (_ error: Error?, _ success: Bool, _ result: Any?) -> Void) {
        
        print("parameter::::::: \(parameter)")
        let strBaseURL = methodname
        GlobalClass.sharedInstance.activity()
        
        if self.InternetConnection(){
            AF.upload(multipartFormData: {
                (MultipartFormData) in
                for (key, value) in parameter {
                    MultipartFormData.append((value ).data(using: .utf8)!, withName: key)
                }
            }, to: strBaseURL, method: type, headers: header)
            .responseJSON(completionHandler: { (response) in
                if let error = response.error {
                    print("error::::: \(error)")
                    completion(error, false, nil)
                    GlobalClass.sharedInstance.removeActivity()
                    return
                }
                
                guard let dicResponse = response.value as? NSDictionary else {
                    completion(nil, false, nil)
                    return
                }
                
                print("connection successfull::::")
                GlobalClass.sharedInstance.removeActivity()
                completion(nil, true, dicResponse)
            })
        }else {
            print("Internet is not connected....")
            GlobalClass.sharedInstance.removeActivity()
        }
    }
    
    
    
    
}
