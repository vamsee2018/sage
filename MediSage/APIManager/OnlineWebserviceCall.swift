//
//  OnlineWebserviceCall.swift
//  DayTranslationApp
//
//  Created by MacMini102 on 20/06/17.
//  Copyright © 2017 MacMini102. All rights reserved.
//

import UIKit
import Alamofire

class OnlineWebserviceCall: NSObject {
    
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
    
    //MARK: - Singleton Pattern
    class var SharedInstance: OnlineWebserviceCall {
        struct Singleton
        {
            static let instance = OnlineWebserviceCall()
        }
        return Singleton.instance
    }
    
    //MARK: - Check Network 
    func reachability() -> Bool {
        let reachability = Reachability()
        
        if (reachability?.isReachable)!{
            return true
        }
        else {
            return false
        }
    }
    
    func PFAGetService(methodname:String, completion: @escaping (_ error: Error?, _ success: Bool, _ result: Any?)->Void)  {

        let strBaseURL: String = "\(basicURL)\(methodname)"
        DispatchQueue.main.async {
            GlobalClass.sharedInstance.activity()
        }
        if self.reachability() {
            Alamofire.request(strBaseURL, method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in

                    switch response.result {

                    case .success(let json):
                        DispatchQueue.main.async {
                            GlobalClass.sharedInstance.removeActivity()
                        }
                        let dicResponse = response.result.value as? [String : AnyObject]
                        if dicResponse != nil {
                            completion(nil, true, dicResponse as Any)
                        }else {
                            completion(nil, true, nil)
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            GlobalClass.sharedInstance.removeActivity()
                        }
                }
            }
        }else {
            DispatchQueue.main.async {
                GlobalClass.sharedInstance.removeActivity()
            }
            completion(nil, false, nil)
        }
    }
    
    func PFAPostService(methodName:String, parameter:[String: String], completion: @escaping (_ error: Error?, _ success: Bool, _ result: Any?)->Void)  {
        let strBaseURL: String = "\(basicURL)\(methodName)"
        let url = URL(string: strBaseURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)
        
        let headers: HTTPHeaders = [
          "Authorization": "TmlrQDM0MjE=",
//          "Accept": "application/json",
//          "Content-Type" :"application/x-www-form-urlencoded"
        ]
        
        DispatchQueue.main.async {
            GlobalClass.sharedInstance.activity()
        }
        if self.reachability() {
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in

                for (key,value) in parameter {
                    multipartFormData.append((value ).data(using: .utf8)!, withName: key)
                }

            }, usingThreshold: UInt64.init(), to: strBaseURL, method: .post, headers: headers, encodingCompletion:  {(encodingResult) in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: {(response) in
                            (response)
                            DispatchQueue.main.async {
                                GlobalClass.sharedInstance.removeActivity()
                            }
                            let dicResponse = response.result.value as? [String : AnyObject]
                            if dicResponse != nil {
                                completion(nil, true, dicResponse as Any)
                            }else {
                                completion(nil, true, nil)
                            }
                        })
                    case .failure(let encodingError):
                        DispatchQueue.main.async {
                            GlobalClass.sharedInstance.removeActivity()
                        }
                }
            })
        }else {
            DispatchQueue.main.async {
                GlobalClass.sharedInstance.removeActivity()
            }
            completion(nil, false, nil)
        }
    }
    
    func PFAPostHeaderService(methodName:String, parameter:[String: String], completion: @escaping (_ error: Error?, _ success: Bool, _ result: Any?)->Void)  {
        let strBaseURL: String = "\(basicURL)\(methodName)"
        
        let headers: HTTPHeaders = [
//            "Authorization": generalFunc.getToken(),
            "Accept": "application/json",
            "Content-Type" :"application/x-www-form-urlencoded"
        ]
        DispatchQueue.main.async {
            GlobalClass.sharedInstance.activity()
        }
        print(parameter)
        if self.reachability() {
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
            
                for (key,value) in parameter {
                    multipartFormData.append((value ).data(using: .utf8)!, withName: key)
                }
                
            }, usingThreshold: UInt64.init(), to: strBaseURL, method: .post, headers: headers, encodingCompletion:  {(encodingResult) in
                    
                    switch encodingResult {
                        
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: { (response) in
                            print(response)
                            DispatchQueue.main.async {
                                GlobalClass.sharedInstance.removeActivity()
                            }
                            
                            let dicResponse = response.result.value as? [String : AnyObject]
                            if dicResponse != nil {
                                completion(nil, true, dicResponse as Any)
                            }else {
                                completion(nil, true, nil)
                            }
                        })
                    case .failure(let encodingError):
                        DispatchQueue.main.async {
                            GlobalClass.sharedInstance.removeActivity()
                        }
                }
            })
        }else {
            DispatchQueue.main.async {
                GlobalClass.sharedInstance.removeActivity()
            }
            completion(nil, false, nil)
        }
    }
    
    
    
    func PFAPostAppdelegeteService(methodName:String, parameter:[String: String], completion: @escaping (_ error: Error?, _ success: Bool, _ result: Any?)->Void)  {
        let strBaseURL: String = "\(basicURL)\(methodName)"
        
        let headers: HTTPHeaders = [
//            "Authorization": generalFunc.getToken(),
            "Accept": "application/json",
            "Content-Type" :"application/x-www-form-urlencoded"
        ]
        
        if self.reachability() {
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
            
                for (key,value) in parameter {
                    multipartFormData.append((value ).data(using: .utf8)!, withName: key)
                }
                
            }, usingThreshold: UInt64.init(), to: strBaseURL, method: .post, headers: headers, encodingCompletion:  {(encodingResult) in
                    
                    switch encodingResult {
                        
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: { (response) in
                            print(response)
                            let dicResponse = response.result.value as? [String : AnyObject]
                            if dicResponse != nil {
                                completion(nil, true, dicResponse as Any)
                            }else {
                                completion(nil, true, nil)
                            }
                        })
                    case .failure(let encodingError):
                        break
                }
            })
        }else {
            completion(nil, false, nil)
        }
    }
}
