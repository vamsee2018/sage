//
//  LoginRegistrationManager.swift
//  Reework
//
//  Created by Vivek Gajbe on 10/30/18.
//  Copyright © 2018 Intelegain. All rights reserved.
//

import UIKit

class LoginRegistrationManager: NSObject {
    static let shared = LoginRegistrationManager()
    var objRequest = RequestManager()
   
    func getCountryList(strProcedureName :String ,strParameterName : [String:String] ,completion:@escaping (_ response:[LoginRegisterEntity],_ success:Bool,_ error : String)->Void)
    {
        
        objRequest.requestCommonPostMethod(strAPIName: strProcedureName, strParameterName: strParameterName, useCommonUrl: <#Bool#>)
        { (result, isSuccess, error) in
            
            if isSuccess{
                var arrCatgry = [LoginRegisterEntity]()
                let objRegistration = LoginRegisterEntity()
                arrCatgry = objRegistration.parseCountryList(info: (result as? Dictionary<String, AnyObject>)!)
                
                completion(arrCatgry, true, error)
            }
            else
            {
                completion([], false, error)
            }
        }
    }

    func getUserDetails(strProcedureName :String ,strParameterName : [String:Any] ,completion:@escaping (_ response: [LoginRegisterEntity],_ success:Bool,_ error : String)->Void)
    {
        
        objRequest.requestCommonPostMethod(strAPIName: strProcedureName, strParameterName: strParameterName)
        { (result, isSuccess, error) in
            
            if isSuccess{
                var arrCatgry = [LoginRegisterEntity]()
                let objLogIn = LoginRegisterEntity()
                arrCatgry = objLogIn.parseLoginDetails(info: (result as? Dictionary<String, AnyObject>)!)
                
                completion(arrCatgry, true, error)
            }
            else
            {
                completion([], false, error)
            }
        }
    }
    
    
    /*
    func validateOTP(strProcedureName :String ,strParameterName : [String:Any] ,completion:@escaping (_ response:clsUserDetailsEntity,_ success:Bool,_ error : String)->Void)
    {
        
        objRequest.requestCommonPostMethod(strAPIName: strProcedureName, strParameterName: strParameterName)
        { (result, isSuccess, error) in
            
            if isSuccess{

                var objUser = clsUserDetailsEntity()
                objUser = objUser.parseUserDetails(withInfo: result as? Dictionary<String, AnyObject>)
                
                completion(objUser, true, error)
            }
            else
            {
                completion(clsUserDetailsEntity(), false, error)
            }
        }
    }*/
}
