//
//  APICallFile.swift
//  MediSage
//
//  Created by Harekrishna on 28/02/20.
//  Copyright © 2020 PFACode. All rights reserved.
//

import UIKit
import Alamofire

class APICallFile: NSObject {
    
    //MARK: - Use in any custom function in all class (file name. shared instance . (andar na badha function use kari kari sakay))
    class var SharedInstance: APICallFile {
        struct Singleton {
            static let Instance = APICallFile()
        }
        return Singleton.Instance
    }
    
    //MARK: - Internet Connection check karva
    func InternetConnection() -> Bool {
        let internetConnection = Reachability()
        if (internetConnection?.isReachable)! {
            return true
        }else {
            return false
        }
    }
}
