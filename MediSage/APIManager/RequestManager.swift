//
//  RequestManager.swift
//  Reework
//
//  Created by Vivek Gajbe on 10/30/18.
//  Copyright © 2018 Intelegain. All rights reserved.
//

import UIKit
import Alamofire

class RequestManager: NSObject
{
    
    /// common method is used for "Get" type of request
    ///
    /// - Parameters:
    ///   - strAPIName: api name (procedure name)
    ///   - strParameterName: param (always be empty)
    ///   - completion: return type
    func requestCommonGetMethod(strAPIName :String ,strParameterName : String, completion:@escaping (_ response:Any?,_ completed:Bool,_ errorMessage : String)->Void)
    {
        let url : String = "\(KJUrlConstants.CommonUrl)\(strAPIName)"
        let user = KJUrlConstants.Username
        let password = KJUrlConstants.ProPassword
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers: HTTPHeaders = ["Authorization": "Basic \(base64Credentials)"]
        
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
            .responseJSON { response in
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    if let code = response.response?.statusCode
                    {
                        if code == 200 //Success
                        {
                            completion(data,true,"")
                        }
                        else if code == 401 //Unauthorized User
                        {
                            completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
                        }
                        else //code not found
                        {
                            completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
                        }
                        
                    }
                }
                break
                
            case .failure(_)://Server side error
                completion(nil,false,(response.error?.localizedDescription)!)
                break
                
            }
        }
    }
    
    func requestGetMethod(strAPIName :String ,strParameterName : String, completion:@escaping (_ response:Any?,_ completed:Bool,_ errorMessage : String)->Void)
    {
        let url : String = "\(strAPIName)"
        
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default)
            .responseJSON { response in
            switch(response.result) {
            case .success(_):
                if let data = response.value{
                    if let code = response.response?.statusCode
                    {
                        if code == 200 //Success
                        {
                            completion(data,true,"")
                        }
                        else if code == 401 //Unauthorized User
                        {
                            completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
                        }
                        else //code not found
                        {
                            completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
                        }
                        
                    }
                }
                break
                
            case .failure(_)://Server side error
                completion(nil,false,(response.error?.localizedDescription)!)
                break
                
            }
        }
    }
    
    /// common method is used for "POST" type of request
    ///
    /// - Parameters:
    ///   - strAPIName: api name (procedure name)
    ///   - strParameterName: input param
    ///   - completion: return type
    func requestCommonPostMethod(strAPIName :String ,strParameterName : [String:Any] ,completion:@escaping (_ response:Any?,_ completed:Bool,_ errorMessage : String)->Void)
    {
        let url : String = "\(KJUrlConstants.CommonUrl)\(strAPIName)"
        
        let user = KJUrlConstants.Username
        let password = KJUrlConstants.ProPassword
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = [ "content-type": "application/json",
                        "cache-control": "no-cache",
                        "Authorization": "Basic \(base64Credentials)"]
        
        //completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
        
        do{
            let postData = try JSONSerialization.data(withJSONObject: strParameterName, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 120.0)
            
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    completion(nil,false,error as? String ?? KJCustomAlertString.WebServiceError )
                } else {
                    DispatchQueue.main.async {
                        let httpResponse = response as! HTTPURLResponse
                        let statusCode = httpResponse.statusCode
                        
                        do{
                            
                            if statusCode == 200 //Success
                            {
                                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
                                {
                                    completion(json,true,"")
                                }
                            }else if statusCode == 201 //Success
                            {
                                completion(data,true,"")
                            }
                            else if statusCode == 401 //Unauthorized User (session logout)
                            {
                                completion(data,false, "")
                            }else if statusCode == 400{
                                if let errorMsg = httpResponse.allHeaderFields["kastle-errors"] as? String{
                                    let msg = self.getTextAfterColon(string: errorMsg, type: ":\"")
                                    completion(data,false, msg)
                                }
                            }else{
                                 completion(nil,false,"")
                            }
                        }catch
                        {
                            print("Error with Json: \(error)")
                            completion(nil,false,error as? String ?? KJCustomAlertString.WebServiceError )
                        }
                        
                    }
                }
            })
            dataTask.resume()
        }catch {
            print("Error with Json: \(error)")
            completion(nil,false,error as? String ?? KJCustomAlertString.WebServiceError)
        }
        
    }
    
    
    ///this is method is for different url post login api
    func requestCommonPostMethodJson(strAPIName :String ,strParameterName : [String:Any] ,strHeaderToken : String  = "", strMethodName: String = "" ,completion:@escaping (_ response:Any?,_ completed:Bool,_ errorMessage : String)->Void)
    {
        let url : String = KJUrlConstants.Login + strAPIName
        var headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache"
        ]
        if strHeaderToken != ""
        {
            headers["Authorization"] = "Bearer \(strHeaderToken)"
        }
        
        do{
            let postData = try JSONSerialization.data(withJSONObject: strParameterName, options: [])
            
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 120.0)
            
            strMethodName == "" ? (request.httpMethod = "POST") : (request.httpMethod = strMethodName)
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    completion(nil,false,error as? String ?? KJCustomAlertString.WebServiceError )
                } else {
                    DispatchQueue.main.async {
                        let httpResponse = response as! HTTPURLResponse
                        let statusCode = httpResponse.statusCode
                        
                        do{
                            
                            if statusCode == 200 //Success
                            {
                                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [Any]
                                {
                                    completion(json,true,"")
                                }
                                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
                                {
                                    completion(json,true,"")
                                }
                                
                            }
                            else if statusCode == 201 //Success for forgot password
                            {
                                completion(data,true,"")
                                //                                    if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [Any]
                                //                                    {
                                //                                        completion(json,true,"")
                                //                                    }
                                //                                    if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
                                //                                    {
                                //                                        completion(json,true,"")
                                //                                    }
                                
                            }
                            else if statusCode == 401 //Unauthorized User (session logout)
                            {
                               
                                completion(data,false, "")
                               
                            }else if statusCode == 400{
                                if let errorMsg = httpResponse.allHeaderFields["kastle-errors"] as? String{
                                    var msg = self.getTextAfterColon(string: errorMsg, type: ":\"")
                                    msg = self.getMsgAfterDot(string: msg, type: "\\")
                                    completion(data,false, msg)
                                }
                            }
                            else if statusCode == 500 //invalid data
                            {
                                if let errorMsg = httpResponse.allHeaderFields["kastle-errors"] as? String{
                                    var msg = self.getTextAfterColon(string: errorMsg, type: ":\"")
                                    msg = self.getMsgAfterDot(string: msg, type: "\"")
                                    completion(data,false, msg)
                                }
                            }
                            else{
                                completion(nil,false,KJCustomAlertString.WebServiceError )
                            }
                        }catch
                        {
                            print("Error with Json: \(error)")
                            completion(nil,false,error as? String ?? KJCustomAlertString.WebServiceError )
                        }
                        
                    }
                }
            })
            dataTask.resume()
        }catch {
            print("Error with Json: \(error)")
            completion(nil,false,error as? String ?? KJCustomAlertString.WebServiceError)
        }
        
    }
    
    
    
    
    // common method is used for "POST" type of request
    ///
    /// - Parameters:
    ///   - strAPIName: api name (procedure name)
    ///   - strUserId: user id
    ///   - profileImage: profile image
    ///   - completion: return type
    
    func requestCommonPostMultipartMethod(strAPIName :String ,strParameterName : [String:Any],profileImage : UIImage ,completion:@escaping (_ response:Any?,_ completed:Bool,_ errorMessage : String)->Void)
    {
        let url : String = KJUrlConstants.CommonUrl + strAPIName
        
        let user = KJUrlConstants.Username
        let password = KJUrlConstants.ProPassword
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers: HTTPHeaders = ["Authorization": "Basic \(base64Credentials)",
                                    "content-type": "multipart/form-data"]
        
        let strParam = strParameterName
        
        let compressImage = self.resizeImage(image:profileImage)
        let imageData = compressImage!.jpegData(compressionQuality: 0.8)
        AF.upload(multipartFormData: { (multipartFormData) in
            
            if let data = imageData{
                multipartFormData.append(data, withName: "profile_image", fileName: "profile_name\(arc4random()).jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in strParam {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            print("Multipart Payload", multipartFormData)
        }, to: url, method: .post, headers: headers )
        .responseJSON { response in
            print("Succesfully uploaded")
            if let err = response.error{
                print(err.localizedDescription)
                completion(nil,false,err.localizedDescription)
                
            }
            if let data = response.value{
                if let code = (data as? [String : AnyObject])?["Status"] as? Int//Check for the code tag
                {
                    if code == 1 //Success
                    {
                        completion(data,true,(((data as? [String : AnyObject])?["Message"] as? String)!))
                    }
                    else   if code == 2 //Unauthorized User
                    {
                        
                    }
                    else  if code == 0//Success
                    {
                        if let Message = (data as? [String : AnyObject])?["Message"] as? String//Check for the code tag
                        {
                            completion(data,false,Message)
                        }else if let MessageDict = (data as? [String : AnyObject])?["Message"] as? [String : AnyObject] {
                            completion(data,false,((MessageDict as? [String : AnyObject])?["message"] as? String)!)
                        }
                    }
                    else if code == 3//Success
                    {
                        completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
                    }
                    else //Server side error
                    {
                        completion(data,false,((data as? [String : AnyObject])?["Message"] as? String)!)
                    }
                }
                else //If code not found
                {
                    completion(data,false,"")
                }
            }
        }
    }
    
    func resizeImage(image : UIImage) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 100)))
        imageView.contentMode = .scaleToFill
        imageView.image = image
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, image.scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    ///function to text after id
    func getTextAfterColon(string: String?, type: String) -> String{
        if let range = string?.range(of: type) {
            let strText = string![range.upperBound...]
            print(strText) // prints "123.456.7891"
            return String(strText)
        }
        return string!
    }
    
    ///function to get string
    func getMsgAfterDot(string: String?, type: String) -> String{
        var str = string
        if (str?.contains(type))! {
            let endIndex = str?.range(of: type)!.lowerBound
            str = str?.substring(to: endIndex!).trimmingCharacters(in: .whitespacesAndNewlines)
            return str!
        }
        return str!
    }
}

