//
//  CommonService.swift
//  MediSage
//
//  Created by Atul Prakash on 24/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

struct CommonService {
    static let requestManager = RequestManager()
    static let searchEntity = ProfileEntity()
    static let homeEntity = HomeEntity()
    typealias profileCompletion = (_ profileArray: ProfileEntity?, _ success: Bool, _ error: String?) -> Void
    typealias communityCompletion = (_ profileArray: HomeEntity?, _ success: Bool, _ error: String?) -> Void
    
    static func getProfile(userId: String, completion: @escaping profileCompletion) {
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MyProfile + userId, strParameterName: "") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let profileArray = searchEntity.parseProfile(withInfo : data as? Dictionary<String, AnyObject>)
                    completion(profileArray, true, nil)
                } else {
                    completion(nil, false, error)
                }
            } else {
                completion(nil, false, error)
            }
        }
    }
    
    static func getMyCommunityList(userId: String, completion: @escaping communityCompletion){
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MYCommunityList + userId, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let communitiesArray = homeEntity.parseCommunityList(info : data as! Dictionary<String, AnyObject>)
                    completion(communitiesArray, true, nil)
                } else {
                    completion(nil, false, error)
                }
            } else {
                completion(nil, false, error)
            }
        }
    }
}
