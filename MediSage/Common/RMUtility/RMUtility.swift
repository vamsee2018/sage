//
//  RMUtility.swift
//  Reework
//
//  Created by Vivek Gajbe on 10/30/18.
//  Copyright © 2018 Intelegain. All rights reserved.
//

import UIKit
import MessageUI
import SystemConfiguration

class RMUtility: NSObject,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    static let sharedInstance = RMUtility()
    /**
     check if internet connection is available
     returns true or false
     */
    
    func isInternetAvailable() -> Bool
    {
        //This check for Bangalore office. As it needs internet flag always on

        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    //MARK: - Localise
    /// method is used for setting localize string file
    ///
    /// - Parameters:
    ///   - key: string
    ///   - value: string
    /// - Returns: string
    func localizedLocalString(forKey key: String, Default value:String) -> String {
        var localizationBundle: Bundle?
        var langCode: String = ""
        if (langCode == "") {
            langCode = self.user_PREFERENCE_getAppLanguage()
        }
        // path to this languages bundle
        if langCode == "en"{
            let langugageBundlepath: String? = Bundle.main.path(forResource: "Base", ofType: "lproj")
            if langugageBundlepath == nil {
                // there is no bundle for that language(new language) use main bundle instead
                localizationBundle = Bundle.main
            }
            else {
                localizationBundle = Bundle(path: langugageBundlepath!)
                if localizationBundle == nil {
                    localizationBundle = Bundle.main
                }
            }
        }else{
            let langugageBundlepath: String? = Bundle.main.path(forResource: langCode, ofType: "lproj")
            if langugageBundlepath == nil {
                // there is no bundle for that language(new language) use main bundle instead
                localizationBundle = Bundle.main
            }
            else {
                localizationBundle = Bundle(path: langugageBundlepath!)
                if localizationBundle == nil {
                    localizationBundle = Bundle.main
                }
            }
        }
       
        return (localizationBundle?.localizedString(forKey: key, value: value, table: nil))!
    }
    //  Converted with Swiftify v1.0.6355 - https://objectivec2swift.com/
    func user_PREFERENCE_getAppLanguage() -> String {
        
        if (UserDefaults.standard.value(forKey: KJConstant.CurrentLanguage) != nil) //setValue(LanguaType.en, forKey: OCConstant.CurrentLanguage)
        {
            let strLangType : String = UserDefaults.standard.value(forKey: KJConstant.CurrentLanguage) as! String
            
            if strLangType == "en"
            {
                return "en"
            }
            else
            {
                return "ko"
            }
            
        }
        return "en" //
    }
    
    /*
     
     Method Name : sendEmail
     
     This method is used for preparing an Email window with logs details
     Note: Log file is yet to be attached
     
     */
    func sendEmail(toRecipients:[String], subject:String, body:String, attachment:Data?,onView : UIViewController,showOnSelf : Bool = false ,IsFromFeedback : Bool = false) {
        
        
        // check if system can send email .
        if MFMailComposeViewController.canSendMail(){
            let EmailComposeVC = MFMailComposeViewController()
            EmailComposeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            // commenting default address if RBL asks to have default address will update email over here.
            EmailComposeVC.setToRecipients(toRecipients)
            EmailComposeVC.setSubject(subject)
            EmailComposeVC.setMessageBody(body, isHTML: false)
            // Resolved crash by checking attachment is nil or not
            if attachment != nil{
                EmailComposeVC.addAttachmentData(attachment!, mimeType: "txt", fileName: "RMLogs.txt")
            }
            //present email view
            if !showOnSelf
            {
                KJConstant.APP_DEL.window?.rootViewController?.present(EmailComposeVC, animated: true, completion: nil)
            }
            else
            {
                onView.present(EmailComposeVC, animated: true, completion: nil)
            }
        }
        else
        {
//            let baseVC = BaseViewController()
//            baseVC.ShowAlert(title: "", message: KJConstant.NoAppConfiguredAlert,onView:onView)
        }
    }
    
    /*
     
     Method Name : mailComposeController
     
     delegate method called when user sends or cancells sending email.
     
     */
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch (result.rawValue) {
        case MFMailComposeResult.cancelled.rawValue:
            controller.dismiss(animated: true, completion: nil)
        case MFMailComposeResult.failed.rawValue:
            controller.dismiss(animated: true, completion: nil)
        case MFMailComposeResult.sent.rawValue:
            
                controller.dismiss(animated: true, completion: nil)
            
        case MFMailComposeResult.saved.rawValue:
            
            controller.dismiss(animated: true, completion: nil)
        default:
            break;
        }
        
    }
    
    /*
     
     Method Name : sendSMS
     
     This method is used for preparing a Message window with all the  contents
     
     */
    func sendSMS(toRecipients:[String], body:String) {
        if MFMessageComposeViewController.canSendText() {
            let messageComposeVC = MFMessageComposeViewController()
            messageComposeVC.body = body
            messageComposeVC.recipients = toRecipients
            messageComposeVC.messageComposeDelegate = self
            KJConstant.APP_DEL.window?.rootViewController?.present(messageComposeVC, animated: true, completion: nil)
        }
    }
    
    /*
     
     Method Name : messageComposeViewController didFinishWith result
     
     This method is used to dismiss the Message window
     
     */
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        // Check the result or perform other tasks.
        // Dismiss the Message compose view controller.
        switch (result.rawValue) {
        case MessageComposeResult.cancelled.rawValue:
            controller.dismiss(animated: true, completion: nil)
            
        case MessageComposeResult.failed.rawValue:
            controller.dismiss(animated: true, completion: nil)
            
        case MessageComposeResult.sent.rawValue:
            controller.dismiss(animated: true, completion: nil)
            
        default:
            break;
        }
    }
    
    
        
    /*
     
     Method Name : setPopUpTableViewHeight
     
     This method is used for changing the popup tableview height according to its contents.
     
     */
    
    class func setPopUpTableViewHeight(tblView:UITableView, cnstHeight:NSLayoutConstraint, screenHeight:CGFloat){
        //Need to check if the content size exeeding the screensize
        if tblView.contentSize.height + KJConstant.bottomHeight > screenHeight - KJConstant.excludeSize {
            cnstHeight.constant = screenHeight - KJConstant.excludeSize
        }
        else{
            cnstHeight.constant = tblView.contentSize.height + KJConstant.bottomHeight
        }
    }
    //MARK: - zigzag view
      /// method is used for applying zigzag effect
         ///
         /// - Parameter givenView: uiview
         func applyZigZagEffect(givenView: UIView) {
             let width = givenView.frame.size.width
             let height = givenView.frame.size.height
             
             // let givenFrame = givenView.frame
             let zigZagWidth = CGFloat(7)
             let zigZagHeight = CGFloat(5)
             let yInitial = height-zigZagHeight
             
             let zigZagPath = UIBezierPath()
             zigZagPath.move(to: CGPoint(x: 0, y: 0))
             zigZagPath.addLine(to: CGPoint(x: 0, y: yInitial))
             
             var slope = -1
             var x = CGFloat(0)
             var i = 0
             while x < width {
                 x = zigZagWidth * CGFloat(i)
                 let p = zigZagHeight * CGFloat(slope)
                 let y = yInitial + p
                 let point = CGPoint(x: x, y: y)
                 zigZagPath.addLine(to: point)
                 slope = slope*(-1)
                 i = i + 1
             }
             zigZagPath.addLine(to: CGPoint(x: width, y: 0))
             
             let shapeLayer = CAShapeLayer()
             shapeLayer.path = zigZagPath.cgPath
             givenView.layer.mask = shapeLayer
         }
    //MARK: - set shodowCorner radious
    /// Method is used for to set view with corner radious  & shadow effect
    ///
    /// - Parameter viw: view container
    func setViewWithCronerRadious_Shadow(viw:UIView,cornerRadious:Double)
    {
        viw.layer.cornerRadius = 20
        viw.layer.masksToBounds = true;
        
        viw.backgroundColor = UIColor.white
        viw.layer.shadowColor = UIColor.darkGray.cgColor
        viw.layer.shadowOpacity = 0.8
        viw.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viw.layer.shadowRadius = CGFloat(cornerRadious)
        viw.layer.masksToBounds = false
    }
    //MARK: - get pulic ip address
    //MARK: - get network public address
       func getPublicIPAddress() -> String {
           var publicIP = ""
           do {
               publicIP = try String(contentsOf: URL(string: "http://icanhazip.com/")!, encoding: String.Encoding.utf8)
               //print(publicIP)
               publicIP = publicIP.trimmingCharacters(in: CharacterSet.whitespaces)
               publicIP = publicIP.replacingOccurrences(of: "\n", with: "")
           }
           catch {
               print("Error: \(error)")
           }
           return publicIP
       }
    //MARK: - get currency symbol
   /*
     * Bear in mind not every currency have a corresponding symbol.
     *
     * EXAMPLE TABLE
     *
     * currency code | Country & Currency | Currency Symbol
     *
     *      BGN      |   Bulgarian lev    |      лв
     *      HRK      |   Croatian Kuna    |      kn
     *      CZK      |   Czech  Koruna    |      Kč
     *      EUR      |       EU Euro      |      €
     *      USD      |     US Dollar      |      $
     *      GBP      |   British Pound    |      £
     */

    func getSymbol(forCurrencyCode code: String) -> String {
       let locale = NSLocale(localeIdentifier: code)
        return locale.displayName(forKey: NSLocale.Key.currencySymbol, value: code) ?? ""
    }
    //MARK: - date conversion method
    //MARK: - date format change
       func formatDate(date: NSDate , format : String) -> String {
           let formatter = DateFormatter()
           
           formatter.dateFormat = format
           return formatter.string(from: date as Date)
       }
       //---------------------------------------------------------------------------
       // function to format date to given format  (string to string conversion)
       //---------------------------------------------------------------------------
       func formatDate(date: String , currentformat : String , requiredformat : String) -> String {
           let formatter = DateFormatter()
           formatter.dateFormat = currentformat
        formatter.timeZone=NSTimeZone(name: "UTC") as TimeZone?
           let formatteddate = formatter.date(from: date)
           formatter.dateFormat = requiredformat
           
           var dateinstring =  ""
           
           if(formatteddate != nil)
           {
               dateinstring = formatter.string(from: formatteddate!)
           }
           return dateinstring
       }
    //MARK: - get qnique feild
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    //Dynamic height for label
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
}
//MARK: - extend string
extension String{
    
    private static let decimalFormatter:NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        return formatter
    }()
    
    private var decimalSeparator:String{
        return String.decimalFormatter.decimalSeparator ?? "."
    }
    
    func isValidDecimal(maximumFractionDigits:Int)->Bool{
        
        // Depends on you if you consider empty string as valid number
        guard self.isEmpty == false else {
            return true
        }
        
        if self.contains(" ") { //To prevent blank spaces
            return false
        }
        
        // Check if valid decimal
        if let _ = String.decimalFormatter.number(from: self){
            
            // Get fraction digits part using separator
            let numberComponents = self.components(separatedBy: decimalSeparator)
            // before decimal it should only accept 6 digits so we have checked this condition and post decimal only two digits it should accept
            if numberComponents.count == 1
            {
                let number = numberComponents[0]
                return number.count <= 13
            }
            let fractionDigits = numberComponents.count == 2 ? numberComponents.last ?? "" : ""
            return fractionDigits.count <= maximumFractionDigits
        }
        
        return false
}
}

class ImageSaver: NSObject {
    func writeToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
    }
    
    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Save finished!")
    }
}
