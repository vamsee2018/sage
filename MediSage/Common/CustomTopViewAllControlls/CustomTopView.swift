//
//  CustomTopView.swift
//  MoBank
//
//  Created by Akshay on 14/03/18.
//  Copyright © 2018 Winjit. All rights reserved.
//

import UIKit

/// class to show top view on each viewcontroller
class CustomTopView: UIView {
    
    //MARK:- Outlets
    @IBOutlet var viwContent: UIView!
    @IBOutlet var viwBottom: UIView!
    @IBOutlet var imgLogo: UIImageView!
     @IBOutlet var imgLeftLogo: UIImageView!
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTicketOrderNumber: UILabel!

    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet var viwNotification: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgMore: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var btnLogo: UIButton!
    @IBOutlet weak var btnLeftLogo: UIButton!
    @IBOutlet weak var lblTitleCenterYConstaint: NSLayoutConstraint!
    //MARK:- View life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    //MARK:- User defined functions
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    func commonInit() {
        // Drawing code
        Bundle.main.loadNibNamed(KJViewIdentifier.CustomTopView, owner: self, options: nil)
        addSubview(viwContent)
        viwContent.frame = self.bounds
    }
    
    //MARK:- button tapped methods
    
    @IBAction func btnHomeTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(KJConstant.donePressed), object: nil, userInfo: nil)
    }
    
    @IBAction func btnMoreTapped(_ sender: Any) {
       
    }
    
    @IBAction func btnInviteTapped(_ sender: Any) {
        
    }
    
    
    /// function to Hide back button
    func hideBackButton(){
        //imgBack.isHidden = true
    }
    
}
