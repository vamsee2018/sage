//
//  CustomNavigationController.swift
//  MoBank
//
//  Created by Akshay Pujari on 18/05/17.
//  Copyright © 2017 Winjit. All rights reserved.
//

/*
 
 Class Description : CustomNavigationController.swift
 
 This is class will have functioanlity to customize navigation controller
 like we want transparent navigationbar .so in future we have to make another changes we can have in this class only.
 
 
 */

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationbarBackItems()
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.isUserInteractionEnabled = true
        self.navigationItem.hidesBackButton = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Bring logger button to front
    }
    
    /// function to set navigationbar items
    func setNavigationbarBackItems(){
            let img = #imageLiteral(resourceName: "backarrow")
            let navbarImageView = UIImageView(image: img)
            //add static height to image ..to solved padding issue
            navbarImageView.frame = CGRect(x: 0, y: 20, width: 30, height: 40)
            navbarImageView.contentMode = .scaleAspectFit
            let barButton = UIBarButtonItem(customView: navbarImageView)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = barButton

    }
  }
