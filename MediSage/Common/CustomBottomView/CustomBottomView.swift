//
//  CustomBottomView.swift
//  NFH
//
//  Created by Ghous Ansari on 23/05/19.
//  Copyright © 2019 intelegain. All rights reserved.
//

import UIKit
var isHomeTint = true
class CustomBottomView: UIView {

    //MARK:- referencing outlets
    @IBOutlet var viwContent: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var imgHome: UIImageView!
    
    //MARK:- View life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit() {
        // Drawing code
        Bundle.main.loadNibNamed(KJViewIdentifier.CustomBottomView, owner: self, options: nil)
        
        addSubview(viwContent)
        
        viwContent.frame = self.bounds
        middleView.layer.shadowColor = UIColor.lightGray.cgColor
        middleView.layer.shadowOffset = CGSize(width: 30, height: 30)
        middleView.layer.shadowOpacity = 0.5
        middleView.layer.shadowRadius = 4.0
        
        imageTint()
        
    }
    
    @IBAction func btnHome(_ sender: UIButton) {
        if sender.tag == 0{
//        {
//            let storyBoard = UIStoryboard(name: KJViewIdentifier.SideMenu, bundle: nil)
//            let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.RootViewController) as! RootViewController
//            UIApplication.shared.keyWindow?.rootViewController = vc
//            isHomeTint = true
//            imageTint()
        }
        else
        {
            print("notification button tapped")
            isHomeTint = false
            imageTint()
        }
    }
    
    func imageTint()
    {
//        if isHomeTint
//        {
//            self.imgHome.tintImage(imgName: "home", imgView: self.imgHome, color: KJThemeConstant.defaltTheamColor)
//            self.imgNotification.tintImage(imgName: "notification", imgView: self.imgNotification, color: UIColor.darkGray)
//        }
//        else
//        {
//            self.imgHome.tintImage(imgName: "home", imgView: self.imgHome, color: UIColor.darkGray)
//            self.imgNotification.tintImage(imgName: "notification", imgView: self.imgNotification, color: RMThemeConstant.defaltTheamColor)
//        }
    }
    
}
