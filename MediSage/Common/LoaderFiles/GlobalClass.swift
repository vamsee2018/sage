import UIKit
// rm -rf .git  -  for remove .git from folder

class GlobalClass: NSObject {
      
  static let sharedInstance: GlobalClass = GlobalClass()
  static let language =  Locale.preferredLanguages[0].components(separatedBy: "-").first ?? Locale.preferredLanguages[0]
  var activityBackgroundView : UIView!
  var myActivityIndicator:UIActivityIndicatorView!
  var connected: Bool!
  var strLabel = UILabel()
  var msgFrame = UIView()
  let loading = InstagramActivityIndicator()
  
  // MARK: activity
  func activity() {
    
  
    if GlobalClass.sharedInstance.activityBackgroundView == nil {
        DispatchQueue.main.async(execute: {
            let view = UIApplication.shared.keyWindow!
            //  set fullview bacground
            let screenSize: CGRect = UIScreen.main.bounds
            self.activityBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
            //Set background color
            
            self.activityBackgroundView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
            self.activityBackgroundView.tag = 103
            
            // set center gray color bacground view
            self.msgFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 40 , width: 120, height: 120))
            self.msgFrame.center = self.activityBackgroundView.center
            self.msgFrame.layer.cornerRadius = 15
            self.msgFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
            
            //  set activity view controller in center gray view
            self.loading.frame = CGRect(x:(self.msgFrame.frame.width/2)-40 , y:(self.msgFrame.frame.height/2)-40, width: 80, height: 80)
            //loading.animationDuration = 0
            
            self.loading.rotationDuration = 3
            self.loading.numSegments = 10
            self.loading.strokeColor = UIColor.white
            self.loading.lineWidth = 3
            self.loading.startAnimating()
            let label = UILabel(frame: CGRect(x:0 , y:0, width: 80, height: 80))
            label.text = "Please \n Wait..."
            label.numberOfLines = 2
            label.textColor = UIColor.white
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 12)
            self.loading.addSubview(label)
            self.msgFrame.addSubview(self.loading)
            
            self.activityBackgroundView.addSubview(self.msgFrame)
            view.addSubview(self.activityBackgroundView)
        })
    }
    
  }

  
  // MARK: removeActivity
  func removeActivity(){
    
    DispatchQueue.main.async(execute: {
        if (GlobalClass.sharedInstance.activityBackgroundView != nil)
        {
          GlobalClass.sharedInstance.activityBackgroundView.removeFromSuperview()
            GlobalClass.sharedInstance.activityBackgroundView = nil
        }
      
      // self.activityBackgroundView.viewWithTag(103)?.removeFromSuperview()
    })
  }
    
    
}

extension NSTextAlignment {
    
    static func alignment() -> NSTextAlignment {
        return .left
    }
}

