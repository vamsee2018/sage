//
//  RMDatePicker.swift
//
//
//
//

import UIKit

protocol RMDatePickerDelegate: class {
    
    func RMDatePicker(rmDatePicker: RMDatePicker, didSelect date: NSDate)
    func RMDatePickerDidCancelSelection(rmDatePicker: RMDatePicker)
    
}

class RMDatePicker: UIView {
    
    // MARK: - Config
    struct Config {
        
        let contentHeight: CGFloat = 250
        let bouncingOffset: CGFloat = 20
        var startDate: NSDate?
        var minDate : Date?
        var maxDate : Date?
        var confirmButtonTitle = KJConstant.Done
        var cancelButtonTitle = KJConstant.Cancel
        var headerHeight: CGFloat = 50
        var animationDuration: TimeInterval = 0.2
        // #dummydata to be updated layer as design colours are not confirmed
        var contentBackgroundColor: UIColor = KJThemeConstant.whiteColor
        var headerBackgroundColor: UIColor = KJThemeConstant.whiteColor
        var confirmButtonColor: UIColor = KJThemeConstant.blackColor
        var cancelButtonColor: UIColor = KJThemeConstant.blackColor
        var overlayBackgroundColor: UIColor = UIColor.black.withAlphaComponent(0.6)
        
    }
    
    var config = Config()
    
    weak var delegate: RMDatePickerDelegate?
    
    // MARK: - IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var bottomConstraint: NSLayoutConstraint!
    var overlayButton: UIButton!
    
    // MARK: - Init
    /*
     This method is used to load nib
     
     */
    class func getFromNib() -> RMDatePicker
    {
        
        return UINib.init(nibName: String(describing: self), bundle: nil).instantiate(withOwner: self, options: nil).last as! RMDatePicker
    }
    
    
    
    
    
    // MARK: - IBAction
    // This method is called when done button is tapped on view.
    @IBAction func confirmButtonDidTapped(sender: AnyObject) {
        // set date which is selected from picker
        config.startDate = datePicker.date as NSDate
        
        dismiss()
        // call delegate method to set date
        delegate?.RMDatePicker(rmDatePicker: self, didSelect: datePicker.date as NSDate)
        
    }
    // This method is called when cancel button is tapped on view.
    @IBAction func cancelButtonDidTapped(sender: AnyObject) {
        dismiss()
        delegate?.RMDatePickerDidCancelSelection(rmDatePicker: self)
    }
    
    // MARK: - setup Picker
    /*
     This method will set up picker in view passed as parameter
     
     */
    
    private func setup(parentVC: UIViewController)
    {
        
        // Loading configuration
        datePicker.minimumDate = config.minDate
        datePicker.maximumDate = config.maxDate
        
        if let startDate = config.startDate {
            datePicker.date = startDate as Date
        }
        
        headerViewHeightConstraint.constant = config.headerHeight
        
        confirmButton.setTitle(config.confirmButtonTitle, for: .normal)
        cancelButton.setTitle(config.cancelButtonTitle, for: .normal)
        
        confirmButton.setTitleColor(UIColor.red, for: .selected)
        cancelButton.setTitleColor(UIColor.red, for: .selected)
        
        
        // Overlay view constraints setup
        
        overlayButton = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        overlayButton.backgroundColor = config.overlayBackgroundColor
        overlayButton.alpha = 0
        
        overlayButton.addTarget(self, action: #selector(cancelButtonDidTapped(sender:)), for: .touchUpInside)
        
        KJConstant.APP_DEL.window!.addSubview(overlayButton)
    
        // Setup picker constraints
        
        frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: config.contentHeight + config.headerHeight)
        
        bottomConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: parentVC.view, attribute: .bottom, multiplier: 1, constant: 0)
      
        overlayButton.addSubview(self)
        
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - frame.height, width: UIScreen.main.bounds.width, height: frame.height)
 
        move(goUp: false)
        
    }
    
    // THis method moves up picker view .
    // constrent are set in move method
    private func move(goUp: Bool) {
       bottomConstraint.constant = goUp ? config.bouncingOffset : config.contentHeight + config.headerHeight
    }
    
    // MARK: - show alertview method
    
    func show(inVC parentVC: UIViewController, completion: (() -> ())? = nil) {
        // setup date picker in parent view
        setup(parentVC: parentVC)
        // move up picker in parent view
        move(goUp: true)
        self.datePicker.date = Date()
        UIView.animate(
            withDuration: config.animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseIn, animations: {
                
                parentVC.view.layoutIfNeeded()
                self.overlayButton.alpha = 1
                
        }, completion: { (finished) in
            completion?()
        }
        )
        
    }
    
    // This function will dismiss date picker.
    func dismiss(completion: (() -> ())? = nil) {
        
        move(goUp: false)
        
        UIView.animate(
            withDuration: config.animationDuration, animations: {
                
                self.layoutIfNeeded()
                self.overlayButton.alpha = 0
                
        }, completion: { (finished) in
            completion?()
            self.removeFromSuperview()
            self.overlayButton.removeFromSuperview()
        }
        )
        
    }
    
}
