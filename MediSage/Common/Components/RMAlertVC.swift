//
//  CPAlertVC.swift
//  RMAlertView
//
//

import UIKit

/*
 clas name  : RMAlertVC.swift
 
 This class is custom class which will show Alert view controller as wire frame.
 
 
 */

// Enum for animation type for alertview
enum RMAlertAnimationType{
    case scale
    case rotate
    case bounceUp
    case bounceDown
}

class RMAlertVC: UIViewController {
    
    //MARK: - DECLARE
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
//    @IBOutlet weak var posButton: RMButton!
//    @IBOutlet weak var negButton: RMButton!
//    @IBOutlet weak var cancelButton: RMButton!
//
    
    let backgroundColor: UIColor = .black
    let backgroundOpacity: CGFloat = 0.5
    let animateDuration: TimeInterval = 1.0
    
    let scaleX: CGFloat = 0.3
    let scaleY: CGFloat = 1.5
    let rotateRadian:CGFloat = 1.5 // 1 rad = 57 degrees
    
    private var negHandler: RMAlertActionHandler?
    private var posHandler: RMAlertActionHandler?
    private var cancelHandler: RMAlertActionHandler?
    
    //MARK: - LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.alpha = 1.0
        alertView.layer.cornerRadius = KJThemeConstant.buttonRadius
        view.backgroundColor = backgroundColor.withAlphaComponent(backgroundOpacity)
//        if negButton != nil{
//            negButton.isHidden = true
//        }
//
//        // set custom properties to labels and buttons in alertview.
//        titleLabel.textColor = KJThemeConstant.blueColor
//      //  titleLabel.font = RequestManager.shared.getSpecificFont(size: KJThemeConstant.FontSizeXL, fontName: KJThemeConstant.MuktaVaani_Regular)
//        messageLabel.textColor = KJThemeConstant.blackColor
//        posButton.titleLabel?.textColor = KJThemeConstant.blueColor
    }
    
    //MARK: - CONFIG
    /*
     MEthod : ShowAlert
     
     this method will show alert on viewcontroller which is passed as parameter
     */
    
    /*
    class func showAlert(title: String, message: String,onView : UIViewController, animationType: RMAlertAnimationType = .scale) -> RMAlertVC{
        
        let alertStoryboard = UIStoryboard(name: "RMAlert", bundle: nil)
        let alertVC = alertStoryboard.instantiateViewController(withIdentifier: "RMAlertVC") as! RMAlertVC
        KJConstant.APP_DEL.window?.addSubview(alertVC.view)
        alertVC.titleLabel.text = title
        //This code is added to show the line spacing in message
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        
        let attributes = [NSAttributedString.Key.foregroundColor: KJThemeConstant.grayColor, NSAttributedString.Key.font: //KJThemeConstant.shared.getSpecificFont(size: KJThemeConstant.FontSizeXS, fontName: KJThemeConstant.MuktaVaani_Regular), NSAttributedString.Key.paragraphStyle: style]
        //replace blank alert with apologies message
//        if message == ""
//        {
//            alertVC.messageLabel.attributedText = NSMutableAttributedString(RMLocalise.WebServiceError, attributes: attributes)
//        }
//        else
//        {
        //  alertVC.messageLabel.attributedText = NSMutableAttributedString(string: message, attributes: attributes)
//        }
        
        return alertVC
        
    }
        */
    /*
     Method : addAction
     this method will add action to alert view.
     
     */
    /*
    func addAction(_ action: CPAlertAction){
        switch action.type{
        case .normal:
            posButton.setTitle(action.title, for: .normal)
            posHandler = action.handler
        case .cancel:
            guard negButton != nil else { return }
            negButton.isHidden = false
            negButton.setTitle(action.title, for: .normal)
            negHandler = action.handler
        case .dismiss:
            guard cancelButton != nil else { return }
            cancelButton.isHidden = false
            cancelButton.setTitle(action.title, for: .normal)
            cancelHandler = action.handler
        }
        
    }
    */
    
    //MARK: - ACTION
    // Action on positive button on alertview
    @IBAction func tapPositiveButton(_ sender: Any) {
        
        if let btn = sender as? UIButton
        {
            if btn.tag == 1011
            {
                if let posHandler = self.posHandler{
                    posHandler()
                    
                    return
                }
            }
        }
        
        self.view.removeFromSuperview()
        
        if let posHandler = self.posHandler{
            posHandler()
        }
        
        
    }
    
    // Action on negative button on alertview
    @IBAction func tapNegativeButton(_ sender: Any) {
        
        if let btn = sender as? UIButton
        {
            if btn.tag == 1011
            {
                if let negHandler = self.negHandler{
                    negHandler()
                    
                    return
                }
            }
        }
        
        self.view.removeFromSuperview()
        
        if let negHandler = self.negHandler{
            negHandler()
        }
    }
    
    // Action on cancel button on alertview
    @IBAction func tapCancelButton(_ sender: Any) {
        
        self.view.removeFromSuperview()
        
        if let cancelHandler = self.cancelHandler{
            cancelHandler()
        }
    }
}
