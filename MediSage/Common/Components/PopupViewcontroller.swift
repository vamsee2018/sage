//
//  PopupViewcontroller.swift
//
//
//  
//

/*
 Class  : PopupViewcontroller.swift
 this is custom popup is commonly used in app . array of string is passed to tableview and user can select any option that is sent back to previous view via delegate methods.
 
 */

import UIKit

enum AlertAnimationType{
    case scale
    case rotate
    case bounceUp
    case bounceDown
}

//This will be used to set the pop up type
enum PopUpType
{
    case singleCell
    case detailCell
    case emiPlanCell
    case detailCellForDebitCards //Only for debit card cell where credit card number is shown in XXXX XXXX XXXX 3434 format
}

//custom delegate to send selected item to parent view
protocol customPopupDelegate : class
{
    //Function to call after did select row
    func didSelectRow(strItem : String,selectedItemIndex: Int)
}

class PopupViewcontroller: UIViewController,UITableViewDataSource,UITableViewDelegate {

    //MARK: - declaration of variable and outlets
    
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var alertView: UIView!
   
    @IBOutlet weak var tblOptions: UITableView!
    @IBOutlet weak var posButton: RMButton!
    
    let backgroundColor: UIColor = .black
    let backgroundOpacity: CGFloat = 0.5
    let animateDuration: TimeInterval = 1.0
    
    var arrOptions = [AnyObject]()
    var delegate : customPopupDelegate?
    
    static var tableType : PopUpType = .singleCell
    
    let scaleX: CGFloat = 0.3
    let scaleY: CGFloat = 1.5
    let rotateRadian:CGFloat = 1.5
    
    var iSelectedIndex : Int = -1
    
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // alertView.alpha = 0
        alertView.layer.cornerRadius = KJThemeConstant.popupRadius
        view.backgroundColor = backgroundColor.withAlphaComponent(backgroundOpacity)
        
        tblOptions.alwaysBounceVertical = false
        
        posButton.setTitle(KJConstant.Cancel, for: .normal)
        //posButton.setButton(btnType: .GradientButton)
        tblOptions.delegate = self
        tblOptions.rowHeight = UITableView.automaticDimension
        tblOptions.estimatedRowHeight = 50
       
    }
    override func viewDidAppear(_ animated: Bool) {
        RMUtility.setPopUpTableViewHeight(tblView: tblOptions, cnstHeight: constraintHeight, screenHeight: self.view.frame.size.height)
    }
    
    //MARK: - CONFIG
    class func showPopup(arrStrings: [AnyObject],withSelectedIndex :Int,onviewController : UIViewController, animationType: AlertAnimationType = .scale, tblType: PopUpType = .singleCell)-> PopupViewcontroller{
        // instantiate popup storyboard
        let popupStoryboard = UIStoryboard(name: KJConstant.vcPop, bundle: nil)
        // instantiate view to be shown
        let popupVC = popupStoryboard.instantiateViewController(withIdentifier: "PopupViewcontroller") as! PopupViewcontroller
        
        //First set the table type
        tableType = tblType
        
        KJConstant.APP_DEL.window?.addSubview(popupVC.view)
        
        // pass property to popup view and set delegate
        if tableType == .singleCell
        {
            popupVC.arrOptions = arrStrings as [AnyObject]
        }

        popupVC.iSelectedIndex = withSelectedIndex
        popupVC.tblOptions.delegate = popupVC
        popupVC.tblOptions.dataSource = popupVC
        popupVC.delegate = onviewController as? customPopupDelegate
        popupVC.tblOptions.reloadData()
        
        return popupVC
        
    }
    
    
    //MARK: - ACTION
    //This function will run when user tap on cancel button
    @IBAction func tapPositiveButton(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    //MARK: - tableview methods
    func numberOfSections(in tableView: UITableView) -> Int {
      return  1
    }
    
    // number of rows in table view
    // popup table will have number of rows is equal to array count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptions.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if PopupViewcontroller.tableType == .singleCell
//        {
            // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
            cell.tintColor = UIColor.green
            
            // set accessory type none when cell is not selected
//            if indexPath.row == iSelectedIndex
//            {
//                cell.accessoryType = .checkmark
//            }else{
//                cell.accessoryType = .none
//            }
            
        cell.textLabel?.text = self.arrOptions[indexPath.row] as? String
        cell.textLabel?.textColor = KJThemeConstant.defaltTheamColor
            cell.textLabel?.numberOfLines = 4
            
            return cell
        
    }

    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if PopupViewcontroller.tableType == .singleCell
        {
            let cell = tableView.cellForRow(at: indexPath)
            
            // check if cell has check mark if yes then remove it else add checkmark
            if cell!.accessoryType == .checkmark
            {
                cell!.accessoryType = .none
                iSelectedIndex = -1
            }
            else
            {
                iSelectedIndex = indexPath.row
                
                cell!.accessoryType = .checkmark
                tblOptions.reloadRows(at: [indexPath], with: .none)
                
            }
            // call delegate method to set selected item
            if let _ = self.delegate
            {
                self.delegate?.didSelectRow(strItem: self.arrOptions[indexPath.row] as! String,selectedItemIndex: indexPath.row)
            }
            
            self.view.removeFromSuperview()
        }
       
    }
    
    /*
     
     Method Name : heightForRowAt indexPath
     
     This method will inform the tableview about height to be returned for a particular cell
     
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if PopupViewcontroller.tableType == .singleCell
        {
            return UITableView.automaticDimension
        }
        else
        {
            return 60.0
        }
        
    }
}
