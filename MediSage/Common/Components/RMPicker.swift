//
//  RMPicker.swift
//
//
//
//


/*
 Class  : RMPicker.swift
 this is custom picker is commonly used in app . array of string is passed to picker and user can select any option that is sent back to previous view via delegate methods.
 
 */

import UIKit
//custom delegate
protocol RMPickerDelegate: class {
    // set of methods to be invoked
    func RMPicker(selectedString : String ,atIndex: Int)
    func RMPickerDidCancelSelection(rmPicker: RMPicker)
    
}

class RMPicker: UIView,UIPickerViewDelegate,UIPickerViewDataSource {
    
    // MARK: - Config
    struct Config {
        
         let contentHeight: CGFloat = 250
         let bouncingOffset: CGFloat = 20
        
        var strSelected: String?
        // set title to buttons in picker
        var confirmButtonTitle = KJConstant.Done
        var cancelButtonTitle = KJConstant.Cancel
        
        var headerHeight: CGFloat = 50
        
        var animationDuration: TimeInterval = 0.2
        
        // #dummydata to be updated layer as design colours are not confirmed
        var contentBackgroundColor: UIColor = KJThemeConstant.lighGrayColor
        var headerBackgroundColor: UIColor = KJThemeConstant.whiteColor
        var confirmButtonColor: UIColor = KJThemeConstant.blackColor
        var cancelButtonColor: UIColor = KJThemeConstant.blackColor
        
        var overlayBackgroundColor: UIColor = UIColor.black.withAlphaComponent(0.6)
        
    }
    
    var config = Config()

    var arrData = [String]()
    
    weak var delegate: RMPickerDelegate?
    
    // MARK: - IBOutlets
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var bottomConstraint: NSLayoutConstraint!
    var overlayButton: UIButton!
    
    // MARK: - IBAction
    
    // This method is called when done button is tapped on view.
    @IBAction func confirmButtonDidTapped(sender: AnyObject) {
        // get index of string
        let index = picker.selectedRow(inComponent: 0)
        // get string from index in array
       config.strSelected = arrData[index]
        
        dismiss()
        // call delegate method to send selected string
        delegate?.RMPicker(selectedString: config.strSelected!, atIndex:index)
        
    }
    // This method is called when done button is tapped on view.
    @IBAction func cancelButtonDidTapped(sender: AnyObject) {
        dismiss()
        delegate?.RMPickerDidCancelSelection(rmPicker: self)
    }
    
    // MARK: - Private
    private func setup(parentVC: UIViewController)
    {
        // set delegate and datasource
        picker.delegate = self
        picker.dataSource = self
        headerViewHeightConstraint.constant = config.headerHeight
        
        confirmButton.setTitle(config.confirmButtonTitle, for: .normal)
        cancelButton.setTitle(config.cancelButtonTitle, for: .normal)
        
        confirmButton.setTitleColor(UIColor.red, for: .selected)
        cancelButton.setTitleColor(UIColor.red, for: .selected)
        
          // Overlay view constraints setup
        
        overlayButton = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        overlayButton.backgroundColor = config.overlayBackgroundColor
        overlayButton.alpha = 0
        
        overlayButton.addTarget(self, action: #selector(cancelButtonDidTapped(sender:)), for: .touchUpInside)
        
        if !overlayButton.isDescendant(of: parentVC.view) { parentVC.view.addSubview(overlayButton) }
        
        overlayButton.translatesAutoresizingMaskIntoConstraints = false
        
        parentVC.view.addConstraints([
            NSLayoutConstraint(item: overlayButton, attribute: .bottom, relatedBy: .equal, toItem: parentVC.view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: overlayButton, attribute: .top, relatedBy: .equal, toItem: parentVC.view, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: overlayButton, attribute: .leading, relatedBy: .equal, toItem: parentVC.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: overlayButton, attribute: .trailing, relatedBy: .equal, toItem: parentVC.view, attribute: .trailing, multiplier: 1, constant: 0)
            ]
        )
        
        
        // Setup picker constraints
        
        frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: config.contentHeight + config.headerHeight)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        bottomConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: parentVC.view, attribute: .bottom, multiplier: 1, constant: 0)
        
        if !isDescendant(of: parentVC.view) { parentVC.view.addSubview(self) }
        
        parentVC.view.addConstraints([
            bottomConstraint,
            NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: parentVC.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: parentVC.view, attribute: .trailing, multiplier: 1, constant: 0)
            ]
        )
        addConstraint(
            NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: frame.height)
        )
        
        move(goUp: false)
        
    }
    
    // THis method moves up picker view .
    // constrent are set in move method
    private func move(goUp goUp: Bool) {
        bottomConstraint.constant = goUp ? config.bouncingOffset : config.contentHeight + config.headerHeight
    }
    
    // MARK: - Show method
    // This method shows picker 
    // Parameter :Parent view and Array of strings.
    func show(inVC parentVC: UIViewController,arrData : [String], completion: (() -> ())? = nil) {
        self.arrData = arrData
        setup(parentVC: parentVC)
        picker.reloadAllComponents()
        move(goUp: true)
        
        UIView.animate(
            withDuration: config.animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseIn, animations: {
                
                parentVC.view.layoutIfNeeded()
                self.overlayButton.alpha = 1
                
            }, completion: { (finished) in
                completion?()
            }
        )
        
    }
    
    // This method dismisses picker .
    func dismiss(completion: (() -> ())? = nil) {
        
        move(goUp: false)
        
        UIView.animate(
            withDuration: config.animationDuration, animations: {
                
                self.layoutIfNeeded()
                self.overlayButton.alpha = 0
                
            }, completion: { (finished) in
                completion?()
                self.removeFromSuperview()
                self.overlayButton.removeFromSuperview()
            }
        )
    }
    
    //MARK: - Picker view delegate and datasources
    // number of component in picker view is 1
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // this method sets title to row in pickerview
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrData[row]
    }
    // Number of rows in component is array count
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return arrData.count
    }
    // method is called when component is selected from pickerview.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      config.strSelected = arrData[row]
    }
    
}
