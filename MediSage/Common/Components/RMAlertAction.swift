//
//  RMAlertAction.swift
//  Mobank
//

//

import Foundation

enum RMAlertActionType{
    case normal
    case cancel
    case dismiss
}

typealias RMAlertActionHandler = () -> Void

class CPAlertAction{
    
    let title: String
    let type: RMAlertActionType
    let handler: RMAlertActionHandler?
    
    init(title: String, type: RMAlertActionType, handler: RMAlertActionHandler?){
        self.title = title
        self.type = type
        self.handler = handler
    }
    
}
