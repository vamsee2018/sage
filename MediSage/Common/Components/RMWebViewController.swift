//
//  RMWebViewController.swift
//  MoBank
//
//  Created by Akshay Pujari on 24/05/17.
//  Copyright © 2017 Winjit. All rights reserved.
//

/*
 
 Class Description : RMWebViewController.swift
 
 This class is common for application , wherever user wants to redirect to in app web view this class will work as common web viewcontroller.
 
 
 */

import UIKit
import WebKit
class RMWebViewController: BaseViewController,UIWebViewDelegate ,WKUIDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var wbView: UIWebView!
    @IBOutlet weak var viwActivity: UIActivityIndicatorView!
    //MARK: - Variable declaration
    var urlToLoad: String?
    var titleString: String?
    var pdfSavedPath : Data?
    var bisHTMLContent = false
    var isFromLogin = false
    var strPageName = ""
    var isFromOrderVC = false
    //MARK: - View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setControllerPreference()
    }
    
    func setControllerPreference(){
        
        
        
    }
   
    func loadWebURL(strUrl : String)
    {
        
    }
    
    //MARK: - Webview delegate methods
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        showActivityIndicator()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        hideActivityIndicator()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        hideActivityIndicator()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK: - Activity methods
    
    // Method to Show activity indicator
    func showActivityIndicator()
    {
        self.viwActivity.isHidden = false
        self.viwActivity.startAnimating()
    }
    
    // Method to hide activity indicator
    func hideActivityIndicator()
    {
        self.viwActivity.isHidden = true
        self.viwActivity.stopAnimating()
    }
  
}
