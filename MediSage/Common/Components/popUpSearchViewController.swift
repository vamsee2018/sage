//
//  popUpSearchViewController.swift
//  Pratibha
//
//  Created by Ghous Ansari on 18/11/19.
//  Copyright © 2019 Intelegain. All rights reserved.
//

import UIKit

/// Used for getting data from selected Vc
@objc protocol searchDelegate: class{
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity,iScreenCount: Int)
    @objc optional
    func popupSearchOkSelected(selectedEntity: [LoginRegisterEntity],iScreenCount: Int)
}

class popUpSearchViewController: UIViewController,UIGestureRecognizerDelegate, UISearchBarDelegate{
    
    //MARK:- referencing outlets
    @IBOutlet weak var tblViewSearch: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var viwOuter: UIView!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var searchBarHeightConstraint: NSLayoutConstraint!

    //MARK:- declaring variable
    var arrSearchData = [LoginRegisterEntity]()
    var arrSearchList = [LoginRegisterEntity]()
    var arrName = [String]()
    var arrId = [String]()
    var arrCode = [String]()
    var delegate : searchDelegate?
    let gestureTapOutside = UITapGestureRecognizer()
    var strHeader = ""
    var iScreeCount = -1
    var bisCountrycode = false
    var isBusinessTypeSelected = false
    var isSourceNameSelected = false
    var isComingFromEditProfile = false
    var isComingFromUserDetails = false

    var bIsMultiSelect = false
    
    //MARK:- view load cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setControllerPreference()
        // Do any additional setup after loading the view.
    }
    
    //set custom functions
    func setControllerPreference(){
        self.viwOuter.layer.cornerRadius = 20
        
        lblHeading.text = strHeader
        searchBar.placeholder = "Search"
        arrSearchList = arrSearchData
        //This will hide/unhide keyboard
        gestureTapOutside.addTarget(self, action: #selector(hideKeyboardOnTapOutside))
        gestureTapOutside.delegate = self
        self.view.addGestureRecognizer(gestureTapOutside)
        //below line is to hide back button
        gestureTapOutside.cancelsTouchesInView = false
        if bIsMultiSelect
        {
            btnOk.isHidden = false
        }
        
        DispatchQueue.main.async {
           // self.searchBar.becomeFirstResponder()
            self.tblViewSearch.reloadData()
        }
    }
    func getScreenData(strHeader : String,arrData :[LoginRegisterEntity],iScreenCount: Int,bIsMultiSelect:Bool = false)
    {
        self.bIsMultiSelect = bIsMultiSelect
        self.strHeader = strHeader
        self.arrSearchData = arrData
        self.iScreeCount = iScreenCount
    }
    //This will hide keyboard
    @objc func hideKeyboardOnTapOutside(){
        self.view.endEditing(true)
    }
    
    func removeLeadingSpaceFromString(strText : String) -> String
    {
        let trimmedText = strText.trimmingCharacters(in: CharacterSet.whitespaces)
        //print("old = [\(strText)], trimmed = [\(trimmedText)]")
        
        return trimmedText
    }
    
    
    //MARK:- search bar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var searchText  = searchBar.text ?? ""
        searchText = removeLeadingSpaceFromString(strText: searchText)
        if searchText != ""
        {
            var arrFilter = [LoginRegisterEntity]()
            if bIsMultiSelect == false
            {
                if searchText.isDigits{
                    arrFilter = arrSearchData.filter({ (ent) -> Bool in
                        let tmp: NSString = ent.strCountryCode! as NSString
                        let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                        return range.location != NSNotFound
                    })
                }else if isComingFromUserDetails == true{
                    arrFilter = arrSearchData.filter({ (ent) -> Bool in
                        let tmp: NSString = ent.strCityName! as NSString
                        let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                        return range.location != NSNotFound
                    })
                }else{
                    arrFilter = arrSearchData.filter({ (ent) -> Bool in
                        let tmp: NSString = ent.strCountryName! as NSString
                        let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                        return range.location != NSNotFound
                    })
                }
                
            }
           
            arrSearchList.removeAll()
            arrSearchList = arrFilter
            if arrSearchList.count <= 0 //Filter array is empty
            {
                arrSearchList = arrSearchData
            }
            self.tblViewSearch.reloadData()
        }else{
            self.arrSearchList.removeAll()
            self.arrSearchList = arrSearchData
            self.tblViewSearch.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.tblViewSearch.reloadData()
    }
    
    //MARK:- button tapped methods
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
}


//MARK:- table view delegate
extension popUpSearchViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print(self.arrSearchList[indexPath.row].strName, self.arrSearchList[indexPath.row].strId)
        if bIsMultiSelect == false
        {
            DispatchQueue.main.async {
                self.delegate?.popupSearchDidSelected(index: indexPath.row, selectedEntity: self.arrSearchList[indexPath.row], iScreenCount: self.iScreeCount)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

//MARK:- table view datasource
extension popUpSearchViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblViewSearch.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! searchCell
        let ent = self.arrSearchList[indexPath.row]
       
        if isBusinessTypeSelected == true{
            cell.lblTitle.text = ent.strYear

        }else if isComingFromEditProfile == true{
            cell.lblTitle.text = ent.strAffiliation
        }else if isComingFromUserDetails == true{
            cell.lblTitle.text = ent.strCityName
            
        }else{
            cell.lblTitle.text = ent.strCountryName
        }

        return cell
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}


//MARK:- search table view cell
class searchCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
}
//MARK:- search table view cell
class searchMultiSelectCell: UITableViewCell{
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
}

//MARK: Check String Has Numbers
extension String {
  var isDigits: Bool {
    guard !self.isEmpty else { return false }
    return !self.contains { Int(String($0)) == nil }
  }
}
