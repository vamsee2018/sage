//
//  CustomTopViewBackButton.swift
//  NFH
//
//  Created by Ghous Ansari on 12/09/19.
//  Copyright © 2019 intelegain. All rights reserved.
//

import Foundation
import UIKit

/// class to show top view on each viewcontroller
class CustomTopViewBack: UIView {
    
    //MARK:- Outlets
    @IBOutlet var viwContent: UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK:- View life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    //MARK:- User defined functions
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    func commonInit() {
        // Drawing code
        Bundle.main.loadNibNamed(KJViewIdentifier.CustomTopViewBack, owner: self, options: nil)
        addSubview(viwContent)
        viwContent.frame = self.bounds
    }
    
    
}

