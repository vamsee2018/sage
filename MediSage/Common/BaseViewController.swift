//
//  BaseViewController.swift
//  Reework
//
//  Created by Vivek Gajbe on 10/30/18.
//  Copyright © 2018 Intelegain. All rights reserved.
//

import UIKit
import Firebase
// This enum is used for identifying back button type
enum BackButtonType : Int {
    case LeftArrowButton
    case NoButton
    
}

enum gradientType {
    case fullThickShadow
    case fullFadedShadow
    case fullBottomShadow
}

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
//    var alertVC: RMAlertVC!
//    var alertVCSession: RMAlertVC!
//    var alertPopUp: PopupViewcontroller!
    var viwTop = CustomTopView()
    let myActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    let gestureTapOutside = UITapGestureRecognizer()
    
    //    var alertPopUp: PopupViewcontroller!
    //    var delegateBaseVC: BaseViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //This will hide/unhide keyboard
        gestureTapOutside.addTarget(self, action: #selector(hideKeyboardOnTapOutside))
        gestureTapOutside.delegate = self
        self.view.addGestureRecognizer(gestureTapOutside)
        //below line is to hide back button
        self.navigationItem.setHidesBackButton(true, animated: false)
        //below line is added to enable touches in view inheriting BaseView
        gestureTapOutside.cancelsTouchesInView = false
        //customizeNavigationwithBackButton()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(notificationPressed), name: NSNotification.Name(KJConstant.notificationPressed), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getTotalUnreadNotification), name: NSNotification.Name(KJConstant.notificationUnread), object: nil)
    }
    
    
    //MARK: - Custom methods
    
    
    //Remove gesture from Button, add it to whole view
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if touch.view is UIButton
        {
            return false
        }
        
        return true
    }
    
    @objc func navigatetoChatScreen(currentVC:UIViewController)
    {
       
    }
    //This will hide keyboard
    @objc func hideKeyboardOnTapOutside(){
        self.view.endEditing(true)
    }
    /*
     
     Method Name : setBackButton
     
     This is to decide if toshow back button or not on navigationItem.
     Input: Int
     
     */
    
    func setBackButton(toShow : BackButtonType)
    {
        //configure navigation bar title.
        
        
        switch toShow {
        case .LeftArrowButton://back arrow
              customizeNavigationwithBackButton()
        case .NoButton: break//back arrow
        }
    }
    
    //---------------------------------------------------------------------------
    // function is to set value for label to update cart count
    //--------------------------------------------------------------------------
    func updateCount(){
        if let notificationCount = UserDefaults.standard.value(forKey:"countValue") as? String {
            if notificationCount.count > 0{
                viwTop.lblNotificationCount.text = notificationCount
            }else{
                viwTop.viwNotification.isHidden = true
            }
        }else{
            viwTop.viwNotification.isHidden = true
        }
    }
    
    //This function will navigate screen on to root view controller
    
    @objc func donePressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///Notification button pressed
    @objc func notificationPressed(){
        
    }
    ///logout button pressed
    @objc func logoutPressed(){
        
    }
   
    //MARK: - method used for custom navigation
    //---------------------------------------------------------------------------
    // function is to customize navigationcontrol
    //---------------------------------------------------------------------------
    
    func customizeNavigationwithBackButton() //for all option in top bar
    {
        viwTop.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
        viwTop.btnMenu.addTarget(self, action: #selector(showSideMenu), for: .touchUpInside)
        viwTop.btnBack.addTarget(self, action: #selector(backToParent), for: .touchUpInside)
        viwTop.btnMore.addTarget(self, action: #selector(notificationBtnClicked), for: .touchUpInside)

        getTotalUnreadNotification()
        navigationController?.navigationBar.addSubview(viwTop)
    }
    
    func customizeNavigationwithBackButtonWithTitle() //top bar with back button title
    {
        viwTop.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
        viwTop.btnMenu.addTarget(self, action: #selector(backToParent), for: .touchUpInside)
        getTotalUnreadNotification()
        navigationController?.navigationBar.addSubview(viwTop)
    }
    
    func navigationWithBackAndLogo() //top bar with back button title
    {
        viwTop.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
        viwTop.btnBack.addTarget(self, action: #selector(backToParent), for: .touchUpInside)
        navigationController?.navigationBar.addSubview(viwTop)
    }
    
    ///Get Triggered notification status as read as false when come to this screen
    @objc func getTotalUnreadNotification(){
        
    }
    
    @objc func showSideMenu()
    {
        print("Touched")
        HamburgerMenu().triggerSideMenu()

    }
    
    @objc func backToParent()
    {
        print("Back Button Touched")
        NotificationCenter.default.post(name: Notification.Name("isPlayingVideo"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("notificationList"), object: nil)

        self.navigationController?.popViewController(animated: true)
        
       // navigationController?.popToViewController(ofClass: HomeViewController.self)

    }
    @objc func homeBtnClicked(){
    }
    
    @objc func notificationBtnClicked(){
        print("NOTIFICATION CLICKED")
        let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NotificationsViewController) as! NotificationsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //---------------------------------------------------------------------------
    // function is to set shadow to UIview
    //---------------------------------------------------------------------------
    func createShadowToView(viw : UIView)
    {
        viw.layer.masksToBounds = false
        viw.layer.shadowColor = UIColor.darkGray.cgColor
        viw.layer.shadowOpacity = 0.5
        viw.layer.shadowOffset = CGSize(width: 4, height:7)
        viw.layer.shadowRadius = 5.0
    }
    
    //---------------------------------------------------------------------------
    // function is to makeRoundedBorder to view
    //---------------------------------------------------------------------------
    func makeRoundedBorder (radius:CGFloat , view:UIView ,bordercolor : UIColor , borderwidth : CGFloat)
    {
        view.layer.cornerRadius = radius
        view.clipsToBounds = true
        view.layer.borderColor=bordercolor.cgColor
        view.layer.borderWidth=borderwidth
    }
    //MARK: - set gradient effect to view
    func setGradienbtEffectToView(viw : UIView)
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = viw.frame.size
        let color1 = KJThemeConstant.ButtonCommonColorLight.cgColor
        let color2 = KJThemeConstant.ButtonCommonColorDark.cgColor
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.0, 1.0]
        
        viw.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    //function for setting gradient to the view
    func shadowView(shadowType: gradientType,view:UIView,cornerRadius: CGFloat)
    {
        switch shadowType {
            
        case .fullThickShadow:
            
            view.layer.cornerRadius = cornerRadius
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOffset = CGSize(width: 0, height: 1)
            view.layer.shadowOpacity = 0.5
            view.layer.shadowRadius = 5.0
            
        case .fullFadedShadow:
            view.layer.cornerRadius = cornerRadius
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOffset = CGSize(width: 3, height: 3)
            view.layer.shadowOpacity = 0.5
            view.layer.shadowRadius = 7.0
            
        case .fullBottomShadow:
            view.layer.cornerRadius = cornerRadius
            view.layer.shadowColor = UIColor.lightGray.cgColor
            view.layer.shadowOffset = CGSize(width: 3, height: 3)
            view.layer.shadowOpacity = 0.5
            view.layer.shadowRadius = 4.0
        }
    }
    //---------------------------------------------------------------------------
    //MARK:- make rounded button
    //---------------------------------------------------------------------------
    func makeRoundedButtonBorder (radius:CGFloat , btn: UIButton ,bordercolor : UIColor , borderwidth : CGFloat)
    {
        btn.layer.cornerRadius = radius
        btn.clipsToBounds = true
        btn.layer.borderColor=bordercolor.cgColor
        btn.layer.borderWidth=borderwidth
    }
    
    
    //MARK: - loader method
    //---------------------------------------------------------------------------
    // function to Show loader
    //---------------------------------------------------------------------------
    func showLoader()
    {
        KJConstant.iLoaderCount = KJConstant.iLoaderCount + 1
        
        if KJConstant.iLoaderCount == 1
        {
            KJConstant.APP_DEL.window?.makeToastActivity()
        }
    }
    
    //---------------------------------------------------------------------------
    // function to hide loader
    //---------------------------------------------------------------------------
    func hideLoader()
    {
        KJConstant.iLoaderCount = KJConstant.iLoaderCount - 1
        
        if KJConstant.iLoaderCount > 0
        {
            return
        }
        
        KJConstant.iLoaderCount = 0
        
        KJConstant.APP_DEL.window?.hideRMLoader()
    }
    
    //---------------------------------------------------------------------------
    // function to Show loader
    //---------------------------------------------------------------------------
    func showLoader(viw:UIView)
    {
        KJConstant.iLoaderCount = KJConstant.iLoaderCount + 1
        
        if KJConstant.iLoaderCount == 1
        {
            viw.makeToastActivity()
        }
    }
    
    //---------------------------------------------------------------------------
    // function to hide loader
    //---------------------------------------------------------------------------
    func hideLoader(viw:UIView)
    {
        KJConstant.iLoaderCount = KJConstant.iLoaderCount - 1
        
        if KJConstant.iLoaderCount > 0
        {
            return
        }
        KJConstant.iLoaderCount = 0
        viw.hideRMLoader()
    }
    //MARK: - date formating method
    //---------------------------------------------------------------------------
    // function is to format a date as per input format
    //---------------------------------------------------------------------------
    func formatDate(date: NSDate , format : String) -> String {
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        return formatter.string(from: date as Date)
    }
    //---------------------------------------------------------------------------
    // function to format date to given format  (string to string conversion)
    //---------------------------------------------------------------------------
    func formatDate(date: String , currentformat : String , requiredformat : String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = currentformat
        formatter.timeZone=NSTimeZone(name: "UTC") as TimeZone?
        let formatteddate = formatter.date(from: date)
        formatter.dateFormat = requiredformat
        
        var dateinstring =  ""
        
        if(formatteddate != nil)
        {
            dateinstring = formatter.string(from: formatteddate!)
        }
        return dateinstring
    }
    
   
    //---------------------------------------------------------------------------
    // function to format date to given format  (string to date conversion)
    //---------------------------------------------------------------------------
    func formatDateFromString(date: String , currentformat : String , requiredformat : String) -> NSDate {
        
        let formatter = DateFormatter()
        formatter.dateFormat = currentformat
        formatter.timeZone=NSTimeZone(name: "UTC") as TimeZone?
        let formatteddate = formatter.date(from: date)
        
        return formatteddate! as NSDate
    }
    //Function to handle the scenario of when got some error
    func ShowAlert(title : String ,message:String, onView : UIViewController)
    {
        
    }
    
    //MARK: - save data to user default
    //---------------------------------------------------------------------------
    // function to set value to userdefault
    //---------------------------------------------------------------------------
    
    func setUserDefault (key : String , value : Bool)
    {
        let defaults =  UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    //---------------------------------------------------------------------------
    // function to get value from userdefault
    //---------------------------------------------------------------------------
    func getUserDefault (key : String ) -> Bool?
    {
        let defaults =  UserDefaults.standard
        
        let value = defaults.bool(forKey: key)
        
        return value as Bool?
        
    }
    
    
    //MARK: - share dynamic link
    func shareDynamicLink(CurrentVC : UIViewController,urlComp:URLComponents,strTitle:String,strID:String,imgUrl:String){
        
        var convertedURL = String()
        var comp = URLComponents()
        comp = urlComp
        
        guard let linkParam = comp.url else { return }
        
        print("I am sharing \(linkParam.absoluteString)")
       
        let urlString: String = linkParam.absoluteString
        let finalLink = urlString.replacingOccurrences(of: "%2F", with: "/")
        print("I am Final URL sharing \(finalLink)")

        if finalLink.contains("/video"){
            convertedURL = finalLink.replacingOccurrences(of: "/video", with: "?video")
        }else if finalLink.contains("/series"){
             convertedURL = finalLink.replacingOccurrences(of: "/series", with: "?series")
        }else if finalLink.contains("/news"){
            convertedURL = finalLink.replacingOccurrences(of: "/news", with: "?news")
        }else if finalLink.contains("/live_event"){
            convertedURL = finalLink.replacingOccurrences(of: "/live_event", with: "?live_event")
        }
        
        print("I am Final URL  \(convertedURL)")

        guard let shareLink = DynamicLinkComponents(link: URL(string: convertedURL)! , domainURIPrefix: "https://mymedisage.page.link") else {
            print("Could not create FDL component")
            return
        }
        
        if let myBundleId = Bundle.main.bundleIdentifier
        {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleId)
            shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: myBundleId)
        }
        shareLink.iOSParameters?.appStoreID = KJUrlConstants.applicationId
        
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        shareLink.socialMetaTagParameters?.title = strTitle
        shareLink.socialMetaTagParameters?.descriptionText = strID
        
        if let imgUrl = URL(string: imgUrl )
        {
            shareLink.socialMetaTagParameters?.imageURL = imgUrl
        }
        guard let longUrl = shareLink.url else {
            return
        }
        print("The long dynamic link is :\(longUrl)")
        
        shareLink.shorten { (url, warnings, error) in
            if let error = error
            {
                print("Oh no! Got an error \(error)")
                return
            }
            if let warnings = warnings
            {
                for warning in warnings
                {
                    print("FDL warning: \(warning) ")
                }
            }
            guard let url = url else {return}
            print("I have a short Url share\(url.absoluteString) ")
            
            let img = UIImageView()
            let str = shareLink.socialMetaTagParameters?.imageURL?.absoluteString
            img.loadImageUsingCache(withUrl: str ?? "")
            
            let shareItems:Array = [img.image!, url,strTitle,strID] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            activityViewController.setValue(strTitle, forKey: "subject")
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo,UIActivity.ActivityType.saveToCameraRoll,UIActivity.ActivityType.assignToContact]
            
            if let popoverController = activityViewController.popoverPresentationController {
                popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                popoverController.sourceView = CurrentVC.view
                popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }
            
            CurrentVC.present(activityViewController, animated: true, completion: nil)
        }
    }
    
}

//MARK:- This extension is for loading and caching images
//let imageCache = NSCache<NSString, AnyObject>()
extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String,WithBackgroundImageNamed:String = "Logo")
    {
        let url = URL(string: urlString)

        self.sd_setImage(with: url, placeholderImage: UIImage(named: WithBackgroundImageNamed))

    }
}

//MARK:- Extension for HTML to String
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

// Enum for storing date formats
enum RMDateFormat : String{
    case YYMM = "yyMM"
    case DDMMYYYY = "dd-MM-yyyy"
    case YYYYMMDDHHMM = "yyyy-MM-dd HH:mm"
    case YMMDDHMSSSSSS = "y-MM-dd H:m:ss.SSSS"
    case YYYYMMDD = "yyyy-MM-dd"
    case DDMMMYYYY = "dd MMM yyyy"
    case YYYYMMDDHHMMSSAAA = "yyyy-MM-dd'T'hh:mm:ss aaa"
    case YYYYMMDDHHMMSS = "yyyy-MM-dd'T'hh:mm:ss"
    case DDMMM = "dd MMM"
    case YYYYMMDDTHHMMSS = "yyyy-MM-dd'T'HH:mm:ss"
    case YYYYMMDDTHHMMSSSSS = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    case DDMMMYYYYWithComma = "dd MMM, yyyy"
    case ddmmyyyy = "dd/MM/yyyy"
    case ddMMMYYYYHHMMAAA = "dd MMM yyyy hh:mm aaa"
    case MMDDYY = "MM/dd/yy"
    case yyyyMMDDHHMMSSSpace = "yyyy-MM-dd hh:mm:ss"
    case YYYYMMDDHHMMSSSSS = "yyyy-MM-ddHH:mm:ss.SSS"
    case HHMMAAA = "HH:mm aaa"
    case YYYYMMMDDHHMMSSZZZZ = "yyyy-MM-dd HH:mm:ssz"
    case DDMMYYYYWithSpace = "dd MM yyyy"
    case YYYYMMDDBACKSLASH = "yyyy/MM/dd"
}

//MARK: - UIDevice extention for device mode
extension UIDevice {
    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case iPhone_x
        case unknown
    }
    var screenType: ScreenType {
        guard iPhone else { return .unknown }
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 1920:
            return .iPhone6Plus
        case 2436:
            return .iPhone6Plus
        case 1792,2688://iPhone_Xr,Xs,XSMax
            return .iPhone_x
        default:
            return .unknown
        }
    }
    
    var modelNameRM: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad7,5", "iPad7,6":                      return "iPad 6"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}



