//
//  KJCustomAlertString.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJCustomAlertString: NSObject {
    //Profile
    static let newPasswordWarning =  "Please enter new Password."
    static let rePasswordWarning =  "Please re-enter Password."
    static let CurrentPasswordWarning =  "Please enter current Password."
    
    static let Only_for_registered_user = "only for registered user"
    static let Alert = "Alert"
    static let LogoutText = "Are you sure you want to logout?"
    static let ChangedPasswordSuccessAlert =  "Password changed Successfully."
    static let ProfileUpdatedSuccessAlert =  "Profile Updated Successfully."
    static let PasswordUpdatedSuccessAlert = "Password Updated Successfully."
    static let OTPResentAlert = "OTP has been Resent Successfully."
    static let DeleteAddressAlert = "Are you sure you want to delete the address"
    static let ClearAllWishlistAlert = "Are you sure you want to clear all wishlist"
    static let DeleteWislistAlert = "Are you sure you want to delete the wishlist item"
    static let NoAppConfiguredAlert = "No app installed to handle this action. To use this feature, please check if you have respective app installed or email services are enabled. Thank you."
    static let ProfileInfoInitialAlert = "Kindly update your profile page to help us support you better."
    static let WebServiceError = "Due to technical issue we are unable to process your request at this time. Please try again after sometime."
    static let Thank_you_for_submitting_feedback = "Thank you for submitting feedback"
    //Payment screen
    static let PaymentFailedAlert = "Restart your failed payments instantly by click here to start the payment precess again,Call on below number for more assistance 8017016633"
    static let clickingHere = "click here"
    
    static let PaymentSuccessAlert = "Kindly check an SMS for Login details to explore your Pratibha in case you have not received an SMS with credentials, click here to resend it"
    static let GiftPaymentSuccessMsg = "Thanks for selecting Pratibha Products as a Gift. Login credentials have been SMS to the Gift Receiver."
    static let clickHereToResend = "click here to resend"
    static let DeniedByRisk = "You have exceeded daily no. of transactions allowed per card on this portal.Please try again by tomorrow now."
    
    //Category
    static let Please_login_to_add_product_quantity = "Please login to add product quantity"
    static let Please_login_to_add_the_product_in_wish_list = "Please login to add the product in wish list"
    static let product_is_out_of_stock = "product is out of stock"
    static let no_such_color_present  = "no such color present "
    static let Please_login_to_add_item = "Please login to add item"
    static let Please_login_to_view_cart = "Please login to view cart"
    //Login Alert
    static let ValidMobileNoMessage = "Please enter valid 10 digit mobile number."
    static let ValidOTPMessage = "Please enter valid 5 digit OTP."
    static let ValidPinCodeMessage = "Please enter valid 6 digit Pin Code."
    static let ValidSamePasswordMessage = "Password and Re-enter Password is not same."
    static let ValidPasswordMessage = "Please enter valid 6-10 digit Password."
    static let OTPExpireMessage = "Your OTP has expired. Please regenerate OTP"
    
    //Registration alert
    static let FirstNameEmptyWarning = "Please enter First Name."
    static let LastNameEmptyWarning = "Please enter Last Name."
    static let EmailEmptyWarning = "Please enter Valid Email."
    static let EnterDOBWarning = "Please enter Date of Birth"
    static let ValidDetailsWarning = "Please enter Valid details"
    static let selectAddressWarning =  "Please enter Address."
    static let selectCountryWarning =  "Please select valid Country."
    static let SelectStateWarning = "Please select valid State."
    static let SelectCityWarning = "Please select valid City."
    static let OTPSendAlert = "OTP has been sent to your mobile."
    static let freeTrialRegAlert = "Kindly log in with your registered mobile number and password."
    
    //Exam and Contest
    static let NotAValidFormat = "Please upload the mentioned file size and format"//"The file format is improper.Kindly upload the correct file format."
    static let SuccessParticipation = "Thank you for your Participation"
    
    
    //Query handling
    static let queryTypeWarning = "Please select query type"
    static let queryDescWarning = "Please add query description"
    
    
    //Proceed to pay
    static let SelectCouponWarning = "Please select valid Coupon."
    
    //plan alert
    static let validPackSelection = "Please select atleast one topic"
    
    //Cart
    static let Please_Select_Atleast_Quantity = "Please select atleast one quantity"
    static let Maximum_Quantity = "Maximum quantity is 1000"
    static let Quantity_NotAvailable = "Quantity not available. Available quantity is #"
    static let Are_you_sure_you_want_to_delete_this_product_from_cart = "Are you sure you want to delete this product from cart?"
    
    //course seen
    static let courseSeenPercent = "Course Seen Percentage :"
    //Exam
    static let validateExam = "You have already participated in this exam. Thank you! Watch out for future exams."
    static let vaidateExamDate = "Your exam is scheduled on"
    static let admissionClosureWarning = "Exam admission is closed"
    //Contest
    static let validateContest = "You have already participated in this contest. Thank you! Watch out for future contest."
    static let vaidateContestDate = "Your contest is scheduled on"
    static let no_exam_Result_list = "Exam certificates will be provided to eligible candidates in due course of time. Watch out for exam notifications and participate to win prizes and certificates."//"You have not participated in any exams yet !  Watch out for exam notifications and participate to win prizes and certificates."
    static let no_contest_Result_list = "Contests certificates will be provided to eligible candidates in due course of time. Watch out for contest notifications and participate to win prizes and certificates."//"You have not participated in any contest yet !  Watch out for contest notifications and participate to win prizes and certificates."
    static let no_contest_exam_Result_list = "Contests and exam certificates will be provided to eligible candidates in due course of time. Watch out for contest and exam notifications and participate to win prizes and certificates."
    static let no_result_found = "No results to display"
    
    static let enhanceTopMsg = "Upgrade to Super Value pack to avail fabulous discounts and benefits"
    
    static let topicListEnrollAlert = "Subscribe to unlock this video and many more exciting topics"//"Subscribe to unlock this video and many more exciting content"
    static let NoContestAlert = "Sorry! No contests are open at the moment. Watch out for upcoming contest notifications."
    static let NoExamAlert = "Sorry! No exams are open at the moment. Watch out for upcoming exam notifications"
    
    static let SuperValuePackUpdateAlert = "You are already a Super Value pack user."
    static let ContestExamDOBAlert = "Please enter your date of birth in the Profile section in order to participate in the contest."
    static let ExamDOBAlert = "Please enter your date of birth in the Profile section in order to participate in the exam."
    static let EligibleContestAlert = "You are not eligible for this contest due to 'Age' limit."
    static let EligibleExamAlert = "You are not eligible for this exam due to 'Age' limit."
    static let paymentStatusAlert = "Your payment is in progress, please wait..."
    
    static let NoFBAlert = "Facebook is not installed "
    static let NoInstagramAlert = "Instagram is not installed "
    static let NoWhatsAppAlert = "WhatsApp is not installed "
    
    //Achivment
    static let NoCertificateGenerated = "Watch the videos and complete each topic to receive certificates."
    //credit point
    static let creditPointTitleText = "Credit Points system is not active right now. It will be made available soon. Watch out for notifications."
    
    static let NoLiveWebSession = "Sorry! No live session at the moment. Watch out for notifications to join live web sessions."
    static let NoUpcommingWebSession = "Watch out for notifications for upcoming web sessions."
    
    static let UpgradePackHeaderText = "Select courses and upgrade packs to learn more and enjoy fabulous discounts and benefits."
    //    static let AlreadyregisteredwithSameCourseAlert = "You are using a registered mobile number. To make this purchase, use a different mobile number or Buy as a Gift."//"You are already a registered user. Kindly log in with your mobile number and password"
    static let AlreadyregisteredwithSameCourseAlert = "You are using a registered mobile number. To make this purchase, use a different number or Buy as a gift or select 'Upgrade' at My Courses section."
    static let AlacartaAlreadySubscribeAlert = "You have already subscribed to this topic."
    static let Success = "Success"
    
    //NFH errro message
    
    static let please_enterId = "Please enter ID"
    static let please_enter_fullName = "Please enter full name"
    static let please_enter_house = "Please enter house"
    static let please_enter_road = "Please enter road"
    static let please_enter_block = "Please enter block"
    static let please_enter_area = "Please enter area"
    static let please_enter_occupation = "Please enter occupation"
    static let please_enter_cpr_no = "Please enter CPR Number"
    static let please_enter_mon_no = "Please enter Mobile Number"
    static let Please_select_Nationality = "Please select Nationality"
    static let please_enter_email_ids = "Please enter email ID"
    static let please_select_dob = "Please select date of birth"
    static let please_enter_aggrement_no = "Please enter agreement number"
    static let please_enter_country_code = "Please select country"
    static let please_enter_password = "Please enter password"
    static let please_enter_sec_qtn = "Please select any one security question"
    static let please_enter_sec_ans = "Please enter security answer"
    static let please_enter_valid_cpr_no = "Please enter valid cpr number"
    static let please_enter_valid_id_no = "Please enter valid id number"
    
    static let terms_condition = "Please accept terms and condition"
    static let password_not_matched = "Password and Confirm password didn't matched"
    
    static let DeleteUploadImageAlert = "Are you sure you want to delete the upload image"
}
