//
//  KJConstant.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJConstant: NSObject {
    //MARK: -ad loader count
    static var iLoaderCount = 0
    static let APP_DEL : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    static let uiScreen: UIScreen = UIScreen.main
    
    //MARK: - Constant Numbers
    static let IOS = "IOS"
    static let numbers = "numbers"
    static let alphaNumerics = "alphaNumerics"
    static let stringWithWhiteSpaces = "stringWithWhiteSpaces"
    static let nameLength = 50
    static let cityLength = 35
    static let addressLength = 50
    static let contactNumberLength = 10
    static let contactNumberLength2 = 15
    static let passwordLength = 20
    static let CPRNumberLength = 25
    static let OTPNumberLength = 6
    static let accountRelatedFieldsLength = 35 // as per email
    static let allEntries = "allEntries"
    static let withRegex = "withRegex"
    static let amtTextLength = 20
    static let minWeightLength = 5
    static let minHeightLength = 3
    static let cycliderInVehicle = 1
    static let vpaLength = 40 // as per email
    static var vpaMinLimit = 1.00
    static var vpaMaxLimit = 100000.00
    static let NoTip = "NoTip"
    static let PincodeLength = 20
    static let LandmarkLength = 50
    static let commentMaxLength = 200
    static let FeedbackCommentMaxLength = 400
    //MARK: - Popup table related constants
    static let bottomHeight:CGFloat = 65
    static let excludeSize:CGFloat = 130
    static let iOTPSecreenTimerLimit = 120
    //MARK: - Popup
    static let vcPop = "Popup"
    //Language
    static let CurrentLanguage = "CurrentLanguage"
    
    //side menu title
    static let DeviceManagement = "Device Management"
    static let View_Petal_Products = "View Petal Products"
    static let Dashboard = "Dashboard"
    static let TreatmentHistory = "Treatment History"
    static let Health_Information = "Health Information"
    static let AlarmSetting = "Alarm Setting"
    static let SkinAnalysis = "Skin Analysis"
    static let Setting = "Setting"
    static let privacyConfidential = "Privacy & Confidential"
    static let Questionnaire = "Questionnaire"
    static let PostQueries = "Post Queries"
    static let Logout = "Logout"
    
    static let deviceOs = "Apple iOS"
    
    static let icon1 = "menu_like"
    static let icon2 = "my-orders"
    static let icon3 = "my-account"
    static let icon4 = "menu_bookappointment"
    static let icon5 = "menu_contactus"
    static let icon6 = "menu_blog"
    static let icon7 = "Testimonials"
    static let icon8 = "menu_aboutus"
    static let icon9 = "menu_currency"
    static let icon10 = "menu_legalpolicies"
    static let icon11 = "CancelMembership"
    static let icon12 = "2logout"
    
    ///userdefaults keys
    static let isBulkCartSelected = "isBulkCartSelected"
    
    //Notifcation name
    static let SortSelected = "SortSelected"
    static let FilterSelected = "FilterSelected"
    static let AddressUpdated = "AddressUpdated"
    
    //Filter Screens
    static let Availability = "Availability"
    //MyOrder Icon
    static let MyOrder_Pending = UIImage.init(named: "myorder_pending")
    static let MyOrder_QuantityApprove = UIImage.init(named: "myorder_qtyapproved")
    static let MyOrder_OrderApprove = UIImage.init(named: "myorder_orderapproved")
    static let MyOrder_PartialPaymentRequested = #imageLiteral(resourceName: "myorder_fullpayment")
    static let MyOrder_PartialPaid = #imageLiteral(resourceName: "myorder_partiallypaid")
    static let MyOrder_FullPaymentRequested = #imageLiteral(resourceName: "myorder_fullpayment")
    static let MyOrder_FullyPaid = #imageLiteral(resourceName: "myorder_fullpayment")
    static let MyOrder_Shipped = #imageLiteral(resourceName: "Shipped")
    static let MyOrder_Cancelled = #imageLiteral(resourceName: "myorder_cancel")
    static let MyOrder_HandledOffline = #imageLiteral(resourceName: "myorder_handledoffline")
    static let MyOrder_BankPayment = #imageLiteral(resourceName: "myorder_fullpayment")
    
    //My Addresses Icon
    static let MyAddressOfficeIcon = "myaddress_office"
    static let MyAddressHomeIcon = "myaddress_home"
    static let MyAddressShopIcon = "myaddress_shop"
    static let MyAccountLocation = "myaccount_myaddress"
   
    //Notification
    static let showSideMenu = "showSideMenu"
    static let donePressed = "donePressed"
    static let notificationPressed = "notificationPressed"
    static let notificationUnread = "notificationUnread"
    
    //Cell Identifier
    static let  menuCellIdentifier = "cellSidemenu"
    static let  categoryCellIdentifier = "clsCategoryCell"
    static let  collectionCellIdentifier = "clsNewCollectionCell"
    static let  cellWatchTutorialIdentifier = "cellwatchtutorial"
    static let  cellMyAddressesIdentifier = "cellmyaddresses"
    //UserDefault Keys
    static let IsFirstTime = "IsFirstTime"
    static let kdeviceSavedTreatment = "deviceSavedTreatment"
    static let kDurationTreatment = "DurationTreatment"
    static let kSoundDuringTreatment = "SoundDuringTreatment"
    static let kSoundInterval = "SoundInterval"
    static let kDeviceId = "DeviceId"
    
    static let kUserDetails = "UserDetails"
    static let kProfileDetails = "ProfileDetails"
    static let kProfileImage = "ProfileImage"
    static let userCurrentCity = "UserCity"
    static let RegUserDetails = "RegUserDetails"
    static let NotificationVC = "NotificationVC"
    static let SubCategoryId = "SubCategoryId"
    static let SubCategoryName = "SubCategoryName"
    static let BookAppointmentVC = "BookAppointmentVC"
    static let kCurrencyDetails = "CurrencyDetails"
    static let isSelectedCurrency = "isSelectedCurrency"
    static let isSelectedBlogFilter = "isSelectedBlogFilter"
    static let kBlogFilterDeatils = "BlogFilterDeatils"
    static let kLatitude = "kLatitude"
    static let kLongitude = "kLongitude"
    static let kStarRating = "isRated"
    static let Token = "token"
    static let CPRNo = "CPRNO"
    
    //Notifcation name
    static let SelectedDevice = "PETAL.USER.SELECTEDDEVICE"
    
    //String
    static let Ok = "Ok"
    static let Done = "Done"
    static let Cancel = "Cancel"
    static let Confirm = "Confirm"
    static let Submit = "Submit"
    static let Select = "Select"
    static let success = "success"
    static let Delete = "Delete"
    static let DeleteAddress = "Delete Address"
    static let AppIOS = "APP-IOS"
    static let DeleteWishlist = "Delete Wishlist"
    static let ClearAllWislist = "Clear All"
    static let Alert = "Alert"
    //MARK:- policy api name
    static let privacy_policy = "privacy-policy"
    static let Terms_Conditions = "terms-and-conditions"

    static let Feedback = "Feedback"
    static let Complaints = "Complaints"
    static let Suggestions = "Suggestions"
    
    static let ForgotPassword = "Forgot Password"
    static let ResetPassword = "Reset Password"
    static let EnterOTP = "Enter OTP"
    static let WatchTutorial = "Watch Tutorial"
    static let FAQ = "FAQ"
    static let MyAddresses = "My Addresses"
    static let AddAddress = "Add Address"
    static let EditAddress = "Edit Address"
    static let Wishlist = "Wishlist"
    static let AboutUs = "About Us"
    static let ChooseCurrency = "Choose Currency"
    
    //AESDES key
    static let AesKey = "abSh756rnsgd75kABpq132xbgnkSH609"
    static let AesIV = "IYWNMQBAWHBZCFQS"
    static let MyAccountUserNotLogIn = "Login to view my account"
    static let MyOrderUserNotLogIn = "Please login to view my order"
    static let GSTNLimitWarning = "Maximum 15 characters are allow in the GSTN No."
    static let ProfileUpdateSuccessMessage = "Profile updated successfully"
    static let PasswordUpdateSuccessMessage = "Password updated successfully"
    static let OldPasswordEmptyWarning = "Old password cannot be blank"
    static let NewPasswordEmptyWarning = "New password cannot be blank"
    static let ConfirmPasswordEmptyWarning = "Confirm password cannot be blank"
    static let InvalidPasswordUpdateWarning = "Minimum 6 and maximum 20 characters are allowed in the password."
    static let MismatchPasswordWarning = "Password does not match"
    static let InsertUpdateAddressSuccessMessage = "Address added successfully"
    static let InvalidConfirmPasswordWarning = "Minimum 6 and maximum 20 characters are allowed in the password."
    static let PasswordConfirmNotMatch = "Password does not match"
    static let WishListUserNotLogInMessage = "Login to view wish list"
    static let UpdatedAddressSuccessMessage = "Address updated successfully"
    static let MyOrderNotLogIn = "Login to view cart"
    
    //Alert message
    static let NoInternetConnection = "No internet connection"
    static let NoAppConfiguredAlert = "No app installed to handle this action. To use this feature, please check if you have respective app installed or email services are enabled. Thank you."
    static let FeedbackSuccessMessage = "Thank you for your valuable Feedback."
    static let FeedbackUserNotLogIn = "Please login to add Feedback"

    static let FormSubmissionMsg = "Form submitted successfully."
    
    static let ValidMobileNoMessage = "Mobile number cannot be blank"
    static let ValidOTPMessage = "Please enter valid 4 digit OTP."
    static let InvalidValidMobileNoMessage = "Invalid mobile number"
    static let InvalidValidAlternateMobileNoMessage = "Invalid alternate mobile number"

    //Registration alert
    static let PasswordEmptyWarning = "Password cannot be blank"
    static let InvalidPasswordWarning = "Minimum 6 and maximum 20 characters are allowed in the password."
    static let InvalidPasswordWithoutSpecialCharacter = "Password must contain minimum 6 character including one number and one special character"
    static let InvalidNameWarning = "Name can only contain alphabets, apostrophe and space characters"
    static let NameLimitWarning = "Maximum 20 characters are allow in the your name."
    static let CityWarning = "Alpha numeric character with min 2 and max 35 character."

    static let CompanyNameLimitWarning = "Alpha numeric character with min 2 and max 50 character"

    static let FirstNameEmptyWarning = "First Name cannot be blank."
    static let LastNameEmptyWarning = "Last Name cannot be blank."

    static let EmailEmptyWarning = "Email ID is blank or incorrect."
    static let InvalidEmailWarning = "Please enter a valid email address."
    static let EmailEmptyWarningError = "Please enter an email address"
    static let UserAgreementWarningError = "Please verify above that you are a certified medical professional."
    static let TNCWarningError = "Please agree to the terms and conditions above."
    static let EnterDOBWarning = "Date of Birth cannot be blank."
    static let ValidDetailsWarning = "Please enter Valid details"
    static let selectAddressWarning =  "Please enter Address."
    static let selectCountryWarning =  "Please select country"
    static let mobileNumberWarning =  "Mobile number should be between 6 to 15 digits"
    static let SelectGenderWarning = "Please select a gender"
    static let selectCityWarning = "City cannot be blank"
    static let InvalidCityWarning = "Only alphabetical characters are allowed"
    static let selectCountryCodeWarning = "Select country code"
    static let SelectStateWarning = "Please select valid State."
    static let SelectCityWarning = "Please select valid City."
    static let BusinessNameLimitWarning = "Minimimum 2 and maximum 50 characters are allow in the business name."
    static let FullNameLimitWarning = "Minimimum 2 and maximum 50 characters are allow in the name."
    static let EmptyAddressWarning = "Address cannot be blank"
    static let EmptyPincodeWarning = "Pincode cannot be blank"
    static let CityLimitWarning = "Minimimum 2 and maximum 50 characters are allow in the city."
    static let AddressLimitWarning = "Minimimum 2 and maximum 200 characters are allow in the address."
    static let PincodeLimitWarning = "Minimimum 5 and maximum 20 characters are allow in the pincode."
    static let LandmarkLimitWarning = "Minimimum 2 and maximum 50 characters are allow in the landmark."
    static let EmptyAppointmentDateWarning = "Please select booking date"
    static let EmptyCommentWarning = "Comment cannot be blank"
    static let CommentLimitWarning = "Minimimum 2 and maximum 200 characters are allow in the comment."
    static let ContactUsCommentLimitWarning = "Minimimum 2 and maximum 400 characters are allow in the comment."
    static let RaisedTicketCommentLimitWarning = "Minimimum 2 and maximum 1000 characters are allow in the comment."

    static let ContactUsSendSuccessMsg = "Appointment booked successfully.Our representative will connect with you shortly."
    static let EmptyReasonForContactWarning = "Please select contact reason"
    static let CurrencyChangeMessage = "Currency changed successfully"
    static let CurrencyUserNotLogInMessage = "Please login to change currency"
    static let AddressDeletedSuccessMessage = "Address deleted successfully"
    static let WishlistDeletedSuccessMessage = "Wishlist item deleted successfully"
    static let OldPassswordNewPasswordAlertMessage = "Old password and new password should not be same"
    
    static let GenderMessage = "Choose any one of the option"
    static let ValidMessage = "Please Enter Valid Message"
    static let Select_Country = "Country"
    static let Select_City = "Select City"
    static let DeleteUploadedImage = "Delete Image"
    
}
