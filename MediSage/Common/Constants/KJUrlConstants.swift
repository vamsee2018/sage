//
//  KJUrlConstants.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJUrlConstants: NSObject {
  
    //MARK: - Development Url
    static let CommonUrl = "https://dev2.mymedisage.com/mediapi/" //static content Base url Plain
    static let ImageUrl = "https://dev2.mymedisage.com/" //static content Base url Plain//

    //MARK: - Live Url
//    static let CommonUrl = "https://mymedisage.com/mediapi/" //Production content Base url Plain
//    static let ImageUrl = "https://mymedisage.com/" //Production content Base url Plain
    
    //MARK:- Production Authorization
    static let Username = "medimob"
//    static let ProPassword = "medisageApp123"
    static let ProPassword = "boom_boom"

    static let DevPassword = "boom_boom"
    static let applicationId = "1497265614"

    //MARK:- API's For Login n Register
    static let CountryList = "country_data"
    static let NumberVerify = "user/number_verify"
    static let Register = "user/register"
    static let PasswordVerify = "user/password_verify"
    static let SetNewPassword = "set_new_password"
    static let ForgotPassword = "send_forgot_password_OTP"
    static let ValidateOTP = "user/otp_verify"
    static let Login = "ValidateUserLogin"
    static let StateList = "state"
    static let CityList = "cities"
    static let AffiliationList = "affiliations"
    static let GetYears = "years"
    static let QualificationsList = "qualifications"
    static let SpecialisationList = "specialities"
    static let PracticeTypeList = "qualifications"
    static let ClinicalChallengesList = "qualifications"
    
    //MARK:- API's For Home
    static let BannerList = "slider_images"
    static let TrendingList = "trending"
    static let NewReleasesList = "new"
    static let ExpertsList = "experts"
    static let JournalList = "newsletters"
    static let Video = "video/"
    static let CheckHomePageFlag = "home_page_flags/"


    //MARK:- API's For MY Partners
    static let PartnerList = "partners"
    static let PartnerDetails = "partner_details/"
    static let PartnerAreaDetails = "partner_division_detail/"

    //MARK:- API's For Playing Videos
    static let VideoDetails = "get_video_details/"

    //MARK:- API's For MY Experts
    static let MYExpertsList = "myexpert_list/"
    static let MYExpertDetails = "expert_detail/"

    //MARK:- API's For MY Community
    static let MYCommunityList = "communities/"
    static let CommunityList = "communities"
    static let UpdateMyCommunity = "set_member_communities/"
    static let CommunityDetails = "community/"


    //MARK:- API's For MY Events
    static let MyEvents = "live_events_list/"
    static let RegisterEvent = "register_live_event"
    static let LiveEventDetails = "live_event_detail/"
    
    //MARK:- API's For Profile
    static let MyProfile = "member_detail/"
    static let MyCertificateList = "live_events/certificates/"

    //MARK:- API's For Edit Profile
    static let EditBasicInfo = "user/update/basic_detail"
    static let EditProfessionalInfo = "user/update/additional_detail"

    //MARK:- API's For Search
    static let Search = "search"
    static let SearchSuggestions = "https://analytics-dev.mymedisage.com/api/v1/keywords"

    //MARK:- API's For Series
    static let SeriesDetails = "series_detail/series/"
    
    //MARK:- API's For TNC & Privacy Policy
    static let TermsConditions = "tnc"
    static let PrivacyPolicy = "privacy"
    static let AboutUS = "about_us"

    //MARK:- API's For News
    static let News = "news_articles/"
    static let NewsDetails = "news_article/"
    static let NewsFeedback = "news_article_feedback"
    //MARK:- API's For Like and Insightful videos
    static let VideoFeedback = "feedback"
    
    //MARK:- API's For Video Playback
    static let VideoPlayback = "video_playback"
    
    //MARK:- API's For Notifications List
    static let NotificationList = "notifications_list/"
    static let NotificationFeedback = "notification_feedback"

    //MARK:- API's For Cases
    static let CasesList = "cases_list/"
    static let CaseDetail = "cases/"
    static let CaseFeedback = "case_feedback"

}
