//
//  KJRegX.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJRegX: NSObject {
    //Below regex is for email address
    static let emailAddress = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    
    //Below regex is for OTP code
    static let OTP = "\\d{6}"
    
    //Below regex is for mobile number
    static let contactNo = "\\d{10}"
    
    static let AlphaNumericWithSpecialCharacter =  ".*[^A-Za-z0-9 ].*" //its allow to enter characters, numbers, or space(" ")
    static let CharacterOnly = ".*[^A-Za-z].*" //its allow to enter only characters

    static let OnlyCharactersWithSpace =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz " //its allow to enter characters, or space(" ")
     static let OnlyCharacters =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" //its allow to enter characters, or space(" ")
    static let Password = "[a-zA-Z0-9!@#$%^&*]+"
    static let strWightDecimal = "^\\d{1,3}(?:\\.\\d{1,2})?$"
}
