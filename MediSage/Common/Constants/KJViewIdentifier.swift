//
//  KJViewIdentifier.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJViewIdentifier: NSObject {
    //MARK: - Pre Login Storyboard names
    static let Main = "Main"
    static let SideMenu = "SideMenu"
    static let Home = "Home"
    static let Profile = "Profile"
    static let Search = "Search"
    static let Partner = "Partner"

    static let LogOut = "Logout"

    static let RMWebViewController = "RMWebViewController"
    
    //MARK: - Pre Login ViewController identifiers
    static let IntroScreenViewController = "IntroScreenViewController"
    static let SplashScreenViewController = "SplashScreenViewController"
    static let LoginViewController = "LoginViewController"
    static let InitialViewController = "InitialViewController"
    static let WebViewController = "WebViewController"
    static let CreatePasswordViewController = "CreatePasswordViewController"
    static let OTPViewController = "OTPViewController"
    static let UserDetailsViewController = "UserDetailsViewController"
    static let UserInterestViewController = "UserInterestViewController"
    static let CongratulationsViewController = "CongratulationsViewController"

    //MARK:- Side Menu
    static let SideMenuViewController = "SideMenuViewController"
    static let RootViewController = "RootViewController"
    static let sessionExpireViewController = "sessionExpireViewController"
    static let PrivacyPolicyViewController = "PrivacyPolicyViewController"
    static let TermsConditionsViewController = "TermsCpnditionsViewController"
    static let AboutUsViewController = "AboutUsViewController"

    //MARK:- Post Login
    static let HomeViewController = "NewHomeViewController"
    static let CasesViewController = "CasesViewController"
    static let CasesDetailViewController = "CasesDetailViewController"
    static let AnswerDetailsVC = "AnswerDetailsVC"
    static let MyCommunityViewController = "MyCommunityViewController"
    static let CommunityVideosViewController = "CommunityVideosViewController"
    static let ViewAllCommunityViewController = "ViewAllCommunityViewController"
    static let PDFViewController = "PDFViewController"
    static let MyEventsViewController = "MyEventsViewController"
    static let MyEventPreviewViewController = "MyEventPreviewViewController"
    static let NewsDetailsViewController = "NewsDetailsViewController"
    static let NewsViewController = "NewsViewController"
    static let MyExpertsViewController = "MyExpertsViewController"
    static let ExpertDetailsViewController = "ExpertDetailsViewController"
    static let SeriesViewController = "SeriesViewController"
    static let ExpertViewAllVC = "ExpertViewAllVC"
    static let NotificationsViewController = "NotificationsViewController"
    static let ProfileViewController = "ProfileViewController"
    static let SearchViewController = "SearchViewController"
    static let PartnerViewController = "PartnerViewController"
    static let PartnerDetailsViewController = "PartnerDetailsViewController"
    static let PartnerAreasViewController = "PartnerAreasViewController"
    static let VideoPlayingViewController = "VideoPlayingViewController"
    
    static let ProfileEditViewController = "ProfileEditViewController"
    static let MyCommunityEditViewController = "MyCommunityEditViewController"
    static let ViewAllViewController = "ViewAllViewController"
    static let MyCommunityViewAllVC = "MyCommunityViewAllVC"
    static let ProfessionalInfoEditViewController = "ProfessionalInfoEditViewController"
    static let ZoomImageViewController = "ZoomImageViewController"
    static let CertificatesViewController = "CertificatesViewController"
    static let PdfCertificateViewController = "PdfCertificateViewController"
    
    //MARK: - XIB identifier
    static let DescriptionHeader = "DescriptionHeader"
    static let CustomTopView = "CustomTopView"
    static let CustomBottomView = "CustomBottomView"
    static let sideMenuHeader = "sideMenuHeader"
    static let CustomTopViewBack = "CustomTopViewBack"
    static let CustomTopViewSearchBar = "CustomTopViewSearchBar"
    static let CustomTopViewWishlist = "CustomTopViewWishlist"
}
