//
//  KJThemeConstant.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJThemeConstant: NSObject {
    //MARK: - Predefined colours
    static let defaltTheamColor = UIColor.init(red:22/255.0, green:104/255.0, blue:176/255.0, alpha:1.0) //UIColor.init(red: 58/255.0, green: 90/255.0, blue: 172/255.0, alpha: 1.0)
    static let cellShadowColor = UIColor.init(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0)
    static let defaltThemeBlueColor = UIColor.init(red: 34/255.0, green: 49/255.0, blue: 127/255.0, alpha: 1.0)
    static let whiteColor = UIColor.white
    static let clearColor = UIColor.clear
    static let blackColor = UIColor.black
    static let grayColor = UIColor.gray
    static let greenColor = UIColor.green
    static let redColor = UIColor.red
    static let lighGrayColor = UIColor.lightGray
    static let blueColor = UIColor.blue
    static let defaultGrayBorderColor = UIColor.lightGray.withAlphaComponent(0.3)
    static let defaultOrangeColor = UIColor.init(hex: "ff6c00")
    
    static let messageLblColor = "3C80FF"
    
    //button color
    static let graphGreenColor = UIColor.init(red: 0/255.0, green: 255/255.0, blue: 132/255.0, alpha: 1.0)
    static let ButtonCommonColorLight = UIColor.init(red: 0/255.0, green: 248/255.0, blue: 139/255.0, alpha: 1.0)
    //    static let ButtonCommonColorMediumLight = UIColor.init(red: 0/255.0, green: 208/255.0, blue: 137/255.0, alpha: 0.5)
    //    static let ButtonCommonColorMedium = UIColor.init(red: 0/255.0, green: 2019/255.0, blue: 186/255.0, alpha: 0.5)
    //    static let ButtonCommonColorDarkMedium = UIColor.init(red: 0/255.0, green: 172/255.0, blue: 147/255.0, alpha: 0.5)
    static let ButtonCommonColorDark = UIColor.init(red: 0/255.0, green: 210/255.0, blue: 201/255.0, alpha: 1.0)
    
    //MARK: - Predefined button radius
    static let buttonRadius = CGFloat(4.0)
    static let popupRadius = CGFloat(10.0)
    
    //MARK: - Blur values
    static let blurBlackAlpha:CGFloat = 0.5
    
    //MARK: - PLIST constant for fontSize
    static let MuktaVaani_Regular = "MuktaVaani-Regular"
    static let MuktaVaani_Light = "MuktaVaani-Regular"
    
    static let Roboto_Bold = "Roboto-Bold"
    static let Roboto_Regular = "Roboto-Regular"
    static let Aleo_Bold = "Aleo-Bold"
    static let Aleo_Regular = "Aleo-Regular"
}
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
