//
//  KJValidation.swift
//  KanhaiJewels
//
//  Created by Harshad Wagh on 13/05/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class KJValidation: NSObject {
    /*
     
     Method : isValid
     Method will check if string is valid or not
     Inputs : String to be validated and RegeX assosicated with it.
     */
    
    class func isValid(strInput: String,regex:String) -> Bool {
        
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with:strInput)
    }
    
    /*
     
     Method : isBlank
     // function is for checking if the textfield is blank
     Inputs : textfield's text
     */
    
    
    
    class func isBlank(txtField : UITextField) -> Bool
    {
        if (txtField.text?.lengthOfBytes(using: .utf8))! > 0
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    /*
     
     Method : isMatching
     // to check if the two textfields have same string
     Inputs : to strings
     */
    
    
    class func isMatching(firstTxtField : UITextField , secondTxtField : UITextField) -> Bool
    {
        
        if (firstTxtField.text == secondTxtField.text) {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK: - Custom function
    /*
     
     Method Name : textfieldValidation
     
     This method is used for setting the data of cell.
     
     */
    class func textfieldEntryValidation(getTextField:UITextField, passedString: String, validationType:String, regex:String = KJRegX.Password) -> Bool{
        
        //This type will allow only numbers entry in the textfield.
        if validationType == KJConstant.numbers{
            let characterSet  = CharacterSet.decimalDigits.inverted
            let inputString = passedString.components(separatedBy: characterSet)
            let filtered = inputString.joined()
            return  passedString == filtered
        }
        
        //This type will allow only numbers and characters entry in the textfield.
        if validationType == KJConstant.alphaNumerics{
            let characterSet  = CharacterSet.alphanumerics.inverted
            let inputString = passedString.components(separatedBy: characterSet)
            let filtered = inputString.joined()
            return  passedString == filtered && passedString.count <= KJConstant.accountRelatedFieldsLength
        }
        
        //This type will allow only string entry in the textfield with or wwithout whitespaces.
        if validationType == KJConstant.stringWithWhiteSpaces{
            let characterSet  = CharacterSet.init(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
            let inputString = passedString.components(separatedBy: characterSet)
            let filtered = inputString.joined()
            return  passedString == filtered
        }
        
        //This type will allow all numbers, characters and special symbol entries in the textfield.
        if validationType == KJConstant.allEntries{
            let characterSet  = CharacterSet.init(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789~`!@#$%^&*()_+={[}]|:;\"'<,>?/-*").inverted
            let inputString = passedString.components(separatedBy: characterSet)
            let filtered = inputString.joined()
            return  passedString == filtered
        }
        //This type will allow with regex entry in the textfield.
        if validationType == KJConstant.withRegex{
            let characterSet  = CharacterSet.init(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 '}{:,)(\r\n\\s+.‘’?]*").inverted
            let inputString = passedString.components(separatedBy: characterSet)
            let filtered = inputString.joined()
            return  passedString == filtered
        }
        
        return false
    }
    
    class func textViewEntryValidation(getTextView:UITextView, passedString: String, validationType:String, regex:String = KJRegX.Password) -> Bool{
     
        //This type will allow only string entry in the textfield with or wwithout whitespaces.
        if validationType == KJConstant.stringWithWhiteSpaces{
            let characterSet  = CharacterSet.init(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
            let inputString = passedString.components(separatedBy: characterSet)
            let filtered = inputString.joined()
            return  passedString == filtered
        }
        return false
    }
}
