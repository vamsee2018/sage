//
//  CustomTopViewSearchBar.swift
//  KanhaiJewels
//
//  Created by gaus ansari on 6/6/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import Foundation
import UIKit

/// class to show top view on each viewcontroller
class CustomTopViewSearchBar: UIView {
    
    //MARK:- Outlets
    @IBOutlet var viwContent: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    //MARK:- View life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    //MARK:- User defined functions
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    func commonInit() {
        // Drawing code
        Bundle.main.loadNibNamed(KJViewIdentifier.CustomTopViewSearchBar, owner: self, options: nil)
        addSubview(viwContent)
        viwContent.frame = self.bounds
    }
}

