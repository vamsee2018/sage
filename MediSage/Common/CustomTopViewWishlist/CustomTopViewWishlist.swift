//
//  CustomTopViewWishlist.swift
//  KanhaiJewels
//
//  Created by Vinayak Bhor on 09/06/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class CustomTopViewWishlist: UIView {

      //MARK:- Outlets
     @IBOutlet var viwContent: UIView!
     @IBOutlet weak var btnBack: UIButton!
     @IBOutlet weak var imgLogo: UIImageView!
     @IBOutlet weak var imgMenu: UIImageView!
     @IBOutlet weak var lblTitle: UILabel!
     @IBOutlet weak var lblSubTitle: UILabel!
     @IBOutlet weak var btnClear: UIButton!
    //MARK:- View life cycle
     
     override init(frame: CGRect) {
         super.init(frame: frame)
         self.commonInit()
     }
     
     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         self.commonInit()
     }
     
     //MARK:- User defined functions
     
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     func commonInit() {
         // Drawing code
         Bundle.main.loadNibNamed(KJViewIdentifier.CustomTopViewWishlist, owner: self, options: nil)
         addSubview(viwContent)
         viwContent.frame = self.bounds
     }
     
     

}
