//
//  AppDelegate.swift
//  MediSage
//
//  Created by Harshad Wagh on 22/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import FirebaseCore
import FirebaseMessaging
import UserNotifications
import Messages
import AVKit
import AVFoundation
import FirebaseDynamicLinks
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate {

    var window: UIWindow?
    var initialViewController :UIViewController?
    var rootViewController = UINavigationController()
    let gcmMessageIDKey = "gcm.message_id"
    var userData = [String:Any]()
    var strUserID  = String()
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
   
        UNUserNotificationCenter.current().delegate = self

        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
        }
        if launchOptions != nil{
            let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            if userInfo != nil {
                NotificationCenter.default.post(name: .checkNotification, object: nil,userInfo: userInfo as? [AnyHashable : Any])
            }
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
      
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        }
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().token { (token, error) in
          if let error = error {
            print("Error fetching FCM token: \(error)")
          } else if let token = token {
            print("FCM token: \(token)")
          }
        }
        return true
    }
//    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//
//    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        } catch {
            print("AVAudioSessionCategoryPlayback not work")
        }
    }
    
   
    //MARK: - Firebase delegate method
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Messaging.messaging().apnsToken = deviceToken as Data
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("deviceTokenString: \(deviceTokenString)")
        //set apns token in messaging
        Messaging.messaging().apnsToken = deviceToken
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // Print full message.
        print(userInfo)
        let alert = UIAlertController(title: "Test111", message:"Message", preferredStyle: UIAlertController.Style.alert)
                
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
               
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)

    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Print full message.
        print(userInfo)

        let alert = UIAlertController(title: "Test2222", message:"Message", preferredStyle: UIAlertController.Style.alert)
                
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
               
        // show the alert
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
    //MARK: - dynamic links
    // Reports app open from a Universal Link for iOS 9 or later
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        if let incomingUrl = userActivity.webpageURL
        {
            print(incomingUrl)
            
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl)
            { (dynamiclink, error) in
                
                guard error == nil else
                {
                    print("Found an Error \(error?.localizedDescription ?? "")")
                    return
                }
                if let dynamicLink = dynamiclink
                {
                    self.handleIncomingDynamicLink(dynamicLink)
                    //print(dynamicLink)
                }
                // ...
                print("handled")
            }
            if linkHandled
            {
                return true
            }
            else
            {
                return false
            }
           
        }
        return false
    }
   
    func handleIncomingDynamicLink(_ dynamicLink : DynamicLink){
        //        bIsDeeplinkyNavigate = true
        
        guard let url = dynamicLink.url else
        {
            print("Thats weird,your dynamic link object has no url")
            return
        }
        var strUrl = url.absoluteString
        strUrl = strUrl.replacingOccurrences(of: "%3A" , with: ":")
        //print("Your Incoming Url is : \(strUrl)")
        
        if strUrl.contains("news"){
            self.parseDeepLinking(strUrl: strUrl)
        } else if strUrl.contains("video"){
             self.parseDeepLinking(strUrl: strUrl)
        }else if strUrl.contains("series"){
            self.parseDeepLinking(strUrl: strUrl)
        }
        
    }
    
    
    func parseDeepLinking(strUrl:String){
        
     //   var arrString = [String]()
        var dictionaryParam = [String:Any]()
        if strUrl.contains("video"){
            let arrString = strUrl.components(separatedBy: "/")
            dictionaryParam["videoID"] = arrString.count > 1 ? arrString[4] : ""
            dictionaryParam["video"] = "video"
        }else if strUrl.contains("series"){
            let arrString = strUrl.components(separatedBy: "/")
            dictionaryParam["seriesID"] = arrString.count > 1 ? arrString[4] : ""
            dictionaryParam["series"] = "series"
        }else if strUrl.contains("news"){
            let finalURL = strUrl.split(separator: "/",maxSplits: 4)
            print(finalURL)
            dictionaryParam["articleLink"] = finalURL.count > 1 ? finalURL[4] : ""
            dictionaryParam["articleID"] = finalURL.count > 1 ? finalURL[3] : ""

            dictionaryParam["news"] = "news_article"
        }
        
        
        NotificationCenter.default.post(name: .dynamicLink, object: nil,userInfo: dictionaryParam)
    
    }

     func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print("I have received a URL through a custom scheme")
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        }
        return false
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
         let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
         if dynamicLink != nil {
              print("Dynamic link : \(String(describing: dynamicLink?.url))")
              return true
         }
         return false
    }
    
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension AppDelegate:UNUserNotificationCenterDelegate {
    //This method is to handle a notification that arrived while the app was running in the foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)

        // Change this to your preferred presentation option
        completionHandler([.alert])
    }
    
    //This method is to handle a notification that arrived while the app was not in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
      let userInfo = response.notification.request.content.userInfo
      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }
         
        NotificationCenter.default.post(name: .checkNotification, object: nil,userInfo: userInfo)
        
      // Print full message.
      print(userInfo)
      completionHandler()
    }
}


extension DateFormatter {

    convenience init (format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}

extension String {

    func toDate (dateFormatter: DateFormatter) -> Date? {
        return dateFormatter.date(from: self)
    }

    func toDateString (dateFormatter: DateFormatter, outputFormat: String) -> String? {
        guard let date = toDate(dateFormatter: dateFormatter) else { return nil }
        return DateFormatter(format: outputFormat).string(from: date)
    }
}

extension Date {

    func toString (dateFormatter: DateFormatter) -> String? {
        return dateFormatter.string(from: self)
    }
}

extension URL {

    func appending(_ queryItem: String, value: String?) -> URL {

        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []

        // Create query item
        let queryItem = URLQueryItem(name: queryItem, value: value)

        // Append the new query item in the existing query items array
        queryItems.append(queryItem)

        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems

        // Returns the url from new url components
        return urlComponents.url!
    }
}
