//
//  SceneDelegate.swift
//  MediSage
//
//  Created by Harshad Wagh on 22/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
      
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            // This is UNNotificationResponse
            if let notificationResponse = connectionOptions.notificationResponse {
                window.makeKeyAndVisible()
                let alert = UIAlertController(title: "Test333", message:"Message", preferredStyle: UIAlertController.Style.alert)
                        
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                       
                // show the alert
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                return
            }
        }
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if let url = URLContexts.first?.url{
            print(url)

        }
    }

    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        if let incomingUrl = userActivity.webpageURL
        {
            print(incomingUrl)
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl)
            { (dynamiclink, error) in
                
                guard error == nil else
                {
                    print("Found an Error \(error?.localizedDescription ?? "")")
                    return
                }
                if let dynamicLink = dynamiclink
                {
                    self.handleIncomingDynamicLink(dynamicLink)
                    //print(dynamicLink)
                }
                // ...
                print("handled")
            }
            
        }
    }
    func handleIncomingDynamicLink(_ dynamicLink : DynamicLink){
        //        bIsDeeplinkyNavigate = true
        
        guard let url = dynamicLink.url else
        {
            print("Thats weird,your dynamic link object has no url")
            return
        }
        var strUrl = url.absoluteString
        strUrl = strUrl.replacingOccurrences(of: "%3A" , with: ":")
        print("Your Incoming Url is : \(strUrl)")
        
        if strUrl.contains("news"){
            self.parseDeepLinking(strUrl: strUrl)
        } else if strUrl.contains("video"){
            self.parseDeepLinking(strUrl: strUrl)
        }else if strUrl.contains("series"){
            self.parseDeepLinking(strUrl: strUrl)
        }else if strUrl.contains("live_event"){
            self.parseDeepLinking(strUrl: strUrl)
        }
        
    }
    
    
    func parseDeepLinking(strUrl:String){
        
        //   var arrString = [String]()
        var dictionaryParam = [String:Any]()
        if strUrl.contains("video"){
            let arrString = strUrl.components(separatedBy: "/")
            dictionaryParam["videoID"] = arrString.count > 1 ? arrString[4] : ""
            dictionaryParam["video"] = "video"
        }else if strUrl.contains("series"){
            let arrString = strUrl.components(separatedBy: "/")
            dictionaryParam["seriesID"] = arrString.count > 1 ? arrString[4] : ""
            dictionaryParam["series"] = "series"
        }else if strUrl.contains("news"){
            let finalURL = strUrl.split(separator: "/",maxSplits: 4)
            print(finalURL)

            dictionaryParam["articleLink"] = finalURL.count > 1 ? finalURL[4] : ""
            dictionaryParam["articleID"] = finalURL.count > 1 ? finalURL[3] : ""
            dictionaryParam["news"] = "news_article"
        }else if strUrl.contains("live_event"){
            let arrString = strUrl.components(separatedBy: "/")
            dictionaryParam["eventID"] = arrString.count > 1 ? arrString[4] : ""
            dictionaryParam["live_event"] = "live_event"
        }
        
        
        NotificationCenter.default.post(name: .dynamicLink, object: nil,userInfo: dictionaryParam)
        
       
    }
}

