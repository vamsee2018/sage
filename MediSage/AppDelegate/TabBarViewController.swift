//
//  TabBarViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 09/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import SDWebImage

protocol CustomTabBarControllerDelegate {
    func onTabSelected(isTheSame: Bool)
}

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.tintColor = UIColor.init(hex:"001D4D") //UIColor.white
        self.tabBar.unselectedItemTintColor = UIColor.white
        
//        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "ic_Events40X40", withExtension: "gif")!)
//        let advTimeGif = UIImage.sd_image(withGIFData: imageData)
        
        
      //  self.tabBar.items?[2].image = advTimeGif//UIImage(named: "Webinar 5")
     
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == (self.tabBar.items!)[0]{
           //Do something if index is 0
            print("SELECTED INDEX",item)
            NotificationCenter.default.post(name: Notification.Name("moveToHome"), object: nil)

            NotificationCenter.default.post(name: Notification.Name("isPlayingVideo"), object: nil)
        }
        else if item == (self.tabBar.items!)[1]{
           //Do something if index is 1
            print("SELECTED INDEX",item)
            NotificationCenter.default.post(name: Notification.Name("isPlayingVideo"), object: nil)
        }
        else if item == (self.tabBar.items!)[2]{
           //Do something if index is 1
            print("SELECTED INDEX",item)
            NotificationCenter.default.post(name: Notification.Name("isPlayingVideo"), object: nil)
        }
        else if item == (self.tabBar.items!)[3]{
           //Do something if index is 1
            print("SELECTED INDEX",item)
            NotificationCenter.default.post(name: Notification.Name("isPlayingVideo"), object: nil)
        }
        else if item == (self.tabBar.items!)[4]{
           //Do something if index is 1
            print("SELECTED INDEX",item)
            NotificationCenter.default.post(name: Notification.Name("isPlayingVideo"), object: nil)
           // self.tabBar.items?[4].image = UIImage(named: "user(3)")

        }
    }
    
}

extension UIImage {

   class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
    let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
   }
}




/*
find /Users/medisage/Library/Developer/Xcode/DerivedData/MediSage-fdkitvkikkdarvfeazamouyyrqxa/Build/Products/Debug-iphonesimulator/MediSage.app.dSYM/Contents/Resources/DWARF/MediSage -name "*.dSYM" | xargs -I \{\} $/Users/medisage/Documents/Harshad/medisage_new_ios/Pods/FirebaseCrashlytics/FirebaseCrashlytics/upload-symbols -gsp /Users/medisage/Documents/Harshad/medisage_new_ios/MediSage/ImageAssets/GoogleService-Info.plist -p platform \{\}


/Users/medisage/Documents/Harshad/medisage_new_ios/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/medisage/Documents/Harshad/medisage_new_ios/MediSage/ImageAssets/GoogleService-Info.plist -p ios /Users/medisage/Library/Developer/Xcode/DerivedData/MediSage-fdkitvkikkdarvfeazamouyyrqxa/Build/Products/Debug-iphonesimulator/MediSage.app.dSYM/Contents/Resources/DWARF/MediSage
*/
