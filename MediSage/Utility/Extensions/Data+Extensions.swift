//
//  Data+Extensions.swift
//  MediSage
//
//  Created by VAMC on 19/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

extension Data {
    public func decode<T: Decodable>() throws -> T {
        return try JSONDecoder().decode(T.self, from: self)
    }

    public func parseJSON() -> Any? {
        do {
            let responseBody = try JSONSerialization.jsonObject(with: self, options: .allowFragments)
            return responseBody
        } catch let error {
            return error
        }
    }
}

extension Dictionary {
    var jsonData: Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return jsonData
        } catch {
            return Data()
        }
    }

    var json: String {
        let invalidJson = "Not a valid JSON"
        return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
    }

    var validJson: Bool {
        return (String(bytes: jsonData, encoding: String.Encoding.utf8) != nil) ? true : false
    }

    func printJson() {

    }
}
