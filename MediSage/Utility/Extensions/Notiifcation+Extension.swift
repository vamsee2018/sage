//
//  Notiifcation+Extension.swift
//  MediSage
//
//  Created by Atul Prakash on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let newsArticleCount = Notification.Name(rawValue: "NewsArticleCount")
    static let dynamicLink = Notification.Name(rawValue: "DynamicLink")
    static let checkNotification = Notification.Name(rawValue: "CheckNotification")
    static let profileEdited = Notification.Name(rawValue: "EditProfile")
}

