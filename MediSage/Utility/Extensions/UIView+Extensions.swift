//
//  UIView+Extensions.swift
//  MediSage
//
//  Created by Atul Prakash on 09/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    static var className: String {
        return String(describing: self)
    }
}

extension UIView {
     func roundedBottom(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomRight , .bottomLeft],
                                     cornerRadii: CGSize(width: 5  , height: 5))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

extension UIView {
    func setCardView(){
        layer.cornerRadius = 5
        layer.borderColor  =  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.51)
        layer.borderWidth = 1.0
        layer.shadowOpacity = 0.5
        layer.shadowColor =  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5)
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width:2, height: 2)
        layer.masksToBounds = true
    }
}

extension UIView {
func addBottomShadow() {
    layer.masksToBounds = false
    layer.shadowRadius = 1.5
    layer.shadowOpacity = 1
    layer.shadowColor = UIColor.lightText.cgColor
    layer.shadowOffset = CGSize(width: 0 , height: 2)
    layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                 y: bounds.maxY - layer.shadowRadius,
                                                 width: bounds.width,
                                                 height: layer.shadowRadius)).cgPath
}
}

