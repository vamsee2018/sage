//
//  NewHomeViewController.swift
//  MediSage
//
//  Created by Atul Prakash on 21/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import Foundation
import AVKit
import SDWebImage
import AVFoundation
import Firebase
import FirebaseAnalytics

class NewHomeViewController: BaseViewController {
    
    //MARK:- Custom Properties
    var checkFlag = HomeEntity()
    
    var tableArray = [String: [HomeEntity]]()
    var selectedCommunities = [String]()
    var totalNumberOfRows = 1
    
    var memberID = 50
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()
    let refreshControl = UIRefreshControl()
    var userDefaults = UserDefaults.standard
    let group = DispatchGroup()
    var url = [String]()
    let requestManager = RequestManager()
    let homeEntity = HomeEntity()
    
    
    //MARK:- IBOutlet Connections
    @IBOutlet weak var homeTableView: UITableView!
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (userDefaults.value(forKey: KJConstant.RegUserDetails) != nil) {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            if let id = userData["userID"] as? Int { strUserID = "\(id)" }
            if let mobile = userData["mobileNumber"] as? String { strMobileNumber = mobile }
        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        if userDefaults.bool(forKey: "First_Launch") == true{
            userDefaults.setValue(true, forKey: "First_Launch")
        }else{
            userDefaults.setValue(true, forKey: "First_Launch")
        }
        
        let view = UIView(frame: CGRect(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        view.backgroundColor = UIColor.green
        self.tabBarController?.tabBar.addSubview(view)
        registerTableViewCell()
        self.callWebServices(refreshControl: false)
        self.addNotificationObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setupTopView()
        self.tabBarController?.tabBar.isHidden = false
        
        Analytics.logEvent("Home", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Home Page",
            AnalyticsParameterContentType: "Home"
        ])
        
        self.addPullToRefreshToTableView()
    }
    
    func addNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(notificationPressed(_ :)), name: .checkNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HandledDynamicLink(_ :)), name: .dynamicLink, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HandledNewsCount(_ :)), name: .newsArticleCount, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(communityRefresh(_:)), name: .profileEdited, object: nil)
    }
    
    func setupTopView() {
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.btnShare.isHidden = false
        self.viwTop.imgShare.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        self.viwTop.btnShare.addTarget(self, action: #selector(btnInviteTapped), for: .touchUpInside)
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
    }
    
    func addPullToRefreshToTableView() {
        //Pull to Refresh
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        let color = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        refreshControl.tintColor = color
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching List...", attributes: [NSAttributedString.Key.foregroundColor: color])
        homeTableView.refreshControl = refreshControl
    }
    
    func registerTableViewCell() {
        // Register banner tableview cell
        self.homeTableView.register(UINib(nibName: BannerTableViewCell.className, bundle: nil), forCellReuseIdentifier: BannerTableViewCell.className)
        
        // Register sections tableview cell
        self.homeTableView.register(UINib(nibName: SectionTableViewCell.className, bundle: nil), forCellReuseIdentifier: SectionTableViewCell.className)
        
        // Register experts tableview cell
        self.homeTableView.register(UINib(nibName: ExpertTableViewCell.className, bundle: nil), forCellReuseIdentifier: ExpertTableViewCell.className)
        
        // Register options tableview cell
        self.homeTableView.register(UINib(nibName: OptionsTableViewCell.className, bundle: nil), forCellReuseIdentifier: OptionsTableViewCell.className)
        
        // Register Communitites tableview cell
        self.homeTableView.register(UINib(nibName: CommunityTableViewCell.className, bundle: nil), forCellReuseIdentifier: CommunityTableViewCell.className)
        
        homeTableView.estimatedRowHeight = 230
        homeTableView.rowHeight = UITableView.automaticDimension
        homeTableView.separatorStyle = .none
        homeTableView.tableFooterView = UIView()
    }
    
    @objc func btnInviteTapped() {
        if let link = NSURL(string: "http://onelink.to/medisage") {
            let message = "I would like to invite you to the Medisage Doctor community, a single source for staying up-to-date through expert videos, news articles and case discussions"
            print(message)
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @objc func refresh() {
        self.callWebServices(refreshControl: true)
    }
    
    @objc func communityRefresh(_ notification: Notification) {
        self.callWebServices(refreshControl: false)
    }
    
    @objc func notificationPressed(_ notification: Notification){
        
        if let dict = notification.userInfo as NSDictionary? {
            if dict["action"] as? String ?? "" == "video"{
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
                vc.strVideoID = dict["id"] as? String ?? ""
                // vc.strVideoTitle = userInfo["video"] as? String ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["action"] as? String ?? "" == "live_event"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
                let eventLink = dict["live_event_link"] as? String ?? ""
                let linkID = dict["id"] as? String ?? ""
                vc.strURL = "\(eventLink)\(strUserID)/\(linkID)"
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["action"] as? String ?? "" == "news_article"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
                let linkID = dict["id"] as? String ?? ""
                vc.currentNewsID = Int(linkID) ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["action"] as? String ?? "" == "live_event_registration"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
                let linkID = dict["id"] as? String ?? ""
                vc.eventID = Int(linkID) ?? 0
                tabBarController?.selectedIndex = 2
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func HandledDynamicLink(_ notification: Notification){
        
        if let dict = notification.userInfo as NSDictionary? {
            if dict["video"] as? String ?? "" == "video"{
                
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
                vc.strVideoID = dict["videoID"] as? String ?? ""
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["news"] as? String ?? "" == "news_article"{
                
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
                vc.currentNewsID = dict["articleID"] as? Int ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if dict["series"] as? String ?? "" == "series"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.SeriesViewController) as! SeriesViewController
                let linkID = dict["seriesID"] as? String ?? ""
                vc.seriesID = Int(linkID)
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["live_event"] as? String ?? "" == "live_event"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
                tabBarController?.selectedIndex = 2
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    @objc func HandledNewsCount(_ notification: Notification){
        checkFlags()
    }
    
    //MARK:- Button Actions
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        onTabSelected(isTheSame: true)
    }
    
    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
        HamburgerMenu().triggerSideMenu()
    }
    
    @objc func hideHamburger(){
        HamburgerMenu().closeSideMenu()
    }
}
