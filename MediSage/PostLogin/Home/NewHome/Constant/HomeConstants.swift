//
//  HomeConstants.swift
//  MediSage
//
//  Created by Atul Prakash on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

enum HomeFixedTable: Int {
    case banner = 0, options, trending, newReleases, expert, journal
}

enum TableSectionHeader: String {
    case trending = "TRENDING"
    case newReleases = "NEW RELEASES"
    case expert = "TRENDING EXPERT"
    case journal = "JOURNAL ARTICLES"
}

enum BannerTypes: String {
    case video = "video"
    case liveEvent = "live_event"
    case newsArticle = "news_article"
    case partnerDivision = "partner_division"
}
