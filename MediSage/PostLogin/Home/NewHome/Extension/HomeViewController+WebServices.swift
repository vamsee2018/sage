//
//  HomeViewController+WebServices.swift
//  MediSage
//
//  Created by Atul Prakash on 21/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation
import UIKit

extension NewHomeViewController {
    func callWebServices(refreshControl: Bool) {
        if !refreshControl {
            self.showLoader()
        }
        checkFlags()
        getBannerList()
        getExpertsList()
        getTrendingList()
        getNewReleaseList()
        getJournalList()
        getProfile()
        
        group.notify(queue: .main) {
            if refreshControl {
                self.refreshControl.endRefreshing()
            } else {
                self.hideLoader()
            }
            self.homeTableView.reloadData()
        }
    }
    
    //MARK:- Check Profile Update, News Article Added or Not and Event in buffer time or not
    func checkFlags() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CheckHomePageFlag + strUserID, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    self.checkFlag = self.homeEntity.checkFlag(withInfo : data as? Dictionary<String, AnyObject>)
                    
                    if self.checkFlag.isNewsAdded ?? 0 > 0 {
//                        self.btnNews.setImage(UIImage(named: "News-Icon-1"), for: .normal)
                    } else {
//                        self.btnNews.setImage(UIImage(named: "News-Icon-(302-x-302)"), for: .normal)
                    }
                    if self.checkFlag.isLiveEventStart == true {
                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "icon-screen-2", withExtension: "gif")!)
                        let advTimeGif = UIImage.sd_image(withGIFData: imageData)
//                        self.btnEvents.setImage(advTimeGif, for: .normal)
                    } else {
//                        self.btnEvents.setImage(UIImage(named: "MY EVENT icon-26"), for: .normal)
                    }
                    
                    if self.checkFlag.isProfileIncomplete == true {
//                        self.homeTableViewTopConstraint.constant = 40
//                        self.viewInCompleteProfile.isHidden = false
                    } else {
//                        self.homeTableViewTopConstraint.constant = 0
//                        self.viewInCompleteProfile.isHidden = true
                    }
                    
                    if self.checkFlag.isNotification == true {
                        self.viwTop.viwNotification.layer.cornerRadius = 5
                        self.viwTop.viwNotification.isHidden = false
                    }else{
                        self.viwTop.viwNotification.layer.cornerRadius = 5
                        self.viwTop.viwNotification.isHidden = true
                    }
                }
            } else {
                self.view.makeToast(message: error)
            }
            self.group.leave()
        }
    }
    
    //MARK:- Get Banner API Call
    func getBannerList() {
        //Check for internet or return from method
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.BannerList, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let bannerArray = self.homeEntity.parseHomeBannerList(info : data as! Dictionary<String, AnyObject>)
                    self.tableArray[KJUrlConstants.BannerList] = bannerArray
                }
            } else {
                self.view.makeToast(message: error)
            }
            self.group.leave()
        }
    }
    
    //MARK:- Get Trending API Call
    func getTrendingList() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.Video + strUserID + "/" + KJUrlConstants.TrendingList, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let trendingArray = self.homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.tableArray[KJUrlConstants.TrendingList] = trendingArray
                }
            } else {
                self.view.makeToast(message: error)
            }
            self.group.leave()
        }
    }
    
    //MARK:- Get Experts API Call
    func getExpertsList() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.ExpertsList, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let expertsArray = self.homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.tableArray[KJUrlConstants.ExpertsList] = expertsArray
                }
            } else {
                self.view.makeToast(message: error)
            }
            self.group.leave()
        }
    }
    
    //MARK:- Get New Release API Call
    func getNewReleaseList() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.Video + strUserID + "/" + KJUrlConstants.NewReleasesList, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let newReleaseArray = self.homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.tableArray[KJUrlConstants.NewReleasesList] = newReleaseArray
                }
            } else {
                self.view.makeToast(message: error)
            }
            self.group.leave()
        }
    }
    
    //MARK:- Get journal API Call
    func getJournalList() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.JournalList, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    let journalArray = self.homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.tableArray[KJUrlConstants.JournalList] = journalArray
                }
            } else {
                self.view.makeToast(message: error)
            }
            self.group.leave()
        }
    }
    
    //MARK:- Get Profile API Call to fetch all selected communities
    func getProfile() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        CommonService.getProfile(userId: strUserID) { (entity, success, error) in
            if success {
                if let entity = entity {
                    self.selectedCommunities.removeAll()
                    for values in entity.arrFilterValues {
                        if let title = values.strVideoTitle {
                            self.selectedCommunities.append(title)
                        }
                    }
                    self.getMyCommunityList()
                }
            } else {
                self.view.makeToast(message: error ?? "")
            }
            self.group.leave()
        }
    }
    
    //MARK:- API Call to Get My Community List
    func getMyCommunityList() {
        group.enter()
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            group.leave()
            return
        }
        
        CommonService.getMyCommunityList(userId: strUserID) { (entity, success, error) in
            if success {
                if let entity = entity {
                    for title in self.selectedCommunities {
                        let filterList = entity.communityDataArr.filter{$0.strCommunityName == title}
                        if filterList.count > 0 {
                            self.tableArray[title] = filterList
                        }
                    }
                }
            } else {
                self.view.makeToast(message: error ?? "")
            }
            self.group.leave()
        }
    }
}
