//
//  HomeViewController+CustomTabBar.swift
//  MediSage
//
//  Created by Atul Prakash on 21/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation
import UIKit

extension NewHomeViewController: CustomTabBarControllerDelegate {
    func onTabSelected(isTheSame: Bool) {
        let sb = UIStoryboard(name: KJViewIdentifier.Profile, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ProfileViewController) as! ProfileViewController
         tabBarController?.selectedIndex = 4
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
