//
//  HomeViewController+TableView.swift
//  MediSage
//
//  Created by Atul Prakash on 19/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation
import UIKit

extension NewHomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableArray.count + selectedCommunities.count) == 0 {
            return 1
        } else {
            totalNumberOfRows = 6 + selectedCommunities.count
            return totalNumberOfRows
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell()
        // If all API fails and no data to list then just show the options table
        if tableArray.count == 0 {
            if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: OptionsTableViewCell.className, for: indexPath) as? OptionsTableViewCell {
                cell.delegate = self
                cellToReturn = UITableViewCell()
            }
        } else {
            switch indexPath.row {
            case HomeFixedTable.banner.rawValue:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: BannerTableViewCell.className, for: indexPath) as? BannerTableViewCell {
                    if let banner = self.tableArray[KJUrlConstants.BannerList], banner.count > 0 {
                        cell.pageControl?.currentPage = banner.count
                        cell.bannerArray = banner
                        cell.startTimer()
                        cell.delegate = self
                        cellToReturn = cell
                    }
                }
            case HomeFixedTable.options.rawValue:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: OptionsTableViewCell.className, for: indexPath) as? OptionsTableViewCell {
                    cell.delegate = self
                    cellToReturn = cell
                }
            case HomeFixedTable.trending.rawValue:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: SectionTableViewCell.className, for: indexPath) as? SectionTableViewCell {
                    if let section = self.tableArray[KJUrlConstants.TrendingList], section.count > 0 {
                        cell.playHidden = false
                        cell.viewAllButton.isHidden = true
                        cell.cellLabel?.text = TableSectionHeader.trending.rawValue
                        cell.sectionArray = section
                        cell.delegate = self
                        cellToReturn = cell
                    }
                }
            case HomeFixedTable.newReleases.rawValue:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: SectionTableViewCell.className, for: indexPath) as? SectionTableViewCell {
                    if let section = self.tableArray[KJUrlConstants.NewReleasesList], section.count > 0 {
                        cell.playHidden = false
                        cell.viewAllButton.isHidden = true
                        cell.cellLabel?.text = TableSectionHeader.newReleases.rawValue
                        cell.sectionArray = section
                        cell.delegate = self
                        cellToReturn = cell
                    }
                }
            case HomeFixedTable.expert.rawValue:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: ExpertTableViewCell.className, for: indexPath) as? ExpertTableViewCell {
                    if let expert = self.tableArray[KJUrlConstants.ExpertsList], expert.count > 0 {
                        cell.expertArray = expert
                        cell.cellLabel?.text = TableSectionHeader.expert.rawValue
                        cell.delegate = self
                        cellToReturn = cell
                    }
                }
            case totalNumberOfRows - 1:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: SectionTableViewCell.className, for: indexPath) as? SectionTableViewCell {
                    if let section = self.tableArray[KJUrlConstants.JournalList], section.count > 0 {
                        cell.collectionReloadData()
                        cell.playHidden = true
                        cell.viewAllButton.isHidden = true
                        cell.cellLabel?.text = TableSectionHeader.journal.rawValue
                        cell.sectionArray = section
                        cell.delegate = self
                        cellToReturn = cell
                    }
                }
            default:
                if let cell = self.homeTableView.dequeueReusableCell(withIdentifier: CommunityTableViewCell.className, for: indexPath) as? CommunityTableViewCell {
                    let index = totalNumberOfRows - (indexPath.row + 2)
                    if let section = self.tableArray[selectedCommunities[index]], section.count > 0 {
                        cell.cellLabel?.text = selectedCommunities[index]
                        cell.communityArray = section
                        cell.viewAllButton.tag = index
                        cell.delegate = self
                        cellToReturn = cell
                    }
                }
            }
        }
        return cellToReturn
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension HomeViewController: UITableViewDelegate {
    
}
