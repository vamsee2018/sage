//
//  HomeViewController+ButtonActions.swift
//  MediSage
//
//  Created by Atul Prakash on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation
import UIKit

extension NewHomeViewController: BannerCellDelegate {
    func bannerCellClicked(of type: BannerTypes, entity: HomeEntity) {
        switch type {
        case .video:
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            viewController.strVideoID = entity.strActionID ?? ""
            viewController.strVideoTitle = entity.strVideoTitle ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        case .liveEvent:
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
            tabBarController?.selectedIndex = 2
            viewController.eventLinkID = entity.strActionID ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        case .newsArticle:
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
            viewController.currentNewsID = Int(entity.strActionID ?? "") ?? 0
            self.navigationController?.pushViewController(viewController, animated: true)
        case .partnerDivision:
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.PartnerAreasViewController) as! PartnerAreasViewController
            viewController.divisionID = Int(entity.strActionID ?? "") ?? 0
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension NewHomeViewController: OptionsButtonDelegate {
    func casesActions() {
        let storyBoard = UIStoryboard(name: KJViewIdentifier.CasesViewController, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.CasesViewController) as! CasesViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func eventsActions() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
        tabBarController?.selectedIndex = 2
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func newsActions() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension NewHomeViewController: ExpertsViewAllDelegate {
    func expertsViewAllAction() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyExpertsViewController) as! MyExpertsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func selectedExpertItemAtIndexpath(entity: HomeEntity) {
        let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
        viewController.expertID = entity.strVideoID ?? 0
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension NewHomeViewController: SectionViewAllDelegate {
    func selectedItemAtIndexpath(entity: HomeEntity, title: String) {
        if title == TableSectionHeader.journal.rawValue || title.isEmpty {
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
            viewController.strURL = KJUrlConstants.ImageUrl + "\(entity.strImagePath  ?? "")" + "\(entity.strFileName ?? "")"
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            viewController.strVideoID = "\(entity.strVideoID ?? 0)"
            viewController.strVideoTitle = entity.strVideoTitle ?? ""
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func sectionViewAllAction() {
    }
}

extension NewHomeViewController: CommunityViewAllDelegate {
    func viewAllCommunityAction(at index: Int) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
        let entity = self.tableArray[selectedCommunities[index]]?.first
        viewController.communityID = "\(entity?.intCommunityID ?? 0)"
         self.navigationController?.pushViewController(viewController, animated: true)
    }
}
