//
//  ExpertsCollectionViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 19/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class ExpertsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
