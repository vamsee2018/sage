//
//  ExpertTableViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 18/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

protocol ExpertsViewAllDelegate: AnyObject {
    func expertsViewAllAction()
    func selectedExpertItemAtIndexpath(entity: HomeEntity)
}

class ExpertTableViewCell: UITableViewCell {

    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: ExpertsViewAllDelegate?
    
    var expertArray = [HomeEntity]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }
    
    func registerCollectionViewCell() {
        self.collectionView.register(UINib(nibName: ExpertsCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ExpertsCollectionViewCell.className)
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func viewAll(_ sender: Any) {
        delegate?.expertsViewAllAction()
    }
    
}

extension ExpertTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return expertArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpertsCollectionViewCell.className, for: indexPath) as? ExpertsCollectionViewCell {
            
            let entity = expertArray[indexPath.item]
            cell.label?.text = entity.strDoctorName
            cell.imageView?.layer.masksToBounds = true
            cell.imageView?.layer.cornerRadius = 50
            cell.imageView?.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
            cell.imageView?.layer.borderWidth = 1.0
            cell.imageView?.layer.shadowColor = UIColor.black.cgColor
            cell.imageView?.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.imageView?.layer.shadowOpacity = 0.3
            cell.imageView?.layer.shadowRadius = 7.0
            cell.imageView?.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(entity.strImagePath  ?? "")" + "\(entity.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            return cell
        }
        return UICollectionViewCell()
    }
}

extension ExpertTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

extension ExpertTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let entity = expertArray[indexPath.item]
        delegate?.selectedExpertItemAtIndexpath(entity: entity)
    }
}
