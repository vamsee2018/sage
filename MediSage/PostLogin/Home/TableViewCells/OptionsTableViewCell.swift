//
//  OptionsTableViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 18/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

protocol OptionsButtonDelegate: AnyObject {
    func casesActions()
    func eventsActions()
    func newsActions()
}

class OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var casesButton: UIButton! {
        didSet {
            self.casesButton?.layer.cornerRadius = self.casesButton.frame.size.width/2
        }
    }
    
    @IBOutlet weak var eventsButton: UIButton! {
        didSet {
            self.eventsButton.layer.cornerRadius = self.eventsButton.frame.size.width/2
        }
    }
    
    @IBOutlet weak var newsButton: UIButton! {
        didSet {
            self.newsButton.layer.cornerRadius = self.newsButton.frame.size.width/2
        }
    }
    
    weak var delegate: OptionsButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        observeNotification()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func observeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateButtonImage(_ :)), name: .newsArticleCount, object: nil)
    }
    
    @objc func updateButtonImage(_ notification: Notification){
        if let dict = notification.userInfo as NSDictionary? {
            if dict["index"] as? String ?? "" == "1"{
                self.newsButton?.setImage(UIImage(named: "News-Icon-1"), for: .normal)
            }else{
                self.newsButton?.setImage(UIImage(named: "News-Icon-(302-x-302)"), for: .normal)
            }
        }
    }
    
    // MARK:- Button Actions
    @IBAction func casesAction(_ sender: Any) {
        delegate?.casesActions()
    }
    
    @IBAction func eventsAction(_ sender: Any) {
        delegate?.eventsActions()
    }
    
    @IBAction func newsAction(_ sender: Any) {
        delegate?.newsActions()
    }
}
