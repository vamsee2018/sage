//
//  BannerCollectionViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 19/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
