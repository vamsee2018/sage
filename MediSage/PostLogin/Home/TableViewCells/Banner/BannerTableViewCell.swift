//
//  BannerTableViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 18/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

protocol BannerCellDelegate: AnyObject {
    func bannerCellClicked(of type: BannerTypes, entity: HomeEntity)
}

class BannerTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var bannerArray = [HomeEntity]()
    weak var delegate: BannerCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }
    
    func registerCollectionViewCell() {
        self.collectionView.register(UINib(nibName: BannerCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BannerCollectionViewCell.className)
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }
    
    func startTimer() {
        _ =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        self.pageControl.currentPage = bannerArray.count
        if let coll  = collectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)! < bannerArray.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)

                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension BannerTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCollectionViewCell.className, for: indexPath) as? BannerCollectionViewCell{
            
            let entity = bannerArray[indexPath.item]
            cell.imageView?.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(entity.strImagePath  ?? "")" + "\(entity.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imageView?.layer.cornerRadius = 10
            cell.imageView?.contentMode = .scaleToFill
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.numberOfPages = bannerArray.count
        self.pageControl.currentPage = bannerArray.count
    }
}

extension BannerTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 165)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

extension BannerTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let entity = bannerArray[indexPath.row]
        switch entity.strActionType {
        case BannerTypes.video.rawValue:
            delegate?.bannerCellClicked(of: .video, entity: entity)
        case BannerTypes.liveEvent.rawValue:
            delegate?.bannerCellClicked(of: .liveEvent, entity: entity)
        case BannerTypes.newsArticle.rawValue:
            delegate?.bannerCellClicked(of: .newsArticle, entity: entity)
        case BannerTypes.partnerDivision.rawValue:
            delegate?.bannerCellClicked(of: .partnerDivision, entity: entity)
        default:
            break
        }
    }
}
