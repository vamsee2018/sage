//
//  SectionTableViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 18/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

protocol SectionViewAllDelegate: AnyObject {
    func sectionViewAllAction()
    func selectedItemAtIndexpath(entity: HomeEntity, title: String)
}

class SectionTableViewCell: UITableViewCell {

    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewAllButton: UIButton!
    
    weak var delegate: SectionViewAllDelegate?
    var sectionArray = [HomeEntity]()
    var playHidden: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }
    
    func registerCollectionViewCell() {
        self.collectionView.register(UINib(nibName: SectionCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: SectionCollectionViewCell.className)
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }
    
    func collectionReloadData(){
        DispatchQueue.main.async(execute: {
            self.collectionView.reloadData()
            self.collectionView?.contentOffset.x = 0
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func viewAllAction(_ sender: Any) {
        delegate?.sectionViewAllAction()
    }
    
}

extension SectionTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SectionCollectionViewCell.className, for: indexPath) as? SectionCollectionViewCell {
            
            let entity = sectionArray.count <= indexPath.item ? sectionArray[sectionArray.count - 1] : sectionArray[indexPath.item]
            cell.label?.text = entity.strVideoTitle
            cell.imageView?.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(entity.strImagePath  ?? "")" + "\(entity.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imageView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            // cell.backView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            cell.backView?.layer.cornerRadius = 10
            cell.backView?.layer.shadowColor = UIColor.gray.cgColor
            cell.backView?.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.backView?.layer.shadowOpacity = 0.2
            cell.backView?.layer.shadowRadius = 7.0
            cell.playImageView?.isHidden = playHidden ?? false
            return cell
        }
        return UICollectionViewCell()
    }
}

extension SectionTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 172, height: 172)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}

extension SectionTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let entity = sectionArray[indexPath.item]
        delegate?.selectedItemAtIndexpath(entity: entity, title: self.cellLabel?.text ?? "")
    }
}
