//
//  CommunityTableViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

protocol CommunityViewAllDelegate: AnyObject {
    func viewAllCommunityAction(at index: Int)
}

class CommunityTableViewCell: UITableViewCell {

    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: CommunityViewAllDelegate?
    var communityArray = [HomeEntity]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCollectionViewCell()
    }
    
    func registerCollectionViewCell() {
        self.collectionView.register(UINib(nibName: CommunityCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: CommunityCollectionViewCell.className)
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func viewAllAction(_ sender: UIButton) {
        print(sender.tag)
        delegate?.viewAllCommunityAction(at: sender.tag)
    }
}

extension CommunityTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //make protocol
    }
}

extension CommunityTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return communityArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CommunityCollectionViewCell.className, for: indexPath) as? CommunityCollectionViewCell {
            
            let entity = communityArray[indexPath.item]
            cell.imageView?.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(entity.strImagePath  ?? "")" + "\(entity.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.backView?.layer.cornerRadius = 10
            cell.backView?.layer.shadowColor = UIColor.black.cgColor
            cell.backView?.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.backView?.layer.shadowOpacity = 0.3
            cell.backView?.layer.shadowRadius = 5.0
            
            return cell }
        return UICollectionViewCell()
    }
}

extension CommunityTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 172, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
