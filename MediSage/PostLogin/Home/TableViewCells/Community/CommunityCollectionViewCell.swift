//
//  CommunityCollectionViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class CommunityCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var playImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
