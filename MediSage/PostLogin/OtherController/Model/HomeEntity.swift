//
//  HomeEntity.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class HomeEntity: NSObject {

    var strImageName : String? = ""
    var strDoctorName: String? = ""
    var strImagePath: String? = ""
    var strBannerImage: String? = ""
    var strPath: String? = ""
    var strFileName: String? = ""
    var intPartOfSeries: Int?
    var strVideoTitle: String? = ""
    var strVideoID: Int?
    var strCommunityVideoID: String? = ""
    var strCommunityName: String? = ""
    var striFramePath: String? = ""
    var strTitle: String? = ""
    var intID: Int?
    var strDesignation: String?
    var upcomingArr = [HomeEntity]()
    var pastArr = [HomeEntity]()

    var communityArr = [HomeEntity]()
    var communityDataArr = [HomeEntity]()
    var seriesInfoArr = [HomeEntity]()
    var seriesVideosArr = [HomeEntity]()
    var expertInfoArr = [HomeEntity]()
    var arrFilterName = [HomeEntity]()
    var arrVideos = [HomeEntity]()
    var videoArr = [HomeEntity]()
    var intCommunityID: Int?
    var intPartnerID: Int?
    var intExpertID: Int?
    var intEventID: Int?
    var intIsPartOfSeries: Int?
    var intVideoViewCount: Int?
    var strVideoLink: String? = ""
    var strLastWatched: String? = ""
    var strUpdatedAt: String? = ""
    var strDescription: String? = ""
    var strExpertDescription: String? = ""
    var strExpertise: String? = ""
    var strPartnerName: String? = ""
    var strEventTime: String? = ""
    var strDate: String? = ""
    var strEventTitle: String? = ""
    var strSessionTime: String? = ""
    var strNewsName: String? = ""
    var strJournal: String? = ""
    var strCreatedOn: String? = ""
    var strContent: String? = ""
    var strArticleLink: String? = ""
    var strActiveEventLink: String? = ""
    var strLinkID: String? = ""
    var strActionType: String? = ""
    var strActionID: String? = ""
    var intBufferTime: Int?
    var isRegistered: Bool?
    var isLiveEventStart: Bool?
    var isProfileIncomplete: Bool?
    var isNotification: Bool?

    var isNewsAdded: Int?
    var strMessage : String?
    var strUserStatus:Int? = -1
    var isLike: Int?
    var isInsightful: Int?
    var currentPage: Int?
    var lastPage: Int?

    
    //MARK:- Method for Banner list  api data parsing
    func parseHomeBannerList(info : Dictionary<String,AnyObject>)->[HomeEntity]{
        
        var mainEnt = [HomeEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = HomeEntity()
                
                if let id = dict["id"] as? Int{
                    ent.strVideoID = id
                }
                if let name = dict["name"] as? String{
                    ent.strDoctorName = name
                }
                if let title = dict["title"] as? String{
                    ent.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    ent.strImagePath = imagePath
                }
                if let imageName = dict["image_name"] as? String{
                    ent.strImageName = imageName
                }
                if let action_type = dict["action_type"] as? String{
                    ent.strActionType = action_type
                }
                if let action_id = dict["action_id"] as? String{
                    ent.strActionID = action_id
                }
                if let live_event_link = dict["active_live_event_url"] as? String{
                    ent.strActiveEventLink = live_event_link
                }
                
                mainEnt.append(ent)
            }
        }
        return mainEnt
    }
    
    
    
    //MARK:-  Method for Home list  api data parsing
    func parseHomeList(info : Dictionary<String,AnyObject>)->[HomeEntity]{
        
        var mainEnt = [HomeEntity]()

        let dictClass = info["Data"] as? [String : Any]
//        if let current_page = dictClass?["current_page"] as? Int{
//            ent.currentPage = current_page
//        }
//        if let last_page = dictClass?["last_page"] as? Int{
//            ent.lastPage = last_page
//        }
        if let arrObj = dictClass?["data"] as? [[String:Any]]{
            
            for obj in arrObj
            {
                if let dict = obj as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    if let current_page = dictClass?["current_page"] as? Int{
                        ent.currentPage = current_page
                    }
                    if let last_page = dictClass?["last_page"] as? Int{
                        ent.lastPage = last_page
                    }
                    if let id = dict["id"] as? Int{
                        ent.strVideoID = id
                    }
                    if let isPartOfSeries = dict["is_part_of_series"] as? Int{
                        ent.intPartOfSeries = isPartOfSeries
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let path = dict["image_path"] as? String{
                        ent.strPath = path
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let fileName = dict["file_name"] as? String{
                        ent.strFileName = fileName
                    }
                    if let designation = dict["designation"] as? String{
                        ent.strDesignation = designation
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    
                    mainEnt.append(ent)
                }
            }
        }
        return mainEnt
    }
    
    func parseCommunityList(info : Dictionary<String,AnyObject>)->HomeEntity{
        
        let mainEnt = HomeEntity()
        let dictClass = info["Data"] as? [String:Any]
        
        if let arrObject = dictClass?["communities"] as? [[String:Any]]{
            for item in arrObject {
                
               if let dict = item as? [String:AnyObject]
               {
                   let ent = HomeEntity()
                   
                   if let id = dict["id"] as? Int{
                       ent.intID = id
                   }
                   if let name = dict["name"] as? String{
                       ent.strTitle = name
                   }
                  
                  mainEnt.communityArr.append(ent)
               }

            }
        }
        
        if let arrObj = dictClass?["community_data"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let community_id = dict["community_id"] as? Int{
                        ent.intCommunityID = community_id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let community_name = dict["community_name"] as? String{
                        ent.strCommunityName = community_name
                    }
                    if let videoID = dict["video_link"] as? String{
                        ent.strCommunityVideoID = videoID
                    }
                    if let iFramePath = dict["iframe_path"] as? String{
                        ent.striFramePath = iFramePath
                    }
                    
                    mainEnt.communityDataArr.append(ent)
                    
                }
            }
        }
        return mainEnt
    }
    /// Method for country code list  api data parsing
    func parseCommunityVideoList(info : Dictionary<String,AnyObject>)->[HomeEntity]{
        
        var mainEnt = [HomeEntity]()
        let dictClass = info["Data"] as? [String : Any]
        
        if let arrObj = dictClass?["data"] as? [[String:Any]]{
            
            for obj in arrObj
            {
                if let dict = obj as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    if let current_page = dictClass?["current_page"] as? Int{
                        ent.currentPage = current_page
                    }
                    if let last_page = dictClass?["last_page"] as? Int{
                        ent.lastPage = last_page
                    }
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let designation = dict["designation"] as? String{
                        ent.strDesignation = designation
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    if let videoID = dict["video_link"] as? String{
                        ent.strCommunityVideoID = videoID
                    }
                    if let iFramePath = dict["iframe_path"] as? String{
                        ent.striFramePath = iFramePath
                    }
                    mainEnt.append(ent)
                }
            }
        }
        return mainEnt
    }
       
    func parseEventList(info : Dictionary<String,AnyObject>)->HomeEntity{
        
        var mainEnt = HomeEntity()
        let dictClass = info["Data"] as? [String:Any]
        
        if let arrObject = dictClass?["active_events"] as? [[String:Any]]{
            for item in arrObject {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intEventID = id
                    }
                    if let buffer_time = dict["buffer_time"] as? Int{
                        ent.intBufferTime = buffer_time
                    }
                    if let banner_vimeo_video_id = dict["banner_vimeo_video_id"] as? Int{
                        ent.intID = banner_vimeo_video_id
                    }
                    if let banner_image = dict["register_image1"] as? String{
                        ent.strBannerImage = banner_image
                    }
                    
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let linkID = dict["link_id"] as? String{
                        ent.strLinkID = linkID
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let eventTitle = dict["live_event_text"] as? String{
                        ent.strEventTitle = eventTitle
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["thumbnail_image"] as? String{
                        ent.strImageName = imageName
                    }
                    if let partnerName = dict["partner_name"] as? String{
                        ent.strPartnerName = partnerName
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    if let session_time = dict["session_time"] as? String{
                        ent.strEventTime = session_time
                    }
                    if let iFramePath = dict["iframe_path"] as? String{
                        ent.striFramePath = iFramePath
                    }
                    if let is_registered = dict["is_registered"] as? Bool{
                        ent.isRegistered = is_registered
                    }
                    if let activeEvent = dict["active_live_event_url"] as? String{
                        ent.strActiveEventLink = activeEvent
                    }
                    mainEnt.upcomingArr.append(ent)
                    
                }
                
            }
        }
        
        if let arrObj = dictClass?["past_events"] as? [[String:Any]]{

            for item in arrObj {
               
                    if let dict = item as? [String:AnyObject]
                    {
                        let ent = HomeEntity()
                        
                        if let id = dict["id"] as? Int{
                            ent.strVideoID = id
                        }
                        if let name = dict["name"] as? String{
                            ent.strDoctorName = name
                        }
                        if let title = dict["title"] as? String{
                            ent.strVideoTitle = title
                        }
                        if let eventTitle = dict["live_event_text"] as? String{
                            ent.strEventTitle = eventTitle
                        }
                        if let imagePath = dict["path"] as? String{
                            ent.strImagePath = imagePath
                        }
                        if let imageName = dict["thumbnail_image"] as? String{
                            ent.strImageName = imageName
                        }
                        if let partnerName = dict["partner_name"] as? String{
                            ent.strPartnerName = partnerName
                        }
                        if let description = dict["description"] as? String{
                            ent.strDescription = description
                        }
                        if let session_time = dict["session_time"] as? String{
                            ent.strEventTime = session_time
                        }
                        if let iFramePath = dict["iframe_path"] as? String{
                            ent.striFramePath = iFramePath
                        }
                       
                        mainEnt.pastArr.append(ent)

                    }
            }
        }
        return mainEnt
    }

    //MARK:-  Method for country code list  api data parsing
    func getTNCPolicyDetails(info : Dictionary<String,AnyObject>)->HomeEntity{
        
        let ent = HomeEntity()
        if let obj = info["Data"] as? [String:AnyObject]
        {
            if let title = obj["Description"] as? String{
                ent.strDescription = title
            }
            
        }
        
        return ent
        
    }
    
    
    func parseCommunity(info : Dictionary<String,AnyObject>)->[HomeEntity]{
        var mainEnt = [HomeEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = HomeEntity()
                
                if let id = dict["id"] as? Int{
                    ent.strVideoID = id
                }
                if let name = dict["name"] as? String{
                    ent.strDoctorName = name
                }
                if let title = dict["title"] as? String{
                    ent.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    ent.strImagePath = imagePath
                }
                if let imageName = dict["image_name"] as? String{
                    ent.strImageName = imageName
                }
                
                
                mainEnt.append(ent)
            }
        }
        return mainEnt

    }
    
     /// Method for country code list  api data parsing
       func parseRegisterEvent(info : Dictionary<String,AnyObject>)->HomeEntity{
           
           let myEnt = HomeEntity()
           let dictClass = info["Data"] as? [String : Any]
           
           if let is_time_to_show_event = dictClass?["is_time_to_show_event"] as? Bool{
                myEnt.isLiveEventStart = is_time_to_show_event
            }
           if let arrObj = dictClass?["live_event_data"] as? [String:Any]{
               if let dict = arrObj as? [String:AnyObject]
               {
                  // let myEnt = PartnerDivisionEntity()
                   
                   if let id = dict["id"] as? Int{
                       myEnt.intEventID = id
                   }
                   if let buffer_time = dict["buffer_time"] as? Int{
                     myEnt.intBufferTime = buffer_time
                   }
                   if let partner_id = dict["partner_id"] as? Int{
                       myEnt.intPartnerID = partner_id
                   }
                   if let expert_id = dict["expert_id"] as? Int{
                       myEnt.intExpertID = expert_id
                   }
                    if let name = dict["name"] as? String{
                       myEnt.strDoctorName = name
                   }
                   if let imageName = dict["image_name"] as? String{
                       myEnt.strImageName = imageName
                   }
                   if let title = dict["live_event_text"] as? String{
                       myEnt.strVideoTitle = title
                   }
                   if let imagePath = dict["path"] as? String{
                       myEnt.strImagePath = imagePath
                   }
                   if let path = dict["image_path"] as? String{
                       myEnt.strPath = path
                   }
                   if let video_link = dict["video_id"] as? String{
                       myEnt.strVideoLink = video_link
                   }
                   if let session_time = dict["session_time"] as? String{
                       myEnt.strSessionTime = session_time
                   }

               }
           }
           return myEnt
       }
    
    func parseNewsList(info : Dictionary<String,AnyObject>)->[HomeEntity]{
        var mainEnt = [HomeEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = HomeEntity()
                
                if let id = dict["id"] as? Int{
                    ent.intID = id
                }
                if let likes = dict["likes"] as? Int{
                    ent.isLike = likes
                }
                if let insightful = dict["insightful"] as? Int{
                    ent.isInsightful = insightful
                }
                if let name = dict["name"] as? String{
                    ent.strDoctorName = name
                }
                if let title = dict["header"] as? String{
                    ent.strTitle = title
                }
                if let link = dict["link"] as? String{
                    ent.strArticleLink = link
                }
                if let imagePath = dict["path"] as? String{
                    ent.strImagePath = imagePath
                }
                if let imageName = dict["card_name"] as? String{
                    ent.strNewsName = imageName
                }
                if let description = dict["small_description"] as? String{
                    ent.strDescription = description
                }
                if let imageName = dict["card_image"] as? String{
                    ent.strImageName = imageName
                }
                if let journal = dict["journal"] as? String{
                    ent.strJournal = journal
                }
                if let created_at = dict["created_at"] as? String{
                    ent.strCreatedOn = created_at
                }
                if let community_name = dict["community_name"] as? String{
                    ent.strCommunityName = community_name
                }
                mainEnt.append(ent)
            }
        }
        return mainEnt

    }
    
    //MARK:-  Method for country code list  api data parsing
    func parseNewsDetails(info : Dictionary<String,AnyObject>)->HomeEntity{
        
        let ent = HomeEntity()
        if let obj = info["Data"] as? [String:AnyObject]
        {
            
            if let id = obj["id"] as? Int{
                ent.strVideoID = id
            }
            if let name = obj["name"] as? String{
                ent.strDoctorName = name
            }
            if let title = obj["header"] as? String{
                ent.strTitle = title
            }
            if let imagePath = obj["path"] as? String{
                ent.strImagePath = imagePath
            }
            if let imageName = obj["card_name"] as? String{
                ent.strNewsName = imageName
            }
            if let description = obj["small_description"] as? String{
                ent.strDescription = description
            }
            if let imageName = obj["card_image"] as? String{
                ent.strImageName = imageName
            }
            if let journal = obj["journal"] as? String{
                ent.strJournal = journal
            }
            if let created_at = obj["created_at"] as? String{
                ent.strCreatedOn = created_at
            }
            if let content = obj["content"] as? String{
                ent.strContent = content
            }
            if let community_name = obj["community_name"] as? String{
                ent.strCommunityName = community_name
            }
        }
        
        return ent
        
    }
    
    func parseSeriesDetails(info : Dictionary<String,AnyObject>)->HomeEntity{
        
        var mainEnt = HomeEntity()
        let dictClass = info["Data"] as? [String:Any]
        
        if let arrObject = dictClass?["series_info"] as? [[String:Any]]{
            for item in arrObject {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intEventID = id
                    }
                    if let likes = dict["like"] as? Int{
                        ent.isLike = likes
                    }
                    if let insightful = dict["insightful"] as? Int{
                        ent.isInsightful = insightful
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    if let video_link = dict["video_link"] as? String{
                        ent.strVideoLink = video_link
                    }
                    mainEnt.seriesInfoArr.append(ent)
                    
                }
                
            }
        }
        
        if let arrObj = dictClass?["series_videos"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.strVideoID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let isPartOfSeries = dict["is_part_of_series"] as? Int{
                        ent.intPartOfSeries = isPartOfSeries
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    if let video_link = dict["video_link"] as? String{
                        ent.strVideoLink = video_link
                    }
                    
                    mainEnt.seriesVideosArr.append(ent)
                    
                }
            }
        }
        if let arrObj = dictClass?["expert_info"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = HomeEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    
                    if let imageName = dict["image_name"] as? String{
                       ent.strImageName = imageName
                    }
                    if let designation = dict["designation"] as? String{
                        ent.strDesignation = designation
                    }
                    mainEnt.expertInfoArr.append(ent)
                    
                }
            }
        }
        return mainEnt
    }
    
    //MARK:- Parse VideoFeedback 
    func submitVideoFeedback(withInfo info : Dictionary<String,AnyObject>?) -> HomeEntity
    {
        let ent = HomeEntity()
        if let msg = info?["Message"] as? String
        {
            ent.strMessage = msg
        }
        if let userStatus = info?["Status"] as? Int {
            ent.strUserStatus = userStatus
        }
        
        if let obj = info?["Data"] as? [String:AnyObject]
        {
            if let like = obj["like"] as? Int{
                ent.isLike = like
            }
            if let insightful = obj["insightful"] as? Int{
                ent.isInsightful = insightful
            }
        }

     return ent
    }
    
    //MARK:- Check Profile Update, News Article Added or Not and Event in buffer time or not
    func checkFlag(withInfo info : Dictionary<String,AnyObject>?) -> HomeEntity
    {
        let ent = HomeEntity()
        if let msg = info?["Message"] as? String
        {
            ent.strMessage = msg
        }
        if let userStatus = info?["Status"] as? Int {
            ent.strUserStatus = userStatus
        }
        
        if let obj = info?["Data"] as? [String:AnyObject]
        {
            if let new_news_articles = obj["new_news_articles"] as? Int{
                ent.isNewsAdded = new_news_articles
            }
            if let is_a_event_active = obj["is_a_event_active"] as? Bool{
                ent.isLiveEventStart = is_a_event_active
            }
            if let profile_incomplete = obj["profile_incomplete"] as? Bool{
                ent.isProfileIncomplete = profile_incomplete
            }
            if let notifications_count = obj["notifications_count"] as? Bool{
                ent.isNotification = notifications_count
            }

        }

     return ent
    }
    
    //MARK:- Parse Expert Details
    func parseExpertDetails(info : Dictionary<String,AnyObject>)->HomeEntity{
        
        let mainEnt = HomeEntity()
        let dictClass = info["Data"] as? [String : Any]
        
        if let arrObj = dictClass?["expert"] as? [String:Any]{
            
            if let dict = arrObj as? [String:AnyObject]
            {
                let myEnt = HomeEntity()
                
                if let id = dict["id"] as? Int{
                    myEnt.intExpertID = id
                }
                if let name = dict["name"] as? String{
                    myEnt.strDoctorName = name
                }
                if let imageName = dict["image_name"] as? String{
                    myEnt.strImageName = imageName
                }
                if let title = dict["title"] as? String{
                    myEnt.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    myEnt.strImagePath = imagePath
                }
                if let image_name = dict["image_name"] as? String{
                    myEnt.strImageName = image_name
                }
                if let expertise = dict["expertise"] as? String{
                    myEnt.strExpertise = expertise
                }
                if let designation = dict["designation"] as? String{
                    myEnt.strDesignation = designation
                }
                if let description = dict["description"] as? String{
                    myEnt.strExpertDescription = description
                }
                
                if let arrObject = dictClass?["videos"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = HomeEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                           
                            if let community_id = dict["community_id"] as? Int{
                                ent.intCommunityID = community_id
                            }
                            
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            if let path = dict["image_path"] as? String{
                                ent.strPath = path
                            }
                            if let description = dict["description"] as? String{
                                myEnt.strDescription = description
                            }
                            mainEnt.arrVideos.append(ent)
                            
                        }
                        
                    }
                }
                
                mainEnt.arrFilterName.append(myEnt)
            }
            
        }
        return mainEnt
    }
    
    func parseNotificationList(info : Dictionary<String,AnyObject>)->[HomeEntity]{
        var mainEnt = [HomeEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = HomeEntity()
                
                if let id = dict["id"] as? Int{
                    ent.intID = id
                }
                if let name = dict["name"] as? String{
                    ent.strDoctorName = name
                }
                
                if let title = dict["title"] as? String{
                    ent.strTitle = title
                }
                if let description = dict["text"] as? String{
                    ent.strDescription = description
                }
                if let link = dict["link"] as? String{
                    ent.strArticleLink = link
                }
                if let imagePath = dict["path"] as? String{
                    ent.strImagePath = imagePath
                }
                if let action_type = dict["action_type"] as? String{
                    ent.strActionType = action_type
                }
                if let action_id = dict["action_id"] as? String{
                    ent.strActionID = action_id
                }
                if let live_event_link = dict["live_event_url"] as? String{
                    ent.strActiveEventLink = live_event_link
                }
                mainEnt.append(ent)
            }
        }
        return mainEnt

    }
    
    
    func parseNotificationFeedback(withInfo info : Dictionary<String,AnyObject>?) -> HomeEntity{
        let ent = HomeEntity()
        if let msg = info?["Message"] as? String
        {
            ent.strMessage = msg
        }
        if let userStatus = info?["Status"] as? Int {
            ent.strUserStatus = userStatus
        }
        
        return ent
    }
    
}
