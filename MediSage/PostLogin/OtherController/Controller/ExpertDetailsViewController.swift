//
//  ExpertDetailsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 14/01/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class ExpertDetailsViewController: BaseViewController {

    //MARK:- Custom Properties
    var expertDetailsArray = HomeEntity()
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()
    var expertID = Int()
   
    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblViewExpertVideos: UITableView!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var lblQualification: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblExpertVideos: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblExpertise: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnExpandCollapse: UIButton!
    @IBOutlet weak var lblHeight: NSLayoutConstraint!
    var isLabelAtMaxHeight = false
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(checkVideoPlaying), name: NSNotification.Name("isPlayingVideo"), object: nil)
        getExpertDetails()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
        imgProfile.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        imgProfile.layer.borderWidth = 1.0
        
    }
    
    @objc func checkVideoPlaying(){
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:- API Call for Get Expert Details
    func getExpertDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
               
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MYExpertDetails + "\(expertID)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    //self.expertDetailsArray.removeAll()
                    self.expertDetailsArray = homeEntity.parseExpertDetails(info : data as! Dictionary<String, AnyObject>)
                    self.setExpertDetails()
                    self.tblViewExpertVideos.reloadData()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Set Expert Details
    func setExpertDetails(){
      
        lblDoctorName.text = expertDetailsArray.arrFilterName[0].strDoctorName
        lblDesignation.text = expertDetailsArray.arrFilterName[0].strDesignation
        lblExpertise.text = expertDetailsArray.arrFilterName[0].strExpertise
        lblDescription.attributedText = expertDetailsArray.arrFilterName[0].strExpertDescription?.htmlAttributedString()

        if expertDetailsArray.arrFilterName[0].strImageName == ""{
            
        }else{
            imgProfile.clipsToBounds = true
            imgProfile.layer.cornerRadius = 60
            
            imgProfile.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(expertDetailsArray.arrFilterName[0].strImagePath  ?? "")" + "/" + "\(expertDetailsArray.arrFilterName[0].strImageName ?? "")"), placeholderImage:UIImage(named:"placeholder"))
        }
        if expertDetailsArray.arrVideos.count > 0{
             lblExpertVideos.isHidden = false
        }else{
            lblExpertVideos.isHidden = true
            
        }
        
    }
    
    //MARK:- Button Action
    @IBAction func btnExpandCollapseTapped(_ sender: Any) {
        
        print("LINES :---",lblDescription.maxNumberOfLines)
        
        if lblDescription.maxNumberOfLines > 4{
            if isLabelAtMaxHeight {
                btnExpandCollapse.setImage(UIImage(named: "down-arrow"), for: .normal)
                isLabelAtMaxHeight = false
                lblHeight.constant = 70
            }
            else {
                btnExpandCollapse.setImage(UIImage(named: "up-arrow"), for: .normal)
                isLabelAtMaxHeight = true
                lblHeight.constant = getLabelHeight(text: lblDescription.text ?? "", width: view.bounds.width, font: lblDescription.font)
            }
        }else{
            
        }
        
        
    }

    func getLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let lbl = UILabel(frame: .zero)
        lbl.frame.size.width = width
        lbl.font = font
        lbl.numberOfLines = 0
        lbl.text = text
        lbl.sizeToFit()

        return lbl.frame.size.height
    }
}

//MARK:- TableView Delegate & Data Source Methods
extension ExpertDetailsViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expertDetailsArray.arrVideos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewExpertVideos.dequeueReusableCell(withIdentifier: "upNextTableCell") as! upNextTableCell
        let ent = expertDetailsArray.arrVideos[indexPath.row]
        cell.lblTitle.text = ent.strVideoTitle
        cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + (ent.strImagePath ?? "") + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgThumbnail.layer.cornerRadius = 10
        cell.viewContainer.layer.cornerRadius = 10
        cell.viewContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.viewContainer.layer.shadowOpacity = 0.7
        cell.viewContainer.layer.shadowRadius = 5.0
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ent = expertDetailsArray.arrVideos[indexPath.row]
        let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
        vc.strVideoID = "\(ent.intID ?? 0)"
        vc.strVideoTitle = ent.strVideoTitle ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension UILabel {
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
}
