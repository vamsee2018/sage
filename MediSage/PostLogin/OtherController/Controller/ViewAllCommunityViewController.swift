//
//  ViewAllCommunityViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 28/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class ViewAllCommunityViewController: BaseViewController {

    //MARK:- Custom Properties
    var cardiologyArray = [HomeEntity]()
    var ibfArray = [HomeEntity]()
    var urologyArray = [HomeEntity]()
    var familyHealthArray = [HomeEntity]()
    var communitiesArray = [HomeEntity]() //["CARDIOLOGY","IBF","UROLOGY","FAMILY HEALTH"]
    var memberID = 50
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewCommunities: UICollectionView!
   
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        getMyCommunityList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
         
    }
    
    //MARK:- API Call for Get My Community List
    func getMyCommunityList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MYCommunityList + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.communitiesArray = homeEntity.parseCommunity(info : data as! Dictionary<String, AnyObject>)
                    self.collViewCommunities.reloadData()
                    
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    
    
}

//MARK :- Collection View Delegate & Data Source Methods
extension ViewAllCommunityViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return communitiesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collViewCommunities.dequeueReusableCell(withReuseIdentifier: "communitiesCollectionCell", for: indexPath) as! communitiesCollectionCell
        
        let ent = communitiesArray[indexPath.item]
        cell.lblTitle.text = ent.strVideoTitle
        cell.containerView.layer.borderColor = UIColor.gray.cgColor
        cell.containerView.layer.borderWidth = 1.0
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize(width: (self.view.frame.size.width/2 - 5), height: 180)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}



