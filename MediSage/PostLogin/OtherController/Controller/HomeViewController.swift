//
//  HomeViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import AVKit
import AVFoundation
import Firebase
import FirebaseAnalytics

class HomeViewController: BaseViewController, CustomTabBarControllerDelegate {

    //MARK:- Custom Properties
    var bannerArray = [HomeEntity]()
    var trendingArray = [HomeEntity]()
    var expertsArray = [HomeEntity]()
    var newReleaseArray = [HomeEntity]()
    var journalArray = [HomeEntity]()
    var checkFlag = HomeEntity()
    
    var tableArray = [[HomeEntity]]()

    var memberID = 50
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()
    var refreshControl = UIRefreshControl()
    var userDefaults = UserDefaults.standard
    
    //MARK:- IBOutlet Connections
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var  collViewBanner: UICollectionView!
    @IBOutlet weak var collViewTrending: UICollectionView!
    @IBOutlet weak var collViewNewReleases: UICollectionView!
    @IBOutlet weak var collViewExperts: UICollectionView!
    @IBOutlet weak var collViewRecommend: UICollectionView!
    @IBOutlet weak var collViewJournal: UICollectionView!
    @IBOutlet weak var btnCommunity: UIButton!
    @IBOutlet weak var btnEvents: UIButton!
    @IBOutlet weak var btnNews: UIButton!
    @IBOutlet weak var viewInCompleteProfile: UIView!
    @IBOutlet weak var viewBanner: UIView!
    @IBOutlet weak var viewBannerTopConstraint: NSLayoutConstraint!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var pageControlBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewJournalHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var inCompleteProfileViewHeightConstraint: NSLayoutConstraint!
    
    // Table View Outlet
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var homeTableViewTopConstraint: NSLayoutConstraint!
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        if (userDefaults.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        NotificationCenter.default.addObserver(self, selector: #selector(notificationPressed(_ :)), name: .checkNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HandledDynamicLink(_ :)), name: .dynamicLink, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HandledNewsCount(_ :)), name: .newsArticleCount, object: nil)
        
        if userDefaults.bool(forKey: "First_Launch") == true{
            userDefaults.setValue(true, forKey: "First_Launch")
        }else{
            userDefaults.setValue(true, forKey: "First_Launch")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
               // self.setTipView()
            }
        }
        
        let view = UIView(frame: CGRect(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        view.backgroundColor = UIColor.green
        self.tabBarController?.tabBar.addSubview(view)
        registerTableViewCell()
        checkFlags()
        getBannerList()
        getExpertsList()
        getTrendingList()
        getNewReleaseList()
        getJournalList()
        startTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.btnShare.isHidden = false
        self.viwTop.imgShare.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        self.viwTop.btnShare.addTarget(self, action: #selector(btnInviteTapped), for: .touchUpInside)
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        self.tabBarController?.tabBar.isHidden = false

        Analytics.logEvent("Home", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Home Page",
            AnalyticsParameterContentType: "Home"
        ])
        
        //Pull to Refresh
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        scrollView.refreshControl = refreshControl
        scrollView.isHidden = true
        homeTableView.refreshControl = refreshControl
    }
    
    /*
    func setTipView(){
        let showcase = MaterialShowcase()
        showcase.setTargetView(view: collViewBanner) // always required to set targetView
        showcase.primaryText = "Banner"
        showcase.primaryTextSize = 18
        showcase.secondaryText = "Click to directly access popular pages"
        // showcase.backgroundAlpha = 0.9
        showcase.backgroundPromptColor = UIColor.init(hex: "464d47")
        showcase.shouldSetTintColor = false // It should be set to false when button uses image.
        
        
        let showcase2 = MaterialShowcase()
        showcase2.setTargetView(view: btnEvents) // always required to set targetView
        showcase2.primaryText = "My Events"
        showcase2.primaryTextSize = 18
        showcase2.secondaryText = "Attend Live webinars and digital conferences. Access recordings of missed webinars"
        showcase2.backgroundViewType = .circle // default is .circle
        showcase2.shouldSetTintColor = false // It should be set to false when button uses image.
        showcase2.backgroundPromptColor = UIColor.init(hex: "464d47")
        showcase2.isTapRecognizerForTargetView = true
        
        let showcase3 = MaterialShowcase()
        showcase3.setTargetView(view: btnNews) // always required to set targetView
        showcase3.primaryText = "News"
        showcase3.primaryTextSize = 18
        showcase3.secondaryText = "Get daily news from global medical journals. Hand picked for you."
        showcase3.backgroundViewType = .circle // default is .circle
        showcase3.shouldSetTintColor = false // It should be set to false when button uses image.
        showcase3.backgroundPromptColor = UIColor.init(hex: "464d47")
        showcase3.isTapRecognizerForTargetView = true
        
        let showcase4 = MaterialShowcase()
        showcase4.primaryText = "Search"
        showcase4.primaryTextSize = 18
        showcase4.secondaryText = "Search our database for articles, videos and experts"
        showcase4.setTargetView(tabBar: self.tabBarController!.tabBar, itemIndex: 3)
        showcase4.backgroundViewType = .circle // default is .circle
        showcase4.shouldSetTintColor = false // It should be set to false when button uses image.
        showcase4.backgroundPromptColor = UIColor.init(hex: "464d47")
        showcase4.isTapRecognizerForTargetView = true
        showcase4.targetTintColor = UIColor.clear
        // showcase4.targetHolderColor = UIColor.clear
        
        let showcase5 = MaterialShowcase()
        showcase5.primaryText = "Profile"
        showcase5.primaryTextSize = 18
        showcase5.secondaryText = "Customize your content. See all your CME credits in one place."
        showcase5.setTargetView(tabBar: self.tabBarController!.tabBar, itemIndex: 4)
        showcase5.backgroundViewType = .circle // default is .circle
        showcase5.shouldSetTintColor = false // It should be set to false when button uses image.
        showcase5.backgroundPromptColor = UIColor.init(hex: "464d47")
        showcase5.isTapRecognizerForTargetView = true
        showcase5.targetTintColor = UIColor.clear
        //showcase5.targetHolderColor = UIColor.clear
        
        showcase.delegate = self
        showcase2.delegate = self
        showcase3.delegate = self
        showcase4.delegate = self
        showcase5.delegate = self
        
        //Once the key value changes , it will appear once
        // sequence.temp(showcase).temp(showcase2).temp(showcase3).temp(showcase4).temp(showcase5).setKey(key: "temp").start()
        sequence.temp(showcase).temp(showcase2).temp(showcase3).temp(showcase4).temp(showcase5).start()
    }*/
    
    func registerTableViewCell() {
        // Register banner tableview cell
        self.homeTableView.register(UINib(nibName: BannerTableViewCell.className, bundle: nil), forCellReuseIdentifier: BannerTableViewCell.className)
        
        // Register sections tableview cell
        self.homeTableView.register(UINib(nibName: SectionTableViewCell.className, bundle: nil), forCellReuseIdentifier: SectionTableViewCell.className)
        
        // Register experts tableview cell
        self.homeTableView.register(UINib(nibName: ExpertTableViewCell.className, bundle: nil), forCellReuseIdentifier: ExpertTableViewCell.className)
        
        homeTableView.estimatedRowHeight = 230
        homeTableView.rowHeight = UITableView.automaticDimension
        homeTableView.separatorStyle = .none
        homeTableView.tableFooterView = UIView()
    }
    
    @objc func btnInviteTapped(){
        if let link = NSURL(string: "http://onelink.to/medisage")
        {
            let message = "I would like to invite you to the Medisage Doctor community, a single source for staying up-to-date through expert videos, news articles and case discussions"
            print(message)
           let objectsToShare = [message,link] as [Any]
           let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
           activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
           self.present(activityVC, animated: true, completion: nil)
        }
    }
    @objc func refresh()
    {
        // Code to refresh table view
        refreshControl.endRefreshing()
        checkFlags()
        getBannerList()
        getExpertsList()
        getTrendingList()
        getNewReleaseList()
        getJournalList()
        
    }
    
    @objc func notificationPressed(_ notification: Notification){
        
        if let dict = notification.userInfo as NSDictionary?{
            
            if dict["action"] as? String ?? "" == "video"{
                
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
                vc.strVideoID = dict["id"] as? String ?? ""
                // vc.strVideoTitle = userInfo["video"] as? String ?? ""
                 self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["action"] as? String ?? "" == "live_event"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
                let eventLink = dict["live_event_link"] as? String ?? ""
                let linkID = dict["id"] as? String ?? ""
                vc.strURL = "\(eventLink)\(strUserID)/\(linkID)"
                 self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["action"] as? String ?? "" == "news_article"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
                let linkID = dict["id"] as? String ?? ""
                vc.currentNewsID = Int(linkID) ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["action"] as? String ?? "" == "live_event_registration"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
                let linkID = dict["id"] as? String ?? ""
                vc.eventID = Int(linkID) ?? 0
                tabBarController?.selectedIndex = 2
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }
        
    }
    
    @objc func HandledDynamicLink(_ notification: Notification){
        
        if let dict = notification.userInfo as NSDictionary? {
            if dict["video"] as? String ?? "" == "video"{
                
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
                vc.strVideoID = dict["videoID"] as? String ?? ""
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["news"] as? String ?? "" == "news_article"{
                
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
                vc.currentNewsID = dict["articleID"] as? Int ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if dict["series"] as? String ?? "" == "series"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.SeriesViewController) as! SeriesViewController
                let linkID = dict["seriesID"] as? String ?? ""
                vc.seriesID = Int(linkID)
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else if dict["live_event"] as? String ?? "" == "live_event"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
                tabBarController?.selectedIndex = 2
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    @objc func HandledNewsCount(_ notification: Notification){
        
        checkFlags()
        if let dict = notification.userInfo as NSDictionary? {
            if dict["index"] as? String ?? "" == "1"{
                self.btnNews.setImage(UIImage(named: "News-Icon-1"), for: .normal)
                
            }else{
                self.btnNews.setImage(UIImage(named: "News-Icon-(302-x-302)"), for: .normal)
                
            }
        }
        
    }
     
    //MARK: Timer for Gallery
    func startTimer() {
        _ =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }


    @objc func scrollAutomatically(_ timer1: Timer) {
        self.pageControl.currentPage = bannerArray.count
        if let coll  = collViewBanner {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)! < bannerArray.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)

                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }

            }
        }
    }
    
    //MARK:- Get Country API Call
    func getBannerList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()

        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.BannerList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.bannerArray = loginEntity.parseHomeBannerList(info : data as! Dictionary<String, AnyObject>)
                    self.pageControl.numberOfPages = self.bannerArray.count
                    self.collViewBanner.reloadData()
                    self.tableArray.append(self.bannerArray)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Check Profile Update, News Article Added or Not and Event in buffer time or not
    func checkFlags(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()

        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CheckHomePageFlag + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                   self.checkFlag = loginEntity.checkFlag(withInfo : data as? Dictionary<String, AnyObject>)
                   
                    if self.checkFlag.isNewsAdded ?? 0 > 0{
                        self.btnNews.setImage(UIImage(named: "News-Icon-1"), for: .normal)
                    }else{
                        self.btnNews.setImage(UIImage(named: "News-Icon-(302-x-302)"), for: .normal)
                    }
                    if self.checkFlag.isLiveEventStart == true{
                        
                        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "icon-screen-2", withExtension: "gif")!)
                        let advTimeGif = UIImage.sd_image(withGIFData: imageData)
                        self.btnEvents.setImage(advTimeGif, for: .normal)
                    }else{
                        self.btnEvents.setImage(UIImage(named: "MY EVENT icon-26"), for: .normal)
                        
                    }
                    
                    if self.checkFlag.isProfileIncomplete == true {
                        self.homeTableViewTopConstraint.constant = 40
                        self.viewInCompleteProfile.isHidden = false
                        self.inCompleteProfileViewHeightConstraint.constant = 40
                        self.viewBannerTopConstraint.constant = 40
                        self.pageControlBottomConstraint.constant = 20
                    } else {
                        self.homeTableViewTopConstraint.constant = 0
                        self.viewInCompleteProfile.isHidden = true
                        self.inCompleteProfileViewHeightConstraint.constant = 0
                        self.viewBannerTopConstraint.constant = 0
                        self.pageControlBottomConstraint.constant = 30

                    }
                    if self.checkFlag.isNotification == true{
                        
                        self.viwTop.viwNotification.layer.cornerRadius = 5
                        self.viwTop.viwNotification.isHidden = false
                    }else{
                        self.viwTop.viwNotification.layer.cornerRadius = 5
                        self.viwTop.viwNotification.isHidden = true
                    }

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Country API Call
    func getTrendingList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()

        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.Video + strUserID + "/" + KJUrlConstants.TrendingList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.trendingArray = loginEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewTrending.reloadData()
                    self.tableArray.append(self.trendingArray)
                    self.homeTableView.reloadData()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    //MARK:- Get Country API Call
    func getExpertsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        DispatchQueue.main.async {
            self.showLoader()
        }
        let loginEntity = HomeEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.ExpertsList, strParameterName:"") { (result, isSuccess, error) in
            DispatchQueue.main.async {
            self.hideLoader()
            }
            if isSuccess
            {
                if let data = result
                {
                    self.expertsArray = loginEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewExperts.reloadData()
                    self.tableArray.append(self.expertsArray)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Country API Call
    func getNewReleaseList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()

        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.Video + strUserID + "/" + KJUrlConstants.NewReleasesList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.newReleaseArray = loginEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewNewReleases.reloadData()

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Country API Call
    func getJournalList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.JournalList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.journalArray = loginEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewJournal.reloadData()
                    self.setUI()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func setUI(){
        if self.journalArray.count > 0{
            collViewJournalHeightConstraint.constant = 230
        }else{
            collViewJournalHeightConstraint.constant = 0
        }
    }
    
    func onTabSelected(isTheSame: Bool) {
        let sb = UIStoryboard(name: KJViewIdentifier.Profile, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ProfileViewController) as! ProfileViewController
         tabBarController?.selectedIndex = 4
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- Button Actions
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        onTabSelected(isTheSame: true)
    }
    @IBAction func btnCasesListTapped(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyCommunityViewController) as! MyCommunityViewController
        let sb = UIStoryboard(name: KJViewIdentifier.CasesViewController, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.CasesViewController) as! CasesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnEventsTapped(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
        tabBarController?.selectedIndex = 2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNewsTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnExpertsViewAllTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyExpertsViewController) as! MyExpertsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
        HamburgerMenu().triggerSideMenu()
    }
    
    @objc func hideHamburger(){
        HamburgerMenu().closeSideMenu()
    }
    

}

//MARK :- Collection View Delegate & Data Source Methods
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

       if collectionView == self.collViewBanner {
        return bannerArray.count
       }
       else if collectionView == self.collViewTrending {
        return trendingArray.count
       }
      else if collectionView == self.collViewNewReleases{
        return newReleaseArray.count //smallTopChartArray.count
       }
       else if collectionView == self.collViewExperts {
        return expertsArray.count //weddingCollectionArray.count
       }
      // else if collectionView == self.collViewRecommend {
    //    return 5 //designerChoiceArray.count
    //   }
       else if collectionView == self.collViewJournal{
        return journalArray.count //topPickArray.count
       }
       else{
             return 0
       }
   }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collViewBanner{
            let cell = collViewBanner.dequeueReusableCell(withReuseIdentifier: "bannerCollectionCell", for: indexPath) as! bannerCollectionCell
            
            let ent = bannerArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imgThumbnail.layer.cornerRadius = 10
            cell.imgThumbnail.contentMode = .scaleToFill
            return cell
            
        }
        else if collectionView == collViewTrending{
            let cell = collViewTrending.dequeueReusableCell(withReuseIdentifier: "trendingCollectionCell", for: indexPath) as! trendingCollectionCell
            let ent = trendingArray[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imgThumbnail.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
           // cell.containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.gray.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.containerView.layer.shadowOpacity = 0.2
            cell.containerView.layer.shadowRadius = 7.0
            
            return cell
            
        }
        else if collectionView == collViewNewReleases{
            
            let cell = collViewNewReleases.dequeueReusableCell(withReuseIdentifier: "newReleasesCollectionCell", for: indexPath) as! newReleasesCollectionCell
            let ent = newReleaseArray[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imgThumbnail.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            //cell.containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.gray.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.containerView.layer.shadowOpacity = 0.2
            cell.containerView.layer.shadowRadius = 7.0
            return cell
            
            
        }
        else if collectionView == collViewExperts{
            
            let cell = collViewExperts.dequeueReusableCell(withReuseIdentifier: "expertsCollectionCell", for: indexPath) as! expertsCollectionCell
            let ent = expertsArray[indexPath.item]
            cell.lblTitle.text = ent.strDoctorName
            cell.imgExpert.layer.masksToBounds = true
            cell.imgExpert.layer.cornerRadius = 50
            cell.imgExpert.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
            cell.imgExpert.layer.borderWidth = 1.0
            cell.imgExpert.layer.shadowColor = UIColor.black.cgColor
            cell.imgExpert.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.imgExpert.layer.shadowOpacity = 0.3
            cell.imgExpert.layer.shadowRadius = 7.0
            cell.imgExpert.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            return cell
            
        }
            /*else if collectionView == collViewRecommend{
             let cell = collViewRecommend.dequeueReusableCell(withReuseIdentifier: "recommendCollectionCell", for: indexPath) as! recommendCollectionCell
             
             
             cell.containerView.layer.cornerRadius = 10
             cell.containerView.layer.shadowColor = UIColor.gray.cgColor
             cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
             cell.containerView.layer.shadowOpacity = 0.3
             cell.containerView.layer.shadowRadius = 5.0
             
             return cell
             
             }*/
        else {
            let cell = collViewJournal.dequeueReusableCell(withReuseIdentifier: "journalCollectionCell", for: indexPath) as! journalCollectionCell
            let ent = journalArray[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strPath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imgThumbnail.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
          //  cell.containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.gray.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            cell.containerView.layer.shadowRadius = 7.0
            cell.containerView.layer.shadowOpacity = 0.2
            
            return cell
        }
        
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collViewTrending || collectionView == collViewNewReleases || collectionView == collViewJournal{
            
            return CGSize(width: 172, height: 172)
            
        }else if collectionView == collViewBanner{
            return CGSize(width: collectionView.frame.size.width, height: 165)
        }
        else {
            return CGSize(width: 140, height: 140)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collViewBanner {
            
            let ent = bannerArray[indexPath.row]

            if ent.strActionType == "video"{
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
                vc.strVideoID = ent.strActionID ?? ""
                vc.strVideoTitle = ent.strVideoTitle ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }else if ent.strActionType == "live_event"{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
                tabBarController?.selectedIndex = 2
                vc.eventLinkID = ent.strActionID ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
//                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
//                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
//                vc.strURL = "\(ent.strActiveEventLink ?? "")\(strUserID)/\(ent.strActionID ?? "")"
//                self.navigationController?.pushViewController(vc, animated: true)
            }else if ent.strActionType == "news_article"{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
                vc.currentNewsID = Int(ent.strActionID ?? "") ?? 0 
                self.navigationController?.pushViewController(vc, animated: true)
            }else if ent.strActionType == "partner_division"{
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PartnerAreasViewController) as! PartnerAreasViewController
                vc.divisionID = Int(ent.strActionID ?? "") ?? 0 
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else if collectionView == collViewTrending{
            let ent = trendingArray[indexPath.item]
            
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            vc.strVideoID = "\(ent.strVideoID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID: memberID,
                AnalyticsParameterItemName: ent.strVideoTitle!,
                AnalyticsParameterContentType: "Trending"
            ])
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else if collectionView == collViewNewReleases{
            let ent = newReleaseArray[indexPath.item]
            
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            vc.strVideoID = "\(ent.strVideoID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else if collectionView == collViewExperts{
            let ent = expertsArray[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
            vc.expertID = ent.strVideoID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collViewJournal{
            let ent = self.journalArray[indexPath.item]
            
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
            vc.strURL = KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strFileName ?? "")"
            
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
        }
        
    }
    
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.pageControl.currentPage = bannerArray.count
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        self.pageControl?.currentPage = Int(roundedIndex)
    }
   
}


//MARK:- Custom CollectionView Cell Classes
class bannerCollectionCell: UICollectionViewCell{
    @IBOutlet weak var imgThumbnail: UIImageView!
}

class trendingCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}

class newReleasesCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class expertsCollectionCell: UICollectionViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgExpert: UIImageView!
}
class recommendCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class journalCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: CardView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
