//
//  PDFViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 30/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import WebKit

class PDFViewController: BaseViewController, WKNavigationDelegate{

    //MARK:- declaring variable
    var strURL: String?
    var webView: WKWebView!
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("URL------>>>>>>>>",strURL ?? "")
        
        edgesForExtendedLayout = []
        let webview = WKWebView(frame: UIScreen.main.bounds)
        webview.navigationDelegate = self
        
        view.addSubview(webview)
        guard let urlString = strURL, // forced unwrapped
            let url = URL(string: urlString)
            else {  return }  // if there is any optional we return
        // else continue
        let request: URLRequest = URLRequest(url: url)
        webview.load(request)
        self.tabBarController?.tabBar.isHidden = true
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
    }
    
    //MARK:- webview delegate
    func webViewDidStartLoad(_ webView: UIWebView) {
         GlobalClass.sharedInstance.activity()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        GlobalClass.sharedInstance.removeActivity()
    }

}
