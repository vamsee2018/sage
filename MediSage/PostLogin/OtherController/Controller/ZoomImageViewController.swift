//
//  ZoomImageViewController.swift
//  MediSage
//
//  Created by Harshad Medisage on 03/02/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ZoomImageViewController: BaseViewController,UIScrollViewDelegate {

    @IBOutlet weak var scrollView: ImageScrollView!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var viewPlayingVideo: UIView!
    @IBOutlet weak var imgVideoThumbnail: UIImageView!
    @IBOutlet weak var videoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgEventHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgEventWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!

    var player: AVPlayer?
    var strBannerImagePath: String?
    var strVimeoID: String?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
      //  imgEvent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/live_event_banner_image/" + "\(strBannerImagePath ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        
        self.tabBarController?.tabBar.isHidden = true

        
        if strVimeoID == "0"{
        
            scrollView.setup()
            scrollView.imageContentMode = .aspectFit
            scrollView.initialOffset = .center
            scrollView.imageScrollViewDelegate = self

           // let myImage = UIImage(named: strImagePath)
            let url = URL(string: KJUrlConstants.ImageUrl + "/storage/images/live_event_banner_image/" + "\(strBannerImagePath ?? "")")
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                let image = UIImage(data: imageData)
                scrollView.display(image: image!)

            }
            
        }else{
            
            videoHeightConstraint.constant = 180

            scrollView.setup()
            scrollView.imageContentMode = .aspectFit
            scrollView.initialOffset = .center
            scrollView.imageScrollViewDelegate = self

           // let myImage = UIImage(named: strImagePath)
            let url = URL(string: KJUrlConstants.ImageUrl + "/storage/images/live_event_banner_image/" + "\(strBannerImagePath ?? "")")
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                let image = UIImage(data: imageData)
                scrollView.display(image: image!)

            }
            
            PLAYVIDEOAPI(key: strVimeoID ?? "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
    }
    
    
    //MARK:- Play Video
    func PLAYVIDEOAPI(key: String){
        ApicallWebservices.SharedInstance.VIMEOVIDEOApiCallwithHeader(methodname: "https://api.vimeo.com/videos/\(key)", type: .get, header: ["Content-Type": "application/json", "Authorization": "bearer 75d0309b580962afaab71aa018e17877"], parameter: [:], strLoaderString: "", completion: {
            (error: Error?, success: Bool, result: Any?) in
            if success == true {
                let dicData = result as? NSDictionary ?? [:]
                let filesarr = dicData.object(forKey: "files") as? NSArray ?? []
                if filesarr.count > 0 {
                    let fileurl = filesarr[0] as? NSDictionary ?? [:]
                    let urlFile = fileurl.object(forKey: "link") as? String ?? ""
                    print(urlFile)
                    DispatchQueue.main.async() {
                        self.player = AVPlayer(url: URL(string: "\(urlFile)")!)
                        
                      //  self.createThumbnailOfVideoFromRemoteUrl(url: urlFile)
                        
                        let playerController = AVPlayerViewController()
                        playerController.player = self.player
                        playerController.view.frame = self.viewPlayingVideo.bounds
                        self.addChild(playerController)
                        self.viewPlayingVideo.addSubview(playerController.view)
                        self.player?.play()
                        
                    }
                }else {
                    print("....")
                    self.view.makeToast(message: "Video is no longer...")
                }
                
            }else{
                print("....")
                self.view.makeToast(message: "Video link is not provided...")
            }
        })
    }
}

extension ZoomImageViewController: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}
