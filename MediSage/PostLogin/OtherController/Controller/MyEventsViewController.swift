//
//  MyEventsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import Firebase
import FirebaseAnalytics

class MyEventsViewController: BaseViewController {

    //MARK:- Custom Properties
    var eventArray = HomeEntity()
    var selectedButton = "Upcoming"
    var eventTitle = ""
    var eventTime = ""
    var inviteEventTime = ""
    var inviteEventDate = ""
    var currentTime = ""
    var bufferTime = ""
    var eventID = 0
    var userData = [String:Any]()
    var strUserID  = String()
    let eventStore = EKEventStore()
    var eventDate = Date()
    var isFirstTime = false
    var addToCalenderCount = 0
    var eventLinkID = ""
    var linkID = ""

    let userCalendar = Calendar.current
    let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
    let dateFormatter = DateFormatter()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblViewEvents: UITableView!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnPast: UIButton!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToHome), name: NSNotification.Name("moveToHome"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToItem), name: NSNotification.Name("scrollToIndexPath"), object: nil)

        linkID = eventLinkID
        
        getMyEventsList()
        
        btnUpcoming.backgroundColor = UIColor.init(hex: "001D4D")
        btnUpcoming.setTitleColor(UIColor.white, for: .normal)
        btnUpcoming.layer.cornerRadius = 10
        btnUpcoming.layer.borderColor = UIColor.init(hex: "001D4D").cgColor
        btnUpcoming.layer.borderWidth = 1.0
        btnPast.layer.cornerRadius = 10
        btnPast.layer.borderColor = UIColor.darkGray.cgColor
        btnPast.layer.borderWidth = 1.0
        
        
        // Set automatic dimensions for row height
        // Swift 4.2 onwards
        tblViewEvents.rowHeight = UITableView.automaticDimension
        tblViewEvents.estimatedRowHeight = 320
       
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("My Events", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "My Events Page",
            AnalyticsParameterContentType: "My Events"
        ])
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.tabBarController?.tabBar.isHidden = false

       // self.viwTop.btnMore.addTarget(self, action: #selector(notificationBtnClicked), for: .touchUpInside)
        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
        getMyEventsList()

        // let popTip = PopTip()
        //  popTip.show(text: "Hey! Listen!", direction: .down, maxWidth: 200, in: view, from: btnCommunity.frame)
        
    }
    
    @objc func moveToHome(){
        tabBarController?.selectedIndex = 0
        navigationController?.popToViewController(ofClass: HomeViewController.self)
    }
    @objc func scrollToItem(){
        getMyEventsList()
    }
    //MARK:- API Call for Get My Events List
    func getMyEventsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MyEvents + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.eventArray.upcomingArr.removeAll()
                    self.eventArray.pastArr.removeAll()
                    self.eventArray = homeEntity.parseEventList(info : data as! Dictionary<String, AnyObject>)
                    self.tblViewEvents.reloadData()

                    //self.scrollToSpecificIndex(2)

                    for (index, element) in self.eventArray.upcomingArr.enumerated() {
                      print("Item \(index): \(element)")
                        print("EVENT ID \(index): \(element.intEventID ?? 0)")
                       // self.tblViewEvents.reloadData()

                        if element.strLinkID == self.linkID{
                            self.scrollToSpecificIndex(index)
                        }else if element.intEventID == self.eventID{
                           // self.tblViewEvents.reloadData()
                            self.scrollToSpecificIndex(index)
                        }
                    }
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- API Call for Register for Events
    func registerEvent(eventID: String){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let searchEntity = HomeEntity()
        let strParam : [String:Any] = ["event_id": eventID ,"member_id": strUserID]
        self.showLoader()
        requestManager.requestCommonPostMethod(strAPIName: KJUrlConstants.RegisterEvent , strParameterName: strParam) { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.eventArray = searchEntity.parseRegisterEvent(info : (data as? Dictionary<String, AnyObject>)!)
                    self.addEventToCalendar()
                    self.getMyEventsList()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Add Event to Calender
    func addEventToCalendar() {
    
        if addToCalenderCount == 1{
            switch EKEventStore.authorizationStatus(for: .event) {
            case .authorized:
                print("Access Granted")
            case .denied:
                print("Access denied")
            case .notDetermined:
                // 3
                eventStore.requestAccess(to: .event, completion:
                    {[weak self] (granted: Bool, error: Error?) -> Void in
                        if granted {
                            print("Access Granted")
                        } else {
                            print("Access denied")
                        }
                })
            default:
                print("Case default")
            }
            
        }else{
            eventStore.requestAccess( to: EKEntityType.event, completion:{(granted, error) in
                DispatchQueue.main.async {
                    if (granted) && (error == nil) {
                        let event = EKEvent(eventStore: self.eventStore)
                       // let startDate = Date()
                        let endDate = self.eventDate.addingTimeInterval(3 * 60 * 60)
                        event.title = self.eventTitle
                        event.startDate = self.eventDate
                        event.endDate = endDate
                        let eventController = EKEventEditViewController()
                        eventController.event = event
                        eventController.eventStore = self.eventStore
                        eventController.editViewDelegate = self
                        self.present(eventController, animated: true, completion: nil)
                        
                    }
                }
            })
        }
        
        
    }

    //MARK:- Check Coming from Home Banner
    func scrollToSpecificIndex(_ targetRow: Int){
        let indexPath = IndexPath(row: targetRow, section: 0)
       // if let _ = self.tblViewEvents.cellForRow(at: indexPath) {
         self.tblViewEvents.scrollToRow(at: indexPath, at: .bottom, animated: true)
     //   }
        
       
    }
    
    
    
    //MARK:- Button Actions
    @IBAction func btnUpcomingTapped(_ sender: UIButton) {
        btnPast.layer.borderColor = UIColor.darkGray.cgColor
        btnPast.layer.borderWidth = 1.0
        btnPast.setTitleColor(UIColor.darkGray, for: .normal)
        btnUpcoming.layer.borderColor = UIColor.init(hex: "001D4D").cgColor
        btnUpcoming.layer.borderWidth = 1.0
        
        btnUpcoming.backgroundColor = UIColor.init(hex: "001D4D")
        btnUpcoming.setTitleColor(UIColor.white, for: .normal)
        btnPast.backgroundColor = UIColor.white
        
        selectedButton = "Upcoming"
        getMyEventsList()

    }
    
    @IBAction func btnPastTapped(_ sender: UIButton) {
       btnUpcoming.layer.borderColor = UIColor.darkGray.cgColor
       btnUpcoming.layer.borderWidth = 1.0
       btnUpcoming.setTitleColor(UIColor.darkGray, for: .normal)
       btnPast.layer.borderColor = UIColor.init(hex: "001D4D").cgColor
       btnPast.layer.borderWidth = 1.0
       btnPast.backgroundColor = UIColor.init(hex: "001D4D")
       btnPast.setTitleColor(UIColor.white, for: .normal)
       btnUpcoming.backgroundColor = UIColor.white
       selectedButton = "past"
       getMyEventsList()

    }

    //MARK:- Fucnction for Parallelogram shape to Imageviews
    func getParallelogram(width: CGFloat, height: CGFloat, radius: CGFloat) -> CGPath {
        // Points of the parallelogram
        let points = [
            CGPoint(x: width * 0.05, y: 0),
            CGPoint(x: width , y: 0),
            CGPoint(x: width - width * 0.05, y: height),
            CGPoint(x: 0, y: height)
        ]

        let point1 = points[0]
        let point2 = points[1]
        let point3 = points[2]
        let point4 = points[3]

        let path = CGMutablePath()

        path.move(to: point1)
        path.addArc(tangent1End: point2, tangent2End: point3, radius: radius)
        path.addArc(tangent1End: point3, tangent2End: point4, radius: radius)
        path.addArc(tangent1End: point4, tangent2End: point1, radius: radius)
        path.addArc(tangent1End: point1, tangent2End: point2, radius: radius)
        return path
    }
    
    
}

extension MyEventsViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedButton == "Upcoming"{
            return eventArray.upcomingArr.count
        }else if selectedButton == "past"{
            return eventArray.pastArr.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewEvents.dequeueReusableCell(withIdentifier: "eventsTableViewCell") as! eventsTableViewCell
    
        if selectedButton == "Upcoming"{
            let ent = eventArray.upcomingArr[indexPath.row]
            cell.lblEventTitle.text = ent.strVideoTitle
            cell.lblEventPartner.text = ent.strPartnerName
            cell.lblEventPartner.sizeToFit()
            if ent.isRegistered == true{
                cell.btnRegisterNow.setTitle("Registered", for: .normal)
                cell.btnRegisterNow.backgroundColor = .green
            }else{
                cell.btnRegisterNow.setTitle("Register Now", for: .normal)
                cell.btnRegisterNow.backgroundColor = UIColor.init(hex: "3167B0")

            }
            cell.btnAddToCalender.setTitle("Details", for: .normal)
            cell.btnRegisterNow.isMultipleTouchEnabled = false
            cell.btnRegisterNow.tag = indexPath.row
            cell.btnAddToCalender.tag = indexPath.row
            cell.btnInvite.tag = indexPath.row
            cell.btnInvite.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
            cell.btnInvite.layer.borderWidth = 1.0
            cell.btnInvite.layer.cornerRadius = 10

            cell.btnRegisterNow.layer.cornerRadius = 10
            cell.btnAddToCalender.layer.cornerRadius = 10
            cell.btnAddToCalender.isHidden = false
            cell.btnRegisterNow.addTarget(self, action:#selector(upcomingEventRegister(sender:)), for: .touchUpInside)
            cell.btnAddToCalender.addTarget(self, action:#selector(upcomingEventAddCalender(sender:)), for: .touchUpInside)
            cell.btnInvite.addTarget(self, action:#selector(btnInviteTapped(sender:)), for: .touchUpInside)
            
            cell.imgEvent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/mcat/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imgEvent.layer.cornerRadius = 10
            let shape = CAShapeLayer()
            shape.path = getParallelogram(width: 180, height: 140, radius: 10)
            shape.position = CGPoint(x: cell.imgEvent.bounds.size.width, y: cell.imgEvent.bounds.size.height)
            cell.imgEvent.layer.addSublayer(shape)

            let dateString = ent.strEventTime
            let dateFormatter = DateFormatter(format: "yyyy.MM.dd HH:mm:ss")

            cell.lblEventDate.text = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "dd")
            cell.lblEventMonth.text = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "MMMM")
            cell.lblEventTime.text =  (dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm") ?? "") + " hrs(IST)"
            self.inviteEventTime = (dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm") ?? "") + " hrs(IST)"
            self.inviteEventDate = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "dd MMMM yy") ?? ""
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.gray.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.containerView.layer.shadowOpacity = 0.7
            cell.containerView.layer.shadowRadius = 2.0
            
            
            print(Date.getCurrentDate())
            print(Date.getCurrenTime())
            
            let eventDate =  dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "yyyy-MM-dd")
            let currentDate = Date.getCurrentDate().toDateString(dateFormatter: dateFormatter, outputFormat: "yyyy-MM-dd")
           
            if eventDate == currentDate{
                
               // if Date.getCurrenTime()
                
                let endFormatter = DateFormatter()
                endFormatter.dateFormat = "yyyy-MM-dd H:mm:ss"
                let date = endFormatter.date(from: ent.strEventTime ?? "")
                print("Event time-->",date ?? "")
                
                //minus buffet miniuts
                let subtractMinutes = (date?.addingTimeInterval(TimeInterval(-(ent.intBufferTime ?? 0)*60)))!
                endFormatter.dateFormat = "yyyy-MM-dd H:mm:ss"
                let subtractTime = endFormatter.string(from: subtractMinutes)
                print("Buffer time-->",subtractTime)
                
                let currentTime = Date.getCurrentDate().toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm:ss")
                print("Current time-->",currentTime ?? "")
                let eventTime =  subtractTime.toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm:ss")
                print("Event time-->",eventTime ?? "")
                
                let countDown = (subtractMinutes.addingTimeInterval(TimeInterval(180*60)))
                endFormatter.dateFormat = "yyyy-MM-dd H:mm:ss"
                let finalTime = endFormatter.string(from: countDown)
                print("Final time-->",finalTime)
                
               
                if eventTime ?? "" < currentTime ?? "" {
                    cell.btnAddToCalender.isHidden = true
                    cell.btnRegisterNow.setTitle("Watch Now", for: .normal)
                    cell.btnRegisterNow.backgroundColor = .red
                    cell.btnRegisterNow.addTarget(self, action:#selector(liveEventWatchNowTapped(sender:)), for: .touchUpInside)
                }else{
                    if ent.isRegistered == true{
                        cell.btnRegisterNow.setTitle("Registered", for: .normal)
                        cell.btnRegisterNow.backgroundColor = .green
                    }else{
                        cell.btnRegisterNow.setTitle("Register Now", for: .normal)
                        cell.btnRegisterNow.backgroundColor = UIColor.init(hex: "3167B0")
                        cell.btnAddToCalender.isHidden = false
                        cell.btnRegisterNow.addTarget(self, action:#selector(upcomingEventRegister(sender:)), for: .touchUpInside)
                        cell.btnAddToCalender.addTarget(self, action:#selector(upcomingEventAddCalender(sender:)), for: .touchUpInside)
                    }
                   
                }
            }
            return cell
           
        }else if selectedButton == "past"{
            let ent = eventArray.pastArr[indexPath.row]
            cell.lblEventTitle.text = ent.strVideoTitle
            cell.lblEventPartner.text = ent.strPartnerName
           
            cell.btnRegisterNow.setTitle("Watch Now", for: .normal)
            cell.btnRegisterNow.backgroundColor = UIColor.init(hex: "3167B0")
            cell.btnRegisterNow.layer.cornerRadius = 10
            cell.btnInvite.layer.cornerRadius = 10
            cell.btnInvite.tag = indexPath.row
            cell.btnInvite.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
            cell.btnInvite.layer.borderWidth = 1.0
            cell.btnAddToCalender.isHidden = true
            cell.imgEvent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/mcat/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.imgEvent.layer.cornerRadius = 10
            let shape = CAShapeLayer()
            shape.path = getParallelogram(width: 180, height: 140, radius: 10)
            shape.position = CGPoint(x: cell.imgEvent.bounds.size.width, y: cell.imgEvent.bounds.size.height)
            cell.imgEvent.layer.addSublayer(shape)
           
            cell.btnRegisterNow.tag = indexPath.row
            cell.btnRegisterNow.addTarget(self, action:#selector(pastEventWatchNowTapped(sender:)), for: .touchUpInside)

            
            let dateString = ent.strEventTime
            let dateFormatter = DateFormatter(format: "yyyy.MM.dd HH:mm:ss")
            cell.lblEventDate.text = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "dd")
            cell.lblEventMonth.text = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "MMMM")
            cell.lblEventTime.text =  (dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm") ?? "") + " hrs(IST)"
            self.inviteEventTime = (dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "HH:mm") ?? "") + " hrs(IST)"
            self.inviteEventDate = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "dd MMMM yy") ?? ""
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.gray.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.containerView.layer.shadowOpacity = 0.7
            cell.containerView.layer.shadowRadius = 2.0
            
            return cell
        }else{
            return UITableViewCell()
        }
    
        //return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    
    
    //MARK:- Event Register Now
    @objc func upcomingEventRegister(sender: UIButton){
        if selectedButton == "Upcoming"{
            let ent = eventArray.upcomingArr[sender.tag]

            if ent.isRegistered == true{
            }else{
                let dateString = ent.strEventTime
                let dateFormatter = DateFormatter(format: "yyyy.MM.dd HH:mm:ss")
                let eventDateTime = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "d, MMM yyyy HH:mm")
                eventDate = (dateString?.toDate(dateFormatter: dateFormatter))!
                eventTime = eventDateTime ?? ""
                eventTitle = ent.strEventTitle ?? ""
                addToCalenderCount += 1
                 
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.registerEvent(eventID: "\(ent.intEventID ?? 0)")
                }

            }
        }else{
        }
       
    }
    
    //MARK:- Watch live event now
    @objc func liveEventWatchNowTapped(sender: UIButton){

        let ent = eventArray.upcomingArr[sender.tag]
        let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
        vc.strURL = (ent.strActiveEventLink ?? "") + "\(strUserID)" + "/" + "\(ent.strLinkID ?? "")"
    
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    //MARK:- Watch past event now
    @objc func pastEventWatchNowTapped(sender: UIButton){
        
        if selectedButton == "Upcoming"{
            
        }else{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = eventArray.pastArr[sender.tag]
            vc.strEventID = "\(ent.strVideoID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            vc.isComingFromPastEvent = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //MARK:- Add to Calender
    @objc func upcomingEventAddCalender(sender:UIButton){
        let ent = eventArray.upcomingArr[sender.tag]

        if ent.intID == nil{
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventPreviewViewController) as! MyEventPreviewViewController
            vc.strBannerImagePath = ent.strBannerImage
            vc.strVimeoID = "\(ent.intID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ZoomImageViewController) as! ZoomImageViewController
            vc.strBannerImagePath = ent.strBannerImage
            vc.strVimeoID = "\(ent.intID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
       

      //  vc.modalPresentationStyle = .overFullScreen
       // self.present(vc, animated: true, completion: nil)
        
//        let ent = eventArray.upcomingArr[sender.tag]
//        let dateString = ent.strEventTime
//        let dateFormatter = DateFormatter(format: "yyyy.MM.dd HH:mm:ss")
//        let eventDateTime = dateString?.toDateString(dateFormatter: dateFormatter, outputFormat: "d, MMM yyyy HH:mm")
//        eventDate = (dateString?.toDate(dateFormatter: dateFormatter))!
//        eventTime = eventDateTime ?? ""
//        eventTitle = ent.strEventTitle ?? ""
//        addToCalenderCount += 1
//        addEventToCalendar()
    }
    //MARK:- Add to Calender
    @objc func btnInviteTapped(sender:UIButton){
        
        if selectedButton == "Upcoming"{
            let ent = eventArray.upcomingArr[sender.tag]
            let message = "\("I am attending") \"\(ent.strVideoTitle ?? "")\" \("on") \(self.inviteEventDate) \("at") \(self.inviteEventTime). \("It should be an informative session. You should join too!")"
            
            var comp = URLComponents()
            comp.scheme = "http"
            comp.host = "mymedisage.com/elearning"
            comp.path = "/live_event/\(ent.intEventID ?? 0)/\(ent.intEventID ?? 0)"
            let imgURL = KJUrlConstants.ImageUrl + "storage/images/mcat/" + "\(ent.strImageName ?? "")", _ = UIImage(named:"placeHolder")
            
            self.shareDynamicLink(CurrentVC: self, urlComp: comp, strTitle: message , strID: "" , imgUrl: imgURL )

        }else{
            let ent = eventArray.pastArr[sender.tag]
            let message = "\("I am attending") \"\(ent.strVideoTitle ?? "")\" \("on") \(self.inviteEventDate) \("at") \(self.inviteEventTime). \("It should be an informative session. You should join too!")"
            
            var comp = URLComponents()
            comp.scheme = "http"
            comp.host = "mymedisage.com/elearning"
            comp.path = "/live_event/\(ent.intEventID ?? 0)/\(ent.intEventID ?? 0)"
            let imgURL = KJUrlConstants.ImageUrl + "storage/images/mcat/" + "\(ent.strImageName ?? "")", _ = UIImage(named:"placeHolder")
            
            self.shareDynamicLink(CurrentVC: self, urlComp: comp, strTitle: message , strID: "" , imgUrl: imgURL )

        }
        
    }
}


class eventsTableViewCell : UITableViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblEventMonth: UILabel!
    @IBOutlet weak var lblEventTitle: UILabel!
    @IBOutlet weak var lblEventTime: UILabel!
    @IBOutlet weak var lblEventPartner: UILabel!
    @IBOutlet weak var btnRegisterNow: UIButton!
    @IBOutlet weak var btnAddToCalender: UIButton!
    @IBOutlet weak var btnInvite: UIButton!


}

extension MyEventsViewController: EKEventEditViewDelegate {

    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        controller.dismiss(animated: true, completion: nil)

    }
}

extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        return dateFormatter.string(from: Date())

    }
    
    static func getCurrenTime() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "HH:mm"

        return dateFormatter.string(from: Date())

    }
}

extension UIView {
    func blink(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, alpha: CGFloat = 0.0) {
        UIView.animate(withDuration: duration, delay: delay, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {
            self.alpha = alpha
        })
    }
}

extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
