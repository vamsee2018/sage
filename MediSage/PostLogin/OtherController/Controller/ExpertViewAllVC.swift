//
//  ExpertViewAllVC.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
class ExpertViewAllVC: BaseViewController {

    //MARK:- Custom Properties
    var newlyAddedArray = [HomeEntity]()
    var dataArray = [HomeEntity]()
    var typeWiseExpert = ""
    var memberID = "50"
    var pageNumber = 1
    var currentPage = 1
    var pageLimit = 1
    var expertType = "all"
    var userData = [String:Any]()
    var strUserID  = String()
    var footerAnimation = Bool()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewNewlyAdded: UICollectionView!
   
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        collViewNewlyAdded.loadControl = UILoadControl(target: self, action: #selector(loadMore(sender:)))
        collViewNewlyAdded.loadControl?.heightLimit = 100.0
        NotificationCenter.default.addObserver(self, selector: #selector(checkVideoPlaying), name: NSNotification.Name("isPlayingVideo"), object: nil)
        getNewExpertsList()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
       
        
    }
    @objc func checkVideoPlaying(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    //Pagination Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if footerAnimation == false {
            scrollView.loadControl?.update()
        }
        
    }
    //Pagination Function
    @objc private func loadMore(sender: AnyObject?) {
        if footerAnimation == false {
            self.footerAnimation = true
            pageNumber += 1
            getNewExpertsList()
        }
    }
    
    
    //MARK:- API Call for Get New Experts List
    func getNewExpertsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
      //  let param = expertType
        
        requestManager.requestCommonGetMethod(strAPIName: "\(KJUrlConstants.MYExpertsList)\(strUserID)/\(typeWiseExpert)/\(expertType)?page=\(pageNumber)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                   // self.newlyAddedArray.removeAll()
                    self.newlyAddedArray = homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.dataArray.append(contentsOf: self.newlyAddedArray)
                    self.collViewNewlyAdded.loadControl?.endLoading()
                    self.collViewNewlyAdded.reloadData()
                    if self.pageNumber == self.newlyAddedArray[0].lastPage {
                        self.footerAnimation = true
                    }else {
                        self.footerAnimation = false
                    }
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
   

}

extension ExpertViewAllVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    //MARK :- Collection View Delegate & Data Source Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArray.count
       
    }

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collViewNewlyAdded.dequeueReusableCell(withReuseIdentifier: "newlyAddedCollectionCell", for: indexPath) as! newlyAddedCollectionCell
        
        let ent = self.dataArray[indexPath.item]
        cell.lblTitle.text = ent.strDoctorName
        cell.imgExpert.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgExpert.layer.masksToBounds = true
        cell.imgExpert.layer.cornerRadius = 50
        cell.imgExpert.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        cell.imgExpert.layer.borderWidth = 1.0
        cell.imgExpert.layer.shadowColor = UIColor.black.cgColor
        cell.imgExpert.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.imgExpert.layer.shadowOpacity = 0.3
        cell.imgExpert.layer.shadowRadius = 2.0
        
        return cell
      
}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
//        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
//        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
//        let size:CGFloat = (collViewNewlyAdded.frame.size.width - space) / 2.0
//        return CGSize(width: size, height: size)
        
        return CGSize(width: 140, height: 140)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ent = self.dataArray[indexPath.item]
        let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
        vc.expertID = ent.strVideoID ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


