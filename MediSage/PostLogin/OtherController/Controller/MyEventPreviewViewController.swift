//
//  MyEventPreviewViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 20/01/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MyEventPreviewViewController: BaseViewController,UIScrollViewDelegate {

    //MARK:- Custom Properties
    var player: AVPlayer?
    var strBannerImagePath: String?
    var strVimeoID: String?
    var isComingFromCertificate = false

    //MARK:- IBOutlet Connections
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewPlayingVideo: UIView!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var imgVideoThumbnail: UIImageView!
    @IBOutlet weak var videoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgEventHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: ImageScrollView!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if isComingFromCertificate == true{
//            imgEvent.sd_setImage(with: URL(string: "\(strBannerImagePath ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
//        }else{
//            imgEvent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/live_event_banner_image/" + "\(strBannerImagePath ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
//        }
        self.tabBarController?.tabBar.isHidden = true

        scrollView.setup()
        scrollView.imageContentMode = .aspectFit
        scrollView.initialOffset = .center
        scrollView.imageScrollViewDelegate = self

       // let myImage = UIImage(named: strImagePath)
        let url = URL(string: KJUrlConstants.ImageUrl + "/storage/images/live_event_banner_image/" + "\(strBannerImagePath ?? "")")
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            let image = UIImage(data: imageData)
            scrollView.display(image: image!)

        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
    }
    

}

extension MyEventPreviewViewController: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}
