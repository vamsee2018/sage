//
//  SeriesViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 11/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import FirebaseAnalytics
class SeriesViewController: BaseViewController {
    
    //MARK:- Custom Properties
    var seriesID:Int? = 0
    var seriesArray = HomeEntity()
    var entSeriesFeedback = HomeEntity()

    var userData = [String:Any]()
    var strUserID  = String()
    var strFeedbackType  = String()
    var isLike  = Bool()
    var isInsightful  = Bool()
    var player: AVPlayer?
    var strUserFirstName:String?
    var strUserLastName:String?
    var strVideoTitle = ""
    var totalDuration = 0
    var playbackType = ""
    var startPosition = 0
    var endPosition = 0
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblViewEpisodes: UITableView!
    @IBOutlet weak var viewExpertBG: UIView!
    @IBOutlet weak var imgExpert: UIImageView!
    @IBOutlet weak var imgSeriesThumbnail: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblExpertName: UILabel!
    @IBOutlet weak var lblSeriesTitle: UILabel!
    @IBOutlet weak var lblSeriesDescription: UILabel!
    @IBOutlet weak var viewPlayingVideo: UIView!
    @IBOutlet weak var btnInsightful: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblSeriesDescriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewExpertInfoHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var imgInsightful: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let first_name = userData["first_name"] as? String{
                
                strUserFirstName = first_name
            }
            if let last_name = userData["last_name"] as? String{
                
                strUserLastName = last_name
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        getSeriesDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        Analytics.logEvent("Series", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Series Details Page",
            AnalyticsParameterContentType: "Series Details"
        ])
        
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
    }
    
    //MARK:- API Call for Get Experts List
    func getSeriesDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        let param = seriesID
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.SeriesDetails + strUserID + "/" + "\(param ?? 0)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.seriesArray = homeEntity.parseSeriesDetails(info : data as! Dictionary<String, AnyObject>)
                    self.tblViewEpisodes.reloadData()
                    self.setSeriesThumbail()
                    self.getExpertInfo()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func setSeriesThumbail(){
        lblSeriesTitle.text = self.seriesArray.seriesInfoArr[0].strVideoTitle ?? ""
        if seriesArray.seriesInfoArr[0].strDescription == ""{
           // lblSeriesDescription.isHidden = true
            viewExpertInfoHeightConstraint.constant = 310
            lblSeriesDescriptionHeightConstraint.constant = 0

        }else{
            viewExpertInfoHeightConstraint.constant = 422
            lblSeriesDescriptionHeightConstraint.constant = 94

           // lblSeriesDescription.isHidden = false
            lblSeriesDescription.attributedText = self.seriesArray.seriesInfoArr[0].strDescription?.htmlAttributedString()
        }
        imgSeriesThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/series/" + "\(self.seriesArray.seriesInfoArr[0].strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        
        if self.seriesArray.seriesInfoArr[0].isLike ?? 0 == 1{
          //  btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
            imgLike.image = UIImage(named: "Like_a")
        }else{
           // btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
            imgLike.image = UIImage(named:  "Like (1)")
        }
        if self.seriesArray.seriesInfoArr[0].isInsightful ?? 0 == 1{
            //btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful_a")

        }else{
           // btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful")

        }
        
        
    }
    
    func getExpertInfo(){
        lblExpertName.text = self.seriesArray.expertInfoArr[0].strDoctorName
        imgExpert.layer.cornerRadius = 30
        imgExpert.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        imgExpert.layer.borderWidth = 1.0
        imgExpert.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/expert/" + "\(self.seriesArray.expertInfoArr[0].strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
    }
    
    //MARK:- Play Video
    func PLAYVIDEOAPI(key: String){
        ApicallWebservices.SharedInstance.VIMEOVIDEOApiCallwithHeader(methodname: "https://api.vimeo.com/videos/\(key)", type: .get, header: ["Content-Type": "application/json", "Authorization": "bearer 75d0309b580962afaab71aa018e17877"], parameter: [:], strLoaderString: "", completion: {
            (error: Error?, success: Bool, result: Any?) in
            if success == true {
                let dicData = result as? NSDictionary ?? [:]
                let filesarr = dicData.object(forKey: "files") as? NSArray ?? []
                if filesarr.count > 0 {
                    let fileurl = filesarr[0] as? NSDictionary ?? [:]
                    let urlFile = fileurl.object(forKey: "link") as? String ?? ""
                    print(urlFile)
                    DispatchQueue.main.async() {
                        self.player = AVPlayer(url: URL(string: "\(urlFile)")!)
                        let playerController = AVPlayerViewController()
                        playerController.player = self.player
                        playerController.view.frame = self.viewPlayingVideo.bounds
                        self.addChild(playerController)
                        self.viewPlayingVideo.addSubview(playerController.view)
                        self.player?.play()
                        
                    }
                }else {
                    print("....")
                    self.view.makeToast(message: "Video is no longer...")
                }
                
            }else{
                print("....")
                self.view.makeToast(message: "Video link is not provided...")
            }
        })
    }
    
    //MARK:- SubmitVideoFeedback Api Call
    func submitVideoFeedback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "id": self.seriesID ?? 0,
                                       "feedback_type": strFeedbackType,
                                       "feedback_source" : "series",
                                       "feedback_action": isLike]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                    self.updateFeedbackUI()
                  //  self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")

                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    //MARK:- Update Like and Insightful
    func updateFeedbackUI(){
        if entSeriesFeedback.isLike ?? 0 == 1{
           // btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
            imgLike.image = UIImage(named: "Like_a")

        }else{
          //  btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
            imgLike.image = UIImage(named: "Like (1)")

        }
        if entSeriesFeedback.isInsightful ?? 0 == 1{
           // btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful_a")

        }else{
          //  btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful")

        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnInsightfulTapped(_ sender: UIButton) {
        strFeedbackType = "insightful"
        if entSeriesFeedback.isInsightful == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        strFeedbackType = "like"
        if entSeriesFeedback.isLike == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    @IBAction func btnShareTapped(_ sender: UIButton) {
        
        //Set the default sharing message.
         let message = "\("Dr.") \(strUserFirstName ?? "") \(strUserLastName ?? "") \("would you like to watch this lecture"), \"\(strVideoTitle)\" \("Download the Medisage app to watch.")"
        
        var comp = URLComponents()
        comp.scheme = "http"
        comp.host = "mymedisage.com/elearning"
        comp.path = "/series/\(seriesID ?? 0)/\(seriesID ?? 0)"
        let imgURL = KJUrlConstants.ImageUrl + "/storage/images/series/" + "\(self.seriesArray.seriesInfoArr[0].strImageName ?? "")"
        
        self.shareDynamicLink(CurrentVC: self, urlComp: comp, strTitle: message , strID: "" , imgUrl: imgURL )
        
//        //Set the link to share.
//        if let link = NSURL(string: "http://onelink.to/medisage")
//        {
//            let objectsToShare = [message,link] as [Any]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
//            self.present(activityVC, animated: true, completion: nil)
//        }
    }
   
    @IBAction func btnViewProfileTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func btnPlayVideoTapped(_ sender: UIButton) {
        imgSeriesThumbnail.isHidden = true
        btnPlay.isHidden = true
        let id = self.seriesArray.seriesInfoArr[0].strVideoLink ?? ""
        PLAYVIDEOAPI(key: id)
    }
   
}

extension SeriesViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return seriesArray.seriesVideosArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewEpisodes.dequeueReusableCell(withIdentifier: "upNextTableCell") as! upNextTableCell
        let ent = seriesArray.seriesVideosArr[indexPath.row]
        cell.lblTitle.text = ent.strVideoTitle
        cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgThumbnail.layer.cornerRadius = 10
        cell.viewContainer.layer.cornerRadius = 10
        cell.viewContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.viewContainer.layer.shadowOpacity = 0.7
        cell.viewContainer.layer.shadowRadius = 5.0
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ent = seriesArray.seriesVideosArr[indexPath.row]
        let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
        vc.strVideoID = "\(ent.strVideoID ?? 0)"
        vc.strVideoTitle = ent.strVideoTitle ?? ""
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}



