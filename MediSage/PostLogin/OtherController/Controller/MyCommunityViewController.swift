//
//  MyCommunityViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class MyCommunityViewController: BaseViewController {

   //MARK:- Custom Properties
    var cardiologyArray = [HomeEntity]()
    var gynacArray = [HomeEntity]()
    var urologyArray = [HomeEntity]()
    var familyHealthArray = [HomeEntity]()
    var diabetologyArray = [HomeEntity]()
    var pulmonologyArray = [HomeEntity]()
    var covidArray = [HomeEntity]()
    var neuroPsychiatryArray = [HomeEntity]()
    var paediatricsArray = [HomeEntity]()
    var opthamologyArray = [HomeEntity]()
    var diagnosticsArray = [HomeEntity]()
    var oncologyArray = [HomeEntity]()
    var nephrologyArray = [HomeEntity]()
    var feverArray = [HomeEntity]()
    var dermatologyArray = [HomeEntity]()

    var communitiesArray = HomeEntity() //["CARDIOLOGY","IBF","UROLOGY","FAMILY HEALTH"]
    var memberID = 60
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewCommunities: UICollectionView!
    @IBOutlet weak var collViewCardiology: UICollectionView!
    @IBOutlet weak var collViewGynac: UICollectionView!
    @IBOutlet weak var collViewUrology: UICollectionView!
    @IBOutlet weak var collViewFamilyHealth: UICollectionView!
    @IBOutlet weak var collViewDiabetology: UICollectionView!
    @IBOutlet weak var collViewOpthamology: UICollectionView!
    @IBOutlet weak var collViewPulmonology: UICollectionView!
    @IBOutlet weak var collViewCovid: UICollectionView!
    @IBOutlet weak var collViewNeuro: UICollectionView!
    @IBOutlet weak var collViewPaediatrics: UICollectionView!
    @IBOutlet weak var collViewFever: UICollectionView!
    @IBOutlet weak var collViewDiagnostics: UICollectionView!
    @IBOutlet weak var collViewOncology: UICollectionView!
    @IBOutlet weak var collViewNephrology: UICollectionView!
    @IBOutlet weak var collViewDermatology: UICollectionView!

    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var collViewCardiologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewGynacHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewUrologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewFamilyHealthHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewDiabetologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewOpthamologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewPulmonologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewCovidHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewNeuroHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewPaediatricsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewFeverHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewDiagnosticsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewOncologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewNephrologyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewDermatologyHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var lblCardiology: UILabel!
    @IBOutlet weak var lblGynac: UILabel!
    @IBOutlet weak var lblUrology: UILabel!
    @IBOutlet weak var lblFamilyHealth: UILabel!
    @IBOutlet weak var lblDiabetology: UILabel!
    @IBOutlet weak var lblOpthamology: UILabel!
    @IBOutlet weak var lblPulmonology: UILabel!
    @IBOutlet weak var lblCovid: UILabel!
    @IBOutlet weak var lblNeuro: UILabel!
    @IBOutlet weak var lblPaediatrics: UILabel!
    @IBOutlet weak var lblFever: UILabel!
    @IBOutlet weak var lblDiagnostics: UILabel!
    @IBOutlet weak var lblOncology: UILabel!
    @IBOutlet weak var lblNephrology: UILabel!
    @IBOutlet weak var lblDermatology: UILabel!

    @IBOutlet weak var btnCardiology: UIButton!
    @IBOutlet weak var btnGynac: UIButton!
    @IBOutlet weak var btnUrology: UIButton!
    @IBOutlet weak var btnFamilyHealth: UIButton!
    @IBOutlet weak var btnDiabetology: UIButton!
    @IBOutlet weak var btnOpthamology: UIButton!
    @IBOutlet weak var btnPulmonology: UIButton!
    @IBOutlet weak var btnCovid: UIButton!
    @IBOutlet weak var btnNeuro: UIButton!
    @IBOutlet weak var btnPaediatrics: UIButton!
    @IBOutlet weak var btnFever: UIButton!
    @IBOutlet weak var btnDiagnostics: UIButton!
    @IBOutlet weak var btnOncology: UIButton!
    @IBOutlet weak var btnNephrology: UIButton!
    @IBOutlet weak var btnDermatology: UIButton!
   
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        getMyCommunityList()
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("My Community", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "My Community",
            AnalyticsParameterContentType: "My Community"
        ])
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
        imgThumbnail.layer.cornerRadius = 10
        
        
    }
    
    //MARK:- API Call for Get My Community List
    func getMyCommunityList(){
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        self.showLoader()
        CommonService.getMyCommunityList(userId: strUserID) { (entity, success, error) in
            self.hideLoader()
            if success {
                if let entity = entity {
                    self.communitiesArray = entity
                    self.collViewCommunities.reloadData()
                    self.collViewCardiology.reloadData()
                    self.collViewDiabetology.reloadData()
                    self.collViewNeuro.reloadData()
                    self.collViewUrology.reloadData()
                    self.collViewPulmonology.reloadData()
                    self.collViewCovid.reloadData()
                    self.collViewGynac.reloadData()
                    self.collViewOpthamology.reloadData()
                    self.collViewFamilyHealth.reloadData()
                    self.collViewPaediatrics.reloadData()
                    self.collViewNephrology.reloadData()
                    self.collViewDiagnostics.reloadData()
                    self.collViewFever.reloadData()
                    self.collViewOncology.reloadData()
                    self.collViewDermatology.reloadData()
                }
            } else {
                self.view.makeToast(message: error ?? "")
            }
        }
    }

    func setHeightConstraints(){
        
        if cardiologyArray.count > 0{
            collViewCardiologyHeightConstraint.constant = 200
            lblCardiology.isHidden = false
            btnCardiology.isHidden = false
        }else{
            collViewCardiologyHeightConstraint.constant = 0
            lblCardiology.isHidden = true
            btnCardiology.isHidden = true
        }
        if diabetologyArray.count > 0{
            collViewDiabetologyHeightConstraint.constant = 200
            lblDiabetology.isHidden = false
            btnDiabetology.isHidden = false
        }else{
            collViewDiabetologyHeightConstraint.constant = 0
            lblDiabetology.isHidden = true
            btnDiabetology.isHidden = true
        }
        if urologyArray.count > 0{
            collViewUrologyHeightConstraint.constant = 200
            lblUrology.isHidden = false
            btnUrology.isHidden = false
            
        }else{
            collViewUrologyHeightConstraint.constant = 0
            lblUrology.isHidden = true
            btnUrology.isHidden = true
        }
        if familyHealthArray.count > 0{
            collViewFamilyHealthHeightConstraint.constant = 200
            lblFamilyHealth.isHidden = false
            btnFamilyHealth.isHidden = false
        }else{
            collViewFamilyHealthHeightConstraint.constant = 0
            lblFamilyHealth.isHidden = true
            btnFamilyHealth.isHidden = true
        }
        if pulmonologyArray.count > 0{
            collViewPulmonologyHeightConstraint.constant = 200
            lblPulmonology.isHidden = false
            btnPulmonology.isHidden = false
        }else{
            collViewPulmonologyHeightConstraint.constant = 0
            lblPulmonology.isHidden = true
            btnPulmonology.isHidden = true
        }
        if covidArray.count > 0{
            collViewCovidHeightConstraint.constant = 200
            lblCovid.isHidden = false
            btnCovid.isHidden = false
        }else{
            collViewCovidHeightConstraint.constant = 0
            lblCovid.isHidden = true
            btnCovid.isHidden = true
        }
        if feverArray.count > 0{
            collViewFeverHeightConstraint.constant = 200
            lblFever.isHidden = false
            btnFever.isHidden = false
        }else{
            collViewFeverHeightConstraint.constant = 0
            lblFever.isHidden = true
            btnFever.isHidden = true
        }
        if gynacArray.count > 0{
            collViewGynacHeightConstraint.constant = 200
            lblGynac.isHidden = false
            btnGynac.isHidden = false
        }else{
            collViewGynacHeightConstraint.constant = 0
            lblGynac.isHidden = true
            btnGynac.isHidden = true
        }
        if diagnosticsArray.count > 0{
            collViewDiagnosticsHeightConstraint.constant = 200
            lblDiagnostics.isHidden = false
            btnDiagnostics.isHidden = false
        }else{
            collViewDiagnosticsHeightConstraint.constant = 0
            lblDiagnostics.isHidden = true
            btnDiagnostics.isHidden = true
        }
        if paediatricsArray.count > 0{
            collViewPaediatricsHeightConstraint.constant = 200
            lblPaediatrics.isHidden = false
            btnPaediatrics.isHidden = false
        }else{
            collViewPaediatricsHeightConstraint.constant = 0
            lblPaediatrics.isHidden = true
            btnPaediatrics.isHidden = true
        }
        if neuroPsychiatryArray.count > 0{
            collViewNeuroHeightConstraint.constant = 200
            lblNeuro.isHidden = false
            btnNeuro.isHidden = false
        }else{
            collViewNeuroHeightConstraint.constant = 0
            lblNeuro.isHidden = true
            btnNeuro.isHidden = true
        }
        if oncologyArray.count > 0{
            collViewOncologyHeightConstraint.constant = 200
            lblOncology.isHidden = false
            btnOncology.isHidden = false
        }else{
            collViewOncologyHeightConstraint.constant = 0
            lblOncology.isHidden = true
            btnOncology.isHidden = true
        }
        if opthamologyArray.count > 0{
            collViewOpthamologyHeightConstraint.constant = 200
            lblOpthamology.isHidden = false
            btnOpthamology.isHidden = false
        }else{
            collViewOpthamologyHeightConstraint.constant = 0
            lblOpthamology.isHidden = true
            btnOpthamology.isHidden = true
        }
        if nephrologyArray.count > 0{
            collViewNephrologyHeightConstraint.constant = 200
            lblNephrology.isHidden = false
            btnNephrology.isHidden = false
        }else{
            collViewNephrologyHeightConstraint.constant = 0
            lblNephrology.isHidden = true
            btnNephrology.isHidden = true
        }
        if dermatologyArray.count > 0{
            collViewDermatologyHeightConstraint.constant = 200
            lblDermatology.isHidden = false
            btnDermatology.isHidden = false
        }else{
            collViewDermatologyHeightConstraint.constant = 0
            lblDermatology.isHidden = true
            btnDermatology.isHidden = true
        }
        
    }
    
    //MARK:- Button Actions
    @IBAction func btnViewAllTapped(_ sender: UIButton) {
       guard let button = sender as? UIButton else {
            return
        }

        switch button.tag {
        case 1:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = diabetologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = cardiologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
           let ent = familyHealthArray[0]
           vc.communityID = "\(ent.intCommunityID ?? 0)"
           self.navigationController?.pushViewController(vc, animated: true)
        case 4:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = pulmonologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 5:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = urologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 6:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = covidArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 7:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = neuroPsychiatryArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 8:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = feverArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 9:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = opthamologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 10:
           let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = paediatricsArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 11:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = oncologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 12:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = nephrologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 13:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = diagnosticsArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 14:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = gynacArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 15:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = dermatologyArray[0]
            vc.communityID = "\(ent.intCommunityID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("Unknown language")
            return
        }
    }
    
    
}

//MARK :- Collection View Delegate & Data Source Methods
extension MyCommunityViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collViewCommunities {
            return communitiesArray.communityArr.count
        }
        else if collectionView == self.collViewCardiology {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "CARDIOLOGY"{
                    cardiologyArray.append(values)
                }
            }
            return cardiologyArray.count
        }
        else if collectionView == self.collViewDiabetology{
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "DIABETOLOGY"{
                    diabetologyArray.append(values)
                }
            }
            return diabetologyArray.count
        }
        else if collectionView == self.collViewUrology {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "UROLOGY"{
                    urologyArray.append(values)
                }
            }
            return urologyArray.count
        }
        else if collectionView == self.collViewFamilyHealth {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "FAMILY HEALTH"{
                    familyHealthArray.append(values)
                }
            }
            return familyHealthArray.count
        }else if collectionView == self.collViewPulmonology {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "PULMONOLOGY"{
                    pulmonologyArray.append(values)
                }
            }
            return pulmonologyArray.count
        }
        else if collectionView == self.collViewCovid{
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "COVID-19"{
                    covidArray.append(values)
                }
            }
            return covidArray.count
        }
        else if collectionView == self.collViewNeuro {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "NEURO/PSYCHIATRY"{
                    neuroPsychiatryArray.append(values)
                }
            }
            return neuroPsychiatryArray.count
        }
        else if collectionView == self.collViewPaediatrics {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "PAEDIATRICS"{
                    paediatricsArray.append(values)
                }
            }
            return paediatricsArray.count
        }else if collectionView == self.collViewGynac {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "GYNAECOLOGY - IVF"{
                    gynacArray.append(values)
                }
            }
            return gynacArray.count
        }
        else if collectionView == self.collViewOpthamology{
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "OPTHALMOLOGY"{
                    opthamologyArray.append(values)
                }
            }
            return opthamologyArray.count
        }
        else if collectionView == self.collViewFever {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "FEVER"{
                    feverArray.append(values)
                }
            }
            return feverArray.count
        }
        else if collectionView == self.collViewOncology {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "ONCOLOGY"{
                    oncologyArray.append(values)
                }
            }
            return oncologyArray.count
        }else if collectionView == self.collViewNephrology {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "NEPHROLOGY"{
                    nephrologyArray.append(values)
                }
            }
            return nephrologyArray.count
        }else if collectionView == self.collViewDiagnostics {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "DIAGNOSTICS"{
                    diagnosticsArray.append(values)
                }
            }
            return diagnosticsArray.count
        }else if collectionView == self.collViewDermatology {
            for values in communitiesArray.communityDataArr{
                if values.strCommunityName == "DERMATOLOGY"{
                    dermatologyArray.append(values)
                }
            }
            return dermatologyArray.count
        }
        else{
            return 0
        }
        
       
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collViewCommunities{
            let cell = collViewCommunities.dequeueReusableCell(withReuseIdentifier: "communitiesCollectionCell", for: indexPath) as! communitiesCollectionCell
            
            let ent = communitiesArray.communityArr[indexPath.item]
            cell.lblTitle.text = ent.strTitle
            cell.containerView.layer.borderColor = UIColor.gray.cgColor
            cell.containerView.layer.borderWidth = 1.0
          
            return cell
            
        }
        else if collectionView == collViewCardiology{
            let cell = collViewCardiology.dequeueReusableCell(withReuseIdentifier: "cardiologyCollectionCell", for: indexPath) as! cardiologyCollectionCell
            let ent = cardiologyArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }
            
        else if collectionView == collViewDiabetology{
            
            let cell = collViewDiabetology.dequeueReusableCell(withReuseIdentifier: "diabetologyCollectionCell", for: indexPath) as! diabetologyCollectionCell
            let ent = diabetologyArray[indexPath.item]
            
             cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
            
        }else if collectionView == collViewUrology{
            
            let cell = collViewUrology.dequeueReusableCell(withReuseIdentifier: "urologyCollectionCell", for: indexPath) as! urologyCollectionCell
             let ent = urologyArray[indexPath.item]
            
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
             cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            setHeightConstraints()
            return cell
            
        }else if collectionView == collViewFamilyHealth{
            let cell = collViewFamilyHealth.dequeueReusableCell(withReuseIdentifier: "familyHealthCollectionCell", for: indexPath) as! familyHealthCollectionCell
            let ent = familyHealthArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewNeuro{
            let cell = collViewNeuro.dequeueReusableCell(withReuseIdentifier: "neuroPsychetricsCollectionCell", for: indexPath) as! neuroPsychetricsCollectionCell
            let ent = neuroPsychiatryArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewPaediatrics{
            let cell = collViewPaediatrics.dequeueReusableCell(withReuseIdentifier: "paediatricsCollectionCell", for: indexPath) as! paediatricsCollectionCell
            let ent = paediatricsArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewGynac{
            let cell = collViewGynac.dequeueReusableCell(withReuseIdentifier: "gynacCollectionCell", for: indexPath) as! gynacCollectionCell
            let ent = gynacArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewOpthamology{
            let cell = collViewOpthamology.dequeueReusableCell(withReuseIdentifier: "opthamologyCollectionCell", for: indexPath) as! opthamologyCollectionCell
            let ent = opthamologyArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewFever{
            let cell = collViewFever.dequeueReusableCell(withReuseIdentifier: "feverCollectionCell", for: indexPath) as! feverCollectionCell
            let ent = feverArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewOncology{
            let cell = collViewOncology.dequeueReusableCell(withReuseIdentifier: "oncologyCollectionCell", for: indexPath) as! oncologyCollectionCell
            let ent = oncologyArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewNephrology{
            let cell = collViewNephrology.dequeueReusableCell(withReuseIdentifier: "nephrologyCollectionCell", for: indexPath) as! nephrologyCollectionCell
            let ent = nephrologyArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewDiagnostics{
            let cell = collViewDiagnostics.dequeueReusableCell(withReuseIdentifier: "diagnosticsCollectionCell", for: indexPath) as! diagnosticsCollectionCell
            let ent = diagnosticsArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewCovid{
            let cell = collViewCovid.dequeueReusableCell(withReuseIdentifier: "covidCollectionCell", for: indexPath) as! covidCollectionCell
            let ent = covidArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewPulmonology{
            let cell = collViewPulmonology.dequeueReusableCell(withReuseIdentifier: "pulmonologyCollectionCell", for: indexPath) as! pulmonologyCollectionCell
            let ent = pulmonologyArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }else if collectionView == collViewDermatology{
            let cell = collViewDermatology.dequeueReusableCell(withReuseIdentifier: "dermatologyCollectionCell", for: indexPath) as! dermatologyCollectionCell
            let ent = dermatologyArray[indexPath.item]
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            setHeightConstraints()
            
            return cell
            
        }
        else{
            return UICollectionViewCell()
        }
        
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collViewCardiology || collectionView == collViewDiabetology || collectionView == collViewUrology || collectionView == collViewFamilyHealth || collectionView == collViewNeuro || collectionView == collViewPulmonology || collectionView == collViewCovid || collectionView == collViewGynac || collectionView == collViewOpthamology || collectionView == collViewPaediatrics || collectionView == collViewDiagnostics || collectionView == collViewNephrology || collectionView == collViewFever || collectionView == collViewOncology || collectionView == collViewDermatology{
            
            return CGSize(width: 180, height: 160)
         
        }else {
            return CGSize(width: 180, height: 40)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collViewCommunities{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = communitiesArray.communityArr[indexPath.item]
            vc.communityID = "\(ent.intID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewCardiology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = cardiologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collViewDiabetology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = diabetologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collViewUrology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = urologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewFamilyHealth{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = familyHealthArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewNeuro{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = neuroPsychiatryArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collViewPulmonology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = pulmonologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if collectionView == collViewCovid{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = covidArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewGynac{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = gynacArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewOpthamology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = opthamologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewPaediatrics{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = paediatricsArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewDiagnostics{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = diagnosticsArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewNephrology{
           let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = nephrologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewFever{
           let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = feverArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewOncology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = oncologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewDermatology{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = dermatologyArray[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

//MARK:- Custom CollectionView Cell Classes

class communitiesCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
}

class cardiologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}

class diabetologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class urologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class pulmonologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class familyHealthCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class covidCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class neuroPsychetricsCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class feverCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class opthamologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class paediatricsCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class oncologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class nephrologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class diagnosticsCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class gynacCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}

class dermatologyCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
