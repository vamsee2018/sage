//
//  NewsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 15/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class NewsViewController: BaseViewController {

   //MARK:- Custom Properties
    var newsID = 0
    var currentNewsID = 0
    var newsArray = [HomeEntity]()
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber = String()
    var entSeriesFeedback = HomeEntity()
    var checkFlag = HomeEntity()
    var strFeedbackType  = String()
    var strActionType  = String()
    var isLike  = Bool()
    var isInsightful  = Bool()
    var isComingFromNotification  = Bool()
    var strUserFirstName:String?
    var strUserLastName:String?
    var newsImage = UIImage()
    var dictionaryParam = [String:Any]() // send current Item index in notification for read/unread

    private var lastContentOffset: CGFloat = 0.0

    //MARK:- IBOutlet Connections
    @IBOutlet private weak var newsCollectionView: UICollectionView!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let first_name = userData["first_name"] as? String{
                
                strUserFirstName = first_name
            }
            if let last_name = userData["last_name"] as? String{
                
                strUserLastName = last_name
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
        getLatestNewsList()

        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        self.newsCollectionView.dataSource = self
        self.newsCollectionView.register(UINib.init(nibName: "NewsCardCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NewsCardCollectionCell")
        NotificationCenter.default.addObserver(self, selector: #selector(newsArticleViewed(_ :)), name: NSNotification.Name("NewsArticle"), object: nil)

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("News Article", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "News Article Page",
            AnalyticsParameterContentType: "News Article"
        ])
       
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        
        if isComingFromNotification == true{
            self.viwTop.btnBack.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        }

        navigationController?.navigationBar.isHidden = true
        customizeNavigationwithBackButton()
        
    }
    
    @objc func backPressed(){
       
        NotificationCenter.default.post(name: Notification.Name("notificationList"), object: nil)
        NotificationCenter.default.post(name: .newsArticleCount, object: nil)

        self.navigationController?.popViewController(animated: true)

    }
    
    //MARK:- Notifies news article are swiped up/down
    @objc func newsArticleViewed(_ notification: Notification){
       strActionType = "viewed"
        
        if let dict = notification.userInfo as NSDictionary? {
            let ent = newsArray[dict["index"] as? Int ?? 0]
            newsID = ent.intID ?? 0
            if dict["index"] as? Int ?? 0 == 1{
                self.checkFlags()
            }
        }
        captureReadMore()
    }
    
    
    //MARK:- API Call for Get Latest News List
    func getLatestNewsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.News + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.newsArray.removeAll()
                    self.newsArray = homeEntity.parseNewsList(info : data as! Dictionary<String, AnyObject>)
                   
                    for (index, element) in self.newsArray.enumerated() {
                      print("Item \(index): \(element)")
                        
                        if element.intID == self.currentNewsID{

                            self.newsArray.rearrange(from: index, to: 0)

                        }
                    }
                    self.newsCollectionView.reloadData()

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- SubmitVideoFeedback Api Call
    func submitVideoFeedback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "id": newsID,
                                       "feedback_type": strFeedbackType,
                                       "feedback_source" : "news_article",
                                       "feedback_action" : isLike]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                    self.getLatestNewsList()
                  //  self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    //MARK:- SubmitVideoFeedback Api Call
    func captureReadMore(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "article_id": newsID,
                                       "action_type": strActionType]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.NewsFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)

                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    //MARK:- Check Profile Update, News Article Added or Not and Event in buffer time or not
    func checkFlags(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CheckHomePageFlag + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.checkFlag = loginEntity.checkFlag(withInfo : data as? Dictionary<String, AnyObject>)
                    if self.checkFlag.isNewsAdded ?? 0 > 0{
                        self.view.makeToast(message: "You have \(self.checkFlag.isNewsAdded ?? 0) unread news articles")
                    }else{
                        self.view.makeToast(message: "You are upto date")
                    }
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    
}

// MARK: - UICollectionViewDataSource -
extension NewsViewController: UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = newsCollectionView.dequeueReusableCell(withReuseIdentifier: "NewsCardCollectionCell", for: indexPath) as! NewsCardCollectionCell
        
        
        let ent = newsArray[indexPath.item]
        cell.lblTitle.text = ent.strTitle
        cell.lblDescription.text = ent.strDescription
        let dateString = ent.strCreatedOn ?? ""
        if let convertedDate = dateFromString(dateString) {
           cell.lblDateAndJournal.text = convertedDate + " | " + (ent.strJournal ?? "")
        }
        cell.lblHashtags.text = "#" + (ent.strCommunityName ?? "")
        cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.btnTapMore.tag = indexPath.row
        cell.btnInsightful.tag = indexPath.row
        cell.btnLike.tag = indexPath.row
        cell.btnShare.tag = indexPath.row

        cell.btnTapMore.addTarget(self, action:#selector(tapViewMore(sender:)), for: .touchUpInside)
        cell.btnBack.addTarget(self, action:#selector(btnBackTapped(sender:)), for: .touchUpInside)
        cell.btnInsightful.addTarget(self, action:#selector(btnInsightfulTapped(sender:)), for: .touchUpInside)
        cell.btnLike.addTarget(self, action:#selector(btnLikeTapped(sender:)), for: .touchUpInside)
        cell.btnShare.addTarget(self, action:#selector(btnShareButtonTapped(sender:)), for: .touchUpInside)

        if ent.isLike == 1{
            cell.imgLike.image = UIImage(named: "Like_a")
        }else{
            cell.imgLike.image = UIImage(named: "Like (1)")
        }
        if ent.isInsightful == 1{
            cell.imgInsightful.image = UIImage(named:  "Insightful_a")
        }else{
            cell.imgInsightful.image = UIImage(named:  "Insightful")
        }
        if let indexPath = collectionView.visibleCurrentCellIndexPath {
            print("IndexPath :- ",indexPath.row)
            
            dictionaryParam["index"] = indexPath.row

            NotificationCenter.default.post(name: Notification.Name("NewsArticle"), object: nil,userInfo: dictionaryParam)
        }
        if indexPath.item == 0{
            cell.imgSwipeUp.isHidden = false

            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "Arrow-up_Light-Blue", withExtension: "gif")!)
            let advTimeGif = UIImage.sd_image(withGIFData: imageData)
            cell.imgSwipeUp.image = advTimeGif
        }else{
            cell.imgSwipeUp.isHidden = true
        }
        
        
        return cell
    }
    
   
    
    //MARK:- Tap more news
    @objc func tapViewMore(sender: UIButton){
        let ent = self.newsArray[sender.tag]
        newsID = ent.intID ?? 0
        strActionType = "read"

        captureReadMore()
        let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
        vc.strURL = ent.strArticleLink ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- Button Actions
    @objc func btnInsightfulTapped(sender: UIButton) {
        strFeedbackType = "insightful"
        let ent = self.newsArray[sender.tag]
        newsID = ent.intID ?? 0
        if ent.isInsightful == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    @objc func btnLikeTapped(sender: UIButton) {
        strFeedbackType = "like"
        let ent = self.newsArray[sender.tag]
        newsID = ent.intID ?? 0
        if ent.isLike == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
       
    @objc func btnShareButtonTapped(sender: UIButton){
       
        //Set the default sharing message.
        let ent = self.newsArray[sender.tag]
        let message = "\("Dr.") \(strUserFirstName ?? "") \(strUserLastName ?? "") \("would you like to read the article"), \"\(ent.strTitle ?? "")\" \("Download the Medisage app to read")"
        print(message)
        newsID = ent.intID ?? 0
        var comp = URLComponents()
        comp.scheme = "http"
        comp.host = "mymedisage.com/elearning"
        comp.path = "/news/\(newsID)/\(newsID)"
        let imgURL = KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"
        
        self.shareDynamicLink(CurrentVC: self, urlComp: comp, strTitle: message , strID: "" , imgUrl: imgURL )
        
        
       /*
        //Set the link to share.
        if let link = NSURL(string: "http://onelink.to/medisage")
        {
           let objectsToShare = [message,link] as [Any]
           let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
           activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
           self.present(activityVC, animated: true, completion: nil)
        }*/
    }
    
    
    //MARK:- Back Button
    @objc func btnBackTapped(sender: UIButton){
        var dictionaryParam = [String:Any]()
              
        if self.checkFlag.isNewsAdded ?? 0 > 0{
             dictionaryParam["index"] = "1"
        }else{
            dictionaryParam["index"] = "0"
        }
       
        NotificationCenter.default.post(name: .newsArticleCount, object: nil,userInfo: dictionaryParam)
        self.navigationController?.popViewController(animated: true)
    }
    
    func dateFromString(_ dateString:String,  format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ") -> String? {
      let dateFormatter = DateFormatter()
      let tempLocale = dateFormatter.locale  // save locale temporarily
      dateFormatter.locale = Locale(identifier: "en_US_POSIX")
      dateFormatter.dateFormat =  format//"yyyy-MM-dd HH:mm:ss.SSSSSS"
      let date = dateFormatter.date(from: dateString)
      dateFormatter.dateFormat = "d MMM yyyy"
      dateFormatter.locale = tempLocale // reset the locale

      guard let getdate = date else {
        return nil
      }

      let dateString = dateFormatter.string(from: getdate)
      return dateString
    }
}

extension UICollectionView {
  var visibleCurrentCellIndexPath: IndexPath? {
    for cell in self.visibleCells {
      let indexPath = self.indexPath(for: cell)
      return indexPath
    }
    
    return nil
  }
}

extension RangeReplaceableCollection where Indices: Equatable {
    mutating func rearrange(from: Index, to: Index) {
        precondition(from != to && indices.contains(from) && indices.contains(to), "invalid indices")
        insert(remove(at: from), at: to)
    }
}
