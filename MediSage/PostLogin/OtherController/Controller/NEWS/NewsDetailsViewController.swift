//
//  NewsDetailsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 15/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class NewsDetailsViewController: BaseViewController {

    //MARK:- Custom Properties
    var articleID:Int? = 0
    var newsDetails = HomeEntity()
    var userData = [String:Any]()
    var strUserID  = String()
    var entSeriesFeedback = HomeEntity()
    var strFeedbackType  = String()
    var strMobileNumber = String()
    //MARK:- IBOutlet Connections
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDateAndJournal: UILabel!
    @IBOutlet weak var lblHashtags: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnInsightful: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        getNewsDetails()
    }
       
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = true
        customizeNavigationwithBackButton()
        
        
    }
       
    //MARK:- API Call for Get Latest News List
    func getNewsDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
       
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.NewsDetails + "\(articleID ?? 0)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.newsDetails = homeEntity.parseNewsDetails(info : data as! Dictionary<String, AnyObject>)
                    self.setNewsDetailsUI()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func setNewsDetailsUI(){
        imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/article/" + "\(newsDetails.strImageName ?? "")"), placeholderImage:UIImage(named:"placeholder"))
        lblTitle.text = newsDetails.strTitle
        lblDescription.attributedText = newsDetails.strContent?.htmlAttributedString()
        let dateString = newsDetails.strCreatedOn
        if let convertedDate = dateFromString(dateString ?? "") {
           lblDateAndJournal.text = convertedDate + " | " + (newsDetails.strJournal ?? "")
        }
        lblHashtags.text = "#" + (newsDetails.strCommunityName ?? "")
    }
    
    //MARK:- SubmitVideoFeedback Api Call
    func submitVideoFeedback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "video_id": "",
                                       "feedback_type": strFeedbackType]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                   // self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    
    //MARK:- Button Actions
    @IBAction func btnInsightfulTapped(_ sender: UIButton) {
        strFeedbackType = "insightful"
        submitVideoFeedback()
    }
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        strFeedbackType = "like"
        submitVideoFeedback()
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func shareButtonClicked(sender: AnyObject)
    {
        //Set the default sharing message.
        let message = "Message goes here."
        //Set the link to share.
        if let link = NSURL(string: "http://yoururl.com")
        {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func dateFromString(_ dateString:String,  format: String = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ") -> String? {
      let dateFormatter = DateFormatter()
      let tempLocale = dateFormatter.locale  // save locale temporarily
      dateFormatter.locale = Locale(identifier: "en_US_POSIX")
      dateFormatter.dateFormat =  format//"yyyy-MM-dd HH:mm:ss.SSSSSS"
      let date = dateFormatter.date(from: dateString)
      dateFormatter.dateFormat = "d MMM yyyy"
      dateFormatter.locale = tempLocale // reset the locale

      guard let getdate = date else {
        return nil
      }

      let dateString = dateFormatter.string(from: getdate)
      return dateString
    }
}
