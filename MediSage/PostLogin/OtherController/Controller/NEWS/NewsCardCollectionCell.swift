//
//  NewsCardCollectionCell.swift
//  InshortView
//
//  Created by Mohammmad Tahir on 04/09/19.
//  Copyright © 2019 Mohammad Tahir. All rights reserved.
//

import UIKit

class NewsCardCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDateAndJournal: UILabel!
    @IBOutlet weak var lblHashtags: UILabel!
    @IBOutlet weak var lblReadUnread: UILabel!
    @IBOutlet weak var viewPopUp: UIView!

    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var imgSwipeUp: UIImageView!
    @IBOutlet weak var btnTapMore: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnInsightful: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgInsightful: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 8
    }
}
