//
//  MyExpertsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 06/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
class MyExpertsViewController: BaseViewController {

    //MARK:- Custom Properties
    var newlyAddedArray = [HomeEntity]()
    var mostWatchedArray = [HomeEntity]()
    var typeWiseExpert = "new"
    var memberID = "50"
    var expertType = "all"
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewNewlyAdded: UICollectionView!
    @IBOutlet weak var collViewMostWatched: UICollectionView!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnNational: UIButton!
    @IBOutlet weak var btnInternational: UIButton!
    @IBOutlet weak var imgThumbnail: UIImageView!

    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        NotificationCenter.default.addObserver(self, selector: #selector(checkVideoPlaying), name: NSNotification.Name("isPlayingVideo"), object: nil)
        getNewExpertsList()
        getMostViewedExpertsList()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        imgThumbnail.layer.cornerRadius = 10
        btnAll.backgroundColor = UIColor.init(hex: "001D4D")
        btnAll.setTitleColor(UIColor.white, for: .normal)
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        btnAll.layer.borderWidth = 1.0
        btnAll.layer.borderColor = UIColor.gray.cgColor
        btnNational.layer.borderWidth = 1.0
        btnNational.layer.borderColor = UIColor.gray.cgColor
        btnInternational.layer.borderWidth = 1.0
        btnInternational.layer.borderColor = UIColor.gray.cgColor
 
    }
    @objc func checkVideoPlaying(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:- API Call for Get New Experts List
    func getNewExpertsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        let param = expertType
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MYExpertsList + strUserID + "/new/" + param, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.newlyAddedArray.removeAll()
                    self.newlyAddedArray = homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewNewlyAdded.reloadData()

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- API Call for Get Most Viewed Experts List
    func getMostViewedExpertsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        let param = expertType
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MYExpertsList + strUserID + "/most_watched/" + param, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.mostWatchedArray.removeAll()
                    self.mostWatchedArray = homeEntity.parseHomeList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewMostWatched.reloadData()
  
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnAllTapped(_ sender: UIButton) {
        btnAll.backgroundColor = UIColor.init(hex: "001D4D")
        btnAll.setTitleColor(UIColor.white, for: .normal)
        btnNational.setTitleColor(UIColor.darkGray, for: .normal)
        btnInternational.setTitleColor(UIColor.darkGray, for: .normal)

        btnNational.backgroundColor = UIColor.white
        btnInternational.backgroundColor = UIColor.white

        self.expertType = "all"
        getNewExpertsList()
        getMostViewedExpertsList()
    }
    
    @IBAction func btnNationalTapped(_ sender: UIButton) {
        btnNational.backgroundColor = UIColor.init(hex: "001D4D")
        btnNational.setTitleColor(UIColor.white, for: .normal)
        btnAll.backgroundColor = UIColor.white
        btnInternational.backgroundColor = UIColor.white
        btnAll.setTitleColor(UIColor.darkGray, for: .normal)
        btnInternational.setTitleColor(UIColor.darkGray, for: .normal)
        self.expertType = "national"
        getNewExpertsList()
        getMostViewedExpertsList()
    }

    @IBAction func btnInterNationalTapped(_ sender: UIButton) {
        self.expertType = "international"
        btnInternational.backgroundColor = UIColor.init(hex: "001D4D")
        btnInternational.setTitleColor(UIColor.white, for: .normal)
        btnNational.backgroundColor = UIColor.white
        btnAll.backgroundColor = UIColor.white
        btnNational.setTitleColor(UIColor.darkGray, for: .normal)
        btnAll.setTitleColor(UIColor.darkGray, for: .normal)
        getNewExpertsList()
        getMostViewedExpertsList()
    }
    
    @IBAction func btnNewExpertsViewAllTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertViewAllVC) as! ExpertViewAllVC
        vc.expertType = self.expertType
        vc.typeWiseExpert = "new"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnMostWatchedExpertsViewAllTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertViewAllVC) as! ExpertViewAllVC
        vc.expertType = self.expertType
        vc.typeWiseExpert = "most_watched"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension MyExpertsViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   //MARK :- Collection View Delegate & Data Source Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collViewNewlyAdded {
            return newlyAddedArray.count
        }
        else {
            return mostWatchedArray.count
        }
    }

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

    if collectionView == collViewNewlyAdded{
        let cell = collViewNewlyAdded.dequeueReusableCell(withReuseIdentifier: "newlyAddedCollectionCell", for: indexPath) as! newlyAddedCollectionCell
        
        let ent = newlyAddedArray[indexPath.item]
        cell.lblTitle.text = ent.strDoctorName
        cell.imgExpert.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgExpert.layer.masksToBounds = true
        cell.imgExpert.layer.cornerRadius = 50
        cell.imgExpert.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        cell.imgExpert.layer.borderWidth = 1.0
        cell.imgExpert.layer.shadowColor = UIColor.black.cgColor
        cell.imgExpert.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.imgExpert.layer.shadowOpacity = 0.3
        cell.imgExpert.layer.shadowRadius = 2.0
        
        return cell
        
    }
    else {
        let cell = collViewMostWatched.dequeueReusableCell(withReuseIdentifier: "mostWatchedCollectionCell", for: indexPath) as! mostWatchedCollectionCell
        let ent = mostWatchedArray[indexPath.item]
        cell.lblTitle.text = ent.strDoctorName
        cell.imgExpert.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgExpert.layer.masksToBounds = true
        cell.imgExpert.layer.cornerRadius = 50
        cell.imgExpert.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        cell.imgExpert.layer.borderWidth = 1.0
        cell.imgExpert.layer.shadowColor = UIColor.black.cgColor
        cell.imgExpert.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.imgExpert.layer.shadowOpacity = 0.3
        cell.imgExpert.layer.shadowRadius = 2.0
        
        return cell
        
    }
   
}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: 140)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == self.collViewNewlyAdded {
            let ent = newlyAddedArray[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
            vc.expertID = ent.strVideoID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let ent = mostWatchedArray[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
            vc.expertID = ent.strVideoID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
}

//MARK:- Custom CollectionView Cell Classes

class newlyAddedCollectionCell: UICollectionViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgExpert: UIImageView!
    
}

class mostWatchedCollectionCell: UICollectionViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgExpert: UIImageView!
}
