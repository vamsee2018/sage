//
//  CommunityVideosViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 04/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
class CommunityVideosViewController: BaseViewController {

    //MARK:- Custom Properties
    var communitieVideosArray = [HomeEntity]()
    var VideosArray = [HomeEntity]()
    var communityID: String?
    var userData = [String:Any]()
    var strUserID  = String()
    var pageNumber = 1
    var currentPage = 1
    var pageLimit = 1
    var footerAnimation = Bool()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewCommunitieVideos: UICollectionView!
    
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        collViewCommunitieVideos.loadControl = UILoadControl(target: self, action: #selector(loadMore(sender:)))
        collViewCommunitieVideos.loadControl?.heightLimit = 100.0
        getCommunityVideoList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
    }
    
    //Pagination Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if footerAnimation == false {
            scrollView.loadControl?.update()
        }
        
    }
    //Pagination Function
    @objc private func loadMore(sender: AnyObject?) {
        if footerAnimation == false {
            self.footerAnimation = true
            pageNumber += 1
             getCommunityVideoList()
        }
    }
    
    //MARK:- API Call for Get My Community List
    func getCommunityVideoList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        let param = communityID
        
        requestManager.requestCommonGetMethod(strAPIName: "\(KJUrlConstants.CommunityDetails)\(param ?? "")?page=\(pageNumber)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.communitieVideosArray = homeEntity.parseCommunityVideoList(info : data as! Dictionary<String, AnyObject>)
                   // self.collViewCommunitieVideos.reloadData()
                    self.VideosArray.append(contentsOf: self.communitieVideosArray)
                    self.collViewCommunitieVideos.loadControl?.endLoading()
                    self.collViewCommunitieVideos.reloadData()
                    if self.pageNumber == self.communitieVideosArray[0].lastPage {
                        self.footerAnimation = true
                    }else {
                        self.footerAnimation = false
                    }
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }

}

//MARK :- Collection View Delegate & Data Source MethodsaA
extension CommunityVideosViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return VideosArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collViewCommunitieVideos.dequeueReusableCell(withReuseIdentifier: "cardiologyCollectionCell", for: indexPath) as! cardiologyCollectionCell
        let ent = VideosArray[indexPath.item]
        cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgThumbnail.layer.cornerRadius = 10
        cell.containerView.layer.cornerRadius = 10
        cell.containerView.layer.shadowColor = UIColor.black.cgColor
        cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.containerView.layer.shadowOpacity = 0.3
        cell.containerView.layer.shadowRadius = 5.0
        
        return cell
       
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.view.frame.size.width/2 - 5), height: 140)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
        let ent = VideosArray[indexPath.item]
        vc.strVideoID = "\(ent.intID ?? 0)"
        vc.strVideoTitle = ent.strVideoTitle ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
