//
//  NotificationsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 03/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {

    //MARK:- Custom Properties
    var notificationArray = [HomeEntity]()
    var readUnreadNotification = HomeEntity()
    
    var notificationType = "video"
    
    var userData = [String:Any]()
    var strUserID  = String()
    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet weak var viewEmptyNotification: UIView!
    @IBOutlet weak var lblEmptyMessage: UILabel!

     
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(refreshNotificationList), name: NSNotification.Name("notificationList"), object: nil)
        tblNotification.rowHeight = UITableView.automaticDimension
        tblNotification.estimatedRowHeight = UITableView.automaticDimension
        
        getNotificationList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.btnMore.isUserInteractionEnabled = false

        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true

        customizeNavigationwithBackButton()
        
        
    }
    
    //MARK:- Refresh notification list after read/unread feedback api call
    @objc func refreshNotificationList(){
        // Code to refresh table view
        getNotificationList()
    }
    
    //MARK:- API Call for Get Notification list
    func getNotificationList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let homeEntity = HomeEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.NotificationList + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    //self.expertDetailsArray.removeAll()
                    self.notificationArray = homeEntity.parseNotificationList(info : data as! Dictionary<String, AnyObject>)
                    
                    if self.notificationArray.count > 0{
                        self.viewEmptyNotification.isHidden = true
                        self.tblNotification.isHidden = false
                        self.tblNotification.reloadData()
                        NotificationCenter.default.post(name: .newsArticleCount, object: nil)
                    }else{
                        self.viewEmptyNotification.isHidden = false
                        self.tblNotification.isHidden = true
                    }
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Notification Read/Unread Feedback Api Call
    func notificationFeedback(_ notificationID: Int){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id": strUserID,
                                       "notification_id": notificationID,
                                       "action_type": "read"
        ]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.NotificationFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.readUnreadNotification = loginEntity.parseNotificationFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
}

//MARK:- Tableview Delegate and Data Source Methods
extension NotificationsViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblNotification.dequeueReusableCell(withIdentifier: "notificationCell") as! notificationCell
        let ent = notificationArray[indexPath.row]
        if ent.strActionType == "video"{
            cell.lblNotificationType.text = "Watch Now"
        }else if ent.strActionType == "live_event"{
            cell.lblNotificationType.text = "Join Now"
        }else if ent.strActionType == "news_article"{
            cell.lblNotificationType.text = "Read More"
        }
        cell.lblTitle.text = ent.strTitle
        cell.lblDescription.text = ent.strDescription
        cell.viewContainer.layer.cornerRadius = 10
        cell.viewContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.viewContainer.layer.shadowOpacity = 0.7
        cell.viewContainer.layer.shadowRadius = 2.0
        cell.selectionStyle = .none
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ent = notificationArray[indexPath.row]
        notificationFeedback(ent.intID ?? 0)
        if ent.strActionType == "video"{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            vc.strVideoID = ent.strActionID ?? ""
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ent.strActionType == "live_event"{
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
            vc.strURL = "\(ent.strActiveEventLink ?? "")\(strUserID)/\(ent.strActionID ?? "")"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ent.strActionType == "news_article"{
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.NewsViewController) as! NewsViewController
            vc.currentNewsID = Int(ent.strActionID ?? "") ?? 0
            vc.isComingFromNotification = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ent.strActionType == "live_event_registration"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventsViewController) as! MyEventsViewController
            vc.eventID = Int(ent.strActionID ?? "") ?? 0
            NotificationCenter.default.post(name: Notification.Name("scrollToIndexPath"), object: nil)

            tabBarController?.selectedIndex = 2
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

class notificationCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblNotificationType: UILabel!
    @IBOutlet weak var viewContainer: UIView!
}
