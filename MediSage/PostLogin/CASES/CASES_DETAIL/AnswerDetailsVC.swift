//
//  AnswerDetailsVC.swift
//  MediSage
//
//  Created by VAMC on 27/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class AnswerDetailsVC: UIViewController, UITextViewDelegate {
    @IBOutlet weak var answerDetailsTextView: UITextView!
    var details = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        answerDetailsTextView?.attributedText = details.htmlAttributedString()
        answerDetailsTextView.font = UIFont.systemFont(ofSize: 16)
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
