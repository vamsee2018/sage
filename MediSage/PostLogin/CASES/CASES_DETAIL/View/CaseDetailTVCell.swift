//
//  CaseDetailTVCell.swift
//  MediSage
//
//  Created by VAMC on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
//import ButtonProgressBar_iOS

//protocol AnswerTapDelegate: AnyObject {
//    func AnswerTapped(row: Int)
//}

class CaseDetailTVCell: UITableViewCell {
        @IBOutlet weak var statusImageView: UIImageView!
        @IBOutlet weak var percentageLabel: UILabel!
        @IBOutlet weak var choiceButton: UIButton!
    
    @IBOutlet weak var progressBorr: UIProgressView!

//        weak var delegate: AnswerTapDelegate!
        var answerTapped : (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
//        statusImageView.layer.cornerRadius = statusImageView
        choiceButton?.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        choiceButton?.contentHorizontalAlignment = .left
    }

    @IBAction func answerTapped(_ sender: UIButton) {
        answerTapped?()
//        delegate?.AnswerTapped(row: sender.tag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
