//
//  CasesDetailViewModel.swift
//  MediSage
//
//  Created by Atul Prakash on 11/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

class CasesDetailViewModel {
    var caseDetailModel: CaseDetailModel?
    
    typealias ResponseCallback = (_ result:Any?, _ isSuccess:Bool, _ error: String) -> Void

    func getMemberID() -> String {
        var userData = [String:Any]()
        var strUserID  = String()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil) {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            if let id = userData["userID"] as? Int{
                strUserID = "\(id)"
            }
        }
        return strUserID
    }
    
    func getCaseDetail(caseID: String? = "", completion: @escaping ResponseCallback) {
        guard let caseIDIS = caseID else {
            return
        }
        let queryString = KJUrlConstants.CaseDetail + "\(caseIDIS)/" + getMemberID()
        print(queryString)
        let requestManager = RequestManager()
        requestManager.requestCommonGetMethod(strAPIName: queryString, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    completion(data,true,"")
                } else {
                    completion(nil,false,((result as? [String : AnyObject])?["Message"] as? String)!)
                }
            } else {
                guard let errorMessage = result as? [String : AnyObject] else {
                    completion(nil, false, "")
                    return
                }
                completion(nil,false, errorMessage["Message"] as? String ?? "")
            }
        }
    }
}
