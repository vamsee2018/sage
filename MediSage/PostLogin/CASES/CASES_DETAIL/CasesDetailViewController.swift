//
//  CasesDetailViewController.swift
//  MediSage
//
//  Created by Atul Prakash on 11/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import QuartzCore

class CasesDetailViewController: BaseViewController {
    
    @IBOutlet weak var answerDetailsButton: UIButton!
    @IBOutlet weak var detailScrollView: UIScrollView!
    var imagePath = ""
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var expertImage: UIImageView!
    @IBOutlet weak var expertName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var CollectionViewHeightConstraint: NSLayoutConstraint!
    weak var collectionViewCellDelegate: CollectionViewCellDelegate?
    @IBOutlet weak var answersTableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    
    var caseId: Int? = 0
    var thisWidth:CGFloat = 0
    
    var casesDetailViewModel = CasesDetailViewModel()
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        
        answersTableView.register(UINib(nibName: CaseDetailTVCell.className, bundle: nil), forCellReuseIdentifier: CaseDetailTVCell.className)
        
        answersTableView?.delegate = self
        answersTableView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        registerTableViewCell()
        setUpCollectionView()
        setUpNavBar()
        
        guard let caseIDIS = caseId else {
            return
        }
        getCaseDetails(caseIDIS)
        
        answerDetailsButton?.layer.cornerRadius = (answerDetailsButton?.bounds.height ?? 50)/2
        answerDetailsButton?.layer.backgroundColor = #colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1)
        answerDetailsButton?.layer.masksToBounds = true
        
    }
    
    fileprivate func getCaseDetails(_ caseIDIS: Int) {
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        DispatchQueue.main.async {
            self.showLoader()
        }
        casesDetailViewModel.getCaseDetail(caseID: "\(caseIDIS)") { [weak self] data, isSuccess, errorMessage in
            if let result = data {
                let dictResult = result as? Dictionary<String, Any>
                let disc = dictResult?.jsonData
                self?.casesDetailViewModel.caseDetailModel = self?.parse(json: disc ?? Data())
                self?.populateData()
            }
            DispatchQueue.main.async {
                self?.hideLoader()
            }
        }
    }
    
    func populateData() {
        let collection = self.casesDetailViewModel.caseDetailModel?.data.items ?? []
        self.setUpPageControl(imagesCollectionModel: collection)
        tableHeightConstraint?.constant = answersTableView?.contentSize.height ?? 0
        
        scrollViewHeightConstraint?.constant = (detailScrollView?.contentSize.height ?? 0)
        
        DispatchQueue.main.async(execute: { [self] in
            self.answersTableView?.reloadData()
            self.collectionView?.reloadData()
            adjustHeightOfTable()
        })
        
        // ExpertImage rounded
        expertImage?.layer.cornerRadius = (expertImage?.bounds.height ?? 50)/2
        expertImage?.layer.borderWidth = 3.0
        expertImage?.layer.borderColor = #colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1)
        expertImage?.layer.masksToBounds = true
        
        // ExpertName
        self.expertName.text = self.casesDetailViewModel.caseDetailModel?.data.expertName
        setLogoImage(logo: self.casesDetailViewModel.caseDetailModel?.data.expertImage ?? "", expertImagePath: self.casesDetailViewModel.caseDetailModel?.data.expertImagePath ?? "")
        
        self.descriptionLabel.text = self.casesDetailViewModel.caseDetailModel?.data.dataDescription.stripOutHtml()
        guard let question = self.casesDetailViewModel.caseDetailModel?.data.questions[0].question, !question.isEmpty else {
            return
        }
        self.questionLabel.text = "Q: \(question.stripOutHtml() ?? "")"
        self.questionLabel.textColor = #colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1)
        
        
        if let isValid = self.casesDetailViewModel.caseDetailModel?.data.questions[0].useful {
            if (isValid == true) {
                yesButton.titleLabel?.textColor = #colorLiteral(red: 0.168627451, green: 0.8196078431, blue: 0.8352941176, alpha: 1)
                noButton.titleLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            } else {
                noButton.titleLabel?.textColor = #colorLiteral(red: 0.168627451, green: 0.8196078431, blue: 0.8352941176, alpha: 1)
                yesButton.titleLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            }
        } else {
            noButton.titleLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            yesButton.titleLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    
    func setLogoImage(logo: String, expertImagePath: String)  {
        let editedText = expertImagePath.replacingOccurrences(of: "\"", with: "")
        let logoUrl = KJUrlConstants.ImageUrl + editedText + logo
        self.expertImage.sd_setImage(with: URL(string: logoUrl), placeholderImage:UIImage(named:"placeHolder")) { _, error, _, _ in
            if let err = error {
                print(err)
            }
        }
    }
    
    func registerTableViewCell() {
        answersTableView?.estimatedRowHeight = 80 //UITableView.automaticDimension//380
        answersTableView?.rowHeight = UITableView.automaticDimension
    }
    
    func parse(json: Data) -> CaseDetailModel? {
        let data = json
        let decoder = JSONDecoder()
        if let model = try? decoder.decode(CaseDetailModel.self, from: data) {
            return model
        }
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setUpNavBar() {
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = true
        self.viwTop.imgMore.isHidden = true
        self.viwTop.imgLogo.isHidden = false
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        navigationController?.navigationBar.isHidden = false
        navigationWithBackAndLogo()
    }
    
    //    func loadDeterminate() {
    //        self.choiceA.stopIndeterminate()
    //        self.choiceA.resetProgress()
    //        let timer = Timer.scheduledTimer(timeInterval: 0.01,
    //                                         target: self,
    //                                         selector: #selector(self.animateDeterminate),
    //                                         userInfo: time,
    //                                         repeats: true)
    //        RunLoop.current.add(timer, forMode: .default)
    //    }
    //
    //    @objc func animateDeterminate(sender: Timer) {
    //        if self.choiceA.progress >= 1.0 {
    //            sender.invalidate()
    //        }
    //        else {
    //            self.choiceA.setProgress(progress: self.choiceA.progress + CGFloat(0.02), true)
    //        }
    //    }
    
    @IBAction func seeAnswerDetailsTapped(_ sender: Any) {
//        answer_details
        let answerDetail = self.casesDetailViewModel.caseDetailModel?.data.questions[0].answerDetails ?? ""
        let sb = UIStoryboard(name: KJViewIdentifier.CasesDetailViewController, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.AnswerDetailsVC) as! AnswerDetailsVC
        vc.details = answerDetail
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func wasThisHelpFulTapped(_ sender: UIButton) {
        let viewModel = CasesViewModel()
            viewModel.postUsefilRequest(caseID: caseId ?? 0, feedBack: true) { [weak self] data, isSuccess, errorMessage in
                if isSuccess {
                    if sender.tag == 9 {
                        self?.yesButton.titleLabel?.textColor = #colorLiteral(red: 0.168627451, green: 0.8196078431, blue: 0.8352941176, alpha: 1)
                        self?.noButton.titleLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                    } else {
                        self?.noButton.titleLabel?.textColor = #colorLiteral(red: 0.168627451, green: 0.8196078431, blue: 0.8352941176, alpha: 1)
                        self?.yesButton.titleLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                    }
                } else {
                    self?.view.makeToast(message: errorMessage)
                }
            }
    }
}

extension CasesDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.casesDetailViewModel.caseDetailModel?.data.questions[0].option.count ?? 0
    }
    
    
    fileprivate func showPercentageLabel(_ item: Question, _ indexPath: IndexPath, _ cell: CaseDetailTVCell) {
           let value = "\(item.option[indexPath.row].answerPercentage)"
           let finalVal = value.components(separatedBy: ".").first
           cell.percentageLabel.text = (finalVal ?? "0") + "%"
        
        if let intVal: Int = Int(finalVal ?? "") {
            cell.progressBorr.progress = Float(intVal) / 100
        }
       }
    
    fileprivate func handleOptionsStack(_ item: Question, _ indexPath: IndexPath, _ cell: CaseDetailTVCell) {
      
        // statusImage
        let isCorrectAnwer = item.option[indexPath.row].isCorrectOption
        cell.statusImageView?.isHidden = isCorrectAnwer ? false : true
        cell.statusImageView?.backgroundColor = isCorrectAnwer ? #colorLiteral(red: 0.168627451, green: 0.8196078431, blue: 0.8352941176, alpha: 1) : #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        
        //Selected option with borderColor
        if let colletionObject = self.casesDetailViewModel.caseDetailModel?.data.questions[0].option {
            let optionSelected = item.optionSelected
            if optionSelected == Int(colletionObject[indexPath.row].key) {
                cell.choiceButton?.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            } else {
                cell.choiceButton?.layer.borderColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.answersTableView?.dequeueReusableCell(withIdentifier: "CaseDetailTVCell") as? CaseDetailTVCell {
            guard let item = self.casesDetailViewModel.caseDetailModel?.data.questions[0] else {
                return CaseDetailTVCell()
            }
            cell.selectionStyle = .none
            
            let alphabets = ["A - ","B - ", "C - ","D - ","E - ","F - "]
            
            cell.choiceButton.tag = indexPath.row
            let char = alphabets[indexPath.row]
            cell.choiceButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.choiceButton?.setTitle("  \(char)\(item.option[indexPath.row].value)", for: .normal)
            cell.choiceButton.backgroundColor = .clear
            cell.choiceButton?.layer.cornerRadius = 25
            cell.choiceButton?.layer.borderWidth = 1.0
            cell.choiceButton?.layer.masksToBounds = true
            cell.choiceButton?.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
            cell.progressBorr?.layer.cornerRadius = 25
            cell.progressBorr?.layer.masksToBounds = true
            cell.progressBorr?.clipsToBounds = true
            cell.progressBorr?.layer.borderWidth = 0
            cell.progressBorr?.layer.borderColor = UIColor.clear.cgColor
            cell.progressBorr?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            
            cell.statusImageView?.layer.cornerRadius = 10
            cell.statusImageView?.layer.masksToBounds = true
            cell.statusImageView?.clipsToBounds = true
            cell.statusImageView?.layer.borderWidth = 0
            cell.statusImageView?.layer.borderColor = UIColor.clear.cgColor

            
            //Current question answer tapped
            cell.answerTapped = { [unowned self] in
//                loadDeterminate()
                showPercentageLabel(item, indexPath, cell)
                handleOptionsStack(item, indexPath, cell)
            }
            
            //This is already answered quiestion Earlier
            if item.isAlreadyAnswered {
                showPercentageLabel(item, indexPath, cell)
                handleOptionsStack(item, indexPath, cell)
            }
        
            //TODO: isUsefull
            /*if let isUsefull = item.useful {
             if (isUsefull == true) {
             self.yesButton.titleLabel?.textColor = #colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1)
             } else {
             self.noButton.titleLabel?.textColor = #colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1)
             }
             }*/
            
            
            return cell
        }
        return CaseDetailTVCell()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func adjustHeightOfTable() {
        let height = self.answersTableView.contentSize.height
        tableHeightConstraint?.constant = height
            self.view.layoutIfNeeded()
    }
}

extension CasesDetailViewController {
    fileprivate func setUpCollectionView() {
        collectionView.isPagingEnabled = true
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 200)
        flowLayout.minimumLineSpacing = 5
        flowLayout.minimumInteritemSpacing = 5
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.showsHorizontalScrollIndicator = false
        
        // Comment if you set Datasource and delegate in .xib
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        // Register the xib for collection view cell
        let cellNib = UINib(nibName: "CollectionViewCell", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "collectionviewcellid")
    }
    
    func setUpPageControl(imagesCollectionModel: [Item]?) {
        thisWidth = CGFloat(self.view.frame.width)
        pageControl.numberOfPages = imagesCollectionModel?.count ?? 0
        pageControl.hidesForSinglePage = true
    }
}

