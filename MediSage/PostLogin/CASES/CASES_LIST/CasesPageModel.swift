//
//  CasesPageModel.swift
//  MediSage
//
//  Created by Atul Prakash on 11/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

class CasesPageMadel: NSObject {
    var last_page_url : Int?
    var prev_page_url : Int?
    var from : String?
    var total: String?
    var first_page_url: String?
    var next_page_url: String?
    var per_page: String?
    var last_page: Int?
    var casesCollection = [CasesModel]() // Quitz Quextions


    func handleCasesPageResponse(info : Dictionary<String,AnyObject>) -> CasesPageMadel {
        
        let pageModel = CasesPageMadel()
        let dict: [String : AnyObject]  = info["Data"] as! [String : AnyObject]
        
        if let expertID = dict["last_page_url"] as? Int {
            pageModel.last_page_url = expertID
        }
        if let communityId = dict["prev_page_url"] as? Int {
            pageModel.prev_page_url = communityId
        }
        
        if let title = dict["from"] as? String {
            pageModel.from = title
        }
        if let title = dict["last_page"] as? String {
            pageModel.last_page = last_page
        }
        if let casesImagePath = dict["total"] as? String {
            pageModel.total = casesImagePath
        }
        if let useful = dict["first_page_url"] as? String {
            pageModel.first_page_url = useful
        }
        
        if let totalUseful = dict["next_page_url"] as? String {
            pageModel.next_page_url = totalUseful
        }
        
        if let questionType = dict["per_page"] as? String {
            pageModel.per_page = questionType
        }

        return pageModel
    }
}
