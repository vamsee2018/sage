//
//  CasesViewController.swift
//  MediSage
//
//  Created by Atul Prakash on 08/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import Foundation

class CasesViewController: BaseViewController {
    
    @IBOutlet weak var casesTableView: UITableView!
    var viewModel = CasesViewModel()
    let casesPageModel = CasesPageMadel()
    let casesModel = CasesModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCell()
        getCasesList(withNextPageURL: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavBar()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func registerTableViewCell() {
        casesTableView.estimatedRowHeight = 380//UITableView.automaticDimension//380
        casesTableView.rowHeight = UITableView.automaticDimension
        casesTableView.register(UINib(nibName: CasesTableViewCell.className, bundle: nil), forCellReuseIdentifier: CasesTableViewCell.className)
    }
}

extension CasesViewController {
    func setUpNavBar() {
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = true
        self.viwTop.imgMore.isHidden = true
        self.viwTop.imgLogo.isHidden = false
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        navigationController?.navigationBar.isHidden = false
        navigationWithBackAndLogo()
    }
}

// MARK: UITableViewDelegate
extension CasesViewController: UITableViewDelegate {
    
}

// MARK: UITableViewDataSource
extension CasesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CasesTableViewCell.className,
            for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    fileprivate func handleShareButtonAction(_ casesCell: CasesTableViewCell, _ indexPath: IndexPath) {
        casesCell.shareButtonAction = { [unowned self] in
            let item = self.viewModel.casesList[indexPath.row]
            self.shareCase(title: item.title, urlString: "")
        }
    }
    
    fileprivate func handleUsefulButtonTap(_ casesCell: CasesTableViewCell, _ indexPath: IndexPath, _ tableView: UITableView) {
        casesCell.usefulTapAction = { [unowned self] in
            
            let item = self.viewModel.casesList[indexPath.row]
       
            if (self.viewModel.casesList[indexPath.row].useful == nil || self.viewModel.casesList[indexPath.row].useful == false) {
                self.viewModel.casesList[indexPath.row].useful = false
            }
            
            if let isNotUseful = self.viewModel.casesList[indexPath.row].useful {
                viewModel.postUsefilRequest(caseID: item.id ?? 0, feedBack: true) { [weak self] data, isSuccess, errorMessage in
                    if isSuccess {
                        self?.viewModel.casesList[indexPath.row].useful = !isNotUseful
                        if (self?.viewModel.casesList[indexPath.row].useful == true) {
                            var count: Int = self?.viewModel.casesList[indexPath.row].totalUseful ?? 0
                            count = count + 1
                            self?.viewModel.casesList[indexPath.row].totalUseful = count
                        } else {
                            var count: Int = self?.viewModel.casesList[indexPath.row].totalUseful ?? 0
                            count = count - 1
                            self?.viewModel.casesList[indexPath.row].totalUseful = count
                        }
                    } else {
                        self?.view.makeToast(message: errorMessage)
                    }
                    tableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .automatic)
                }
            }
        }
    }
    
    fileprivate func handleAnswerTap(_ casesCell: CasesTableViewCell?, id: Int, collectionImagePath: String) {
        //Answer button Tapped -> CAse Detail
        casesCell?.casesDetailTapped = { [unowned self] in
//            https://medisage-cases.s3.ap-south-1.amazonaws.com/cases/
            self.navigateToCaseDetail(id: id, collectionImagePath: collectionImagePath)
        }
    }
    
    func navigateToCaseDetail(id: Int, collectionImagePath: String) {
        let sb = UIStoryboard(name: KJViewIdentifier.CasesDetailViewController, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.CasesDetailViewController) as! CasesDetailViewController
        vc.caseId = id
        vc.imagePath = collectionImagePath
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.casesList.count - 1 {
            if let nextPage = viewModel.casesPageModel.next_page_url, !nextPage.isEmpty  {
                getCasesList(withNextPageURL: nextPage)
            }
        }
        
        if let casesCell = cell as? CasesTableViewCell {
            let cases = viewModel.itemAt(indexPath)
            casesCell.indexPath = indexPath
            casesCell.configure(title: cases.expertName ?? "",
                                description: cases.caseDescription ?? "",
                                views: cases.views ?? 0,
                                useful: cases.useful ?? false,
                                totalUseFull: cases.totalUseful ?? 0,
                                casesImagePath: cases.casesImagePath,
                                logo: cases.expertImage ?? "",
                                courousalCollection: cases.images,
                                expertImagePath: cases.expertImagePath ?? "")
            
            
            
            //POLL
            if self.viewModel.casesList[indexPath.row].questionType == "mcq" {
                casesCell.pollLabel.isHidden = false
            } else {
                casesCell.pollLabel.isHidden = true
            }
                        
            //Share
            handleShareButtonAction(casesCell, indexPath)
            //Useful action with webservice
            handleUsefulButtonTap(casesCell, indexPath, tableView)
            //Answer Cases
            print("case id for detail from Answerbutton Tap ====== \(cases.id ?? 0)")
            handleAnswerTap(casesCell, id: cases.id ?? 0, collectionImagePath: cases.casesImagePath ?? "")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Swift 4.2 onwards
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cases = viewModel.itemAt(indexPath)
        print("case id for detail from didSelectRowAt Tap ====== \(cases.id ?? 0)")
        self.navigateToCaseDetail(id: cases.id ?? 0, collectionImagePath: cases.casesImagePath ?? "")
    }
}

extension CasesViewController {
    func shareCase(title: String?, urlString: String?) {
        let items: [Any] = ["I would like to share this intresting patitent case study with you \(title ?? "")., Download the MediSage app to read", URL(string: "https://apps.apple.com/in/app/medisage/id1497265614")!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
}

extension CasesViewController {
    
    //MARK:- Get Cases list API Call
    func getCasesList(withNextPageURL: String) {
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        
        self.showLoader()
        viewModel.getCasesList(withNextPageURL: withNextPageURL) { [weak self] data, isSuccess, errorMessage in
            if let result = data {
                
                // PAGE DETAILS
                if let casesPageModel: CasesPageMadel =  self?.casesPageModel.handleCasesPageResponse(info: result as! Dictionary<String, AnyObject>) {
                    self?.viewModel.casesPageModel = casesPageModel
                }
                
                // PAGE ITEMS
                if let casesList: [CasesModel] =  self?.casesModel.handleCasesList(info: result as! Dictionary<String, AnyObject>) {
                    print(casesList)
                    let prevItems: [CasesModel] = self?.viewModel.casesList as? [CasesModel] ?? [CasesModel]()
                    let currentItmes = casesList
                    self?.viewModel.casesList = prevItems + currentItmes
                    DispatchQueue.main.async {
                        self?.casesTableView?.reloadData()
                    }
                }
            }
            self?.hideLoader()
        }
    }
}


