//
//  CasesModel.swift
//  MediSage
//
//  Created by Atul Prakash on 08/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

class ImageItems:NSObject {
    var case_id: Int?
    var imageName: String?
    
    var imageItemsModel = [ImageItems]()
    
    func handleImageItems(info: [[String : AnyObject]]) -> [ImageItems] {
        var itemsCollection = [ImageItems]()
        for obj in info {
            if let dict = obj as? [String: Any] {
                let imageItems = ImageItems()
                
                if let imageName = dict["image_name"] as? String {
                    imageItems.imageName = imageName
                }
                if let case_id = dict["case_id"] as? Int {
                    imageItems.case_id = case_id
                }
                itemsCollection.append(imageItems)
            }
        }
        return itemsCollection
    }
}

class CasesModel: NSObject {
    var expertID : Int?
    var communityId : Int?
    var title : String?
    var casesImagePath: String?
    var useful: Bool?
    var totalUseful: Int?
    var questionType: String?
    var expertName: String?
    var viewMultiplicationFactor: String?
    var responseCount: Int?
    var views: Int?
    var expertImage: String?
    var id: Int?
    var linkId: String?
    var expertImagePath: String?
    var caseDescription: String?
    var images: [ImageItems]?

    
    var casesModel = [CasesModel]()
    var imageItems = ImageItems()
    
    func handleCasesList(info: Dictionary<String,AnyObject>) -> [CasesModel] {
        var casesCollection = [CasesModel]()
        let info: [String : AnyObject]  = info["Data"] as! [String : AnyObject]
        if let arrObject = info["data"] as? [[String:Any]] {
        print(arrObject)
        for obj in arrObject {
            if let dict = obj as? [String: Any] {
                let casesModel = CasesModel()
                
                if let expertID = dict["expert_id"] as? Int {
                    casesModel.expertID = expertID
                }
                if let communityId = dict["community_id"] as? Int {
                    casesModel.communityId = communityId
                }
                
                if let title = dict["title"] as? String {
                    casesModel.title = title
                }
                if let casesImagePath = dict["cases_image_path"] as? String {
                    casesModel.casesImagePath = casesImagePath
                }
                if let useful = dict["useful"] as? Bool {
                    casesModel.useful = useful
                }
                
                if let totalUseful = dict["total_useful"] as? Int {
                    casesModel.totalUseful = totalUseful
                }
                
                if let questionType = dict["question_type"] as? String {
                    casesModel.questionType = questionType
                }
                
                if let expertName = dict["expert_name"] as? String {
                    casesModel.expertName = expertName
                }
                
                if let expertImagePath = dict["expert_image_path"] as? String {
                    casesModel.expertImagePath = expertImagePath
                }
                
                if let viewMultiplicationFactor = dict["view_multiplication_factor"] as? String {
                    casesModel.viewMultiplicationFactor = viewMultiplicationFactor
                }
                
                if let responseCount = dict["response_count"] as? Int {
                    casesModel.responseCount = responseCount
                }
                if let views = dict["views"] as? Int {
                    casesModel.views = views
                }
                
                if let expertImage = dict["expert_image"] as? String {
                    casesModel.expertImage = expertImage
                }
                
                if let caseDescription = dict["description"] as? String {
                    casesModel.caseDescription = caseDescription
                }
                
                if let id = dict["id"] as? Int {
                    casesModel.id = id
                }
                
                if let linkId = dict["link_id"] as? String {
                    casesModel.linkId = linkId
                }
                
                if let info = dict["items"] as? [[String : AnyObject]] {
                casesModel.images = imageItems.handleImageItems(info: info)
                }
                
                
                casesCollection.append(casesModel)
            }
        }
        }
        return casesCollection
    }
    
}
