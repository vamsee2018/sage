//
//  ColorModel.swift
//  CollectionViewInsideTableViewExample
//
//  Created by John Codeos on 12/21/19.
//  Copyright © 2019 John Codeos. All rights reserved.
//

import Foundation
import UIKit

struct CollectionViewCellModel {
    var case_id: Int?
    var imageName: String?
}
