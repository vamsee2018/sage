import Foundation

// MARK: - CaseDetailModel
struct CaseDetailModel: Codable {
    let status: Int
    let message: String
    let data: DataClass

    enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct DataClass: Codable {
    let id: Int
    let linkID, title, dataDescription: String
    let communityID: JSONNull?
    let views, viewMultiplicationFactor, expertID: Int
    let expertName, expertImage, questionType, expertImagePath: String
    let casesImagePath: String
    let questions: [Question]
    let comments: JSONNull?
    let items: [Item]

    enum CodingKeys: String, CodingKey {
        case id
        case linkID = "link_id"
        case title
        case dataDescription = "description"
        case communityID = "community_id"
        case views
        case viewMultiplicationFactor = "view_multiplication_factor"
        case expertID = "expert_id"
        case expertName = "expert_name"
        case expertImage = "expert_image"
        case questionType = "question_type"
        case expertImagePath = "expert_image_path"
        case casesImagePath = "cases_image_path"
        case questions, comments, items
    }
}

// MARK: - Item
struct Item: Codable {
    let caseID: Int
    let imageName: String

    enum CodingKeys: String, CodingKey {
        case caseID = "case_id"
        case imageName = "image_name"
    }
}

// MARK: - Question
struct Question: Codable {
    let id: Int
    let question, answerDetails: String
    let showAnswer, isAlreadyAnswered: Bool
    let useful: Bool?
    let optionSelected: Int
    let option: [Option]

    enum CodingKeys: String, CodingKey {
        case id, question
        case answerDetails = "answer_details"
        case showAnswer = "show_answer"
        case isAlreadyAnswered, useful, optionSelected, option
    }
}

// MARK: - Option
struct Option: Codable {
    let key, value: String
    let isCorrectOption: Bool
    let totalAnswer: Int
    let answerPercentage: Double

    enum CodingKeys: String, CodingKey {
        case key, value, isCorrectOption
        case totalAnswer = "total_answer"
        case answerPercentage = "answer_percentage"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
