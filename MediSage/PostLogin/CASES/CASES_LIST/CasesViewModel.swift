//
//  CasesViewModel.swift
//  MediSage
//
//  Created by Atul Prakash on 08/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation


class CasesViewModel {
    typealias ResponseCallback = (_ result:Any?, _ isSuccess:Bool, _ error: String) -> Void
    var casesList = [CasesModel]()
    var casesPageModel = CasesPageMadel()
    
    func numberOfItems() -> Int {
        casesList.count
    }
    
    func itemAt(_ index: IndexPath) -> CasesModel {
        return casesList[index.row]
    }
    
    func getCasesList(withNextPageURL: String?, completion: @escaping ResponseCallback) {
        var pageUrl = ""
        if let np = withNextPageURL, !np.isEmpty {
            if let lastComponent = np.components(separatedBy: "/").last {
                pageUrl = KJUrlConstants.CasesList + lastComponent
            }
        } else {
            pageUrl = KJUrlConstants.CasesList + getMemberID()
        }
        
        let requestManager = RequestManager()
        requestManager.requestCommonGetMethod(strAPIName: pageUrl, strParameterName:"") { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    completion(data,true,"")
                } else {
                    completion(nil,false,((result as? [String : AnyObject])?["Message"] as? String)!)
                }
            } else {
                guard let errorMessage = result as? [String : AnyObject] else {
                    completion(nil, false, "")
                    return
                }
                completion(nil,false, errorMessage["Message"] as? String ?? "")
            }
        }
    }
    
    func getMemberID() -> String {
        var userData = [String:Any]()
        var strUserID  = String()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil) {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            if let id = userData["userID"] as? Int{
                strUserID = "\(id)"
            }
        }
        return strUserID
    }
    
    // Use full check for cases list page
    func postUsefilRequest(caseID: Int, feedBack: Bool, completion: @escaping ResponseCallback) {
        let strParam : [String:Any] = ["member_id": getMemberID() ,"case_id": caseID ,"feedback": feedBack]
        let requestManager = RequestManager()
        requestManager.requestCommonPostMethod(strAPIName: KJUrlConstants.CaseFeedback, strParameterName: strParam) { (result, isSuccess, error) in
            if isSuccess {
                if let data = result {
                    completion(data,true,"")
                }
            } else {
                completion(nil,false,((result as? [String : AnyObject])?["Message"] as? String)!)
                //self.view.makeToast(message: error)
            }
        }
    }
}
