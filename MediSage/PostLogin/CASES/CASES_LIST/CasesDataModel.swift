//
//  CasesDataModel.swift
//  MediSage
//
//  Created by VAMC on 22/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import Foundation

// MARK: - CasesModel
struct CasesDataModel: Codable {
    let status: Int
    let message: String
    let data: CasesDataClass

    enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Message"
        case data = "Data"
    }
}

// MARK: - DataClass
struct CasesDataClass: Codable {
    let currentPage: Int
    let data: [CasesDatum]
    let firstPageURL: String
    let from, lastPage: Int
    let lastPageURL: String
    let links: [Link]
    let nextPageURL, path: String
    let perPage: Int
    let prevPageURL: JSONNull?
    let to, total: Int

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
        case firstPageURL = "first_page_url"
        case from
        case lastPage = "last_page"
        case lastPageURL = "last_page_url"
        case links
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to, total
    }
}

// MARK: - Datum
struct CasesDatum: Codable {
    let id: Int
    let linkID, title, datumDescription: String
    let communityID: Int?
    let views: Int
    let questionType: String
    let expertID: Int
    let expertName, expertImage: String
    let viewMultiplicationFactor: Int
    let expertImagePath: String
    let casesImagePath: String
    let responseCount: Int
    let useful: JSONNull?
    let totalUseful: Int
    let items: [CaseItem]

    enum CodingKeys: String, CodingKey {
        case id
        case linkID = "link_id"
        case title
        case datumDescription = "description"
        case communityID = "community_id"
        case views
        case questionType = "question_type"
        case expertID = "expert_id"
        case expertName = "expert_name"
        case expertImage = "expert_image"
        case viewMultiplicationFactor = "view_multiplication_factor"
        case expertImagePath = "expert_image_path"
        case casesImagePath = "cases_image_path"
        case responseCount = "response_count"
        case useful
        case totalUseful = "total_useful"
        case items
    }
}

// MARK: - Item
struct CaseItem: Codable {
    let caseID: Int
    let imageName: String

    enum CodingKeys: String, CodingKey {
        case caseID = "case_id"
        case imageName = "image_name"
    }
}

// MARK: - Link
struct Link: Codable {
    let url: String?
    let label: String
    let active: Bool
}
