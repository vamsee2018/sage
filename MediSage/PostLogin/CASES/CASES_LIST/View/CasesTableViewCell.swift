//
//  CasesTableViewCell.swift
//  MediSage
//
//  Created by Atul Prakash on 08/07/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import SDWebImage
import QuartzCore

//protocol CasesCellDelegate: AnyObject {
//    func usefulTapped()
////    func shareCase(title: String?, urlString: String?)
//}

protocol CollectionViewCellDelegate: AnyObject {
    func collectionView(collectionviewcell: CollectionViewCell?, index: Int, didTappedInTableViewCell: CasesTableViewCell)
}

class CasesTableViewCell: UITableViewCell {
    var shareButtonAction : (() -> ())?
    var usefulTapAction : (() -> ())?
    var casesDetailTapped : (() -> ())?

    @IBOutlet weak var cardView: CardView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    var thisWidth:CGFloat = 0
    
    @IBOutlet weak var pollHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var CollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pollLabel: UILabel!
    weak var collectionViewCellDelegate: CollectionViewCellDelegate?
    
    @IBOutlet private weak var collectionView: UICollectionView!
    var imagesCollectionModel: [ImageItems]?
    var imagePath: String?
    @IBOutlet weak var usefulImageView: UIImageView!
    @IBOutlet weak var expertImage: UIImageView!
    @IBOutlet weak var expertName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var answerButton: UIButton! {
        didSet {
            answerButton.layer.cornerRadius = answerButton.bounds.size.height/2
        }
    }
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var usefulButton: UIButton!
    var indexPath: IndexPath?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func setUpCollectionView() {
        collectionView.isPagingEnabled = true
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 200)
        flowLayout.minimumLineSpacing = 5
        flowLayout.minimumInteritemSpacing = 5
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.showsHorizontalScrollIndicator = false
        
        // Comment if you set Datasource and delegate in .xib
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        // Register the xib for collection view cell
        let cellNib = UINib(nibName: "CollectionViewCell", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "collectionviewcellid")
    }
    
    func setUpPageControl(imagesCollectionModel: [ImageItems]?) {
        thisWidth = CGFloat(self.frame.width)
        pageControl.numberOfPages = imagesCollectionModel?.count ?? 0
        pageControl.hidesForSinglePage = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
        self.cardView?.setCardView()
        setUpCollectionView()
        pollLabel?.roundedBottom()
        
        expertImage?.layer.cornerRadius = (expertImage?.bounds.height ?? 50)/2
        expertImage?.layer.borderWidth = 3.0
        expertImage?.layer.borderColor = #colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1)
        expertImage?.layer.masksToBounds = true
        
        if let count = imagesCollectionModel?.count, count == 0 {
            pollHeightConstraint?.constant = 0
            CollectionViewHeightConstraint?.constant = 0
        } else {
            pollHeightConstraint?.constant = 30
            CollectionViewHeightConstraint?.constant = 200
        }
    }
    
    func setLogoImage(logo: String, expertImagePath: String)  {
        let editedText = expertImagePath.replacingOccurrences(of: "\"", with: "")
        let logoUrl = KJUrlConstants.ImageUrl + editedText + logo
        self.expertImage.sd_setImage(with: URL(string: logoUrl), placeholderImage:UIImage(named:"placeHolder")) { _, error, _, _ in
            if let err = error {
                print(err)
            }
        }
    }
    
    @IBAction func usefulAction(_ sender: UIButton) {
        usefulTapAction?()
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        shareButtonAction?()
    }
    
    func configure(title: String, description: String, views: Int, useful: Bool, totalUseFull: Int, casesImagePath: String?, logo:String?, courousalCollection: [ImageItems]?, expertImagePath: String) {
        setUpPageControl(imagesCollectionModel: courousalCollection)
        
        self.setLogoImage(logo: logo ?? "", expertImagePath: expertImagePath)
        self.updateCellWith(imagePath: casesImagePath ?? "", row: courousalCollection ?? [])
        
        self.expertName?.text = title
        self.descriptionLabel?.text = description.stripOutHtml()
        self.descriptionLabel?.lineBreakMode = .byWordWrapping
        let readmoreFontColor = UIColor.blue
        DispatchQueue.main.async {
            self.descriptionLabel?.addTrailing(with: "... ", moreText: "SeeMore", moreTextFont: self.descriptionLabel?.font ?? UIFont(), moreTextColor: readmoreFontColor)
        }
        
        self.viewCountLabel?.text = "\(views) Views"
        usefulButton?.setTitle(("\(totalUseFull) Useful"), for: .normal)
        
        if useful {
            usefulImageView?.image = #imageLiteral(resourceName: "Insightful_a")
            usefulButton?.setTitleColor(#colorLiteral(red: 0.4, green: 0.8078431373, blue: 0.8274509804, alpha: 1), for: .normal)
        } else {
            usefulImageView?.image = #imageLiteral(resourceName: "Insightful")
            usefulButton?.setTitleColor(UIColor.black, for: .normal)
        }
    }
}

extension CasesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    // The data we passed from the TableView send them to the CollectionView Model
    func updateCellWith(imagePath: String, row: [ImageItems]) {
        self.imagePath = imagePath
        self.imagesCollectionModel = row
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell
        print("I'm tapping the \(indexPath.item)")
        self.collectionViewCellDelegate?.collectionView(collectionviewcell: cell, index: indexPath.item, didTappedInTableViewCell: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesCollectionModel?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // Set the data for each cell (color and color name)
    fileprivate func handleCourousalImages(_ cell: CollectionViewCell, _ indexPath: IndexPath) {
        let image =  self.imagesCollectionModel?[indexPath.item].imageName ?? ""
        guard let editedText = self.imagePath?.replacingOccurrences(of: "\"", with: "") else { return }
        cell.caseImage.sd_setImage(with: URL(string: editedText + image), placeholderImage:UIImage(named:"placeHolder")) { _, error, _, _ in
            if let err = error {
                print(err)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionviewcellid", for: indexPath) as? CollectionViewCell {
            
            handleCourousalImages(cell, indexPath)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    // Add spaces at the beginning and the end of the collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -5;
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.section
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        thisWidth = CGFloat(self.frame.width)
        return CGSize(width: thisWidth, height: self.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        self.pageControl.currentPage = Int(roundedIndex)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}


extension CasesTableViewCell {
    @IBAction func casesDetailTapped(_ sender: UIButton) {
        casesDetailTapped?()
    }
}
