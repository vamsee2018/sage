//
//  SearchEntity.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class SearchEntity: NSObject {

    var strImageName : String? = ""
    var strDoctorName: String? = ""
    var strImagePath: String? = ""
    var strPath: String? = ""
    var strVideoTitle: String? = ""
    var strVideoID: String? = ""
    var striFramePath: String? = ""
    var strLogoImageName : String? = ""

    var intID: Int?
    var strDesignation: String?
    var dataArr = [SearchEntity]()
    var communityArr = [String]()
    var strDescription: String? = ""
    var strRecommendedFor: String? = ""
    var intFrequency: Int?
    var strSearchTerm: String?
    var arrVideos = [SearchEntity]()
    var arrExperts = [SearchEntity]()
    var arrCommunities = [SearchEntity]()
    var arrPartners = [SearchEntity]()
    
    func parseSearchList(info : Dictionary<String,AnyObject>)->SearchEntity{
        
        var mainEnt = SearchEntity()
        let dictClass = info["Data"] as? [String:Any]
        
        if let arrObj = dictClass?["videos"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = SearchEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let recommendedFor = dict["recommended_for"] as? String{
                        ent.strRecommendedFor = recommendedFor
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    if let videoID = dict["video_link"] as? String{
                        ent.strVideoID = videoID
                    }
                    if let iFramePath = dict["iframe_path"] as? String{
                        ent.striFramePath = iFramePath
                    }
                    
                    mainEnt.arrVideos.append(ent)

                }
            }
        }
        
        if let arrObj = dictClass?["experts"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = SearchEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["image_name"] as? String{
                        ent.strImageName = imageName
                    }
                    if let recommendedFor = dict["recommended_for"] as? String{
                        ent.strRecommendedFor = recommendedFor
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    if let videoID = dict["video_link"] as? String{
                        ent.strVideoID = videoID
                    }
                    if let iFramePath = dict["iframe_path"] as? String{
                        ent.striFramePath = iFramePath
                    }
                    
                    mainEnt.arrExperts.append(ent)

                }
            }
        }
        
        if let arrObj = dictClass?["communities"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = SearchEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["logo_image"] as? String{
                        ent.strImageName = imageName
                    }
                    
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    
                    
                    mainEnt.arrCommunities.append(ent)

                }
            }
        }
        if let arrObj = dictClass?["partners"] as? [[String:Any]]{
            
            for item in arrObj {
                
                if let dict = item as? [String:AnyObject]
                {
                    let ent = SearchEntity()
                    
                    if let id = dict["id"] as? Int{
                        ent.intID = id
                    }
                    if let name = dict["name"] as? String{
                        ent.strDoctorName = name
                    }
                    if let title = dict["title"] as? String{
                        ent.strVideoTitle = title
                    }
                    if let imagePath = dict["path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let imageName = dict["logo_image"] as? String{
                        ent.strImageName = imageName
                    }
                    if let logo = dict["app_logo_image"] as? String{
                        ent.strLogoImageName = logo
                    }
                    if let description = dict["description"] as? String{
                        ent.strDescription = description
                    }
                    
                    mainEnt.arrPartners.append(ent)

                }
            }
        }
        
        return mainEnt
    }
    
    
    //MARK:-  Method for Search Suggestion  list  api data parsing
    func parseSearchSuggestionList(info : Dictionary<String,AnyObject>)->[SearchEntity]{
        
        var mainEnt = [SearchEntity]()
        let dictClass : [[String : AnyObject]]  = info["data"] as! [[String : AnyObject]]
            
            for obj in dictClass
            {
                if let dict = obj as? [String:AnyObject]
                {
                    let ent = SearchEntity()

                    if let rewards = dict["frequency"] as? Int{
                        ent.intFrequency = rewards
                    }
                    
                    if let title = dict["term"] as? String{
                        ent.strSearchTerm = title
                    }
                    
                    mainEnt.append(ent)
                    
                }
            }
        
        return mainEnt
    }
    
}
