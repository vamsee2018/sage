//
//  SearchViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
import DropDown

class SearchViewController: BaseViewController,UISearchBarDelegate {

    //MARK:- Custom Properties
    var searchArray = SearchEntity()
    var arrSearchData = [SearchEntity]()
    var arrSearchSuggestions = [SearchEntity]()
    var bIsMultiSelect = false
    var selectedSearchTag = "all"
    var userData = [String:Any]()
    var strUserID  = String()
    var filterArray = [String]()
    var strMobileNumber  = String()
    var dropButton = DropDown()
    var data: [String] = []
    var dataFiltered: [String] = []
    //MARK:- IBOutlet Connections
    @IBOutlet weak var segControl: UISegmentedControl!
    @IBOutlet weak var tblViewSearch: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewNoResult: UIView!
    @IBOutlet weak var viewSearch: UIView!
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
          super.viewDidLoad()

        tblViewSearch.allowsSelection = true
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
//        dropButton.anchorView = searchBar
//        dropButton.bottomOffset = CGPoint(x: 10, y:(dropButton.anchorView?.plainView.bounds.height)!)
//        dropButton.backgroundColor = .white
//        
//        dropButton.direction = .bottom
//        dropButton.width = searchBar.frame.width
//        DropDown.appearance().setupCornerRadius(10) // available since v2.3.6
//
//        dropButton.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)") //Selected item: code at index: 0
//            dropButton.hide()
//            getSearchList()
//        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        filterArray.append(selectedSearchTag)
        getSearchList()
       // getSuggestionList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("Search", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Search Page",
            AnalyticsParameterContentType: "Search"
        ])
       
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false

        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
        tblViewSearch.register(UINib(nibName: "CustomHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "CustomHeaderView")

        
        segControl.apportionsSegmentWidthsByContent = true
        segControl.font(name: "Gotham", size: 10)
        // selected option color
        segControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)

        // color of other options
        segControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray], for: .normal)
        
        segControl.addTarget(self, action: #selector(segmentControl(_:)), for: .valueChanged)
        segControl.selectedSegmentIndex = 0
        
        //SearchBar Settings
        searchBar.layer.borderColor = UIColor.lightGray.cgColor
        searchBar.layer.borderWidth = 0.7
        searchBar.delegate = self
        if let searchTextField = self.searchBar.value(forKey: "searchField") as? UITextField ,
           let clearButton = searchTextField.value(forKey: "_clearButton")as? UIButton {
            clearButton.addTarget(self, action: #selector(self.clearSearch), for: .touchUpInside)
        }
    }
    
    @objc func clearSearch(){
        getSearchList()
    }
    
   
    
    //MARK:- Get Search API Call
    func getSearchList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let searchEntity = SearchEntity()
        
        let strParam : [String:Any] = ["search_term": searchBar.text ?? "" ,"filter": filterArray ,"member_id": strUserID]
        self.showLoader()
        requestManager.requestCommonPostMethod(strAPIName: KJUrlConstants.Search, strParameterName: strParam) { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.searchArray.arrVideos.removeAll()
                    self.searchArray.arrExperts.removeAll()
                    self.searchArray.arrCommunities.removeAll()
                    self.searchArray.arrPartners.removeAll()
                    self.searchArray = searchEntity.parseSearchList(info : data as! Dictionary<String, AnyObject>)
                    if self.searchArray.arrVideos.count > 0 || self.searchArray.arrExperts.count > 0 || self.searchArray.arrCommunities.count > 0 || self.searchArray.arrPartners.count > 0{
                        self.viewNoResult.isHidden = true
                        self.tblViewSearch.isHidden = false

                        self.tblViewSearch.reloadData()
                    }else{
                        self.viewNoResult.isHidden = false
                        self.tblViewSearch.isHidden = true
                    }

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Search Suggestions API Call
    func getSuggestionList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let searchEntity = SearchEntity()
        self.showLoader()
        requestManager.requestGetMethod(strAPIName: KJUrlConstants.SearchSuggestions, strParameterName: "") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.arrSearchSuggestions = searchEntity.parseSearchSuggestionList(info : data as? Dictionary<String, AnyObject> ?? [:])
                    for (_, element) in self.arrSearchSuggestions.enumerated() {
                        self.data.append(element.strSearchTerm ?? "")
                    }
                    self.dataFiltered = self.data

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    @objc func segmentControl(_ segmentedControl: UISegmentedControl) {
        switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            selectedSearchTag = "all"
            filterArray.append(selectedSearchTag)
            getSearchList()
            break
        case 1:
            selectedSearchTag = "videos"
            filterArray.append(selectedSearchTag)
            getSearchList()
            break
        case 2:
            selectedSearchTag = "experts"
            filterArray.append(selectedSearchTag)
            getSearchList()
            break
        case 3:
            selectedSearchTag = "communities"
            filterArray.append(selectedSearchTag)
            getSearchList()
            break
        case 4:
            selectedSearchTag = "partners"
            filterArray.append(selectedSearchTag)
            getSearchList()
            break
        default:
            break
        }
    }
    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
           HamburgerMenu().triggerSideMenu()
       }
       
    //MARK:- search bar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
//        dataFiltered = searchText.isEmpty ? data : data.filter({ (dat) -> Bool in
//               dat.range(of: searchText, options: .caseInsensitive) != nil
//           })
//
//           dropButton.dataSource = dataFiltered
//           dropButton.show()
//
        
        var searchText  = searchBar.text ?? ""
        searchText = removeLeadingSpaceFromString(strText: searchText)

        
        if searchText != ""
        {
            var arrFilter = [SearchEntity]()
            if bIsMultiSelect == false
            {
                arrFilter = arrSearchData.filter({ (ent) -> Bool in
                    let tmp: NSString = ent.strVideoTitle! as NSString
                    let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                    return range.location != NSNotFound
                })
            }
            
            searchArray.arrVideos.removeAll()
            searchArray.arrVideos = arrFilter
            if searchArray.arrVideos.count <= 0 //Filter array is empty
            {
                searchArray.arrVideos = arrSearchData
            }else{
               
            }
            self.getSearchList()
            //self.tblViewSearch.reloadData()
        }else{
            self.searchArray.arrVideos.removeAll()
            self.searchArray.arrVideos = arrSearchData
            self.getSearchList()

           // self.tblViewSearch.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.tblViewSearch.reloadData()
    }
    /*
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = data
        dropButton.hide()
    }*/
    func removeLeadingSpaceFromString(strText : String) -> String
    {
        let trimmedText = strText.trimmingCharacters(in: CharacterSet.whitespaces)
        //print("old = [\(strText)], trimmed = [\(trimmedText)]")
        
        return trimmedText
    }

}

extension SearchViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
            return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedSearchTag == "all"{
            if section == 0{
                return  searchArray.arrVideos.count
            }
            if section == 1{
                return searchArray.arrExperts.count
            }
            if section == 2{
                return searchArray.arrCommunities.count
            }
            if section == 3{
                return searchArray.arrPartners.count
            }else{
                return 0
            }
        }else if selectedSearchTag == "videos"{
            if section == 0{
                return  searchArray.arrVideos.count
            }
            
        }else if selectedSearchTag == "experts"{
            if section == 1{
                return searchArray.arrExperts.count
            }
            
        }else if selectedSearchTag == "communities"{
            if section == 2{
                return searchArray.arrCommunities.count
            }
        }else if selectedSearchTag == "partners"{
            if section == 3{
                return searchArray.arrPartners.count
            }
        }
        return 0
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewSearch.dequeueReusableCell(withIdentifier: "searchTableViewCell") as! searchTableViewCell
        if indexPath.section == 0{
            if searchArray.arrVideos.count != 0{
                let ent = searchArray.arrVideos[indexPath.item]
                cell.lblTitle.text = ent.strVideoTitle
                cell.lblPartner.text = ent.strRecommendedFor
                cell.imgPlay.isHidden = false
                cell.imgSearchContent.contentMode = .scaleToFill
                cell.imgSearchContent.layer.masksToBounds = true
                cell.imgSearchContent.layer.cornerRadius = 10
                cell.imgSearchContent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
                cell.viewContainer.layer.cornerRadius = 5
                cell.viewContainer.layer.shadowColor = UIColor.gray.cgColor
                cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
                cell.viewContainer.layer.shadowOpacity = 0.7
                cell.viewContainer.layer.shadowRadius = 2.0
                
                cell.selectionStyle = .none
                return cell
            }
        }
        if indexPath.section == 1{
            if searchArray.arrExperts.count != 0{
                let ent = searchArray.arrExperts[indexPath.item]
                cell.lblTitle.text = ent.strDoctorName
                cell.lblPartner.text = ent.strRecommendedFor
                cell.imgSearchContent.layer.masksToBounds = true
                cell.imgSearchContent.layer.cornerRadius = 60
                cell.imgSearchContent.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
                cell.imgSearchContent.layer.borderWidth = 1.0
                cell.imgPlay.isHidden = true
                cell.imgSearchContent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/expert/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
                cell.viewContainer.layer.cornerRadius = 5
                cell.viewContainer.layer.shadowColor = UIColor.gray.cgColor
                cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
                cell.viewContainer.layer.shadowOpacity = 0.7
                cell.viewContainer.layer.shadowRadius = 2.0
                cell.selectionStyle = .none
               
                
                
                return cell
            }
        }
        if indexPath.section == 2{
            if searchArray.arrCommunities.count != 0{
                let ent = searchArray.arrCommunities[indexPath.item]
                cell.lblTitle.text = ent.strVideoTitle
                cell.lblPartner.text = ent.strRecommendedFor
                cell.imgPlay.isHidden = true
                cell.imgSearchContent.contentMode = .scaleToFill
                cell.imgSearchContent.layer.masksToBounds = true
                cell.imgSearchContent.layer.cornerRadius = 10
                cell.imgSearchContent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/mcat/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
                cell.viewContainer.layer.cornerRadius = 5
                cell.viewContainer.layer.shadowColor = UIColor.gray.cgColor
                cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
                cell.viewContainer.layer.shadowOpacity = 0.7
                cell.viewContainer.layer.shadowRadius = 2.0
                cell.selectionStyle = .none

                return cell
            }
        }
        if indexPath.section == 3{
            if searchArray.arrPartners.count != 0{
                let ent = searchArray.arrPartners[indexPath.item]
                cell.lblTitle.text = ent.strVideoTitle
                cell.lblPartner.text = ent.strRecommendedFor
                cell.imgSearchContent.contentMode = .scaleAspectFill
                cell.imgSearchContent.layer.masksToBounds = true
                cell.imgSearchContent.layer.cornerRadius = 10
                cell.imgPlay.isHidden = true
                cell.imgSearchContent.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/partner/" + "\(ent.strLogoImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
                cell.viewContainer.layer.cornerRadius = 5
                cell.viewContainer.layer.shadowColor = UIColor.gray.cgColor
                cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
                cell.viewContainer.layer.shadowOpacity = 0.7
                cell.viewContainer.layer.shadowRadius = 2.0
                
                cell.selectionStyle = .none

                return cell
            }
        
        }else{
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if searchArray.arrVideos.count != 0{
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
                let ent = self.searchArray.arrVideos[indexPath.item]
                vc.strVideoID = "\(ent.intID ?? 0)"
                vc.strVideoTitle = ent.strVideoTitle ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else if indexPath.section == 1{
            
            let ent = searchArray.arrExperts[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
            vc.expertID = ent.intID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
           
        }else if indexPath.section == 2{
            if searchArray.arrCommunities.count != 0{
                let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
                let ent = self.searchArray.arrCommunities[indexPath.item]
                vc.communityID = "\(ent.intID ?? 0)"
                self.navigationController?.pushViewController(vc, animated: true)
                
              
            }
        }else if indexPath.section == 3{
            if searchArray.arrPartners.count != 0{
                let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PartnerDetailsViewController) as! PartnerDetailsViewController
                let ent = searchArray.arrPartners[indexPath.item]
                vc.partnerID = ent.intID ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 3{
            return 140
        }
        return 160
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedSearchTag == "all"{
            return 50
        }else if selectedSearchTag == "videos"{
            if section == 0{
                return 50
            }
            return 0
        }else if selectedSearchTag == "experts"{
            if section == 1{
                return 50
            }
            return 0
        }else if selectedSearchTag == "communities"{
            if section == 2{
                return 50
            }
            return 0
        }else if selectedSearchTag == "partners"{
            if section == 3{
                return 50
            }
            return 0
        }
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CustomHeaderView") as! CustomHeaderView
        
        if section == 0 {
            headerView.sectionTitleLabel.text = "VIDEOS"
        }
        if section == 1 {
            headerView.sectionTitleLabel.text = "EXPERTS"
        }
        if section == 2 {
            headerView.sectionTitleLabel.text = "COMMUNITIES"
        }
        if section == 3 {
            headerView.sectionTitleLabel.text = "PARTNERS"
        }else{
            return headerView
        }
         return headerView
    }

}

class searchTableViewCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPartner: UILabel!
    @IBOutlet weak var imgSearchContent: UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnDownload: UIButton!

}


extension UISegmentedControl {
    func font(name:String?, size:CGFloat?) {
        let attributedSegmentFont = NSDictionary(object: UIFont(name: name!, size: size!)!, forKey: NSAttributedString.Key.font as NSCopying)
        setTitleTextAttributes(attributedSegmentFont as [NSObject : AnyObject] as [NSObject : AnyObject] as? [NSAttributedString.Key : Any], for: .normal)
    }
}
