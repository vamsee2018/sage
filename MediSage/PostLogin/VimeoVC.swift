
//  VimeoVC.swift
//  MediSage
//
//  Created by Harekrishna on 08/02/20.
//  Copyright © 2020 PFACode. All rights reserved.


import UIKit
import PlayerKit
import AVFoundation
import WebKit

class VimeoVC: UIViewController {
//        private struct Constants {
//        static let VideoURL = URL(string: "https://github.com/vimeo/PlayerKit/blob/master/Example/PlayerKit/video.mp4?raw=true")!
//    }

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var webView: WKWebView!
    
    var VideoURL = String()

    private let player = RegularPlayer()

    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setup()
        
        PLAYVIDEOAPI(key:"379978977")

    }

    func setup(){
       
        player.delegate = self
        self.addPlayerToView()
        self.player.set(AVURLAsset(url: URL(string: VideoURL)!))
        let strurl = VideoURL //"https://player.vimeo.com/video/379971752"
        webView.load(URLRequest(url: URL(string: strurl)!))
        
//        self.videoView.addSubview(self.player.view)
    }

    private func addPlayerToView() {
        player.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        player.view.frame = self.videoView.bounds
        self.videoView.insertSubview(player.view, at: 0)
    }

    @objc func btnMenuTapping(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func PLAYVIDEOAPI(key: String){
        ApicallWebservices.SharedInstance.VIMEOVIDEOApiCallwithHeader(methodname: "https://api.vimeo.com/videos/\(key)", type: .get, header: ["Content-Type": "application/json", "Authorization": "bearer 75d0309b580962afaab71aa018e17877"], parameter: [:], strLoaderString: "", completion: {
            (error: Error?, success: Bool, result: Any?) in
            if success == true {
                let dicData = result as? NSDictionary ?? [:]
                let filesarr = dicData.object(forKey: "files") as? NSArray ?? []
                if filesarr.count > 0 {
                    let fileurl = filesarr[0] as? NSDictionary ?? [:]
                    let urlFile = fileurl.object(forKey: "link") as? String ?? ""
                    print(urlFile)
                    DispatchQueue.main.async() {
                        self.VideoURL = urlFile
                        self.setup()
                        //                        let player = AVPlayer(url: URL(string: "\(urlFile)")!)
//                        let playerController = AVPlayerViewController()
//                        playerController.player = player
//                        self.present(playerController, animated: true) {
//                            player.play()
//                        }
                    }
                }else {
                    print("....")
                    self.view.makeToast(message: "Video is no longer...")
                }
                
            }else{
                print("....")
                self.view.makeToast(message: "Video link is not provided...")
            }
        })
    }
    

}
extension VimeoVC: PlayerDelegate {

    // MARK: Setup
//    private func addPlayerToView() {
//        player.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        player.view.frame = self.view.bounds
//        self.view.insertSubview(player.view, at: 0)
//    }

    // MARK: Actions
    @IBAction func didTapPlayButton() {
        self.player.playing ? self.player.pause() : self.player.play()
    }

    @IBAction func didChangeSliderValue() {
        let value = Double(self.slider.value)

        let time = value * self.player.duration

        self.player.seek(to: time)
    }

    // MARK: VideoPlayerDelegate
    func playerDidUpdateState(player: Player, previousState: PlayerState) {
        self.activityIndicator.isHidden = true

        switch player.state {
        case .loading:

            self.activityIndicator.isHidden = false

        case .ready:

            break

        case .failed:

            NSLog("🚫 \(String(describing: player.error))")
        }
    }

    func playerDidUpdatePlaying(player: Player) {
        self.playButton.isSelected = player.playing
    }

    func playerDidUpdateTime(player: Player) {
        guard player.duration > 0 else {
            return
        }

        let ratio = player.time / player.duration

        if self.slider.isHighlighted == false {
            self.slider.value = Float(ratio)
        }
    }

    func playerDidUpdateBufferedTime(player: Player) {
        guard player.duration > 0 else {
                    return
                }
        let ratio = Int((player.bufferedTime / player.duration) * 100)
        //self.label.text = "Buffer: \(ratio)%"
    }



}


