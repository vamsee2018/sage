//
//  PartnerEntity.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class PartnerEntity: NSObject {
    var strImageName : String? = ""
    var strVideoName : String? = ""
    var strDoctorName: String? = ""
    var strImagePath: String? = ""
    var strPartnerImageName: String? = ""
    var strDescription: String? = ""
    var strEventTitle: String? = ""
    var strPartnerName: String? = ""

    var strVideoTitle: String? = ""
    var strVideoID: Int?
    var intID: Int?
    var strDesignation: String?
    var strPath: String? = ""
    var strFileName: String? = ""
    //Video Parameters
    var intCommunityID: Int?
    var intPartnerID: Int?
    var intExpertID: Int?
    var intIsPartOfSeries: Int?
    var intVideoViewCount: Int?
    var strVideoLink: String? = ""
    var strLastWatched: String? = ""
    var strUpdatedAt: String? = ""
    var isLike: Bool?
    var isInsightful: Bool?
    var arrFilterName = [PartnerEntity]()
    var arrVideos = [PartnerEntity]()
    var arrSeries = [PartnerEntity]()
    var arrNewsletter = [PartnerEntity]()
    var arrPartners = [PartnerEntity]()

    /// Method for country code list  api data parsing
    func parsePartnerList(info : Dictionary<String,AnyObject>)->[PartnerEntity]{
        
        var mainEnt = [PartnerEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = PartnerEntity()
                
                if let id = dict["id"] as? Int{
                    ent.intID = id
                }
                if let name = dict["name"] as? String{
                    ent.strDoctorName = name
                }
                if let title = dict["title"] as? String{
                    ent.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    ent.strImagePath = imagePath
                }
                if let imageName = dict["logo_image"] as? String{
                    ent.strImageName = imageName
                }
                if let logoName = dict["app_logo_image"] as? String{
                    ent.strPartnerImageName = logoName
                }
                if let designation = dict["designation"] as? String{
                    ent.strDesignation = designation
                }
                if let description = dict["description"] as? String{
                    ent.strDescription = description
                }
                mainEnt.append(ent)
            }
        }
        return mainEnt
    }
    
    /// Method for country code list  api data parsing
    func parsePartnerDetails(info : Dictionary<String,AnyObject>)->PartnerEntity{
        
        var mainEnt = PartnerEntity()
        let dictClass = info["Data"] as? [String : Any]
        
        if let arrObj = dictClass?["partner"] as? [String:Any]{
            
            if let dict = arrObj as? [String:AnyObject]
            {
                let myEnt = PartnerEntity()
                
                if let id = dict["id"] as? Int{
                    myEnt.strVideoID = id
                }
                if let name = dict["name"] as? String{
                    myEnt.strDoctorName = name
                }
                if let title = dict["title"] as? String{
                    myEnt.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    myEnt.strImagePath = imagePath
                }
                if let path = dict["image_path"] as? String{
                    myEnt.strPath = path
                }
                if let imageName = dict["logo_image"] as? String{
                    myEnt.strImageName = imageName
                }
                if let designation = dict["designation"] as? String{
                    myEnt.strDesignation = designation
                }
                if let description = dict["description"] as? String{
                    myEnt.strDescription = description
                }
                if let arrObject = dictClass?["partner_divisions"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            mainEnt.arrPartners.append(ent)
                            
                        }
                        
                    }
                }
                if let arrObject = dictClass?["videos"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            mainEnt.arrVideos.append(ent)
                            
                        }
                        
                    }
                }
                
                if let arrObject = dictClass?["series"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            mainEnt.arrSeries.append(ent)
                            
                        }
                        
                    }
                }
                if let arrObject = dictClass?["newsletters"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let imageName = dict["file_name"] as? String{
                                ent.strFileName = imageName
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            mainEnt.arrNewsletter.append(ent)
                            
                        }
                        
                    }
                }
                mainEnt.arrFilterName.append(myEnt)
            }
            
        }
        return mainEnt
    }
    
    /// Method for country code list  api data parsing
    func parseVideoDetails(info : Dictionary<String,AnyObject>)->PartnerEntity{
        
        let mainEnt = PartnerEntity()
        let dictClass = info["Data"] as? [String : Any]
        
        if let arrObj = dictClass?["video"] as? [String:Any]{
            
            if let dict = arrObj as? [String:AnyObject]
            {
                let myEnt = PartnerEntity()
                
                if let id = dict["id"] as? Int{
                    myEnt.strVideoID = id
                }
                if let likes = dict["liked"] as? Bool{
                    myEnt.isLike = likes
                }
                if let insightful = dict["insightful"] as? Bool{
                    myEnt.isInsightful = insightful
                }
                if let partner_id = dict["partner_id"] as? Int{
                    myEnt.intPartnerID = partner_id
                }
                if let expert_id = dict["expert_id"] as? Int{
                    myEnt.intExpertID = expert_id
                }
                if let is_part_of_series = dict["is_part_of_series"] as? Int{
                    myEnt.intIsPartOfSeries = is_part_of_series
                }
                if let community_id = dict["community_id"] as? Int{
                    myEnt.intCommunityID = community_id
                }
                if let video_view_count = dict["video_view_count"] as? Int{
                    myEnt.intVideoViewCount = video_view_count
                }
                if let name = dict["name"] as? String{
                    myEnt.strDoctorName = name
                }
                if let imageName = dict["image_name"] as? String{
                    myEnt.strImageName = imageName
                }
                if let title = dict["title"] as? String{
                    myEnt.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    myEnt.strImagePath = imagePath
                }
                if let path = dict["image_path"] as? String{
                    myEnt.strPath = path
                }
                if let video_link = dict["video_link"] as? String{
                    myEnt.strVideoLink = video_link
                }
                if let designation = dict["designation"] as? String{
                    myEnt.strDesignation = designation
                }
                if let description = dict["description"] as? String{
                    myEnt.strDescription = description
                }
                
                if let arrObject = dictClass?["up_next"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let partner_id = dict["partner_id"] as? Int{
                                ent.intPartnerID = partner_id
                            }
                            if let expert_id = dict["expert_id"] as? Int{
                                ent.intExpertID = expert_id
                            }
                            if let is_part_of_series = dict["is_part_of_series"] as? Int{
                                ent.intIsPartOfSeries = is_part_of_series
                            }
                            if let community_id = dict["community_id"] as? Int{
                                ent.intCommunityID = community_id
                            }
                            if let video_view_count = dict["video_view_count"] as? Int{
                                ent.intVideoViewCount = video_view_count
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strVideoName = imageName
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            if let path = dict["image_path"] as? String{
                                ent.strPath = path
                            }
                            if let video_link = dict["video_link"] as? String{
                                ent.strVideoLink = video_link
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            mainEnt.arrVideos.append(ent)
                            
                        }
                        
                    }
                }
                
                mainEnt.arrFilterName.append(myEnt)
            }
            
        }
        return mainEnt
    }
    
    /// Method for country code list  api data parsing
    func parseLiveEventDetails(info : Dictionary<String,AnyObject>)->[PartnerEntity]{
        
        var mainEnt = [PartnerEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = PartnerEntity()
                
                if let id = dict["id"] as? Int{
                    ent.intID = id
                }
                if let name = dict["name"] as? String{
                    ent.strDoctorName = name
                }
                if let title = dict["title"] as? String{
                    ent.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    ent.strImagePath = imagePath
                }
                if let eventTitle = dict["live_event_text"] as? String{
                    ent.strEventTitle = eventTitle
                }
                if let imageName = dict["logo_image"] as? String{
                    ent.strImageName = imageName
                }
                if let logoName = dict["app_logo_image"] as? String{
                    ent.strPartnerImageName = logoName
                }
                if let designation = dict["designation"] as? String{
                    ent.strDesignation = designation
                }
                if let video_link = dict["video_id"] as? String{
                    ent.strVideoLink = video_link
                }
                if let partner_name = dict["partner_name"] as? String{
                    ent.strPartnerName = partner_name
                }
                if let likes = dict["liked"] as? Bool{
                    ent.isLike = likes
                }
                if let insightful = dict["insightful"] as? Bool{
                    ent.isInsightful = insightful
                }
                mainEnt.append(ent)
            }
        }
        return mainEnt
    }
    
    
    
}
