//
//  PartnerDivisionEntity.swift
//  MediSage
//
//  Created by Harshad Wagh on 01/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class PartnerDivisionEntity: NSObject {

    var strImageName : String? = ""
    var strVideoName : String? = ""
    var strLinkName: String? = ""
    var strDoctorName: String? = ""
    var strImagePath: String? = ""
    var strFileName: String? = ""
    var strPartnerImageName: String? = ""
    var strDescription: String? = ""
    
    var strVideoTitle: String? = ""
    var strVideoID: Int?
    var intID: Int?
    var strDesignation: String?
    var strPath: String? = ""
    
    //Video Parameters
    var intCommunityID: Int?
    var intPartnerID: Int?
    var intExpertID: Int?
    var intIsPartOfSeries: Int?
    var intVideoViewCount: Int?
    var strVideoLink: String? = ""
    var strLastWatched: String? = ""
    var strUpdatedAt: String? = ""
    var isLike: Int?
    var isInsightful: Int?
    var arrBanner = [PartnerDivisionEntity]()
    var arrFilterName = [PartnerDivisionEntity]()
    var arrVideos = [PartnerDivisionEntity]()
    var arrTrendingVideos = [PartnerDivisionEntity]()
    var arrNewsletter = [PartnerDivisionEntity]()
    var arrWebinars = [PartnerDivisionEntity]()
    var arrReadingMaterials = [PartnerDivisionEntity]()

    ///MARK:- Method for Partner Divisions api data parsing
    func parsePartnerDivisionDetails(info : Dictionary<String,AnyObject>)->PartnerDivisionEntity{
        
        let mainEnt = PartnerDivisionEntity()
        let dictClass = info["Data"] as? [String : Any]
         
        if let arrObj = dictClass?["play_video"] as? [String:Any]{
            
            if let dict = arrObj as? [String:AnyObject]
            {
                let myEnt = PartnerDivisionEntity()
                
                if let id = dict["id"] as? Int{
                    myEnt.strVideoID = id
                }
                if let partner_id = dict["partner_id"] as? Int{
                    myEnt.intPartnerID = partner_id
                }
                if let expert_id = dict["expert_id"] as? Int{
                    myEnt.intExpertID = expert_id
                }
                if let likes = dict["like"] as? Int{
                    myEnt.isLike = likes
                }
                if let insightful = dict["insightful"] as? Int{
                    myEnt.isInsightful = insightful
                }
                if let is_part_of_series = dict["is_part_of_series"] as? Int{
                    myEnt.intIsPartOfSeries = is_part_of_series
                }
                if let community_id = dict["community_id"] as? Int{
                    myEnt.intCommunityID = community_id
                }
                if let video_view_count = dict["video_view_count"] as? Int{
                    myEnt.intVideoViewCount = video_view_count
                }
                if let name = dict["name"] as? String{
                    myEnt.strDoctorName = name
                }
                if let imageName = dict["image_name"] as? String{
                    myEnt.strImageName = imageName
                }
                if let title = dict["title"] as? String{
                    myEnt.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    myEnt.strImagePath = imagePath
                }
                if let path = dict["image_path"] as? String{
                    myEnt.strPath = path
                }
                if let video_link = dict["video_link"] as? String{
                    myEnt.strVideoLink = video_link
                }
                if let designation = dict["designation"] as? String{
                    myEnt.strDesignation = designation
                }
                if let description = dict["description"] as? String{
                    myEnt.strDescription = description
                }
                
                if let arrObject = dictClass?["up_next"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerDivisionEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let partner_id = dict["partner_id"] as? Int{
                                ent.intPartnerID = partner_id
                            }
                            if let expert_id = dict["expert_id"] as? Int{
                                ent.intExpertID = expert_id
                            }
                            if let is_part_of_series = dict["is_part_of_series"] as? Int{
                                ent.intIsPartOfSeries = is_part_of_series
                            }
                            if let community_id = dict["community_id"] as? Int{
                                ent.intCommunityID = community_id
                            }
                            if let video_view_count = dict["video_view_count"] as? Int{
                                ent.intVideoViewCount = video_view_count
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strVideoName = imageName
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            if let path = dict["image_path"] as? String{
                                ent.strPath = path
                            }
                            if let video_link = dict["video_link"] as? String{
                                ent.strVideoLink = video_link
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            mainEnt.arrVideos.append(ent)
                            
                        }
                        
                    }
                }
                if let arrObject = dictClass?["videos"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerDivisionEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let video_link = dict["video_link"] as? String{
                                ent.strVideoLink = video_link
                            }
                            mainEnt.arrVideos.append(ent)
                            
                        }
                        
                    }
                }
                if let arrObject = dictClass?["trending_videos"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerDivisionEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            mainEnt.arrTrendingVideos.append(ent)
                            
                        }
                        
                    }
                }
                
                if let arrObject = dictClass?["live_webinars"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerDivisionEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            mainEnt.arrWebinars.append(ent)
                            
                        }
                        
                    }
                }
                if let arrObject = dictClass?["newsletters"] as? [[String:Any]]{
                    for item in arrObject {
                        
                        if let dict = item as? [String:AnyObject]
                        {
                            let ent = PartnerDivisionEntity()
                            
                            if let id = dict["id"] as? Int{
                                ent.intID = id
                            }
                            if let name = dict["name"] as? String{
                                ent.strDoctorName = name
                            }
                            if let title = dict["title"] as? String{
                                ent.strVideoTitle = title
                            }
                            if let imageName = dict["image_name"] as? String{
                                ent.strImageName = imageName
                            }
                            if let designation = dict["designation"] as? String{
                                ent.strDesignation = designation
                            }
                            if let description = dict["description"] as? String{
                                ent.strDescription = description
                            }
                            if let file_name = dict["file_name"] as? String{
                                ent.strFileName = file_name
                            }
                            if let imagePath = dict["path"] as? String{
                                ent.strImagePath = imagePath
                            }
                            mainEnt.arrNewsletter.append(ent)
                            
                          }
                        
                        }
                      }
                
                mainEnt.arrFilterName.append(myEnt)
             }
        }
        return mainEnt
    }
          
    /// Method for country code list  api data parsing
    func parseDivisionBanner(info : Dictionary<String,AnyObject>)->PartnerDivisionEntity{
        
        let myEnt = PartnerDivisionEntity()
        let dictClass = info["Data"] as? [String : Any]
        
        if let arrObj = dictClass?["partner_division"] as? [String:Any]{
            if let dict = arrObj as? [String:AnyObject]
            {
               // let myEnt = PartnerDivisionEntity()
                
                if let id = dict["id"] as? Int{
                    myEnt.strVideoID = id
                }
                if let partner_id = dict["partner_id"] as? Int{
                    myEnt.intPartnerID = partner_id
                }
                if let expert_id = dict["expert_id"] as? Int{
                    myEnt.intExpertID = expert_id
                }
                
                if let name = dict["name"] as? String{
                    myEnt.strDoctorName = name
                }
                if let imageName = dict["image_name"] as? String{
                    myEnt.strImageName = imageName
                }
                if let title = dict["title"] as? String{
                    myEnt.strVideoTitle = title
                }
                if let imagePath = dict["path"] as? String{
                    myEnt.strImagePath = imagePath
                }
                if let path = dict["image_path"] as? String{
                    myEnt.strPath = path
                }
                if let video_link = dict["video_link"] as? String{
                    myEnt.strVideoLink = video_link
                }
                if let link_name = dict["link_name"] as? String{
                    myEnt.strLinkName = link_name
                }

            }
        }
        
        return myEnt
    }
    
}
