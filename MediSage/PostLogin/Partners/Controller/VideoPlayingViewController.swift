//
//  VideoPlayingViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 11/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import FirebaseAnalytics
class VideoPlayingViewController: BaseViewController {

    //MARK:- Custom Properties
    var strVideoID = ""
    var strVideoTitle = ""
    var strVideoPartner = ""
    var partnerArray = PartnerEntity()
    var eventArray = [PartnerEntity]()
    var entSeriesFeedback = HomeEntity()

    var userData = [String:Any]()
    var strUserID  = String()
    var strEventID = String()
    var isComingFromCommunityVideos = false
    var isComingFromPastEvent = false
    var player: AVPlayer?
    var strFeedbackType  = String()
    var strFeedbackSource  = String()
    var isLike  = Bool()
    var isInsightful  = Bool()
    var strUserFirstName:String?
    var strUserLastName:String?
    var totalDuration = 0
    var playbackType = ""
    var startPosition = 0
    var endPosition = 0
    var playCount = 0
    var strMobileNumber  = String()
    var notificationObserver:NSObjectProtocol?
    //MARK:- IBOutlet Connection
    @IBOutlet weak var tblViewUpNext: UITableView!
    @IBOutlet weak var viewPlayingVideo: UIView!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var lblSeries: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnInsightful: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewUpNext: UIView!
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var viewFeedback: UIView!
    @IBOutlet weak var imgInsightful: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            
            if let first_name = userData["first_name"] as? String{
                
                strUserFirstName = first_name
            }
            if let last_name = userData["last_name"] as? String{
                
                strUserLastName = last_name
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        tblViewUpNext.allowsSelection = true
        
        if isComingFromPastEvent == true{
            strFeedbackSource = "live_event"
             getEventDetails()
        }else{
            strFeedbackSource = "video"
            getVideoDetails()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(checkVideoPlaying), name: NSNotification.Name("isPlayingVideo"), object: nil)
        
        do {
         try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("Common_Video", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Common Video Page",
            AnalyticsParameterContentType: "Common Video"
        ])
       
      //  lblSeries.text = strVideoPartner
        lblTitle.text = strVideoTitle
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
      //  self.viwTop.btnBack.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)

        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
       // PLAYVIDEOAPI(key:strVideoID)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
      
        NotificationCenter.default.removeObserver(self)
       
    }
    
    @objc func checkVideoPlaying(){
        player?.replaceCurrentItem(with: nil)
        self.playbackType = "stop"
        startPosition = endPosition
        submitVideoPlayback()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.player?.rate == 0 {
            print("Pause")
            self.playbackType = "pause"
            submitVideoPlayback()
        }else {
            print("Play")
            getTotalTime()
           // self.playbackType = "play"
           // submitVideoPlayback()
        }
        
        if keyPath == "status" {
            if player?.status == AVPlayer.Status.readyToPlay {
              getTotalTime()
            } else if player?.status == AVPlayer.Status.failed {}
        }
    }
    
    func getTotalTime(){
        if let seconds = player?.currentItem?.asset.duration {
            let totalTime = CMTimeGetSeconds(seconds)
            if let currentTime = player?.currentTime() {
                let time = CMTimeGetSeconds(currentTime)
                startPosition = Int(time)
                print(time)
                print(totalTime)
            }
        }
    }
    //MARK:- API Call for Get Video details
    func getVideoDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = PartnerEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.VideoDetails + strUserID + "/" + strVideoID , strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.partnerArray.arrVideos.removeAll()
                    self.partnerArray = partnerEntity.parseVideoDetails(info : data as! Dictionary<String, AnyObject>)
                    self.tblViewUpNext.reloadData()
                    self.getVideoLinkID()
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- API Call for Get My Community List
    func getEventDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = PartnerEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.LiveEventDetails + strEventID + "/\(strUserID)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.eventArray = partnerEntity.parseLiveEventDetails(info : data as! Dictionary<String, AnyObject>)
                    self.getEventDetailsFeedback()
                    self.PLAYVIDEOAPI(key: self.eventArray[0].strVideoLink ?? "")
                    self.viewUpNext.isHidden = true
                    self.viewFeedback.isHidden = false
                    self.viewSeperator.isHidden = false
                    self.lblTitle.text = self.eventArray[0].strVideoTitle
                }
            }else
            {
                self.view.makeToast(message: error)
                self.viewUpNext.isHidden = true
                self.viewSeperator.isHidden = false
            }
        }
    }
    
    func getEventDetailsFeedback(){
        if eventArray.count > 0{
            
            if eventArray[0].isLike ?? false == true{
              //  btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
                imgLike.image = UIImage(named: "Like_a")
            }else{
                //btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
                imgLike.image = UIImage(named: "Like (1)")
            }
            if eventArray[0].isInsightful ?? false == true{
               // btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful_a")

            }else{
               // btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful")
            }
            
        }else{
            self.view.makeToast(message: "Video not found")
        }
        
    }
    
    
    func getVideoLinkID(){
        if partnerArray.arrFilterName.count > 0{
            
            if partnerArray.arrFilterName[0].isLike ?? false == true{
              //  btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
                imgLike.image = UIImage(named: "Like_a")
            }else{
                //btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
                imgLike.image = UIImage(named: "Like (1)")
            }
            if partnerArray.arrFilterName[0].isInsightful ?? false == true{
               // btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful_a")

            }else{
               // btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful")
            }
            lblTitle.text = partnerArray.arrFilterName[0].strVideoTitle ?? ""
            let id = partnerArray.arrFilterName[0].strVideoLink ?? ""
            PLAYVIDEOAPI(key: id)
        }else{
            self.view.makeToast(message: "Video not found")
        }
        
        if partnerArray.arrVideos.count > 0{
            self.viewUpNext.isHidden = false
            self.viewSeperator.isHidden = true
        }else{
            self.viewUpNext.isHidden = true
            self.viewSeperator.isHidden = false
        }
        
        
    }
    
    func PLAYVIDEOAPI(key: String){
        ApicallWebservices.SharedInstance.VIMEOVIDEOApiCallwithHeader(methodname: "https://api.vimeo.com/videos/\(key)", type: .get, header: ["Content-Type": "application/json", "Authorization": "bearer 75d0309b580962afaab71aa018e17877"], parameter: [:], strLoaderString: "", completion: {
            (error: Error?, success: Bool, result: Any?) in
            if success == true {
                let dicData = result as? NSDictionary ?? [:]
                let filesarr = dicData.object(forKey: "files") as? NSArray ?? []
                if filesarr.count > 0 {
                    let fileurl = filesarr[0] as? NSDictionary ?? [:]
                    let urlFile = fileurl.object(forKey: "link") as? String ?? ""
                    print(urlFile)
                    DispatchQueue.main.async() {
                        self.player = AVPlayer(url: URL(string: "\(urlFile)")!)
                        let playerController = AVPlayerViewController()
                        playerController.player = self.player
                        playerController.view.frame = self.viewPlayingVideo.bounds
                        self.addChild(playerController)
                        self.viewPlayingVideo.addSubview(playerController.view)
                        self.player?.play()
                        self.playCount += 1
                        self.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
                        self.player?.addObserver(self, forKeyPath: "status", options: [], context: nil)
                        NotificationCenter.default.addObserver(self, selector: #selector(self.finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

                        let asset = AVAsset(url: URL(string: "\(urlFile)")!)
                        
                        let duration = asset.duration
                        let durationTime = CMTimeGetSeconds(duration)
                        
                        print("TOTAL DURATION :->>>>>>>",durationTime)
                        self.totalDuration = Int(durationTime)
                        self.getDurationOfVideo()
                    }
                }else {
                    print("....")
                    self.view.makeToast(message: "Video is no longer...")
                }
                
            }else{
                print("....")
                self.view.makeToast(message: "Video link is not provided...")
            }
        })
    }
    
    @objc func finishVideo()
    {
        if playCount >= 1{
            self.playbackType = "stop"
            submitVideoPlayback()
            print("Video Finished")
        }else{
            
        }
        
    }
    
    //MARK:- Submit Video Playback Api Call
    func submitVideoPlayback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "video_id": strVideoID,
                                       "type" : playbackType,
                                       "start_position" : startPosition,
                                       "end_position" : endPosition,
                                       "total_duration" : totalDuration]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoPlayback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                   
                    // self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    func updateLikeInsightful(){
        if partnerArray.arrFilterName.count > 0{
            
            if partnerArray.arrFilterName[0].isLike ?? false == true{
               // btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
                imgLike.image = UIImage(named: "Like_a")

            }else{
              //  btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
                imgLike.image = UIImage(named: "Like (1)")

            }
            if partnerArray.arrFilterName[0].isInsightful ?? false == true{
                imgInsightful.image = UIImage(named:  "Insightful_a")
                
            }else{
                //  btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful")
            }
        }else{
        }
        
    }
    
    
    //MARK:- SubmitVideoFeedback Api Call
    func submitVideoFeedback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        var id = ""
        if isComingFromPastEvent == true{
            id = strEventID
        }else{
            id = strVideoID
        }
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "id": id,
                                       "feedback_type": strFeedbackType,
                                       "feedback_source" : strFeedbackSource,
                                       "feedback_action" : isLike]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                    self.updateFeedbackUI()
                   // self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    func getDurationOfVideo(){
        let interval = CMTime(value: 1, timescale: 2)
        player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in

            let seconds = CMTimeGetSeconds(progressTime)
            let secondString = String(format: "%02d", Int(seconds) % 60)
            let minutString = String(format: "%02d", Int(seconds) / 60)
           
            print("\(minutString):\(secondString)")
        
            self.endPosition = Int(seconds)
        })
    }
    
    //MARK:- Update Like and Insightful
    func updateFeedbackUI(){
        if entSeriesFeedback.isLike ?? 0 == 1{
            // btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
            imgLike.image = UIImage(named: "Like_a")
            
        }else{
            //  btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
            imgLike.image = UIImage(named: "Like (1)")
            
        }
        if entSeriesFeedback.isInsightful ?? 0 == 1{
            // btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful_a")
            
        }else{
            //  btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful")
            
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnInsightfulTapped(_ sender: UIButton) {
        strFeedbackType = "insightful"
        if entSeriesFeedback.isInsightful == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        strFeedbackType = "like"
        if entSeriesFeedback.isLike == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    
    @IBAction func shareButtonClicked(sender: AnyObject)
    {
        //Set the default sharing message.
        let message = "\("Dr.") \(strUserFirstName ?? "") \(strUserLastName ?? "") \("would you like to watch this lecture"), \"\(strVideoTitle)\" \("Download the Medisage app to watch.")"
      
        var comp = URLComponents()
        comp.scheme = "http"
        comp.host = "mymedisage.com/elearning"
        comp.path = "/video/\(strVideoID)/\(strVideoID)"
        let imgURL = KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(partnerArray.arrFilterName[0].strImageName ?? "" )"
        
        self.shareDynamicLink(CurrentVC: self, urlComp: comp, strTitle: message , strID: "" , imgUrl: imgURL )
        
//        //Set the link to share.
//        if let link = NSURL(string: "http://onelink.to/medisage")
//        {
//            let objectsToShare = [message,link] as [Any]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
//            self.present(activityVC, animated: true, completion: nil)
//        }
    }
    
}

extension VideoPlayingViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partnerArray.arrVideos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewUpNext.dequeueReusableCell(withIdentifier: "upNextTableCell") as! upNextTableCell
        let ent = partnerArray.arrVideos[indexPath.row]
        cell.lblTitle.text = ent.strVideoTitle
        cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(ent.strVideoName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgThumbnail.layer.cornerRadius = 10
        cell.viewContainer.layer.cornerRadius = 10
        cell.viewContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.viewContainer.layer.shadowOpacity = 0.7
        cell.viewContainer.layer.shadowRadius = 5.0
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ent = partnerArray.arrVideos[indexPath.row]
        strVideoID = "\(ent.intID ?? 0)"
        lblTitle.text = ent.strVideoTitle
        player?.replaceCurrentItem(with: nil)

        getVideoDetails()
    }
}


