//
//  PartnersViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class PartnersViewController: BaseViewController {

    //MARK:- Custom Properties
    var partnerArray = [PartnerEntity]()
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewPharma: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnRightArrow: UIButton!
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        getPartnerList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("Partners", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Partner Page",
            AnalyticsParameterContentType: "Partner"
        ])
        
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
    
        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
       
    }
    
    //MARK:- API Call for Get Partner List
    func getPartnerList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = PartnerEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.PartnerList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.partnerArray = partnerEntity.parsePartnerList(info : data as! Dictionary<String, AnyObject>)
                    self.collViewPharma.reloadData()
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnRightArrowTapped(_ sender: UIButton) {
        
    }

    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
        HamburgerMenu().triggerSideMenu()
    }
       
    

}

//MARK:- Delegate extension for Collection view
extension PartnersViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collViewPharma {
            return partnerArray.count
        }
        else {
            return 5 //trendingArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collViewPharma.dequeueReusableCell(withReuseIdentifier: "partnersCollectionCell", for: indexPath) as! partnersCollectionCell
        
        let ent = partnerArray[indexPath.item]
        cell.imgPartner.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strPartnerImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgPartner.contentMode = .scaleAspectFill
        cell.imgPartner.layer.cornerRadius = 10
        cell.imgPartner.layer.shadowColor = UIColor.black.cgColor
        cell.imgPartner.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.imgPartner.layer.shadowOpacity = 0.3
        cell.imgPartner.layer.shadowRadius = 2.0
        
        return cell
        
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.view.frame.size.width/3 - 15), height: 100)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.PartnerDetailsViewController) as! PartnerDetailsViewController
        let ent = partnerArray[indexPath.item]
        vc.partnerID = ent.intID ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Collection View Cell
class partnersCollectionCell: UICollectionViewCell{
    @IBOutlet weak var imgPartner: UIImageView!
    
}


