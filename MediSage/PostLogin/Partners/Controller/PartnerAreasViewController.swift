//
//  PartnerAreasViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 11/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import FirebaseAnalytics
class PartnerAreasViewController: BaseViewController {

    //MARK:- Custom Properties
    var partnerAreasArray = [PartnerDivisionEntity]()
    var videoArray = [PartnerDivisionEntity]()
    var seriesArray = [PartnerDivisionEntity]()
    var newsLetterArray = [PartnerDivisionEntity]()
    var partnerArray = PartnerDivisionEntity()
    var bannerArray = PartnerDivisionEntity()

    var divisionID = 0
    var strVideoID = 0
    var userData = [String:Any]()
    var strUserID  = String()
    var player: AVPlayer?
    var entSeriesFeedback = HomeEntity()
    var strFeedbackType  = String()
    var isLike  = Bool()
    var isInsightful  = Bool()
    var strUserFirstName:String?
    var strUserLastName:String?
    var strVideoTitle = ""
    var totalDuration = 0
    var playbackType = ""
    var startPosition = 0
    var endPosition = 0
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewTrendingVideos: UICollectionView!
    @IBOutlet weak var collViewUpcomingLiveEvents: UICollectionView!
    @IBOutlet weak var collViewLatestNewsletter: UICollectionView!
    @IBOutlet weak var collViewReadingMaterials: UICollectionView!
    @IBOutlet weak var tblViewUpNext: UITableView!
    @IBOutlet weak var viewPlayingVideo: UIView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var lblSeries: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var materialHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsletterHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var trendingVideosHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var webinarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var upNextHeight: NSLayoutConstraint!
    @IBOutlet weak var btnInsightful: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewUpNext: UIView!
    @IBOutlet weak var viewSeperator: UIView!
    @IBOutlet weak var imgInsightful: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewUpNext.allowsSelection = true
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let first_name = userData["first_name"] as? String{
                
                strUserFirstName = first_name
            }
            if let last_name = userData["last_name"] as? String{
                
                strUserLastName = last_name
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        do {
         try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(checkVideoPlaying), name: NSNotification.Name("isPlayingVideo"), object: nil)
        getPartnerDivisionBanner()
        getPartnerDivisionDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        imgBanner.layer.cornerRadius = 10

        Analytics.logEvent("Partner_Division_Details", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Partner Division Details Page",
            AnalyticsParameterContentType: "Partner Division Details"
        ])
        
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false

        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
      
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func checkVideoPlaying(){
        player?.replaceCurrentItem(with: nil)
        self.playbackType = "stop"
        startPosition = endPosition

        submitVideoPlayback()
        //self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.player?.rate == 0 {
            print("Pause")
            self.playbackType = "pause"
            submitVideoPlayback()
        }else {
            print("Play")
            getTotalTime()
           // self.playbackType = "play"
           // submitVideoPlayback()
        }
        
        if keyPath == "status" {
            if player?.status == AVPlayer.Status.readyToPlay {
              getTotalTime()
            } else if player?.status == AVPlayer.Status.failed {}
        }
    }
    
    func getTotalTime(){
        if let seconds = player?.currentItem?.asset.duration {
            let totalTime = CMTimeGetSeconds(seconds)
            if let currentTime = player?.currentTime() {
                let time = CMTimeGetSeconds(currentTime)
                startPosition = Int(time)
                print(time)
                print(totalTime)
            }
        }
    }
    
    //MARK:- API Call for Get My Community List
    func getPartnerDivisionDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = PartnerDivisionEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.PartnerAreaDetails + "\(divisionID)" + "/" + strUserID , strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.partnerArray.arrVideos.removeAll()
                    self.partnerArray = partnerEntity.parsePartnerDivisionDetails(info : data as! Dictionary<String, AnyObject>)
                    self.tblViewUpNext.reloadData()
                  //  self.collViewReadingMaterials.reloadData()
                    self.collViewUpcomingLiveEvents.reloadData()
                    self.collViewLatestNewsletter.reloadData()
                    self.collViewTrendingVideos.reloadData()
                    self.getVideoLinkID()
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- API Call for Get My Community List
    func getPartnerDivisionBanner(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = PartnerDivisionEntity()
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.PartnerAreaDetails + "\(divisionID)" + "/" + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.bannerArray = partnerEntity.parseDivisionBanner(info : data as! Dictionary<String, AnyObject>)
                    self.getBanner()
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func getVideoLinkID(){
        
        if partnerArray.arrTrendingVideos.count > 0{
            trendingVideosHeightConstraint.constant = 240
        }else{
            trendingVideosHeightConstraint.constant = 0
        }
        if partnerArray.arrNewsletter.count > 0{
            newsletterHeightConstraint.constant = 219
        }else{
            newsletterHeightConstraint.constant = 0
        }
        if partnerArray.arrWebinars.count > 0{
            webinarHeightConstraint.constant = 240
        }else{
            webinarHeightConstraint.constant = 0
        }
        if partnerArray.arrVideos.count > 0{
            upNextHeight.constant = 260
            viewUpNext.isHidden = false
            viewSeperator.isHidden = true

        }else{
            upNextHeight.constant = 0
            viewUpNext.isHidden = true
            viewSeperator.isHidden = false
        }
        
        if partnerArray.arrFilterName.count > 0{
            let id = partnerArray.arrFilterName[0].strVideoLink ?? ""
            lblTitle.text = partnerArray.arrFilterName[0].strVideoTitle
            strVideoTitle = partnerArray.arrFilterName[0].strVideoTitle ?? ""
            strVideoID = partnerArray.arrFilterName[0].strVideoID ?? 0
            updateLikeInsightful()
            PLAYVIDEOAPI(key: id)
           
        }else{
           // self.view.makeToast(message: "Trying to get property 'id' of non-object")
        }
        
       
    }
    
    func getBanner(){
        imgBanner.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/partner/" + "\(bannerArray.strImageName ?? "")"), placeholderImage:UIImage(named:"placeholder"))
    }
    
    func updateLikeInsightful(){
        if partnerArray.arrFilterName.count > 0{
            
            if partnerArray.arrFilterName[0].isLike ?? 0 == 1{
               // btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
                imgLike.image = UIImage(named: "Like_a")

            }else{
               // btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
                 imgLike.image = UIImage(named: "Like (1)")
            }
            if partnerArray.arrFilterName[0].isInsightful ?? 0 == 1{
                //btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful_a")

            }else{
              //  btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
                imgInsightful.image = UIImage(named:  "Insightful")

            }
        }else{
        }
        
    }
    
    
    func PLAYVIDEOAPI(key: String){
        ApicallWebservices.SharedInstance.VIMEOVIDEOApiCallwithHeader(methodname: "https://api.vimeo.com/videos/\(key)", type: .get, header: ["Content-Type": "application/json", "Authorization": "bearer 75d0309b580962afaab71aa018e17877"], parameter: [:], strLoaderString: "", completion: {
            (error: Error?, success: Bool, result: Any?) in
            if success == true {
                let dicData = result as? NSDictionary ?? [:]
                let filesarr = dicData.object(forKey: "files") as? NSArray ?? []
                if filesarr.count > 0 {
                    let fileurl = filesarr[0] as? NSDictionary ?? [:]
                    let urlFile = fileurl.object(forKey: "link") as? String ?? ""
                    print(urlFile)
                    DispatchQueue.main.async() {
                        self.player = AVPlayer(url: URL(string: "\(urlFile)")!)
                        let playerController = AVPlayerViewController()
                        playerController.player = self.player
                        playerController.view.frame = self.viewPlayingVideo.bounds
                        self.addChild(playerController)
                        self.viewPlayingVideo.addSubview(playerController.view)
                        self.player?.play()
                        self.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
                        self.player?.addObserver(self, forKeyPath: "status", options: [], context: nil)
                        NotificationCenter.default.addObserver(self, selector: #selector(self.finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
                        
                        let asset = AVAsset(url: URL(string: "\(urlFile)")!)
                        
                        let duration = asset.duration
                        let durationTime = CMTimeGetSeconds(duration)
                        
                        print("TOTAL DURATION :->>>>>>>",durationTime)
                        self.totalDuration = Int(durationTime)
                        self.getDurationOfVideo()
                    }
                }else {
                    print("....")
                    self.view.makeToast(message: "Video is no longer...")
                }
                
            }else{
                print("....")
                self.view.makeToast(message: "Video link is not provided...")
            }
        })
    }
    
    @objc func finishVideo()
    {
        self.playbackType = "stop"
        submitVideoPlayback()
        print("Video Finished")
    }
    
    func getDurationOfVideo(){
        let interval = CMTime(value: 1, timescale: 2)
        player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in

            let seconds = CMTimeGetSeconds(progressTime)
            let secondString = String(format: "%02d", Int(seconds) % 60)
            let minutString = String(format: "%02d", Int(seconds) / 60)
            //self.currentTimeLabel.text = "\(minutString):\(secondString)"
            print("\(minutString):\(secondString)")
            self.endPosition = Int(seconds)
        })
    }
    
    //MARK:- SubmitVideoFeedback Api Call
    func submitVideoPlayback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "video_id": strVideoID,
                                       "type" : playbackType,
                                       "start_position" : startPosition,
                                       "end_position" : endPosition,
                                       "total_duration" : totalDuration]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoPlayback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                   
                    // self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    
    //MARK:- SubmitVideoFeedback Api Call
    func submitVideoFeedback(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = HomeEntity()
        
        let strParam : [String:Any] = ["member_id":strUserID ,
                                       "id": partnerArray.arrFilterName[0].strVideoID ?? "",
                                       "feedback_type": strFeedbackType,
                                       "feedback_source" : "video",
                                       "feedback_action" : isLike]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.VideoFeedback, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entSeriesFeedback = loginEntity.submitVideoFeedback(withInfo : data as? Dictionary<String, AnyObject>)
                    self.updateFeedbackUI()
                   // self.view.makeToast(message: self.entSeriesFeedback.strMessage ?? "")
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
        
    }
    
    //MARK:- Update Like and Insightful
    func updateFeedbackUI(){
        if entSeriesFeedback.isLike ?? 0 == 1{
            // btnLike.setImage(UIImage(named: "Like_a"), for: .normal)
            imgLike.image = UIImage(named: "Like_a")
            
        }else{
            //  btnLike.setImage(UIImage(named: "Like (1)"), for: .normal)
            imgLike.image = UIImage(named: "Like (1)")
            
        }
        if entSeriesFeedback.isInsightful ?? 0 == 1{
            // btnInsightful.setImage(UIImage(named: "Insightful_a"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful_a")
            
        }else{
            //  btnInsightful.setImage(UIImage(named: "Insightful"), for: .normal)
            imgInsightful.image = UIImage(named:  "Insightful")
            
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnInsightfulTapped(_ sender: UIButton) {
        strFeedbackType = "insightful"
        if entSeriesFeedback.isInsightful == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    @IBAction func btnLikeTapped(_ sender: UIButton) {
        strFeedbackType = "like"
        if entSeriesFeedback.isLike == 1{
            isLike = false
        }else{
            isLike = true
        }
        submitVideoFeedback()
    }
    @IBAction func shareButtonClicked(sender: AnyObject)
    {
        //Set the default sharing message.
         let message = "\("Dr.") \(strUserFirstName ?? "") \(strUserLastName ?? "") \("would you like to watch this lecture"), \"\(strVideoTitle)\" \("Download the Medisage app to watch.")"
        
        var comp = URLComponents()
        comp.scheme = "http"
        comp.host = "mymedisage.com/elearning"
        comp.path = "/video/\(strVideoID)/\(strVideoID)"
        let imgURL = KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(partnerArray.arrFilterName[0].strImageName ?? "" )"
        
        self.shareDynamicLink(CurrentVC: self, urlComp: comp, strTitle: message , strID: "" , imgUrl: imgURL )
        
        
        
//        //Set the link to share.
//        if let link = NSURL(string: "http://onelink.to/medisage")
//        {
//            let objectsToShare = [message,link] as [Any]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
//            self.present(activityVC, animated: true, completion: nil)
//        }
    }
    

}

extension PartnerAreasViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partnerArray.arrVideos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewUpNext.dequeueReusableCell(withIdentifier: "upNextTableCell") as! upNextTableCell
        let ent = partnerArray.arrVideos[indexPath.row]
        cell.lblTitle.text = ent.strVideoTitle
        cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
        cell.imgThumbnail.layer.cornerRadius = 10
        cell.imgThumbnail.layer.cornerRadius = 10
        cell.viewContainer.layer.cornerRadius = 10
        cell.viewContainer.layer.shadowColor = UIColor.black.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.viewContainer.layer.shadowOpacity = 0.7
        cell.viewContainer.layer.shadowRadius = 5.0
        
        cell.selectionStyle = .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ent = partnerArray.arrVideos[indexPath.row]
      //  strVideoID = ent.intID ?? 0
        divisionID = ent.intID ?? 0
        lblTitle.text = ent.strVideoTitle
        player?.replaceCurrentItem(with: nil)

        getPartnerDivisionDetails()

    }
   
}

class upNextTableCell:UITableViewCell{
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblSeries: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblParts: UILabel!
}

extension PartnerAreasViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collViewTrendingVideos {
            return self.partnerArray.arrTrendingVideos.count
        }
        else if collectionView == self.collViewLatestNewsletter {
            return self.partnerArray.arrNewsletter.count
        }
        else if collectionView == self.collViewUpcomingLiveEvents{
            return self.partnerArray.arrWebinars.count
        }
//        else if collectionView == self.collViewReadingMaterials {
//            return self.partnerArray.arrReadingMaterials.count
//        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collViewTrendingVideos{
            let cell = collViewTrendingVideos.dequeueReusableCell(withReuseIdentifier: "trendingVideosCollectionCell", for: indexPath) as! trendingVideosCollectionCell
            let ent = self.partnerArray.arrTrendingVideos[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/video/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.borderColor = UIColor.darkGray.cgColor
            cell.containerView.layer.borderWidth = 1
            return cell
            
        }
        else if collectionView == collViewLatestNewsletter{
            let cell = collViewLatestNewsletter.dequeueReusableCell(withReuseIdentifier: "newsletterCollectionCell", for: indexPath) as! newsletterCollectionCell
            let ent = self.partnerArray.arrNewsletter[indexPath.item]
           // cell.lblTitle.text = ent.strVideoTitle
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/newsletter/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            return cell
            
        }
        else {
            
            let cell = collViewUpcomingLiveEvents.dequeueReusableCell(withReuseIdentifier: "upcomingLiveEventsCollectionCell", for: indexPath) as! upcomingLiveEventsCollectionCell
            let ent = self.partnerArray.arrWebinars[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            return cell
            
            
        //}else {
            
//            let cell = collViewReadingMaterials.dequeueReusableCell(withReuseIdentifier: "latestNewsletterCollectionCell", for: indexPath) as! latestNewsletterCollectionCell
//            let ent = self.partnerArray.arrReadingMaterials[indexPath.item]
//            cell.lblTitle.text = ent.strDoctorName
//            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/documents/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
//            cell.containerView.layer.cornerRadius = 10
//            cell.containerView.layer.shadowColor = UIColor.black.cgColor
//            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
//            cell.containerView.layer.shadowOpacity = 0.3
//            cell.containerView.layer.shadowRadius = 5.0
//            return cell
            
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        player?.replaceCurrentItem(with: nil)

        if collectionView == collViewTrendingVideos{
            let ent = self.partnerArray.arrTrendingVideos[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let ent = self.self.partnerArray.arrNewsletter[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
            vc.strURL = KJUrlConstants.ImageUrl + "storage/documents/" + "\(ent.strFileName ?? "")"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collViewUpcomingLiveEvents || collectionView == collViewLatestNewsletter || collectionView == collViewTrendingVideos{
            
            return CGSize(width: 180, height: 180)
            
        }
        else {
            return CGSize(width: 0, height:0)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}

//MARK:- Custom CollectionView Cell Classes

class trendingVideosCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!

}

class newsletterCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}

class upcomingLiveEventsCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class readingMaterialsCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
