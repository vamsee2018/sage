//
//  PartnerDetailsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 07/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class PartnerDetailsViewController: BaseViewController {

    //MARK:- Custom Properties
    var partnerAreasArray = [PartnerEntity]()
    var videoArray = [PartnerEntity]()
    var seriesArray = [PartnerEntity]()
    var newsLetterArray = [PartnerEntity]()
    var partnerArray = PartnerEntity()

    var partnerID:Int?
    var userData = [String:Any]()
    var strUserID  = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var collViewPartnerAreas: UICollectionView!
    @IBOutlet weak var collViewLatestVideos: UICollectionView!
    @IBOutlet weak var collViewLatestSeries: UICollectionView!
    @IBOutlet weak var collViewLatestNewsletter: UICollectionView!
    @IBOutlet weak var btnRightArrow: UIButton!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var videosHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seriesHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsletterHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var partnerAreasHeightConstraint: NSLayoutConstraint!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        getPartnerDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Analytics.logEvent("Partner_Details", parameters: [
            AnalyticsParameterItemID: strUserID,
            AnalyticsParameterItemName: "Partner Details Page",
            AnalyticsParameterContentType: "Partner Details"
        ])
        
        
        self.imgBanner.layer.cornerRadius = 10
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
       
    }

    //MARK:- API Call for Get My Community List
    func getPartnerDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = PartnerEntity()
        let param = partnerID
        
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.PartnerDetails + "\(param ?? 0)", strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.partnerArray = partnerEntity.parsePartnerDetails(info : data as! Dictionary<String, AnyObject>)
                    self.collViewPartnerAreas.reloadData()
                    self.collViewLatestSeries.reloadData()
                    self.collViewLatestVideos.reloadData()
                    self.collViewLatestNewsletter.reloadData()
                    self.setPartnerDetails()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func setPartnerDetails(){
        if partnerArray.arrPartners.count > 0{
            partnerAreasHeightConstraint.constant = 60
        }else{
            partnerAreasHeightConstraint.constant = 0
        }
        if partnerArray.arrNewsletter.count > 0{
            newsletterHeightConstraint.constant = 240
        }else{
            newsletterHeightConstraint.constant = 0
        }
        if partnerArray.arrVideos.count > 0{
            videosHeightConstraint.constant = 240
        }else{
            videosHeightConstraint.constant = 0
        }
        if partnerArray.arrSeries.count > 0{
            seriesHeightConstraint.constant = 240
        }else{
            seriesHeightConstraint.constant = 0
        }
        imgBanner.layer.cornerRadius = 10
        imgBanner.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/partner/" + "\(partnerArray.arrFilterName[0].strImageName ?? "")"), placeholderImage:UIImage(named:"placeholder"))
    }

}

extension PartnerDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    //MARK :- Collection View Delegate & Data Source Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collViewPartnerAreas {
            return self.partnerArray.arrPartners.count
        }
        else if collectionView == self.collViewLatestVideos {
            return self.partnerArray.arrVideos.count
        }
        else if collectionView == self.collViewLatestSeries{
            return self.partnerArray.arrSeries.count
        }
        else if collectionView == self.collViewLatestNewsletter {
            return self.partnerArray.arrNewsletter.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collViewPartnerAreas{
            let cell = collViewPartnerAreas.dequeueReusableCell(withReuseIdentifier: "partnerAreasCollectionCell", for: indexPath) as! partnerAreasCollectionCell
            
            let ent = self.partnerArray.arrPartners[indexPath.item]
            cell.lblTitle.text = ent.strDoctorName
            cell.containerView.layer.borderColor = UIColor.darkGray.cgColor
            cell.containerView.layer.borderWidth = 1
            return cell
            
        }
        else if collectionView == collViewLatestVideos{
            let cell = collViewLatestVideos.dequeueReusableCell(withReuseIdentifier: "latestVideosCollectionCell", for: indexPath) as! latestVideosCollectionCell
            let ent = self.partnerArray.arrVideos[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/video/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            
            return cell
            
        }
        else if collectionView == collViewLatestSeries{
            
            let cell = collViewLatestSeries.dequeueReusableCell(withReuseIdentifier: "latestSeriesCollectionCell", for: indexPath) as! latestSeriesCollectionCell
            let ent = self.partnerArray.arrSeries[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            return cell
            
        }else {
            
            let cell = collViewLatestNewsletter.dequeueReusableCell(withReuseIdentifier: "latestNewsletterCollectionCell", for: indexPath) as! latestNewsletterCollectionCell
            let ent = self.partnerArray.arrNewsletter[indexPath.item]
            cell.lblTitle.text = ent.strVideoTitle
            cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "storage/images/newsletter/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
            cell.containerView.layer.cornerRadius = 10
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.containerView.layer.shadowOpacity = 0.3
            cell.containerView.layer.shadowRadius = 5.0
            return cell
            
        }
     }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  collectionView == collViewLatestVideos || collectionView == collViewLatestSeries || collectionView == collViewLatestNewsletter{
            
            return CGSize(width: 180, height: 180)
            
        }
        else {
            return CGSize(width: 160, height: 50)
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == self.collViewPartnerAreas {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.PartnerAreasViewController) as! PartnerAreasViewController
            let ent = self.partnerArray.arrPartners[indexPath.item]
            vc.divisionID = ent.intID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewLatestVideos{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = self.partnerArray.arrVideos[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
             
        }else if collectionView == collViewLatestSeries{
            
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.SeriesViewController) as! SeriesViewController
            let ent = self.partnerArray.arrSeries[indexPath.item]
            vc.seriesID = ent.intID ?? 0
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
            
//            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
//            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
//            let ent = self.partnerArray.arrSeries[indexPath.item]
//            vc.strVideoID = "\(ent.intID ?? 0)"
//            vc.strVideoTitle = ent.strVideoTitle ?? ""
//            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let ent = self.self.partnerArray.arrNewsletter[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PDFViewController) as! PDFViewController
            vc.strURL = KJUrlConstants.ImageUrl + "\(ent.strImagePath  ?? "")" + "\(ent.strFileName ?? "")"
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
}

//MARK:- Custom CollectionView Cell Classes
class partnerAreasCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
}

class latestVideosCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}

class latestSeriesCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
class latestNewsletterCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
