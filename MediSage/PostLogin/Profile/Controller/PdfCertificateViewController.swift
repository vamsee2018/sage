//
//  PdfCertificateViewController.swift
//  MediSage
//
//  Created by Harshad Medisage on 17/02/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class PdfCertificateViewController: BaseViewController,URLSessionDelegate,UIScrollViewDelegate {

    //MARK:- Custom Properties
    var strImageURL  = String()
    var strImagePath  = String()
    var imgView  = UIImageView()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var imgCertificate: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: ImageScrollView!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.setup()
        scrollView.imageContentMode = .aspectFit
        scrollView.initialOffset = .center
        scrollView.imageScrollViewDelegate = self

        print("imagePath",strImagePath)
       // let myImage = UIImage(named: strImagePath)
        let url = URL(string: strImagePath)
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            let image = UIImage(data: imageData)
            scrollView.display(image: image!)

        }
     
        self.tabBarController?.tabBar.isHidden = true
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
    
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
       
    }
    
    //MARK:- Zoom In/Out Image
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return imgCertificate
//    }
   
//    func scrollViewDidZoom(_ scrollView: UIScrollView) {
//        if scrollView.zoomScale > scrollView.maximumZoomScale {
//            scrollView.zoomScale = scrollView.maximumZoomScale
//        } else if scrollView.zoomScale < scrollView.minimumZoomScale {
//            scrollView.zoomScale = scrollView.minimumZoomScale
//        }
//    }
   
    //MARK:- Button Actions
    @IBAction func btnDownloadTapped(_ sender: Any) {
        strImageURL = strImagePath
       
        let urlString = strImagePath
        let url = URL(string: urlString )
        let fileName = String((url?.lastPathComponent ?? "")) as NSString
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString )
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                                    let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                    self.present(activityViewController, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            } else {
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
}

extension PdfCertificateViewController: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}
