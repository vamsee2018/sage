//
//  ProfileViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class ProfileViewController: BaseViewController {

    //MARK:- Custom Properties
    var profileArray = ProfileEntity()
    var memberID = 60
    var userData = [String:Any]()
    var strUserID  = String()
    var strSpecialization  = String()
    var strSpecializationID  = Int()
    var refreshControl = UIRefreshControl()

    var Qualification  = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collViewMyCommunities: UICollectionView!
    @IBOutlet weak var collViewNewlyAdded: UICollectionView!
    @IBOutlet weak var collViewMostViewed: UICollectionView!
    @IBOutlet weak var lblDoctorInitials: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var lblQualification: UILabel!
    @IBOutlet weak var lblAssociation: UILabel!
    @IBOutlet weak var lblSpecialization: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTotalCreditEarnings: UILabel!
    @IBOutlet weak var btnUnlock: UIButton!
    @IBOutlet weak var btnViewCertificate: UIButton!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
          super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshList), name: .profileEdited, object: nil)
        getProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        self.tabBarController?.tabBar.isHidden = false

        imgProfile.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        imgProfile.layer.borderWidth = 1.0
        
        //Pull to Refresh
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        scrollView.refreshControl = refreshControl
    }
    
    @objc func refresh()
    {
        // Code to refresh table view
        refreshControl.endRefreshing()
        self.getProfile()
        
    }
    @objc func refreshList(){
        getProfile()
    }
      
    //MARK:- Get Search API Call
    func getProfile() {
        //Check for internet or return from method
        if !RMUtility.sharedInstance.isInternetAvailable() {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        self.showLoader()
        CommonService.getProfile(userId: strUserID) { (entity, success, error) in
            self.hideLoader()
            if success {
                if let entity = entity {
                    self.profileArray = entity
                    self.collViewMyCommunities.reloadData()
                    self.collViewNewlyAdded.reloadData()
                    self.collViewMostViewed.reloadData()

                    self.setProfileDetails()
                }
            } else {
                self.view.makeToast(message: error ?? "")
            }
        }
    }
    
    func setProfileDetails(){
        
        lblDoctorName.text = "Dr. " + (profileArray.arrFilterName[0].strFirstName ?? "") + " " + (profileArray.arrFilterName[0].strLastName ?? "")
        lblEmail.text = profileArray.arrFilterName[0].strEmail
        lblCity.text = profileArray.arrFilterName[0].strCity
        lblAssociation.text = profileArray.arrFilterName[0].strAssociation
        lblTotalCreditEarnings.text = "\("CREDIT POINTS EARNED:") \(profileArray.arrFilterName[0].intRewardsCount ?? 0)"
        if profileArray.arrFilterName[0].intRewardsCount ?? 0 == 0{
            self.btnViewCertificate.setTitle("Earn Certificates", for: .normal)
        }else{
            self.btnViewCertificate.setTitle("View Certificates", for: .normal)
        }
        lblSpecialization.text = "Specialization : " + (profileArray.arrFilterName[0].strSpecialisation ?? "")
        strSpecialization = profileArray.arrFilterName[0].strSpecialisation ?? ""
        strSpecializationID = profileArray.arrFilterName[0].strSpecialisationID ?? 0
        
        if profileArray.arrFilterName[0].intProfilePercentage == 100{
            self.btnUnlock.isHidden = true
            self.btnViewCertificate.isUserInteractionEnabled = true
        }else{
            self.btnUnlock.isHidden = false
            self.btnViewCertificate.isUserInteractionEnabled = false

        }
        if profileArray.arrFilterName[0].strQualification ?? "" == ""{
            lblQualification.text = "Qualification : NA"
            Qualification = "NA"
        }else{
             lblQualification.text = "Qualification : " + (profileArray.arrFilterName[0].strQualification ?? "")
            Qualification = profileArray.arrFilterName[0].strQualification ?? ""
        }

        if profileArray.arrFilterName[0].strImageName == ""{
            imgProfile.isHidden = true
            lblDoctorInitials.clipsToBounds = true
            lblDoctorInitials.layer.cornerRadius = 60
            lblDoctorInitials.text = String(profileArray.arrFilterName[0].strFirstName?.prefix(1) ?? "") + String(profileArray.arrFilterName[0].strLastName?.prefix(1) ?? "")
        }else{
            imgProfile.clipsToBounds = true
            imgProfile.layer.cornerRadius = 60
            lblDoctorInitials.isHidden = true
            imgProfile.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(profileArray.arrFilterName[0].strImagePath  ?? "")" + "/" + "\(profileArray.arrFilterName[0].strImageName ?? "")"), placeholderImage:UIImage(named:"placeholder"))
        }
        
    }

    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
           HamburgerMenu().triggerSideMenu()
       }
       
    @IBAction func btnProfileEditTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.ProfileEditViewController) as! ProfileEditViewController
        vc.strUserFirstName = profileArray.arrFilterName[0].strFirstName ?? ""
        vc.strUserLastName = profileArray.arrFilterName[0].strLastName ?? ""
        vc.strUserEmail = profileArray.arrFilterName[0].strEmail ?? ""
        vc.strUserCity = profileArray.arrFilterName[0].strCity ?? ""
        self.navigationController?.pushViewController(vc, animated: true)

    }

    @IBAction func btnMyCommunityEditTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.MyCommunityEditViewController) as! MyCommunityEditViewController
        vc.selectedCommunities = profileArray.arrFilterValues
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnProfessionalInfoEditTapped(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.ProfessionalInfoEditViewController) as! ProfessionalInfoEditViewController
        vc.strSpecialization = strSpecialization
        vc.strQualification = Qualification
        vc.specialityID = strSpecializationID
        vc.strState = profileArray.arrFilterName[0].strState ?? ""
        vc.strRegNo = profileArray.arrFilterName[0].strRegisterationNumber ?? ""
        vc.strYear  = profileArray.arrFilterName[0].strYear ?? ""
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnExpertsViewAllTapped(_ sender: UIButton) {
        let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.MyExpertsViewController) as! MyExpertsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnViewCertificatesTapped(_ sender: UIButton) {
        if profileArray.arrFilterName[0].intProfilePercentage == 100{
            let sb = UIStoryboard(name: KJViewIdentifier.Profile, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.CertificatesViewController) as! CertificatesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            showSimpleAlert()
        }
    }
    
    @IBAction func btnLockUnlockTapped(_ sender: UIButton) {
        showSimpleAlert()
    }
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: "Update Profile", message: "To access your certificates,please complete your profile.",         preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "UPDATE",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.ProfessionalInfoEditViewController) as! ProfessionalInfoEditViewController
                                        vc.strSpecialization = self.strSpecialization
                                        vc.strQualification = self.Qualification
                                        vc.specialityID = self.strSpecializationID
                                        vc.strState = self.profileArray.arrFilterName[0].strState ?? ""
                                        vc.strRegNo = self.profileArray.arrFilterName[0].strRegisterationNumber ?? ""
                                        vc.strYear  = self.profileArray.arrFilterName[0].strYear ?? ""
                                        self.navigationController?.pushViewController(vc, animated: true)                                      }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   //MARK :- Collection View Delegate & Data Source Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collViewMyCommunities {
            return profileArray.arrFilterValues.count
        }
        else if collectionView == self.collViewNewlyAdded {
            return profileArray.arrExpertValues.count
        }else{
            return profileArray.arrMostViewedValues.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collViewMyCommunities{
            
            let cell = collViewMyCommunities.dequeueReusableCell(withReuseIdentifier: "myCommunitiesCollectionCell", for: indexPath) as! myCommunitiesCollectionCell
            cell.containerView.layer.borderColor = UIColor.gray.cgColor
            cell.containerView.layer.borderWidth = 1.0
          
            let ent = profileArray.arrFilterValues[indexPath.item]
            
            cell.lblTitle.text = ent.strVideoTitle
            cell.containerView.layer.borderColor = UIColor.gray.cgColor
            cell.containerView.layer.borderWidth = 1.0
            
            return cell
        }else{
            if collectionView == collViewNewlyAdded{
                let cell = collViewNewlyAdded.dequeueReusableCell(withReuseIdentifier: "newAddedCollectionCell", for: indexPath) as! newAddedCollectionCell
                
                let ent = profileArray.arrExpertValues[indexPath.item]
                 cell.lblTitle.text = ent.strDoctorName
                 cell.imgExpert.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/expert/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
                cell.imgExpert.layer.masksToBounds = true
                cell.imgExpert.layer.cornerRadius = 50
                cell.imgExpert.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
                cell.imgExpert.layer.borderWidth = 1.0
                cell.imgExpert.layer.shadowColor = UIColor.black.cgColor
                cell.imgExpert.layer.shadowOffset = CGSize(width: 3, height: 3)
                cell.imgExpert.layer.shadowOpacity = 0.3
                cell.imgExpert.layer.shadowRadius = 2.0
                
                return cell
                
            }
            else {
                let cell = collViewMostViewed.dequeueReusableCell(withReuseIdentifier: "mostViewedCollectionCell", for: indexPath) as! mostViewedCollectionCell
                let ent = profileArray.arrMostViewedValues[indexPath.item]
                cell.imgThumbnail.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "/storage/images/video/" + "\(ent.strImageName ?? "")"), placeholderImage:UIImage(named:"placeHolder"))
                cell.imgThumbnail.layer.cornerRadius = 10
                cell.imgThumbnail.layer.shadowColor = UIColor.black.cgColor
                cell.imgThumbnail.layer.shadowOffset = CGSize(width: 3, height: 3)
                cell.imgThumbnail.layer.shadowOpacity = 0.3
                cell.imgThumbnail.layer.shadowRadius = 2.0
                
                return cell
                
            }
        }
   
}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collViewNewlyAdded{
            return CGSize(width: 140, height: 140)
       }else if collectionView == collViewMostViewed {
            return CGSize(width: 160, height: 120)
       }
       else {
            return CGSize(width: 180, height: 40)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collViewMostViewed{
            let sb = UIStoryboard(name: KJViewIdentifier.Partner, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.VideoPlayingViewController) as! VideoPlayingViewController
            let ent = profileArray.arrMostViewedValues[indexPath.item]
            vc.strVideoID = "\(ent.intID ?? 0)"
            vc.strVideoTitle = ent.strVideoTitle ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == collViewMyCommunities{
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.CommunityVideosViewController) as! CommunityVideosViewController
            let ent = profileArray.arrFilterValues[indexPath.item]
            vc.communityID = "\(ent.intID ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let ent = profileArray.arrExpertValues[indexPath.item]
            let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.ExpertDetailsViewController) as! ExpertDetailsViewController
            vc.expertID = ent.intID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
}

//MARK:- Custom CollectionView Cell Classes

class myCommunitiesCollectionCell: UICollectionViewCell{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!

}

class newAddedCollectionCell: UICollectionViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgExpert: UIImageView!
    
}

class mostViewedCollectionCell: UICollectionViewCell{
   @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgThumbnail: UIImageView!
}
