//
//  ProfileEditViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 25/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging
import Firebase
import FirebaseAnalytics
class ProfileEditViewController: BaseViewController,UITextFieldDelegate,searchDelegate,UIImagePickerControllerDelegate , UINavigationControllerDelegate {

    //MARK:- Custom Properties
    var stateArray = [LoginRegisterEntity]()
    var qualificationArray = [LoginRegisterEntity]()
    var yearArray = [LoginRegisterEntity]()
    var specialityArray = [LoginRegisterEntity]()
    var cityArray = [LoginRegisterEntity]()
    var selectedPickerButton : UIButton?
    var entSelectedCountry1 = LoginRegisterEntity()
    var entProfile = ProfileEntity()
    var profileArray = ProfileEntity()
    var cameraPicker = UIImagePickerController()
    var profileImage: UIImage?
    var profileData : String? = ""
    var strProfileUrl : String = ""
    var memberID = 60
    var specialityID = 0
    var userData = [String:Any]()
    var strUserID  = String()
    var strUserFirstName  = String()
    var strUserLastName  = String()
    var strUserCity = String()
    var strUserEmail = String()
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var txtFirstName: FloatingTF!
    @IBOutlet weak var txtLastName: FloatingTF!
    @IBOutlet weak var txtEmail: FloatingTF!
    @IBOutlet weak var txtCity: FloatingTF!
    @IBOutlet weak var lblDoctorInitials: UILabel!
    @IBOutlet weak var btnCity: UIButton!

   //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        
        let userID = strMobileNumber.reversed()
        let reversed = String(userID)
        print("REVERSED NUMBER",String(userID))
        Analytics.setUserID("medi\(reversed)")
        Analytics.setUserProperty("medi\(reversed)", forName: "medisage-7c113")
        
        txtFirstName.text = strUserFirstName
        txtLastName.text = strUserLastName
        txtEmail.text = strUserEmail
        btnCity.setTitle(strUserCity, for: .normal)
        
        getProfile()
        getCityList()
        
        imgProfile.layer.borderColor = UIColor.init(hex:"001D4D").cgColor
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        imgProfile.layer.masksToBounds = true
        imgProfile.image = UIImage(named: "User")
        imgProfile.contentMode = .scaleToFill
        profileImage = imgProfile.image
        
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor

        viewContainer.layer.shadowColor = UIColor.black.cgColor
        viewContainer.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewContainer.layer.shadowOpacity = 0.3
        viewContainer.layer.shadowRadius = 2.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        btnUpdate.layer.cornerRadius = 10

        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false

        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
    }
    //MARK:- Get Search API Call
    func getProfile(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let searchEntity = ProfileEntity()
        self.showLoader()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MyProfile + strUserID, strParameterName: "") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.profileArray = searchEntity.parseProfile(withInfo : data as? Dictionary<String, AnyObject>)
                   
                    self.setProfileDetails()
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func setProfileDetails(){

        if profileArray.arrFilterName[0].strImageName == ""{
            imgProfile.isHidden = true
            lblDoctorInitials.clipsToBounds = true
            lblDoctorInitials.layer.cornerRadius = 70
            lblDoctorInitials.text = String(profileArray.arrFilterName[0].strFirstName?.prefix(1) ?? "") + String(profileArray.arrFilterName[0].strLastName?.prefix(1) ?? "")
        }else{
            imgProfile.clipsToBounds = true
            imgProfile.layer.cornerRadius = 70
            lblDoctorInitials.isHidden = true
            imgProfile.sd_setImage(with: URL(string: KJUrlConstants.ImageUrl + "\(profileArray.arrFilterName[0].strImagePath  ?? "")" + "/" + "\(profileArray.arrFilterName[0].strImageName ?? "")"), placeholderImage:UIImage(named:"placeholder"))
        }
        
        
    }
    
    //MARK:- Update Profile API Call
    func updateProfile(){
        
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let profileEntity = ProfileEntity()
        
        var strFcmToken = ""
        if let fcmToken = Messaging.messaging().fcmToken {
            print("FCM token: \(fcmToken)")
            strFcmToken = fcmToken
        }
        let deviceID    = UIDevice.current.identifierForVendor?.uuidString ?? ""

        if validateRegistrationData(){
            let strParam : [String:Any] = ["member_id": strUserID ,
                                           "fname": txtFirstName.text ?? "",
                                           "lname": txtLastName.text ?? "",
                                           "email": txtEmail.text ?? "",
                                           "city" : btnCity.titleLabel?.text ?? "",
                                           "device_id" : deviceID,
                                           "device_type":"ios",
                                           "device_token":strFcmToken]
            
            self.showLoader()
            requestManager.requestCommonPostMultipartMethod(strAPIName: KJUrlConstants.EditBasicInfo, strParameterName: strParam, profileImage: profileImage!)
            { (result, isSuccess, error) in
                self.hideLoader()
                if isSuccess
                {
                    if let data = result
                    {
                        self.entProfile = profileEntity.parseProfile(withInfo : data as? Dictionary<String, AnyObject>)
                        NotificationCenter.default.post(name: .profileEdited, object: nil)
                        NotificationCenter.default.post(name: .newsArticleCount, object: nil)

                        self.navigationController?.popViewController(animated: true)
                    }
                }else
                {
                    self.view.makeToast(message: error)
                }
            }
        }else{
            self.view.makeToast(message: "Please submit all basic information")
        }
        
    }
    
    //MARK:- Get City API Call
    func getCityList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CityList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.cityArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
   
    
    @IBAction func btnCityTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :cityArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Cities"
        vc.isComingFromUserDetails = true
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
       // selectedPickerButton = btnCity
    }
    
    @IBAction func btnProfileUpdateTapped(_ sender: Any) {
        updateProfile()
    }
    
    //MARK: - Button delegate method
    @IBAction func btnCameraTapped(_ sender: UIButton)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        cameraPicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }

        self.present(alert, animated: true, completion: nil)
    }
    /// method is used for opening camera option on the screen
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            cameraPicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(cameraPicker, animated: true, completion: nil)
        }
        else
        {
            self.view.makeToast(message: "You don't have camera")
        }
    }
    func openGallary()
    {
        cameraPicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(cameraPicker, animated: true, completion: nil)
    }
    
    //MARK: - Image picker delegate method
    //PickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imgProfile.layer.cornerRadius = 45
        imgProfile.contentMode = .scaleToFill
        profileImage =  imgProfile.image!
        cameraPicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        cameraPicker.dismiss(animated: true, completion: nil)
        print("picker cancel.")
    }
    
    /// method is used for validate registeration data
    ///
    /// - Returns: bool
    func validateRegistrationData() -> Bool{
        if (txtFirstName.text! == "")
        {
            self.view.makeToast(message: KJConstant.FirstNameEmptyWarning)
            return false
        }
        
        if (txtLastName.text == "")
        {
            self.view.makeToast(message: KJConstant.LastNameEmptyWarning)
            return false
        }
        
        if (txtEmail.text! == "")
        {
            self.view.makeToast(message: KJConstant.EmailEmptyWarning)
            return false
        }
        if KJValidation.isValid(strInput: txtEmail.text!, regex: KJRegX.emailAddress) == false{
            self.view.makeToast(message: KJConstant.InvalidEmailWarning)
            return false
        }
       
        return true
    }
    
    //MARK: - textfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
    }
    
    //MARK:- Search City Delegate Method Called
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity, iScreenCount: Int) {
       // if selectedPickerButton == btnCity{
            btnCity.setTitle(selectedEntity.strCityName, for: .normal)
       // }
    }
    

}
