//
//  MyCommunityEditViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 25/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class MyCommunityEditViewController: BaseViewController {

    //MARK:- Custom Properties
    var communityArray = [ProfileEntity]()
    var selectedCommunities = [ProfileEntity]()
    var bIsMultiSelect = false
    var selectedSearchTag = "all"
    var memberID = 60
    var userData = [String:Any]()
    var strUserID  = String()
    var selectedArray = [String]()
    var strItemIDs  = ""
    var btnSelected = false
    var arrItems = [String]()
    var arrSelectedRows:[Int] = []
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblViewCommunities: UITableView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnDown: UIButton!
    @IBOutlet weak var btnUp: UIButton!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        print("SELECTED COMMUNITY",selectedCommunities)
        for items in selectedCommunities{
            self.arrSelectedRows.append(items.intID ?? 0)
            print("Selected Ids",self.arrSelectedRows)
        }
        getCommunities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        btnUpdate.layer.cornerRadius = 10
        self.btnUp.isHidden = true

        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
         
      
    }
    
    //MARK:- Get Communities API Call
    func getCommunities(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let searchEntity = ProfileEntity()
        self.showLoader()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CommunityList , strParameterName: "") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.communityArray = searchEntity.communityList(info : (data as? Dictionary<String, AnyObject>)!)
                    self.tblViewCommunities.reloadData()
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Communities API Call
    func updateCommunities(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
      //  let searchEntity = ProfileEntity()
        
        let stringArray = arrSelectedRows.map(String.init)
        arrItems = stringArray
        
        let strParam : [String:Any] = ["communities_selected":  arrItems]
        self.showLoader()
        requestManager.requestCommonPostMethod(strAPIName: KJUrlConstants.UpdateMyCommunity + strUserID, strParameterName: strParam) { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                   // self.communityArray = searchEntity.communityList(info : (data as? Dictionary<String, AnyObject>)!)
                    self.view.makeToast(message: "Successfully updated..")

                    self.tblViewCommunities.reloadData()
                    NotificationCenter.default.post(name: .profileEdited, object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (tblViewCommunities.contentOffset.y <
                tblViewCommunities.contentSize.height - tblViewCommunities.frame.size.height){
            self.btnDown.isHidden = false
        }else{
            self.btnDown.isHidden = true
        }
        if (tblViewCommunities.contentOffset.y > 0){
            self.btnUp.isHidden = false
        }else{
            self.btnUp.isHidden = true
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnCommunityUpdateTapped(_ sender: Any) {
        updateCommunities()
    }
    @IBAction func btnDownArraowTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let index = IndexPath(row: self.communityArray.count-1, section: 0)
            self.tblViewCommunities.scrollToRow(at: index, at: .bottom, animated: true)
            self.btnUp.isHidden = false
        }
    }
    
    @IBAction func btnUpArraowTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let index = IndexPath(row: 0, section: 0)
            self.tblViewCommunities.scrollToRow(at: index, at: .top, animated: true)
            self.btnUp.isHidden = true

        }
    }
}

extension MyCommunityEditViewController: UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return communityArray.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewCommunities.dequeueReusableCell(withIdentifier: "myCommunityTableViewCell") as! myCommunityTableViewCell
        
        let ent = communityArray[indexPath.row]
        cell.lblTitle.text = ent.strTitle
        let id = ent.intID ?? 0
       
        
//        for items in selectedCommunities{
//            if ent.strTitle == items.strVideoTitle{
//                ent.hasSelected = true
//                self.arrItems.append(String(items.intID ?? 0))
//                self.arrSelectedRows.append(items.intID ?? 0)
//                print("Selected Items",self.arrItems)
//                print("Selected Ids",self.arrSelectedRows)
//
//                cell.imgCheckMark.image = UIImage(named: "Right-Click_Filled")
//            }else{
//               // items.hasSelected = false
//                cell.imgCheckMark.image = UIImage(named: "Right-Click_Outline")
//                print("UN-Selected Item Ids",ent.intID ?? 0)
//
//            }
//        }

      
        cell.btnTick.tag = id //indexPath.row
        
        cell.btnTick.addTarget(self, action: #selector(btnTapped(sender:)), for: .touchUpInside)
       
        if arrSelectedRows.contains(id){
            cell.imgCheckMark.image = UIImage(named: "Right-Click_Filled")

        }else{
            cell.imgCheckMark.image = UIImage(named: "Right-Click_Outline")
            print("Un-Selected",ent.intID ?? 0)

        }
//
        
//        if ent.hasSelected == true{
//            cell.imgCheckMark.image = UIImage(named: "Right-Click_Filled")
//
//        }else{
//            cell.imgCheckMark.image = UIImage(named: "Right-Click_Outline")
//            print("Un-Selected",ent.intID ?? 0)
//
//        }
//
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func btnTapped(sender: UIButton)
    {

        if self.arrSelectedRows.contains(sender.tag){
            self.arrSelectedRows.remove(at: self.arrSelectedRows.index(of: sender.tag)!)
        }else{
            self.arrSelectedRows.append(sender.tag)
            print("Selected ROWS",self.arrSelectedRows)
        }
       // self.tblViewCommunities.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
        self.tblViewCommunities.reloadData()
    }
    
  /*
    //MARK: - Loan Item Selection
    @objc func btnTapped(sender: UIButton)
    {
        let ent = communityArray[sender.tag]
                
        if ent.hasSelected {
            ent.hasSelected = false
            btnSelected = false
           
            self.arrItems.remove(at: sender.tag)
            
            if self.arrItems.count == 0{
                self.strItemIDs = ""
                
                self.tblViewCommunities.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
                
            }else{
               // self.strItemIDs = self.arrItems.joined(separator: ", ") //+ "," + "\(ent.strOrderItemID ?? 0)"
                print("Remove Item Ids",self.arrItems)
                self.tblViewCommunities.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
                
            }
            
        }else {
            
            ent.hasSelected = true
            btnSelected = true
            
            self.arrItems.append(String(ent.intID ?? 0))
            
            self.strItemIDs = self.arrItems.joined(separator: ", ") //+ "," + "\(ent.strOrderItemID ?? 0)"
           // print("Selected Item",self.arrItems)
          //  print("Selected Item Ids",self.strItemIDs)
            
            self.tblViewCommunities.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
            
        }
    }*/

}

class myCommunityTableViewCell: UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCheckMark: UIImageView!
    @IBOutlet weak var btnTick: UIButton!

}
