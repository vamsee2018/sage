//
//  CertificatesViewController.swift
//  MediSage
//
//  Created by Harshad Medisage on 12/02/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class CertificatesViewController: BaseViewController,URLSessionDelegate {

    //MARK:- Custom Properties
    var certificateArray = [ProfileEntity]()
    var userData = [String:Any]()
    var strUserID  = String()
    var strImageURL  = String()
    var pdfURL: URL!
    var strMobileNumber  = String()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblViewCertificate: UITableView!
    @IBOutlet weak var btnRightArrow: UIButton!
    @IBOutlet weak var imgNoCertificate: UIImageView!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
       
        getCertificateList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
    
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        self.tabBarController?.tabBar.isHidden = true

       
    }
    
    //MARK:- API Call for Get Partner List
    func getCertificateList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let partnerEntity = ProfileEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.MyCertificateList + strUserID, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.certificateArray = partnerEntity.parseCertificateList(info : data as! Dictionary<String, AnyObject>)
                    
                    if self.certificateArray.count > 0 {
                        self.tblViewCertificate.isHidden = false
                        self.imgNoCertificate.isHidden = true
                        self.tblViewCertificate.reloadData()
                    }else{
                        self.tblViewCertificate.isHidden = true
                        self.imgNoCertificate.isHidden = false
                    }
                    
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            //self.pdfURL = destinationURL
            
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.pushFile(self.pdfURL)
            }
            
            print("Destination URL:", destinationURL)
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
    
    func pushFile(_ destination: URL) {
        let sb = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.MyEventPreviewViewController) as! MyEventPreviewViewController
        vc.isComingFromCertificate = true
        vc.strBannerImagePath = strImageURL
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}

extension CertificatesViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return certificateArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewCertificate.dequeueReusableCell(withIdentifier: "searchTableViewCell") as! searchTableViewCell
       
        let ent = certificateArray[indexPath.item]
        cell.lblTitle.text = ent.strTitle
        cell.lblPartner.text = ent.strDate
      //  cell.btnDownload.tag = indexPath.row
       // cell.btnDownload.addTarget(self, action:#selector(downloadCertificate(sender:)), for: .touchUpInside)

        cell.imgSearchContent.contentMode = .scaleToFill
        cell.imgSearchContent.layer.masksToBounds = true
        cell.imgSearchContent.layer.cornerRadius = 10
        cell.imgSearchContent.sd_setImage(with: URL(string: ent.strImagePath ?? ""), placeholderImage:UIImage(named:"placeHolder"))
        cell.viewContainer.layer.cornerRadius = 5
        cell.viewContainer.layer.shadowColor = UIColor.gray.cgColor
        cell.viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.viewContainer.layer.shadowOpacity = 0.7
        cell.viewContainer.layer.shadowRadius = 2.0
        
        cell.selectionStyle = .none
        
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: KJViewIdentifier.Profile, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: KJViewIdentifier.PdfCertificateViewController) as! PdfCertificateViewController
        let ent = self.certificateArray[indexPath.row]
        vc.strImagePath = ent.strImagePath ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 160
    }
    
    
}

extension URL    {
    func checkFileExist() -> Bool {
        let path = self.path
        if (FileManager.default.fileExists(atPath: path))   {
            print("FILE AVAILABLE")
            return true
        }else        {
            print("FILE NOT AVAILABLE")
            return false;
        }
    }
}
   
