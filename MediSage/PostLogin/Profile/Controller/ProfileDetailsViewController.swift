//
//  ProfileDetailsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 09/11/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class ProfileDetailsViewController:BaseViewController {

    //MARK:- Custom Properties
      
    //MARK:- IBOutlet Connections
    @IBOutlet weak var tblViewEpisodes: UITableView!
    @IBOutlet weak var collViewPeopleWatched: UICollectionView!
    @IBOutlet weak var viewPartnerBG: UIView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet weak var lblExpertName: UILabel!
    @IBOutlet weak var lblSeriesTitle: UILabel!
    @IBOutlet weak var lblSeriesDescription: UILabel!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
          super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
        self.viwTop.btnMenu.isHidden = false
        self.viwTop.btnBack.isHidden = true
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false

        
        self.viwTop.imgBack.image = UIImage(named: "ic_menu")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
       
    }

   

    @IBAction func hamburgerBtnAction(_ sender: UIBarButtonItem) {
           HamburgerMenu().triggerSideMenu()
       }
       
    

}

