//
//  ProfessionalInfoEditViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 28/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
class ProfessionalInfoEditViewController: BaseViewController,searchDelegate {

    @IBOutlet weak var profileEditContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stateSepratorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stateTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var statecouncilHeightConstraint: NSLayoutConstraint!
    //MARK:- Custom Properties
    var userData = [String:Any]()
    var strUserID  = String()
    var strSpecialization  = String()
    var strQualification  = String()
    var strState  = String()
    var strYear  = String()
    var strRegNo  = String()

    @IBOutlet weak var statecouncilContainer: UIView!
    var stateArray = [LoginRegisterEntity]()
    var qualificationArray = [LoginRegisterEntity]()
    var yearArray = [LoginRegisterEntity]()
    var specialityArray = [LoginRegisterEntity]()
    var cityArray = [LoginRegisterEntity]()
    var profileArray = ProfileEntity()
    var specialityID = 0
    var entProfile = ProfileEntity()
    var strMobileNumber  = String()

    var selectedPickerButton : UIButton?
    //MARK:- IBOutlet Connections
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnQualification: UIButton!
    @IBOutlet weak var btnSpecialisation: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var txtRegisterationNumber: FloatingTF!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var statecontrolButtonHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var yearHeaderLabel: UILabel!
    var isIndian: Bool  = false

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        btnState.setTitle("Please select", for: .normal)

        initialSetUp()
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
            if let mobile = userData["mobileNumber"] as? String{
                
                strMobileNumber = mobile
            }
        }
        btnSpecialisation.setTitle(strSpecialization, for: .normal)
        btnQualification.setTitle(strQualification, for: .normal)
        btnState.setTitle(strState, for: .normal)
        btnYear.setTitle(strYear, for: .normal)
        txtRegisterationNumber.text = strRegNo
        getStateList()
        getQualificationList()
        getYearsList()
        getSpecialityList()
    }
    
   func initialSetUp() {
    isIndian = UserDefaults.standard.bool(forKey: "isIndian")
    yearHeaderLabel.text = isIndian ? "Year" : "Year of Graduation"
    statecouncilHeightConstraint.constant = isIndian ? 50 : 0
    stateSepratorHeightConstraint.constant = isIndian ? 1 : 0
    stateTitleHeightConstraint.constant = isIndian ? 19 : 0
    arrowHeightConstraint.constant = isIndian ? 20 : 0
    statecouncilContainer.isHidden = !isIndian
    statecontrolButtonHeightConstraint.constant = isIndian ? 30 : 0
    profileEditContainerHeightConstraint.constant = isIndian ? 400 : 346
    txtRegisterationNumber.placeholder = isIndian ? "Registration No." : "Medical Licence No."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.viwTop.btnMenu.isHidden = true
        self.viwTop.btnBack.isHidden = false
        self.viwTop.btnMore.isHidden = false
        self.viwTop.imgMore.isHidden = false
        self.viwTop.imgLogo.isHidden = false
        
        
        self.viwTop.imgBack.image = UIImage(named: "back")
        self.viwTop.imgMore.image = UIImage(named: "bell")
        navigationController?.navigationBar.isHidden = false
        customizeNavigationwithBackButton()
        
        btnUpdate.layer.cornerRadius = 10
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor

        viewContainer.layer.shadowColor = UIColor.black.cgColor
        viewContainer.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewContainer.layer.shadowOpacity = 0.3
        viewContainer.layer.shadowRadius = 2.0
    }
    
    //MARK:- Update Profile API Call
    func updateProfessionalInfo(){
        
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let profileEntity = ProfileEntity()
        
        var strFcmToken = ""
        if let fcmToken = Messaging.messaging().fcmToken {
            print("FCM token: \(fcmToken)")
            strFcmToken = fcmToken
        }
      //  let deviceID    = UIDevice.current.identifierForVendor?.uuidString ?? ""
       
        
        if btnState.titleLabel?.text ?? "" == "Please select"{
            strState = ""
        }else{
            strState = btnState.titleLabel?.text ?? ""
        }
        if btnYear.titleLabel?.text ?? "" == "Please select"{
            strYear = ""
        }else{
            strYear = btnYear.titleLabel?.text ?? ""
        }
        if txtRegisterationNumber.text ?? "" == ""{
            strRegNo = ""
        }else{
            strRegNo = txtRegisterationNumber.text ?? ""
        }
        
        let strParam : [String:Any] = ["member_id": strUserID ,
                                       "specialization" : btnSpecialisation.titleLabel?.text ?? "",
                                       "qualification" : btnQualification.titleLabel?.text ?? "",
                                       "state" : strState,
                                       "registered_year" : strYear,
                                       "rno": strRegNo,
                                       "speciality_id" : "\(specialityID)",
                                       "device_type":"ios",
                                       "device_token":strFcmToken]
        
        
        self.showLoader()
        requestManager.requestCommonPostMethod(strAPIName: KJUrlConstants.EditProfessionalInfo, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entProfile = profileEntity.parseProfile(withInfo : data as? Dictionary<String, AnyObject>)
                    NotificationCenter.default.post(name: .profileEdited, object: nil)
                    NotificationCenter.default.post(name: .newsArticleCount, object: nil)

                    self.navigationController?.popViewController(animated: true)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }

    //MARK:- Get Country API Call
    func getStateList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.StateList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.stateArray.removeAll()
                    self.stateArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Country API Call
    func getQualificationList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.QualificationsList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.qualificationArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Speciality API Call
       func getSpecialityList(){
           if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
           {
               self.view.makeToast(message: KJConstant.NoInternetConnection)
               return
           }
           let requestManager = RequestManager()
           self.showLoader()
           let loginEntity = LoginRegisterEntity()
           requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.SpecialisationList, strParameterName:"") { (result, isSuccess, error) in
               self.hideLoader()
               if isSuccess
               {
                   if let data = result
                   {
                       self.specialityArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                   }
               }else
               {
                   self.view.makeToast(message: error)
               }
           }
       }
       
    //MARK:- Get Country API Call
    func getYearsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.GetYears, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.yearArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnStateTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :stateArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select States"
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnState
    }
    
    @IBAction func btnQualificationTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :qualificationArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Highest Qualification"
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnQualification
    }
    @IBAction func btnSpecialisationTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :specialityArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Specialisation"
        vc.isComingFromEditProfile = true
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnSpecialisation
    }
    @IBAction func btnYearTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :yearArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Year"
        vc.isBusinessTypeSelected = true
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnYear
    }

    @IBAction func btnProfileUpdateTapped(_ sender: Any) {
       updateProfessionalInfo()
    }
    
    //MARK:- Search City Delegate Method Called
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity, iScreenCount: Int) {
        if selectedPickerButton == btnState{
            btnState.setTitle(selectedEntity.strCountryName, for: .normal)
            strState = selectedEntity.strCountryName ?? ""
        }else if selectedPickerButton == btnQualification{
            btnQualification.setTitle(selectedEntity.strCountryName, for: .normal)
        }else if selectedPickerButton == btnSpecialisation{
            btnSpecialisation.setTitle(selectedEntity.strAffiliation, for: .normal)
            specialityID = selectedEntity.intID ?? 0
        }else{
            btnYear.setTitle(selectedEntity.strYear, for: .normal)
            strYear = selectedEntity.strYear ?? ""
        }
    }
}
