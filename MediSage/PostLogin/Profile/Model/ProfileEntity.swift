//
//  ProfileEntity.swift
//  MediSage
//
//  Created by Harshad Wagh on 29/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class ProfileEntity: NSObject {
    
   
    var strImageName : String? = ""
    var strDoctorName: String? = ""
    var strImagePath: String? = ""
    var strPath: String? = ""
    var strVideoTitle: String? = ""
    var strTitle: String? = ""
    var strVideoID: String? = ""
    var striFramePath: String? = ""
    var strFirstName: String? = ""
    var strLastName: String? = ""
    var strQualification: String? = ""
    var strSpecialisation: String? = ""
    var strSpecialisationID: Int? = 0

    var strExperience: String? = ""
    var strCity: String? = ""
    var strAssociation: String? = ""
    var strEmail: String? = ""
    var strMobileNumber: String? = ""
    var strYear: String? = ""
    var strRegisterationNumber: String? = ""
    var strState: String? = ""

    var intID: Int?
    var intCertificateCount: Int?
    var intRewardsCount: Int?
    var intProfilePercentage: Float?
    var strDesignation: String?
    var dataArr = [ProfileEntity]()
    var communityArr = [String]()
    var strDescription: String? = ""

    var strPartnerName: String? = ""
    var strEventTime: String? = ""
    var strDate: String? = ""
    var hasSelected = false

    var arrFilterName = [ProfileEntity]()
    var arrFilterValues = [ProfileEntity]()
    var arrExpertValues = [ProfileEntity]()
    var arrMostViewedValues = [ProfileEntity]()

    func parseProfile(withInfo info : Dictionary<String,AnyObject>?) -> ProfileEntity
    {
        let mainEnt = ProfileEntity()
        if let data = info?["Data"] as? [String:Any]{
            
            let myEnt = ProfileEntity()
            if let fName = data["fname"] as? String {
                myEnt.strFirstName = fName
            }
            if let lName = data["lname"] as? String {
                myEnt.strLastName = lName
            }
            if let qualification = data["qualification"] as? String {
                myEnt.strQualification = qualification
            }
            if let specialization = data["specialization"] as? String {
                myEnt.strSpecialisation = specialization
            }
            if let speciality_id = data["speciality_id"] as? Int {
                myEnt.strSpecialisationID = speciality_id
            }
            if let associations = data["associations"] as? String {
                myEnt.strAssociation = associations
            }
            if let email = data["email"] as? String {
                myEnt.strEmail = email
            }
            if let imagePath = data["path"] as? String{
                myEnt.strImagePath = imagePath
            }
            if let imageName = data["profile_image"] as? String{
                myEnt.strImageName = imageName
            }
            if let city = data["city"] as? String{
                myEnt.strCity = city
            }
            if let year = data["registered_year"] as? Int{
                myEnt.strYear = "\(year)"
            }
            if let state = data["registration_state"] as? String{
                myEnt.strState = state
            }
            if let registration_number = data["registration_number"] as? String{
                myEnt.strRegisterationNumber = registration_number
            }
            if let certificate_count = data["certificate_count"] as? Int{
                myEnt.intCertificateCount = certificate_count
            }
            if let rewards = data["rewards"] as? Int{
                myEnt.intRewardsCount = rewards
            }
            if let profile_percentage = data["profile_percentage"] as? Float{
                myEnt.intProfilePercentage = profile_percentage
            }
            
            
            if let arrObject = data["community"] as? [[String:Any]]{
                for item in arrObject {
                    
                    if let dict = item as? [String:AnyObject]
                    {
                        let ent = ProfileEntity()
                        
                        if let id = dict["id"] as? Int{
                            ent.intID = id
                        }
                        if let name = dict["name"] as? String{
                            ent.strDoctorName = name
                        }
                        if let title = dict["title"] as? String{
                            ent.strVideoTitle = title
                        }
                        mainEnt.arrFilterValues.append(ent)
                        
                    }
                    
                }
            }
            if let arrObject = data["new_experts"] as? [[String:Any]]{
                for item in arrObject {
                    
                    if let dict = item as? [String:AnyObject]
                    {
                        let ent = ProfileEntity()
                        
                        if let id = dict["id"] as? Int{
                            ent.intID = id
                        }
                        if let name = dict["name"] as? String{
                            ent.strDoctorName = name
                        }
                        if let title = dict["title"] as? String{
                            ent.strVideoTitle = title
                        }
                        if let imageName = dict["image_name"] as? String{
                            ent.strImageName = imageName
                        }
                        if let designation = dict["designation"] as? String{
                            ent.strDesignation = designation
                        }
                        if let description = dict["description"] as? String{
                            ent.strDescription = description
                        }
                        if let imagePath = data["path"] as? String{
                            ent.strImagePath = imagePath
                        }
                        mainEnt.arrExpertValues.append(ent)
                        
                    }
                    
                }
            }
            if let arrObject = data["trending_videos"] as? [[String:Any]]{
                for item in arrObject {
                    
                    if let dict = item as? [String:AnyObject]
                    {
                        let ent = ProfileEntity()
                        
                        if let id = dict["id"] as? Int{
                            ent.intID = id
                        }
                        if let name = dict["name"] as? String{
                            ent.strDoctorName = name
                        }
                        if let title = dict["title"] as? String{
                            ent.strVideoTitle = title
                        }
                        if let imageName = dict["image_name"] as? String{
                            ent.strImageName = imageName
                        }
                        if let designation = dict["designation"] as? String{
                            ent.strDesignation = designation
                        }
                        if let description = dict["description"] as? String{
                            ent.strDescription = description
                        }
                        if let imagePath = data["path"] as? String{
                            ent.strImagePath = imagePath
                        }
                        mainEnt.arrMostViewedValues.append(ent)
                        
                    }
                    
                }
            }
            mainEnt.arrFilterName.append(myEnt)
           
        }
        return mainEnt
    }

 
    //MARK:-  Method for country code list  api data parsing
    func communityList(info : Dictionary<String,AnyObject>)->[ProfileEntity]{
        
        var mainEnt = [ProfileEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
        
        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = ProfileEntity()
                
                if let id = dict["id"] as? Int{
                    ent.intID = id
                }
               
                if let title = dict["title"] as? String{
                    ent.strTitle = title
                }
               
                mainEnt.append(ent)
            }
        }
        return mainEnt
    }
    
    //MARK:-  Method for Certificate list  api data parsing
    func parseCertificateList(info : Dictionary<String,AnyObject>)->[ProfileEntity]{
        
        var mainEnt = [ProfileEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]
            
            for obj in dictClass
            {
                if let dict = obj as? [String:AnyObject]
                {
                    let ent = ProfileEntity()

                    if let rewards = dict["rewards"] as? Int{
                        ent.intRewardsCount = rewards
                    }
                    
                    if let title = dict["title"] as? String{
                        ent.strTitle = title
                    }
                    if let imagePath = dict["img_path"] as? String{
                        ent.strImagePath = imagePath
                    }
                    if let date = dict["date"] as? String{
                        ent.strDate = date
                    }
//                    if let imagePath = dict["pdf_path"] as? String{
//                        ent.strImagePath = imagePath
//                    }
                    mainEnt.append(ent)
                    
                }
            }
        
        return mainEnt
    }
       
}
