//
//  ViewOneSideMenuViewController.swift
//  HamburgerMenu
//
//  Created by Prathamesh Salvi on 24/10/18.
//  Copyright © 2018 Big Rattle Technologies Private Limited. All rights reserved.
//

import UIKit

class ViewOneSideMenuViewController: BaseViewController {

    //MARK:- Custom Properties
    var entData = HomeEntity()
    var strType = String()
    var strApiName = String()
    var strTitle = String()

    //MARK:- IBOutlet Connections
     @IBOutlet weak var  txtView: UITextView!
    
    //MARK:- View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = strTitle
        
        
        gettncPrivacyAboutDetails()
    }
    
    @IBAction func backBarBtnAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Get Country API Call
    func gettncPrivacyAboutDetails(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = HomeEntity()
       
        let defaults = UserDefaults.standard
        strType = defaults.object(forKey: "TNCPOLICY") as! String
        if strType == "Terms & Conditions"{
            strApiName = KJUrlConstants.TermsConditions
        }else if strType == "About Us"{
            strApiName = KJUrlConstants.AboutUS
        }else if strType == "Privacy Policy"{
            strApiName = KJUrlConstants.PrivacyPolicy
        }else{
            
        }
    
        requestManager.requestCommonGetMethod(strAPIName: strApiName, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.entData = loginEntity.getTNCPolicyDetails(info : data as! Dictionary<String, AnyObject>)
                    self.showDetails()

                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    func showDetails(){
        let ent = self.entData
        
        txtView.attributedText = ent.strDescription?.htmlAttributedString()
    }

}

//extension for html string parsing
extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
}
