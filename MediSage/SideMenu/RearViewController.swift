//
//  RearViewController.swift
//  HamburgerMenu
//
//  Created by Prathamesh Salvi on 05/10/18.
//  Copyright © 2018 Big Rattle Technologies Private Limited. All rights reserved.
//

import UIKit

class RearViewController: UIViewController {

    
    @IBOutlet weak var lblVersion : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        lblVersion.font = UIFont(name: "Gotham-Bold", size: 18)
        lblVersion.text = "App Version : \(appVersion ?? "")"
    }
    
    
    @IBAction func tncTapped(_ sender: UIStoryboardSegue) {
        let defaults = UserDefaults.standard
        defaults.set("Terms & Conditions",forKey: "TNCPOLICY")
        defaults.synchronize()
       
       self.performSegue(withIdentifier: "openViewOneSegue", sender: self)
        HamburgerMenu().closeSideMenu()

    }
    
    @IBAction func privacyPolicyTapped(_ sender: UIButton) {
        let defaults = UserDefaults.standard
        defaults.set("Privacy Policy",forKey: "TNCPOLICY")
        defaults.synchronize()
        HamburgerMenu().closeSideMenu()
       self.performSegue(withIdentifier: "openViewOneSegue", sender: self)
        
    }
    @IBAction func aboutUSTapped(_ sender: UIButton) {
        let defaults = UserDefaults.standard
        defaults.set("About Us",forKey: "TNCPOLICY")
        defaults.synchronize()
        HamburgerMenu().closeSideMenu()
       self.performSegue(withIdentifier: "openViewOneSegue", sender: self)
        
    }
    @IBAction func shareTapped(_ sender: UIButton) {
        let defaults = UserDefaults.standard
        defaults.set("Share App",forKey: "TNCPOLICY")
        defaults.synchronize()
        HamburgerMenu().closeSideMenu()
        //Set the link to share.
        if let link = NSURL(string: "http://onelink.to/medisage")
        {
           let objectsToShare = [link] as [Any]
           let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
           activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
           self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        HamburgerMenu().closeSideMenu()
        
        UserDefaults.standard.removeObject(forKey: KJConstant.kUserDetails)
        UserDefaults.standard.removeObject(forKey: KJConstant.kProfileImage)
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        
        let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
   

}
