//
//  LoginViewController.swift
//  mediSage
//
//  Created by Harshad Wagh on 21/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging

class LoginViewController: BaseViewController,UITextFieldDelegate {

    //MARK:- Custom Properties
    var strOtp  = String()
    var strUserName  = String()
    var strMobileNumber  = String()
    var strCountry: String?
    var strCountryCode  = String()
    var strParameter = [String:Any]()
    var iReminderTimeOut = 60
    var timer = Timer()
    
    var userArray = LoginRegisterEntity()
       
    //MARK:- IBOutlet Connections
    @IBOutlet weak var txtPassword: FloatingTF!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnHideShow: UIButton!
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        lblUserName.text = "Welcome Dr. " + strUserName
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    //MARK:- Password Verify Api Call
    func verifyPassword(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        if self.validateRegistrationData() //display toast for validation
        {
            
            let objLoginManager = RequestManager()
            let loginEntity = LoginRegisterEntity()
            let deviceID    = UIDevice.current.identifierForVendor?.uuidString ?? ""
            var strFcmToken = ""
            if let fcmToken = Messaging.messaging().fcmToken {
                print("FCM token: \(fcmToken)")
                strFcmToken = fcmToken
            }
            let strParam : [String:Any] = ["mobile_number":strMobileNumber ,
                                           "password": txtPassword.text ?? "",
                                           "device_id": deviceID,
                                           "device_type":"ios",
                                           "device_token":strFcmToken]
            self.showLoader()
            objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.PasswordVerify, strParameterName: strParam)
            { (result, isSuccess, error) in
                self.hideLoader()
                if isSuccess
                {
                    if let data = result
                    {
                        self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                    }
                    if self.userArray.strUserStatus == 1{
                    //   let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.UserDetailsViewController) as! UserDetailsViewController
                    let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                    UserDefaults.standard.synchronize()
                    //  vc.strUserFirstName = self.userArray.strUserFirstName ?? ""
                    //   vc.strUserLastName = self.userArray.strUserLastName ?? ""
                    UIApplication.shared.keyWindow?.rootViewController = vc
                    
                    //  self.navigationController?.pushViewController(vc, animated: true)
                      }else{
                        self.view.makeToast(message: self.userArray.strMessage ?? "")
                      }
                    
                }
                else
                {
                    self.view.makeToast(message: error)
                }
            }
        }else{
            self.view.makeToast(message: KJConstant.mobileNumberWarning)
        }
    }
    
    func validateRegistrationData() -> Bool{
        if(txtPassword.text == ""){
            self.view.makeToast(message: KJConstant.PasswordEmptyWarning)
            return false
        }

        if (txtPassword.text!.count < 6 || (txtPassword.text!.count) == 20)
        {
            self.view.makeToast(message: KJConstant.InvalidPasswordWarning)
            // txtPassword.setError(KJConstant.InvalidPasswordWarning, show: true)
            return false
        }
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- Button Actions
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: Any) {
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        
        let objLoginManager = RequestManager()
        let loginEntity = LoginRegisterEntity()
        
        let strParam : [String:Any] = ["mobile_number":strMobileNumber]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.ForgotPassword, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                }
                
                if self.userArray.strUserStatus == 1{
                    let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.OTPViewController) as! OTPViewController
                    vc.strMobileNumber = self.strMobileNumber
                    vc.strUserName = self.strUserName
                    vc.isComingFromForgotPassword = true
                    UIApplication.shared.keyWindow?.rootViewController = vc
                }else{
                    self.view.makeToast(message: self.userArray.strMessage ?? "")
                }
                
            }
            else
            {
                self.view.makeToast(message: error)
            }
            
        }
    }
    
    @IBAction func btnHideShowPasswordTapped(_ sender: Any) {
       let isSecureTextEntry = txtPassword.isSecureTextEntry
       txtPassword.isSecureTextEntry = isSecureTextEntry ? false : true
       if isSecureTextEntry {
           btnHideShow.setImage(UIImage(named: "show"), for: .normal)
       } else {
           btnHideShow.setImage(UIImage(named: "hide"), for: .normal)
       }
    }
    
    @IBAction func btnSignInTapped(_ sender: Any) {
        verifyPassword()
    }
    
    @IBAction func btnRequestOTPTapped(_ sender: Any) {
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
       
        let objLoginManager = RequestManager()
        let loginEntity = LoginRegisterEntity()
        
        let strParam : [String:Any] = ["mobile_number":strMobileNumber ,"country_code": strCountryCode,"country": strCountry ?? ""]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.NumberVerify, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                }
                
                if self.userArray.strIsRegister == 1{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.LoginViewController) as! LoginViewController
                    vc.strMobileNumber = self.strMobileNumber
                    vc.strUserName = self.userArray.strUserFirstName ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: KJViewIdentifier.OTPViewController) as! OTPViewController
                    vc.strMobileNumber = self.strMobileNumber
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            else
            {
                self.view.makeToast(message: error)
            }
        }
    }
}
