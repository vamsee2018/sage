//
//  CreatePasswordViewController.swift
//  mediSage
//
//  Created by Harshad Wagh on 21/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging

class CreatePasswordViewController: BaseViewController {

    //MARK:- Custom Properties
    var strMobileNumber  = String()
    var strDoctorName  = String()

    var userArray = LoginRegisterEntity()
    
    //MARK:- IBOutlet Connections
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtNewPassword: FloatingTF!
    @IBOutlet weak var txtConfirmPassword: FloatingTF!
    @IBOutlet weak var btnHideShowNewPassword: UIButton!
    @IBOutlet weak var btnHideShowConfirmPassword: UIButton!

   
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    //MARK:- API Call for Create Password
    func createPassword(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        
        let objLoginManager = RequestManager()
        let loginEntity = LoginRegisterEntity()
        var strFcmToken = ""
        if let fcmToken = Messaging.messaging().fcmToken {
            print("FCM token: \(fcmToken)")
            strFcmToken = fcmToken
        }
        let deviceID    = UIDevice.current.identifierForVendor?.uuidString ?? ""
      
        let strParam : [String:Any] = ["mobile_number":strMobileNumber ,
                                       "password": txtNewPassword.text ?? "",
                                       "device_id": deviceID,
                                       "device_type":"ios",
                                       "device_token":strFcmToken]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.SetNewPassword, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                }
                let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)

                let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.LoginViewController) as! LoginViewController
                vc.strMobileNumber = self.strMobileNumber
                vc.strUserName = self.strDoctorName
                UIApplication.shared.keyWindow?.rootViewController = vc

               // self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else
            {
                self.view.makeToast(message: error)
            }
            
        }
    }
    
    
    //MARK:- Validation for password fields
    func validatePaswordData() -> Bool{
        
        if (txtNewPassword.text! == "")
        {
            self.view.makeToast(message: KJConstant.PasswordEmptyWarning)
            return false
        }
        if (txtNewPassword.text!.count <= 8 || (txtNewPassword.text!.count) > 20)
        {
            self.view.makeToast(message: KJConstant.InvalidPasswordWarning)
            return false
        }
        if (txtConfirmPassword.text! == "")
        {
            self.view.makeToast(message: KJConstant.ConfirmPasswordEmptyWarning)
            return false
        }
        if (txtConfirmPassword.text!.count <= 8 || (txtConfirmPassword.text!.count) > 20)
        {
            self.view.makeToast(message: KJConstant.InvalidConfirmPasswordWarning)
            return false
        }
        if (txtNewPassword.text! != txtConfirmPassword.text!){
            self.view.makeToast(message: KJConstant.PasswordConfirmNotMatch)
            return false
        }
        return true
    }
    
    //MARK:- Button Actions
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHideShowPasswordTapped(_ sender: Any) {
        
        if (sender as AnyObject).tag == 1{
            let isSecureTextEntry = txtNewPassword.isSecureTextEntry
            txtNewPassword.isSecureTextEntry = isSecureTextEntry ? false : true
            if isSecureTextEntry {
                btnHideShowNewPassword.setImage(UIImage(named: "show"), for: .normal)
            } else {
                btnHideShowNewPassword.setImage(UIImage(named: "hide"), for: .normal)
            }
        }else{
            let isSecureTextEntry = txtConfirmPassword.isSecureTextEntry
            txtConfirmPassword.isSecureTextEntry = isSecureTextEntry ? false : true
            if isSecureTextEntry {
                btnHideShowConfirmPassword.setImage(UIImage(named: "show"), for: .normal)
            } else {
                btnHideShowConfirmPassword.setImage(UIImage(named: "hide"), for: .normal)
            }
        }
        
    }
    
    @IBAction func btnLoginTapped(_ sender: Any) {
        if self.validatePaswordData() //Check password and confirm password matches
        {
           createPassword()
        }
    }
    

}
