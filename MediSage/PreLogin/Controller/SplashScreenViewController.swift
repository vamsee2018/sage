//
//  SplashScreenViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 03/12/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseDynamicLinks
import Firebase

class SplashScreenViewController: UIViewController {

    //MARK:- IBOutlet Connections
    @IBOutlet weak var imgLogo: UIImageView!
    var timer = Timer()
    var window: UIWindow?
    var userData = [String:Any]()
    var strUserID  = String()
    var userDefaults = UserDefaults.standard

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
        }

        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "MediSage Logo _APP 1", withExtension: "gif")!)
        let advTimeGif = UIImage.sd_image(withGIFData: imageData)
        imgLogo.image = advTimeGif
       
        if userDefaults.bool(forKey: "First_Time_Launch") == true{
            userDefaults.setValue(true, forKey: "First_Time_Launch")
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(updateTimer(_:)), userInfo: nil, repeats: false)

        }else{
            userDefaults.setValue(true, forKey: "First_Time_Launch")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "IntroScreenViewController") as! IntroScreenViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        }
       // Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(updateTimer(_:)), userInfo: nil, repeats: false)

    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    
    //MARK:- Timer for OTP
    @objc func updateTimer(_ timer: AnyObject)
    {
        if UserDefaults.standard.bool(forKey: "isUserLoggedIn") {
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            UIApplication.shared.keyWindow?.rootViewController = vc
           
        }else{
            let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
            UIApplication.shared.keyWindow?.rootViewController = vc

        }
    }
    
}
