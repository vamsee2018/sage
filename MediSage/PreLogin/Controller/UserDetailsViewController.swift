//
//  UserDetailsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 28/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging

class UserDetailsViewController: BaseViewController,searchDelegate {

   //MARK:- Custom Properties
    var strUserFirstName  = String()
    var strUserLastName  = String()
    var strUserCity = String()
    var strUserEmail = String()
    var strCountryCode  = String()
    var strMobileNumber  = String()
    var strCountry  = String()
    var strSpecialityID  = String()
    var strSpecialization  = String()
    var userData = [String:Any]()
    var strUserID  = String()
    var strOTPType  = String()
    var isMasterUser = Bool()
    var isUserOutsideIndia = Bool()
    var isConnectionShared = Bool()

    var selectedPickerButton : UIButton?
    var cityArray = [LoginRegisterEntity]()
    var specialityArray = [LoginRegisterEntity]()
    var userEntity = LoginRegisterEntity()

    //MARK:- IBOutlet Connections
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtFirstName: FloatingTF!
    @IBOutlet weak var txtLastName: FloatingTF!
    @IBOutlet weak var txtEmail: FloatingTF!
    @IBOutlet weak var txtSetPassword: FloatingTF!
    @IBOutlet weak var btnHideShow: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var btnSpecialisation: UIButton!
    @IBOutlet weak var btnCheckCertifiedProfessional: UIButton!
    @IBOutlet weak var btnCheckTNC: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var isIndian: Bool  = false
    @IBOutlet weak var abroadUserTextField: FloatingTF!
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getSpecialityList()

        isIndian = UserDefaults.standard.bool(forKey: "isIndian")
        prepareCountryLayout()
        
        if isMasterUser == true{
             lblTitle.text = "Please update your details"
        } else{
             lblTitle.text = "Please provide your details"
        }
        txtFirstName.text = strUserFirstName
        txtLastName.text = strUserLastName
        txtEmail.text = strUserEmail
        btnCity.setTitle(strUserCity, for: .normal)
        btnCheckCertifiedProfessional.isSelected = false
        btnCheckCertifiedProfessional.setImage(UIImage(named: "Right-Click_Outline"), for: .normal)
        btnCheckTNC.isSelected = false
        btnCheckTNC.setImage(UIImage(named: "Right-Click_Outline"), for: .normal)
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func prepareCountryLayout() {
        if isIndian {
            getCityList()
            btnCity.isHidden = false
            abroadUserTextField.isHidden = true
        } else {
            abroadUserTextField.isHidden = false
            btnCity.isHidden = true
        }
    }
    
    //MARK:- Get Country API Call
    func getCityList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CityList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.cityArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Speciality API Call
    func getSpecialityList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.SpecialisationList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.specialityArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Update Register API Call
    func userRegister(){
        
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let userEntity = LoginRegisterEntity()
        let deviceID   = UIDevice.current.identifierForVendor?.uuidString ?? ""
        
        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
        }
        var strFcmToken = ""
        if let fcmToken = Messaging.messaging().fcmToken {
            print("FCM token: \(fcmToken)")
            strFcmToken = fcmToken
        }
        
        if btnSpecialisation.titleLabel?.text ?? "" == "Please select"{
            strSpecialization = ""
        }else{
            strSpecialization = btnSpecialisation.titleLabel?.text ?? ""
        }
        
        let strParam : [String:Any] = [
                                       "fname": txtFirstName.text ?? "",
                                       "lname": txtLastName.text ?? "" ,
                                       "city" : strUserCity,
                                       "email" : txtEmail.text ?? "",
                                       "specialization" : strSpecialization,
                                       "mobile_number" : strMobileNumber,
                                       "country_code" : strCountryCode ,
                                       "country" : strCountry ,
                                       "device_id" : deviceID,
                                       "speciality_id" : strSpecialityID,
                                       "device_type":"ios",
                                       "device_token":strFcmToken,
                                       "show_to_my_connection" : isConnectionShared]
        self.showLoader()
        requestManager.requestCommonPostMethod(strAPIName: KJUrlConstants.Register, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.userEntity = userEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                    
                    
                    if self.userEntity.strUserStatus == 1{
                        if self.isUserOutsideIndia == true{
                            self.view.makeToast(message: self.userEntity.strMessage ?? "")

                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                                let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.OTPViewController) as! OTPViewController
                                vc.strMobileNumber = self.strMobileNumber
                                vc.strCountry = self.strCountry
                                vc.strCountryCode = self.strCountryCode
                                vc.strUserName = self.userEntity.strUserFirstName ?? ""
                                vc.strOTPType = "register_otp"
                                vc.isUserOutsideIndia = true
                                UIApplication.shared.keyWindow?.rootViewController = vc
                            }
                            
                            
                        }else{
                            let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                            UserDefaults.standard.synchronize()
                            UIApplication.shared.keyWindow?.rootViewController = vc
                        }
                        
                    }else{
                        self.view.makeToast(message: self.userEntity.strMessage ?? "")
                    }
                    
                   
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
          
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCheckCertifiedProfessionalTapped(_ sender: Any) {
        btnCheckCertifiedProfessional.isSelected = !btnCheckCertifiedProfessional.isSelected
        if btnCheckCertifiedProfessional.isSelected {
            btnCheckCertifiedProfessional.setImage(UIImage(named: "Right-Click_Filled"), for: .normal)
        }
        else
        {
            btnCheckCertifiedProfessional.setImage(UIImage(named: "Right-Click_Outline"), for: .normal)
        }
    }
    @IBAction func btnCheckTNCTapped(_ sender: Any) {
        btnCheckTNC.isSelected = !btnCheckTNC.isSelected
        if btnCheckTNC.isSelected {
            btnCheckTNC.setImage(UIImage(named: "Right-Click_Filled"), for: .normal)
        }
        else
        {
            btnCheckTNC.setImage(UIImage(named: "Right-Click_Outline"), for: .normal)
        }
    }
    @IBAction func btnLetsBeginTapped(_ sender: Any) {
        if validateRegistrationData(){
            userRegister()
        }
    }
    
    @IBAction func btnCityTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :cityArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Cities"
        vc.isComingFromUserDetails = true
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnCity
    }
    
    @IBAction func btnSpecialisationTapped(_ sender: Any) {
           let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
           let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
           let strHeader = "Search"
           vc.getScreenData(strHeader : strHeader,arrData :specialityArray,iScreenCount: 1)
           vc.delegate = self
           vc.strHeader = "Select Specialisation"
           vc.isComingFromEditProfile = true
           
           vc.modalPresentationStyle = .overFullScreen
           self.present(vc, animated: true, completion: nil)
           
           //self.setupPicker(arrdate: self.getCountryStringArray())
           selectedPickerButton = btnSpecialisation
       }
    
    //MARK:- Search City Delegate Method Called
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity, iScreenCount: Int) {
        if selectedPickerButton == btnCity{
            btnCity.setTitle(selectedEntity.strCityName, for: .normal)
            strUserCity = selectedEntity.strCityName ?? ""
        }else if selectedPickerButton == btnSpecialisation{
            btnSpecialisation.setTitle(selectedEntity.strAffiliation, for: .normal)
            strSpecialization = selectedEntity.strAffiliation ?? ""
            strSpecialityID = "\(selectedEntity.intID ?? 0)"
        }
    }
    
    func validateRegistrationData() -> Bool{
        if (txtFirstName.text! == "")
        {
            self.view.makeToast(message: KJConstant.FirstNameEmptyWarning)
            return false
        }
        
        if (txtLastName.text == "")
        {
            self.view.makeToast(message: KJConstant.LastNameEmptyWarning)
            return false
        }
        
        if (txtEmail.text! == "")
        {
            self.view.makeToast(message: KJConstant.EmailEmptyWarning)
            return false
        }
        if KJValidation.isValid(strInput: txtEmail.text!, regex: KJRegX.emailAddress) == false{
            self.view.makeToast(message: KJConstant.InvalidEmailWarning)
            return false
        }
        if btnCheckCertifiedProfessional.isSelected == false
        {
            self.view.makeToast(message: KJConstant.UserAgreementWarningError)
            return false
        }
        if btnCheckTNC.isSelected == false
        {
            self.view.makeToast(message: KJConstant.TNCWarningError)
            return false
        }
        return true
    }
}
