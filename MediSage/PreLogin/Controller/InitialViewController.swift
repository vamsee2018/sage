//
//  InitialViewController.swift
//  mediSage
//
//  Created by Harshad Wagh on 21/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class InitialViewController: BaseViewController,UITextFieldDelegate,searchDelegate {
   
    //MARK:- Custom Properties
    var countryArray = [LoginRegisterEntity]()
    var selectedPickerButton : UIButton?
    var entSelectedCountry1 = LoginRegisterEntity()
    var userArray = LoginRegisterEntity()
    var strCountry: String?
    var strCountryCode: String?
    var isShareConnection = Bool()
    //MARK:- IBOutlet Connections
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var abroadUserTextField: FloatingTF!
    @IBOutlet weak var txtMobileNumber: FloatingTF!
    @IBOutlet weak var txtCountryCode: FloatingTF!
    @IBOutlet weak var btnMyConnection: UISwitch!
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        strCountry = "India"
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
       
        
        getCountryCode()
    }
    
    //MARK:- Get Country API Call
    func getCountryCode(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.CountryList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.countryArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    
    //MARK:- Button Action
    @IBAction func btnMyConnection(_ sender: UISwitch) {
        if (sender.isOn == true){
            print("on")
            isShareConnection = true
        }
        else{
            isShareConnection = false
            print("off")
        }
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        txtMobileNumber.resignFirstResponder()
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        if self.validateRegistrationData() //display toast for validation
        {
            if (txtMobileNumber.text?.count)! >= 5 || (txtMobileNumber.text?.count)! >= 14 {
                
                txtCountryCode.text = txtCountryCode.text?.replacingOccurrences(of: "+", with: "")
                
                let objLoginManager = RequestManager()
                let loginEntity = LoginRegisterEntity()

                let strParam : [String:Any] = ["mobile_number":txtMobileNumber.text ?? "","country_code": txtCountryCode.text ?? "","country": strCountry ?? ""]
                self.showLoader()
                objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.NumberVerify, strParameterName: strParam)
                { (result, isSuccess, error) in
                    self.hideLoader()
                    if isSuccess
                    {
                        if let data = result
                        {
                            self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                        }
                        
                        if self.userArray.strIsRegister == 1{
                            
                            let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.OTPViewController) as! OTPViewController
                            vc.strMobileNumber = self.txtMobileNumber.text!
                            vc.strCountry = self.strCountry
                            vc.strCountryCode = self.txtCountryCode.text!
                            vc.strUserName = self.userArray.strUserFirstName ?? ""
                            vc.strOTPType = "login_otp"
                            vc.isSharedConnection = self.isShareConnection
                            UIApplication.shared.keyWindow?.rootViewController = vc
                           
                        }else if self.txtCountryCode.text != "91" {
                            let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.UserDetailsViewController) as! UserDetailsViewController
                            vc.strMobileNumber = self.txtMobileNumber.text!
                            vc.strCountryCode = self.strCountryCode ?? ""
                            vc.strCountry = self.strCountry ?? ""
                            vc.strUserFirstName = self.userArray.strUserLastName ?? ""
                            vc.strUserLastName = self.userArray.strUserLastName ?? ""
                            vc.strUserCity = self.userArray.strCityName ?? ""
                            vc.strUserEmail = self.userArray.strEmail ?? ""
                            vc.strOTPType = "register_otp"
                            vc.isUserOutsideIndia = true
                            vc.isConnectionShared = self.isShareConnection

                            UIApplication.shared.keyWindow?.rootViewController = vc
                        }else if self.userArray.isMasterUser == true{
                            
                            let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.OTPViewController) as! OTPViewController
                            vc.strMobileNumber = self.txtMobileNumber.text!
                            vc.strCountry = self.strCountry
                            vc.strCountryCode = self.txtCountryCode.text!
                            vc.strUserName = self.userArray.strUserFirstName ?? ""
                            vc.strLastName = self.userArray.strUserLastName ?? ""
                            vc.strFirstName = self.userArray.strUserFirstName ?? ""
                            vc.strEmail = self.userArray.strEmail ?? ""
                            vc.strCity = self.userArray.strCityName ?? ""
                            vc.isMasterUser = self.userArray.isMasterUser ?? false
                            vc.strOTPType = "register_otp"
                            vc.isSharedConnection = self.isShareConnection

                            UIApplication.shared.keyWindow?.rootViewController = vc
                        }else{
                            let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.OTPViewController) as! OTPViewController
                            vc.strMobileNumber = self.txtMobileNumber.text!
                            vc.strCountry = self.strCountry
                            vc.strCountryCode = self.txtCountryCode.text!
                            vc.strUserName = self.userArray.strUserFirstName ?? ""
                            vc.strOTPType = "register_otp"
                            vc.isSharedConnection = self.isShareConnection

                            UIApplication.shared.keyWindow?.rootViewController = vc
                        }
                       
                    }
                    else
                    {
                        self.view.makeToast(message: error)
                    }
                }
            }else{
                self.view.makeToast(message: KJConstant.mobileNumberWarning)
            }
        }
                  
    }
    
    //MARK: - Validate data
    func validateRegistrationData() -> Bool{
        if(txtCountryCode.text == ""){
            self.view.makeToast(message: KJConstant.selectCountryCodeWarning)
            return false
        }
        if txtMobileNumber.text == ""{
            self.view.makeToast(message: KJConstant.ValidMobileNoMessage)
            return false
        }
        return true
    }
    
        
    //MARK: - textfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCountryCode {
            textField.resignFirstResponder()
            let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
            let strHeader = "Search Country"
            vc.getScreenData(strHeader : strHeader,arrData :countryArray,iScreenCount: 1)
            vc.bisCountrycode = true
            vc.delegate = self
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            animateViewMoving(up: true, moveValue: 100)
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateViewMoving(up: false, moveValue: 100)
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    //MARK:- Search City Delegate Method Called
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity, iScreenCount: Int) {
        txtCountryCode.text = "+ " + "\(selectedEntity.strCountryCode ?? "")"
        strCountry = selectedEntity.strCountryName
        strCountryCode = selectedEntity.strCountryCode
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //To prevent user, entering space at the start of textfield
        if (range.location == 0 && string == " "){
            return false
        }
        
        guard let text = textField.text else { return true }
        //To validate each entry in the textfield
        let newLength = text.utf16.count + string.utf16.count - range.length
        if textField == txtMobileNumber{
            if newLength <= KJConstant.contactNumberLength{
                return KJValidation.textfieldEntryValidation(getTextField: txtMobileNumber, passedString: string, validationType: KJConstant.numbers)
            }else if newLength <= KJConstant.contactNumberLength2 {
                return KJValidation.textfieldEntryValidation(getTextField: txtMobileNumber, passedString: string, validationType: KJConstant.numbers)
            }
            else{
                return newLength <= KJConstant.contactNumberLength
            }
        }
        return false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let isIndian = txtCountryCode.text == "91" ? true : false
        let defaults = UserDefaults.standard
        defaults.set(isIndian,forKey: "isIndian")
        defaults.synchronize()
    }
        
}


