//
//  OTPViewController.swift
//  mediSage
//
//  Created by Harshad Wagh on 21/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging
class OTPViewController: BaseViewController,UITextFieldDelegate {

    //MARK:- Custom Properties
    var iReminderTimeOut = 60
    var timer = Timer()
    var strOtp  = String()
    var isComingFromForgotPassword = false
    var resendOTPCount = -1
    var strMobileNumber  = String()
    var userArray = LoginRegisterEntity()
    var strCountry: String?
    var strCountryCode: String?
    var strUserName  = String()
    var strOTPType  = String()
    var strFirstName  = String()
    var strLastName  = String()
    var strEmail  = String()
    var strCity  = String()
    var isMasterUser  = Bool()
    var isUserOutsideIndia = Bool()
    var isSharedConnection = Bool()
    //MARK:- IBOutlet Connections
    @IBOutlet weak var txtField1: FloatingTF!
    @IBOutlet weak var txtField2: FloatingTF!
    @IBOutlet weak var txtField3: FloatingTF!
    @IBOutlet weak var txtField4: FloatingTF!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblTimeRemaining: UILabel!
    @IBOutlet weak var viewContainer: UIView!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblUserName.text = "Welcome Dr. " + strUserName
        setControllerPreferences()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.timer.invalidate()
     }
     
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(true)
        
        self.lblMobileNumber.text =  "We've sent a four digit OTP on" + " " + "******" + strMobileNumber.suffix(4)
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
       
     }
    
     //MARK:- Set Initial View Setting
     func setControllerPreferences(){
         btnVerify.layer.cornerRadius = 10
         btnResend.layer.cornerRadius = 10
         txtField1.becomeFirstResponder()
        if #available(iOS 12.0, *) {
            txtField1.textContentType = .oneTimeCode
            txtField2.textContentType = .oneTimeCode
            txtField3.textContentType = .oneTimeCode
            txtField4.textContentType = .oneTimeCode

        } else {
            // Fallback on earlier versions
        }
        
         timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer(_:)), userInfo: nil, repeats: true)
     }
    
    //MARK:- Timer for OTP
    @objc func updateTimer(_ timer: AnyObject)
    {
        iReminderTimeOut -= 1
        lblTimeRemaining.text = "You should receive an OTP in the next " + "\(iReminderTimeOut/60):" + String(format: "%02d", iReminderTimeOut%60) + " seconds"
       if iReminderTimeOut == 0
       {
           txtField1.isUserInteractionEnabled = false
           txtField2.isUserInteractionEnabled = false
           txtField3.isUserInteractionEnabled = false
           txtField4.isUserInteractionEnabled = false
           timer.invalidate()
           lblTimeRemaining.text = ""
           btnResend.isHidden = false
           btnVerify.isHidden = true
       }
    }
    
    
    //MARK:- Password Verify Api Call
    func verifyOTP(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        if self.validateMobile_OTPData() //display toast for validation
        {
            let deviceID    = UIDevice.current.identifierForVendor?.uuidString ?? ""
            var strFcmToken = ""
            if let fcmToken = Messaging.messaging().fcmToken {
                print("FCM token: \(fcmToken)")
                strFcmToken = fcmToken
            }
            let objLoginManager = RequestManager()
            let loginEntity = LoginRegisterEntity()
            let strParam : [String:Any] = ["credential": strMobileNumber ,
                                           "credential_type": "mobile_number",
                                           "otp": strOtp,
                                           "otp_type":strOTPType,
                                           "device_id": deviceID,
                                           "device_type":"ios",
                                           "device_token":strFcmToken,
                                           "show_to_my_connection" : isSharedConnection]
            
            self.showLoader()
            objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.ValidateOTP, strParameterName: strParam)
            { (result, isSuccess, error) in
                self.hideLoader()
                if isSuccess
                {
                    if let data = result
                    {
                        self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                    }
                    

                    if self.userArray.strUserStatus == 1{
                        if self.strOTPType == "register_otp"{
                            if self.isMasterUser == true{
                                let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                                let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.UserDetailsViewController) as! UserDetailsViewController
                                vc.strMobileNumber = self.strMobileNumber
                                vc.strCountryCode = self.strCountryCode ?? ""
                                vc.strCountry = self.strCountry ?? ""
                                vc.strUserFirstName = self.strUserName
                                vc.strUserLastName = self.strLastName
                                vc.strUserCity = self.strCity
                                vc.strUserEmail = self.strEmail 
                                vc.strMobileNumber = self.strMobileNumber
                                vc.isMasterUser = self.isMasterUser
                                UIApplication.shared.keyWindow?.rootViewController = vc
                            }else if self.isUserOutsideIndia == true{
                                let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                                let vc = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                                UserDefaults.standard.synchronize()
                                UIApplication.shared.keyWindow?.rootViewController = vc
                            }else{
                                let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
                                let vc = storyBoard.instantiateViewController(withIdentifier: KJViewIdentifier.UserDetailsViewController) as! UserDetailsViewController
                                vc.strMobileNumber = self.strMobileNumber
                                vc.strCountryCode = self.strCountryCode ?? ""
                                vc.strCountry = self.strCountry ?? ""
                                vc.strUserFirstName = self.strUserName
                                vc.strUserLastName = self.strLastName 
                                vc.strUserCity = self.strCity 
                                vc.strUserEmail = self.strEmail 
                                vc.strMobileNumber = self.strMobileNumber
                                UIApplication.shared.keyWindow?.rootViewController = vc
                            }
                        }else{

                            let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                            UserDefaults.standard.synchronize()
                            UIApplication.shared.keyWindow?.rootViewController = vc
                        }
                    }else{
                            self.view.makeToast(message: self.userArray.strMessage ?? "")
                        
                    }
                
                }
                else
                {
                    self.view.makeToast(message: error)
                }
            }
        }else{
            self.view.makeToast(message: KJConstant.mobileNumberWarning)
        }
    }
    
    //MARK: - Textfield delegate methods
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //To prevent user, entering space at the start of textfield
        if (range.location == 0 && string == " "){
            return false
        }
        
        guard textField.text != nil else { return true }
        //To validate each entry in the textfield
        
        if textField == txtField1 || textField == txtField2 || textField == txtField3 || textField == txtField4 {
            
            //below code is used for otp text feild shifting and only one char in one text feild
            if textField.text!.count < 1  && string.count > 0{
                let nextTag = textField.tag + 1
                
                // get next responder
                var nextResponder = textField.superview?.viewWithTag(nextTag)
                
                if (nextResponder == nil){
                    
                    nextResponder = textField.superview?.viewWithTag(1)
                }
                
                textField.text = string
                textField.placeholder = ""
                strOtp = txtField1.text! + txtField2.text! + txtField3.text! + txtField4.text!
                nextResponder?.becomeFirstResponder()
                
                return false
            }
            else if textField.text!.count >= 1  && string.count == 0{
                // on deleting value from Textfield
                let previousTag = textField.tag - 1
                
                // get next responder
                var previousResponder = textField.superview?.viewWithTag(previousTag)
                
                if (previousResponder == nil){
                    previousResponder = textField.superview?.viewWithTag(1)
                }
                textField.text = ""
                textField.placeholder = ""
                strOtp = txtField1.text! + txtField2.text! + txtField3.text! + txtField4.text!
                previousResponder?.becomeFirstResponder()
                return false
            }
        }
        
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(up: true, moveValue: 100)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateViewMoving(up: false, moveValue: 100)
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    //MARK:-Check OTP Validations
    func validateMobile_OTPData() -> Bool
    {
        if (strOtp == "" || strOtp.count < 3 )
        {
            self.view.makeToast(message: KJConstant.ValidOTPMessage)
            return false
        }
        return true
    }
   
    //MARK:- Button Actions
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLetsBeginTapped(_ sender: Any) {
        verifyOTP()
    }
    
    @IBAction func btnResendCodeTapped(_ sender: Any) {
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let objLoginManager = RequestManager()
        let loginEntity = LoginRegisterEntity()
        
        let strParam : [String:Any] = ["mobile_number":strMobileNumber,"country_code": strCountryCode ?? "","country": strCountry ?? ""]
        self.showLoader()
        objLoginManager.requestCommonPostMethod(strAPIName: KJUrlConstants.NumberVerify, strParameterName: strParam)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.userArray = loginEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                    self.btnResend.isHidden = true
                    self.btnVerify.isHidden = false
                    self.iReminderTimeOut = 60//RMConstant.iOTPSecreenTimerLimit
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer(_:)), userInfo: nil, repeats: true)
                    self.txtField1.isUserInteractionEnabled = true
                    self.txtField2.isUserInteractionEnabled = true
                    self.txtField3.isUserInteractionEnabled = true
                    self.txtField4.isUserInteractionEnabled = true
                    
                }
            }
            else
            {
                self.view.makeToast(message: error)
            }
        
        }
    }
  
}
