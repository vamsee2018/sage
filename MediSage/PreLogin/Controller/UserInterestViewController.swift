//
//  UserInterestViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 28/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class UserInterestViewController: BaseViewController,UITextFieldDelegate,searchDelegate {

    //MARK:- Custom Properties
    var affiliationArray = [LoginRegisterEntity]()
    var selectedPickerButton : UIButton?
    var entSelectedCountry1 = LoginRegisterEntity()
    
    //MARK:- IBOutlet Connections
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnPracticeType: UIButton!
    @IBOutlet weak var btnClinicalChallenges: UIButton!
    @IBOutlet weak var btnAffiliation: UIButton!
    @IBOutlet weak var btnLetsBegin: UIButton!

    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        getAffiliationList()
    }
    
    //MARK:- Get Country API Call
    func getAffiliationList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.AffiliationList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.affiliationArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPracticeTypeTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :affiliationArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Practice Types"
       
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnPracticeType
    }
    
    @IBAction func btnClinicalChallengesTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :affiliationArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Clinical Challenges"
         
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnClinicalChallenges
    }
    @IBAction func btnAffiliationTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :affiliationArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Affiliations"
         
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnAffiliation
    }
    
    //MARK: - textfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
    }
    
    //MARK:- Search City Delegate Method Called
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity, iScreenCount: Int) {
        // txtCountryCode.text = "+ " + "\(selectedEntity.strCountryCode ?? "")"
    }
}
