//
//  IntroScreenViewController.swift
//  MediSage
//
//  Created by Medisage MacbookAir on 09/03/21.
//  Copyright © 2021 Harshad Wagh. All rights reserved.
//

import UIKit

class IntroScreenViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- IBOutlet Connections
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnLetsBegin: UIButton!

    //MARK:- Custom Properties
    var window: UIWindow?
    var imgArray = ["Image-3a","Image-1_a","Image-4a","Image-2a"]
    
    //MARK:- View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
       // collectionView.isPagingEnabled = true
        collectionView.reloadData()
        
       // self.pageControl.currentPage = 0
        btnLetsBegin.layer.cornerRadius = 10
    }
 
    // MARK: Custom Button Action
    @IBAction func btnSkipTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        self.pageControl.currentPage += 1
        scrollToIndex(index: self.pageControl.currentPage)

    }
   
    func scrollToIndex(index:Int) {
        self.collectionView?.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnLetsBeginTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: KJViewIdentifier.Main, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carasoulcell", for: indexPath) as! CarasoulCollectionCell
        cell.imgView.contentMode = .scaleAspectFit
        // Configure the cell
        if indexPath.row == 0{

            cell.imgView.image = UIImage(named: imgArray[0])
            cell.lblTitle.text = "READ DAILY \nMEDICAL NEWS"
            
            cell.lblDesc.text = "Stay up-to-date with the latest advancements in the healthcare sector through daily curated news on the MediSage App from global medical journals like JAMA, Nature, Lancet, EJM etc"
        }else if indexPath.row == 1{

            cell.imgView.image = UIImage(named: imgArray[1])
            cell.lblTitle.text = "WATCH \nSHORT VIDEOS"
            cell.lblDesc.text = "Watch short engaging videos addressing your key clinical challenges. Videos published by Subject experts from around the world.."
            
        }else if indexPath.row == 2{
     
            cell.imgView.image = UIImage(named: imgArray[2])
            cell.lblTitle.text = "FOLLOW GLOBAL \nSUBJECT EXPERTS"
            cell.lblDesc.text = "Get credible information from national and international experts and take-aways to apply in every day practice. Also, participate in panel discussion or interact with surgeons during a live surgery."
        }else if indexPath.row == 3{
            cell.imgView.image = UIImage(named: imgArray[3])
            cell.lblTitle.text = "ATTEND \nLIVE WEBINARS"
            cell.lblDesc.text = "Now attend live webinars by global subject matter experts and engage with them via live Q&A and chat on the MediSage app. Also, watch recordings of past webinars & events at your convenience on the app."
        }
       

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
            self.btnLetsBegin.isHidden = true
            self.btnSkip.isHidden = false
            self.btnNext.isHidden = false
            self.pageControl.isHidden = false
        }else{
            self.btnLetsBegin.isHidden = false
            self.btnSkip.isHidden = true
            self.btnNext.isHidden = true
            self.pageControl.isHidden = true
        }
        self.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //  let frameSize = collectionView.frame.size
        return CGSize(width: collectionView.frame.size.width, height: 450);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
}

class CarasoulCollectionCell:UICollectionViewCell{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!

}
