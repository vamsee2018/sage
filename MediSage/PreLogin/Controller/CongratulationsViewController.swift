//
//  CongratulationsViewController.swift
//  MediSage
//
//  Created by Harshad Wagh on 28/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseMessaging

class CongratulationsViewController: BaseViewController,UITextFieldDelegate,searchDelegate,UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    //MARK:- Custom Properties
    var stateArray = [LoginRegisterEntity]()
    var qualificationArray = [LoginRegisterEntity]()
    var yearArray = [LoginRegisterEntity]()
    var specialityArray = [LoginRegisterEntity]()

    var selectedPickerButton : UIButton?
    var entSelectedCountry1 = LoginRegisterEntity()
    var userEntity = LoginRegisterEntity()

    @IBOutlet weak var stateCouncilContainerConstraint: NSLayoutConstraint!
    var cameraPicker = UIImagePickerController()
    var profileImage: UIImage?
    var profileData : String? = ""
    var strProfileUrl : String = ""
    var strUserFirstName  = String()
    var strUserLastName  = String()
    var strUserCity = String()
    var strUserPassword = String()
    var strUserEmail = String()
    var userData = [String:Any]()
    var strUserID  = String()
    var strSpecialityID  = String()
    var strMobileNumber  = String()
    var strCountry: String?
    var strCountryCode: String?
    
    //MARK:- IBOutlet Connections
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnQualification: UIButton!
    @IBOutlet weak var btnSpecialisation: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtRegisterationNumber: FloatingTF!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        stateCouncilContainerConstraint.constant = (UserDefaults.standard.bool(forKey: "isIndian")) == true ? 60 : 0
        
        
        viewContainer.layer.cornerRadius = 10
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        imgProfile.layer.masksToBounds = true
        imgProfile.image = UIImage(named: "User")
        imgProfile.contentMode = .scaleToFill
        profileImage = imgProfile.image
        
        getStateList()
        getQualificationList()
        getYearsList()
    }
    
    //MARK:- Update Register API Call
    func userRegister(){
        
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let userEntity = LoginRegisterEntity()
        let deviceID   = UIDevice.current.identifierForVendor?.uuidString ?? ""

        if (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) != nil)
        {
            userData = (UserDefaults.standard.value(forKey: KJConstant.RegUserDetails) as? [String:Any])!
            
            if let id = userData["userID"] as? Int{
                
                strUserID = "\(id)"
            }
        }
        var strFcmToken = ""
        if let fcmToken = Messaging.messaging().fcmToken {
            print("FCM token: \(fcmToken)")
            strFcmToken = fcmToken
        }
        
        let strParam : [String:Any] = ["member_id": strUserID ,
                                       "fname": strUserFirstName,
                                       "lname": strUserLastName ,
                                       "password": strUserPassword,
                                       "city" : strUserCity,
                                       "specialization" : btnSpecialisation.titleLabel?.text ?? "",
                                       "qualification" : btnQualification.titleLabel?.text ?? "",
                                       "state" : btnState.titleLabel?.text ?? "",
                                       "registered_year" : btnYear.titleLabel?.text ?? "",
                                       "rno": txtRegisterationNumber.text ?? "",
                                       "practice_type" : "",
                                       "clinical_challenges" : "",
                                       "affiliation" : "",
                                       "mobile_number" : strMobileNumber,
                                       "country_code" : strCountryCode ?? "",
                                       "country" : strCountry ?? "",
                                       "device_id" : deviceID,
                                       "speciality_id" : strSpecialityID,
                                       "device_type":"ios",
                                       "device_token":strFcmToken]
        self.showLoader()
        requestManager.requestCommonPostMultipartMethod(strAPIName: KJUrlConstants.Register, strParameterName: strParam, profileImage: profileImage!)
        { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.userEntity = userEntity.checkRegisterUser(withInfo : data as? Dictionary<String, AnyObject>)
                    let storyBoard = UIStoryboard(name: KJViewIdentifier.Home, bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    UIApplication.shared.keyWindow?.rootViewController = vc
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
       
    
    //MARK:- Get Country API Call
    func getStateList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.StateList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.stateArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Country API Call
    func getQualificationList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.QualificationsList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.stateArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    //MARK:- Get Speciality API Call
    func getSpecialityList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.SpecialisationList, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.specialityArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    
    //MARK:- Get Country API Call
    func getYearsList(){
        if !RMUtility.sharedInstance.isInternetAvailable()//Check for internet or return from method
        {
            self.view.makeToast(message: KJConstant.NoInternetConnection)
            return
        }
        let requestManager = RequestManager()
        self.showLoader()
        let loginEntity = LoginRegisterEntity()
        requestManager.requestCommonGetMethod(strAPIName: KJUrlConstants.GetYears, strParameterName:"") { (result, isSuccess, error) in
            self.hideLoader()
            if isSuccess
            {
                if let data = result
                {
                    self.stateArray = loginEntity.countryCodeList(info : data as! Dictionary<String, AnyObject>)
                }
            }else
            {
                self.view.makeToast(message: error)
            }
        }
    }
    
    
    @IBAction func btnStateTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :stateArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select States"
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnState
    }
    
    @IBAction func btnQualificationTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :stateArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Highest Qualification"
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnQualification
    }
    @IBAction func btnSpecialisationTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :specialityArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Specialisation"
        vc.isComingFromEditProfile = true
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnSpecialisation
    }
    @IBAction func btnYearTapped(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Popup", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "popUpSearchViewController") as! popUpSearchViewController
        let strHeader = "Search"
        vc.getScreenData(strHeader : strHeader,arrData :stateArray,iScreenCount: 1)
        vc.delegate = self
        vc.strHeader = "Select Year"
        vc.isBusinessTypeSelected = true
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        //self.setupPicker(arrdate: self.getCountryStringArray())
        selectedPickerButton = btnYear
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Button delegate method
    @IBAction func btnProfileImageClicked(_ sender: UIButton)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        cameraPicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    /// method is used for opening camera option on the screen
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            cameraPicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(cameraPicker, animated: true, completion: nil)
        }
        else
        {
            self.view.makeToast(message: "You don't have camera")
        }
    }
    func openGallary()
    {
        cameraPicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(cameraPicker, animated: true, completion: nil)
    }
    
    //MARK: - Image picker delegate method
    //PickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imgProfile.layer.cornerRadius = 45
        imgProfile.contentMode = .scaleToFill
        profileImage =  imgProfile.image!
        cameraPicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        cameraPicker.dismiss(animated: true, completion: nil)
        print("picker cancel.")
    }
    
    
    
    //MARK: - textfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
    }
    
    //MARK:- Search City Delegate Method Called
    func popupSearchDidSelected(index: Int, selectedEntity: LoginRegisterEntity, iScreenCount: Int) {
        if selectedPickerButton == btnState{
            btnState.setTitle(selectedEntity.strCountryName, for: .normal)
        }else if selectedPickerButton == btnQualification{
            btnQualification.setTitle(selectedEntity.strCountryName, for: .normal)
        }else if selectedPickerButton == btnSpecialisation{
            btnSpecialisation.setTitle(selectedEntity.strAffiliation, for: .normal)
            strSpecialityID = "\(selectedEntity.intID ?? 0)"
        }else{
            btnYear.setTitle(selectedEntity.strYear, for: .normal)
            
        }
        
    }
    
}
