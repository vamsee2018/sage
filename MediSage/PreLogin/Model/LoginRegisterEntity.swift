//
//  LoginRegisterEntity.swift
//  MediSage
//
//  Created by Harshad Wagh on 23/10/20.
//  Copyright © 2020 Harshad Wagh. All rights reserved.
//

import UIKit

class LoginRegisterEntity: NSObject {
    var strCountryCode : String? = ""
    var strCountryName: String? = ""
    var strStateName: String? = ""
    var strCityName: String? = ""
    var strAffiliation: String? = ""
    var strEmail: String? = ""
    var strMobileNumber: String? = ""

    var strBusinessType: String?
    var strSourceName: String?
    var strYear: String? = ""
    var strMessage : String?
    var strIsRegister: Int? = -10
    var strUserFirstName:String?
    var strUserLastName:String?
    var intID:Int? = -1
    var strUserID:Int? = -1
    var isMasterUser: Bool? = false

    var strUserStatus:Int? = -1

    //MARK:-  Method for country code list  api data parsing
    func countryCodeList(info : Dictionary<String,AnyObject>)->[LoginRegisterEntity]{
        
        var mainEnt = [LoginRegisterEntity]()
        let dictClass : [[String : AnyObject]]  = info["Data"] as! [[String : AnyObject]]

        for obj in dictClass
        {
            if let dict = obj as? [String:AnyObject]
            {
                let ent = LoginRegisterEntity()
                
                if let id = dict["id"] as? Int{
                    ent.intID = id
                }
                if let countryCode = dict["phonecode"] as? String{
                    ent.strCountryCode = countryCode
                }
                if let name = dict["name"] as? String{
                    ent.strCountryName = name
                }
                if let affiliation = dict["title"] as? String{
                    ent.strAffiliation = affiliation
                }
                if let cityName = dict["city"] as? String{
                    ent.strCityName = cityName
                }
                if let year = dict["year"] as? Int{
                    ent.strYear = "\(year)"
                }
                mainEnt.append(ent)
            }
        }
        return mainEnt
    }

    
    func checkRegisterUser(withInfo info : Dictionary<String,AnyObject>?) -> LoginRegisterEntity{
        let ent = LoginRegisterEntity()
        if let msg = info?["Message"] as? String
        {
            ent.strMessage = msg
        }
        if let userStatus = info?["Status"] as? Int {
            ent.strUserStatus = userStatus
        }
        if let obj = info?["Data"] as? [String:AnyObject]
        {
            
            if let arrObj = obj["universal_data"] as? [String:Any]{
                if let dict = arrObj as? [String:AnyObject]
                {
                    if let userName = dict["fname"] as? String {
                        ent.strUserFirstName = userName
                        ent.isMasterUser = true
                    }
                    if let lastName = dict["lname"] as? String {
                        ent.strUserLastName = lastName
                        ent.isMasterUser = true
                    }
                    if let cityName = dict["city"] as? String{
                        ent.strCityName = cityName
                        ent.isMasterUser = true
                    }
                    if let email = dict["email"] as? String{
                        ent.strEmail = email
                        ent.isMasterUser = true
                    }
                    if let mobileNumber = dict["mobile_number"] as? String{
                        ent.strMobileNumber = mobileNumber
                        ent.isMasterUser = true
                    }
                }
            }
            
            var dictUserDetails = [String:Any]()

            if let userStatus = obj["Status"] as? Int {
                ent.strUserStatus = userStatus
            }
            if let userName = obj["first_name"] as? String {
                dictUserDetails["first_name"] = userName
                ent.strUserFirstName = userName
            }
            if let lastName = obj["last_name"] as? String {
                dictUserDetails["last_name"] = lastName
                ent.strUserLastName = lastName
            }
            if let userID = obj["member_id"] as? Int {
                dictUserDetails["userID"] = userID
                ent.strUserID = userID
            }
            if let isRegister = obj["is_registered"] as? Int {
                ent.strIsRegister = isRegister
            }
            if let mobileNumber = obj["mobile_number"] as? String{
                dictUserDetails["mobileNumber"] = mobileNumber
                ent.strMobileNumber = mobileNumber
            }
            let defaults = UserDefaults.standard
            defaults.setValue(dictUserDetails, forKey: KJConstant.RegUserDetails)
            defaults.synchronize()
        }
        
        return ent
    }
    
}
